/*
 *  TU Delft / Knowledge Centre WMC
 *
 *  Iuri Barcelos, August 2015
 *
 *  Simple base class for materials.
 *
 */

#include <jem/base/Error.h>
#include <jem/util/Properties.h>

#include "Material.h"
#include "IsotropicMaterial.h"
#include "ANNMaterial.h"
#include "MelroMaterial.h"
#include "VoglerMaterial.h"
#include "FE2Material.h"
#include "ObserverMaterial.h"
#include "MNNMaterial.h"
#include "StableMNNMaterial.h"
#include "J2Material.h"
#include "InvariantsMaterial.h"
#include "MemInvariantsMaterial.h"

using namespace jem;

//-----------------------------------------------------------------------
//   newInstance
//-----------------------------------------------------------------------

Ref<Material>  newMaterial

  ( const String&     name,
    const Properties& conf,
    const Properties& props,
    const Properties& globdat )

{
  Properties    matProps = props.getProps ( name );
  Properties    matConf  = conf.makeProps ( name );

  Ref<Material> mat;
  String        type;
  idx_t         rank;

  matProps.get ( type, "type" );
  matConf.set  ( "type", type );

  matProps.get ( rank, "rank" );
  matConf.set  ( "rank", rank );

  if      ( type == "Isotropic" )
    mat = newInstance<IsotropicMaterial> ( rank, globdat );
  else if ( type == "J2" )
    mat = newInstance<J2Material> ( rank, globdat );
  else if ( type == "Melro" )
    mat = newInstance<MelroMaterial> ( rank, globdat );
  else if ( type == "Invariants" )
    mat = newInstance<InvariantsMaterial> ( rank, globdat );
  else if ( type == "MemInvariants" )
    mat = newInstance<MemInvariantsMaterial> ( rank, globdat );
  else if ( type == "Vogler" )
    mat = newInstance<VoglerMaterial> ( rank, globdat );
  else if ( type == "Neural" )
    mat = newInstance<ANNMaterial> ( rank, globdat );
  else if ( type == "FE2" )
    mat = newInstance<FE2Material> ( name, props, conf, globdat, rank );
  else if ( type == "Observer" )
    mat = newInstance<ObserverMaterial> ( matProps, matConf, rank, globdat );
  else if ( type == "MNN" )
    mat = newInstance<MNNMaterial> ( matProps, matConf, rank, globdat );
  else if ( type == "StableMNN" )
    mat = newInstance<StableMNNMaterial> ( matProps, matConf, rank, globdat );
  else
    matProps.propertyError ( name, "Invalid material: " + type );

  return mat;
}

//=======================================================================
//   class Material
//=======================================================================

//-----------------------------------------------------------------------
//   constructors and destructor
//-----------------------------------------------------------------------

Material::Material

  ( const idx_t        rank,
    const Properties&  globdat )
{
  desperateMode_ = false;
}


Material::Material

  ( const String&      name,
    const Properties&  props,
    const Properties&  conf,
    const idx_t        rank )

{}

Material::~Material()
{}

//-----------------------------------------------------------------------
//   configure
//-----------------------------------------------------------------------

void Material::configure

  ( const Properties& props,
    const Properties& globdat )
{}

//-----------------------------------------------------------------------
//   getConfig
//-----------------------------------------------------------------------

void Material::getConfig

  ( const Properties& props,
    const Properties& globdat ) const
{}

//-----------------------------------------------------------------------
//   hasThermal
//-----------------------------------------------------------------------

bool Material::hasThermal ()
{
  // Default implementation.

  return false;
}

//-----------------------------------------------------------------------
//   hasSwelling
//-----------------------------------------------------------------------

bool Material::hasSwelling ()
{
  // Default implementation.

  return false;
}

//-----------------------------------------------------------------------
//   hasCrackBand
//-----------------------------------------------------------------------

bool Material::hasCrackBand ()
{
  // Default implementation.

  return false;
}

//-----------------------------------------------------------------------
//   setCharLength
//-----------------------------------------------------------------------

void Material::setCharLength

  ( const idx_t       ipoint,
    const double      le )

{
  // Default implementation
}

//-----------------------------------------------------------------------
//   getHistory
//-----------------------------------------------------------------------

void Material::getHistory

  ( Vector&           hvals,
    const idx_t       mpoint ) 

{
  // Default implementation
  hvals.resize ( 0 );
}

//-----------------------------------------------------------------------
//   setHistory
//-----------------------------------------------------------------------

void Material::setHistory

  ( const Vector&    hvals,
    const idx_t      mpoint )

{
  // Default implementation
}

//-----------------------------------------------------------------------
//   copyHistory
//-----------------------------------------------------------------------

void Material::copyHistory

  ( const idx_t      master,
    const idx_t      slave )

{
  // Default implementation
}

//-----------------------------------------------------------------------
//   createIntPoints
//-----------------------------------------------------------------------

void Material::createIntPoints

  ( const idx_t       npoints )

{}

//-----------------------------------------------------------------------
//   setConc
//-----------------------------------------------------------------------

void Material::setConc

  ( const idx_t       ipoint,
    const double      conc )

{
}

//-----------------------------------------------------------------------
//   setDeltaT
//-----------------------------------------------------------------------

void Material::setDeltaT

  ( const idx_t       ipoint,
    const double      deltaT )

{
}

//-----------------------------------------------------------------------
//   commit 
//-----------------------------------------------------------------------

void Material::commit ()
{}

//-----------------------------------------------------------------------
//   checkCommit 
//-----------------------------------------------------------------------

void Material::checkCommit 

  ( const Properties& params )

{}

//-----------------------------------------------------------------------
//   commitOne
//-----------------------------------------------------------------------

void Material::commitOne 

 ( const idx_t ipoint )

{}

//-----------------------------------------------------------------------
//   cancel 
//-----------------------------------------------------------------------

void Material::cancel ()
{}

//-----------------------------------------------------------------------
//   getFileName 
//-----------------------------------------------------------------------

String Material::getFileName

  ( const idx_t      ipoint ) const
{
  return "";
}

//-----------------------------------------------------------------------
//   getDissipation 
//-----------------------------------------------------------------------

double Material::getDissipation

  ( const idx_t      ipoint ) const
{
  return 0.;
}

//-----------------------------------------------------------------------
//   getDissipation 
//-----------------------------------------------------------------------

double Material::getDissipationGrad

  ( const idx_t      ipoint ) const
{
  return 0.;
}


//-----------------------------------------------------------------------
//   getDissStress 
//-----------------------------------------------------------------------

void Material::getDissStress

  ( const Vector&    sstress,
    const Vector&    strain,
    const idx_t      ipoint )
{}

//-----------------------------------------------------------------------
//   pointCount 
//-----------------------------------------------------------------------

idx_t  Material::pointCount () const

{
  return 0;
}

//-----------------------------------------------------------------------
//   isLoading 
//-----------------------------------------------------------------------

bool  Material::isLoading 

  ( idx_t ipoint ) const

{
  return false;
}

//-----------------------------------------------------------------------
//   wasLoading 
//-----------------------------------------------------------------------

bool  Material::wasLoading 

  ( idx_t ipoint ) const

{
  return false;
}

//-----------------------------------------------------------------------
//   isInelastic
//-----------------------------------------------------------------------

bool Material::isInelastic

  ( idx_t ipoint ) const

{
  return false;
}

//-----------------------------------------------------------------------
//   despair 
//-----------------------------------------------------------------------

bool Material::despair () 

{
  desperateMode_ = true;

  idx_t np = pointCount();

  hasSwitched_.resize ( np );
  useSecant_  .resize ( np );

  useSecant_ = false;

  for ( idx_t ip = 0; ip < np; ++ip )
  {
    hasSwitched_[ip] = ( isLoading(ip) != wasLoading(ip) );
  }

  return np > 0;
}

//-----------------------------------------------------------------------
//   endDespair 
//-----------------------------------------------------------------------

void Material::endDespair ()

{
  desperateMode_ = false;
  useSecant_ = false;
}

//-----------------------------------------------------------------------
//   writeState 
//-----------------------------------------------------------------------

void Material::writeState ()

{
}

