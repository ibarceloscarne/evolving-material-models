/*
 * Copyright (C) 2021 TU Delft. All rights reserved.
 *
 * Author: Iuri Barcelos, i.rocha@tudelft.nl
 * Date:   Apr 2021
 * 
 */

#include <jem/base/Error.h>
#include <jem/base/System.h>
#include <jem/base/Float.h>
#include <jem/numeric/algebra/matmul.h>
#include <jem/util/Timer.h>
#include <jive/Array.h>
#include <jive/model/Actions.h>
#include <jive/model/ModelFactory.h>
#include <jive/model/StateVector.h>
#include <jive/util/error.h>

#include "MaterialLayer.h"
#include "LearningNames.h"
#include "XAxonSet.h"
#include "XNeuronSet.h"

using jem::Error;
using jem::numeric::matmul;
using jem::util::Timer;

using jive::IdxVector;
using jive::util::XDofSpace;
using jive::util::sizeError;
using jive::model::StateVector;

//-----------------------------------------------------------------------
//   static data
//-----------------------------------------------------------------------

const char* MaterialLayer::RANK     = "rank";
const char* MaterialLayer::HISTSIZE = "histSize";
const char* MaterialLayer::MATERIAL = "material";

//-----------------------------------------------------------------------
//   constructor & destructor
//-----------------------------------------------------------------------

MaterialLayer::MaterialLayer

  ( const String&      name,
    const Properties&  conf,
    const Properties&  props,
    const Properties&  globdat ) : Model ( name )

{
  const idx_t  STRAIN_COUNTS[4] = { 0, 1, 3, 6 };

  globdat_ = globdat;
  props_   = props.getProps ( myName_ );

  Properties myConf  = conf.makeProps ( myName_ );

  idx_t rank = 0;
  props_.get  ( rank,   RANK     );
  props_.get  ( hSize_, HISTSIZE );

  sSize_ = STRAIN_COUNTS[rank];

  myConf. set ( RANK, rank );

  sNeurons_.resize ( sSize_ );
  sNeurons_ = 0;

  hNeurons_.resize ( hSize_ );
  hNeurons_ = 0;

  bool input = false;
  bool output = false;

  props_.find ( input, LearningNames::FIRSTLAYER );
  props_.find ( output, LearningNames::LASTLAYER );

  if ( input || !output )
  {
    throw Error ( JEM_FUNC, "A Material layer must be the output layer" );
  }

  Ref<XAxonSet>   aset = XAxonSet  ::get ( globdat, getContext()    );
  Ref<XNeuronSet> nset = XNeuronSet::get ( globdat, getContext()    );
  Ref<XDofSpace>  dofs = XDofSpace:: get ( aset->getData(), globdat );

  for ( idx_t in = 0; in < sNeurons_.size(); ++in )
  {
    sNeurons_[in] = nset->addNeuron();
  }

  for ( idx_t in = 0; in < hNeurons_.size(); ++in )
  {
    hNeurons_[in] = nset->addNeuron();
  }

  globdat.get ( inpNeurons_, LearningParams::PREDECESSOR       );
  globdat.set ( LearningParams::PREDECESSOR, sNeurons_.clone() );
}

MaterialLayer::~MaterialLayer ()
{}

//-----------------------------------------------------------------------
//   configure
//-----------------------------------------------------------------------

void MaterialLayer::configure

  ( const Properties& props,
    const Properties& globdat )

{
}

//-----------------------------------------------------------------------
//   getConfig
//-----------------------------------------------------------------------

void MaterialLayer::getConfig

  ( const Properties& conf,
    const Properties& globdat )

{
}

//-----------------------------------------------------------------------
//   takeAction
//-----------------------------------------------------------------------

bool MaterialLayer::takeAction

  ( const String&     action,
    const Properties& params,
    const Properties& globdat )

{
  using jive::model::Actions;

  //System::out() << "Action " << action << '\n';

  if ( action == Actions::SHUTDOWN )
  {
    System::out() << "Time spent updating " << tUpd_ << '\n';
  }

  if ( action == LearningActions::PROPAGATE ||
       action == LearningActions::RECALL       )
  {
    Ref<NData> data;
    Ref<NData> state = nullptr;

    params.get  ( data,  LearningParams::DATA  );
    params.find ( state, LearningParams::STATE );

    if ( state == nullptr )
    {
      material_.resize ( data->batchSize() );

      for ( idx_t p = 0_idx; p < data->batchSize(); ++p )
      {
	material_[p] = newMaterial ( MATERIAL, Properties(), props_, globdat_ );
	material_[p]->configure ( props_.findProps ( MATERIAL ), globdat_ );
	material_[p]->createIntPoints ( 1 );
      }

      BayRef bay = dynamicCast<Bayesian> ( material_[0] );

      //if ( bay->propCount() != inpNeurons_.size() )
      //{
      //  sizeError ( JEM_FUNC, "Material properties", bay->propCount(), inpNeurons_.size() );
      //}

      Vector hvals;
      material_[0]->getHistory ( hvals, 0_idx );

      if ( hvals.size() != hSize_ )
      {
        sizeError ( JEM_FUNC, "Internal variables", hvals.size(), hSize_ );
      }
    }

    propagate_ ( data, state, globdat );

    if ( data->outSize() != sNeurons_.size() )
    {
      sizeError ( JEM_FUNC, "Output layer size", data->outSize(), sNeurons_.size() );
    }

    data->outputs = select ( data->activations, sNeurons_, ALL );

    return true;
  }

  if ( action == LearningActions::BACKPROPAGATE )
  {
    Ref<NData> data;
    Ref<NData> state = nullptr;

    params.get  ( data,  LearningParams::DATA  );
    params.find ( state, LearningParams::STATE );

    select ( data->deltas, sNeurons_, ALL ) = data->outputs;

    backPropagate_ ( data, state, globdat );

    return true;
  }

  return false;
}

//-----------------------------------------------------------------------
//   propagate_
//-----------------------------------------------------------------------

void MaterialLayer::propagate_

  ( const Ref<NData>  data,
    const Ref<NData>  state,
    const Properties& globdat  )

{
  Matrix mus   ( select ( data->activations, inpNeurons_, ALL ) );

  for ( idx_t p = 0_idx; p < data->batchSize(); ++p )
  {
    MatRef mat = material_[p];
    BayRef bay = dynamicCast<Bayesian> ( mat );
    
    bay->setProps ( mus(ALL,p) );

    Vector hist;
    Vector stress ( sSize_ );
    Matrix stiff  ( sSize_, sSize_ );
    Vector strain ( data->xinputs ( ALL, p ) );

    if ( state == nullptr )
    {
      mat->getHistory ( hist, 0_idx );

      data->values ( hNeurons_, p ) = hist;
    }
    else
    {
      data->values ( hNeurons_, p ) = state->activations ( hNeurons_, p );

      mat->setHistory ( Vector ( data->values ( hNeurons_, p ) ), 0_idx );
    }

    tUpd_.start();
    mat->update ( stiff, stress, strain, 0_idx );
    tUpd_.stop();

    data->values      ( sNeurons_, p ) = strain;
    data->activations ( sNeurons_, p ) = stress;

    mat->getHistory ( hist, 0_idx );

    data->activations ( hNeurons_, p ) = hist;

    //if ( p == 0 ) System::out() << "Sample 0 props " << mus(ALL,p) << '\n';
    //System::out() << "Forward propagation props " << props << '\n';
    //System::out() << "Forward propagation values " << data->values(sNeurons_,p) << '\n';
    //System::out() << "Forward propagation activations " << data->activations(sNeurons_,p) << '\n';
  }
}

//-----------------------------------------------------------------------
//   backPropagate_
//-----------------------------------------------------------------------

void MaterialLayer::backPropagate_

  ( const Ref<NData>   data,
    const Ref<NData>   state,
    const Properties&  globdat  )

{
  double fdstep = 1.e-10;
  idx_t  bsize  = data->batchSize();

  Matrix dsigmadmu    ( inpNeurons_.size(), sSize_ );
  Matrix dalphadmu    ( inpNeurons_.size(), hSize_ );
  Matrix dalphadalpha ( hSize_,             hSize_ );
  Matrix dsigmadalpha ( hSize_,             sSize_ );

  dsigmadmu    = 0.0; dalphadmu       = 0.0;
  dalphadalpha = 0.0; dsigmadalpha    = 0.0;

  Matrix mus     ( select ( data->activations, inpNeurons_, ALL ) );
  Matrix hists   ( select ( data->values,      hNeurons_,   ALL ) );
  Matrix sdeltas ( select ( data->deltas,      sNeurons_,   ALL ) );
  Matrix hdeltas ( select ( data->deltas,      hNeurons_,   ALL ) );

  Matrix sdeltasbp ( sSize_, bsize );
  Matrix hdeltasbp ( hSize_, bsize );

  sdeltasbp = 0.0; hdeltasbp = 0.0;

  for ( idx_t p = 0_idx; p < bsize; ++p )
  {
    MatRef mat = material_[p];
    BayRef bay = dynamicCast<Bayesian> ( mat );

    Vector fstress ( sSize_ );
    Vector bstress ( sSize_ );
    Vector fhist   ( hSize_ );
    Vector bhist   ( hSize_ );

    Matrix stiff  ( sSize_, sSize_ );
    Vector strain ( data->xinputs ( ALL, p ) );

    Vector pprops ( mus  (ALL,p) );
    Vector phist  ( hists(ALL,p) );

    mat->setHistory ( phist, 0_idx );

    for ( idx_t mu = 0_idx; mu < inpNeurons_.size(); ++mu )
    {
      pprops[mu] += fdstep;
      bay->setProps ( pprops );
      tUpd_.start();
      mat->update ( stiff, fstress, strain, 0_idx );
      tUpd_.stop();
      mat->getHistory ( fhist, 0_idx );

      pprops[mu] -= 2.0*fdstep; 
      bay->setProps ( pprops );
      tUpd_.start();
      mat->update ( stiff, bstress, strain, 0_idx );
      tUpd_.stop();
      mat->getHistory ( bhist, 0_idx );

      dsigmadmu ( mu, ALL ) = ( fstress - bstress ) / 2.0 / fdstep;
      dalphadmu ( mu, ALL ) = ( fhist   - bhist   ) / 2.0 / fdstep;

      pprops = mus(ALL,p);
    }

    bay->setProps ( pprops );

    for ( idx_t alpha = 0_idx; alpha < hSize_; ++alpha )
    {
      phist[alpha] += fdstep;
      mat->setHistory ( phist, 0_idx );
      tUpd_.start();
      mat->update ( stiff, fstress, strain, 0_idx );
      tUpd_.stop();
      mat->getHistory ( fhist, 0_idx );

      phist[alpha] -= 2.0*fdstep;
      mat->setHistory ( phist, 0_idx );
      tUpd_.start();
      mat->update ( stiff, bstress, strain, 0_idx );
      tUpd_.stop();
      mat->getHistory ( bhist, 0_idx );

      dsigmadalpha ( alpha, ALL ) = ( fstress - bstress ) / 2.0 / fdstep;
      dalphadalpha ( alpha, ALL ) = ( fhist   - bhist   ) / 2.0 / fdstep;

      phist = hists(ALL,p);
    }

    //System::out() << "Backprop dsigmadmu " << dsigmadmu << '\n';
    //System::out() << "Backprop dalphadmu " << dalphadmu << '\n';
    //System::out() << "Backprop dsigmadalpha " << dsigmadalpha << '\n';
    //System::out() << "Backprop dalphadalpha " << dalphadalpha << '\n';

    // Backpropagate through the network

    data->deltas ( inpNeurons_,p ) =
      matmul ( dsigmadmu, sdeltas(ALL,p) ) +
        matmul ( dalphadmu, hdeltas(ALL,p) ); 

    // Backpropagate through time

    if ( state != nullptr )
    {
      state->deltas ( hNeurons_, p ) = 
	matmul ( dsigmadalpha, sdeltas(ALL,p) ) +
	  matmul ( dalphadalpha, hdeltas(ALL,p) );
    }
  }
  //System::out() << "backpropagated\n";
}

//=======================================================================
//   related functions
//=======================================================================

//-----------------------------------------------------------------------
//   newMaterialLayer
//-----------------------------------------------------------------------


Ref<Model>            newMaterialLayer

  ( const String&       name,
    const Properties&   conf,
    const Properties&   props,
    const Properties&   globdat )

{
  // Return an instance of the class.

  return newInstance<MaterialLayer> ( name, conf, props, globdat );
}

//-----------------------------------------------------------------------
//   declareMaterialLayer
//-----------------------------------------------------------------------

void declareMaterialLayer ()
{
  using jive::model::ModelFactory;

  // Register the StressModel with the ModelFactory.

  ModelFactory::declare ( "Material", newMaterialLayer );
}

