
/*
 *  Copyright (C) 2016 DRG. All rights reserved.
 *
 *  This file is part of Jive, an object oriented toolkit for solving
 *  partial differential equations.
 *
 *  Commercial License Usage
 *
 *  This file may be used under the terms of a commercial license
 *  provided with the software, or under the terms contained in a written
 *  agreement between you and DRG. For more information contact DRG at
 *  http://www.dynaflow.com.
 *
 *  GNU Lesser General Public License Usage
 *
 *  Alternatively, this file may be used under the terms of the GNU
 *  Lesser General Public License version 2.1 or version 3 as published
 *  by the Free Software Foundation and appearing in the file
 *  LICENSE.LGPLv21 and LICENSE.LGPLv3 included in the packaging of this
 *  file. Please review the following information to ensure the GNU
 *  Lesser General Public License requirements will be met:
 *  https://www.gnu.org/licenses/lgpl.html and
 *  http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
 *  This file is part of Jive, an object oriented toolkit for
 *  solving partial differential equations.
 *
 *  Jive version: 2.2
 *  Date:         Thu 28 Jan 10:31:15 CET 2016
 *
 *  Modified: Iuri Barcelos, August 2016
 *
 *  Incorporation of fpm's desperateMode to the normal NonlinModule.
 *
 */


#include <jem/base/limits.h>
#include <jem/base/Float.h>
#include <jem/base/System.h>
#include <jem/base/Exception.h>
#include <jem/base/ClassTemplate.h>
#include <jem/base/array/operators.h>
#include <jem/io/Writer.h>
#include <jem/util/Event.h>
#include <jem/util/Flex.h>
#include <jem/numeric/algebra/utilities.h>
#include <jive/util/utilities.h>
#include <jive/util/Globdat.h>
#include <jive/util/FuncUtils.h>
#include <jive/algebra/VectorSpace.h>
#include <jive/model/Actions.h>
#include <jive/model/StateVector.h>
#include <jive/app/ModuleFactory.h>
#include <jive/implict/Names.h>
#include <jive/implict/ConHandler.h>
#include <jive/implict/SolverInfo.h>
#include <jive/implict/NonlinRunData.h>

#include "XNonlinModule.h"
#include "SolverNames.h"
#include "declare.h"

JEM_DEFINE_CLASS( jive::implict::XNonlinModule );


JIVE_BEGIN_PACKAGE( implict )


using jem::max;
using jem::newInstance;
using jem::Float;
using jem::System;
using jem::Exception;
using jem::io::endl;
using jem::numeric::axpy;
using jive::util::FuncUtils;
using jive::model::Actions;
using jive::model::StateVector;


//=======================================================================
//   class XNonlinModule::RunData_
//=======================================================================


class XNonlinModule::RunData_ : public NonlinRunData
{
 public:

  typedef NonlinRunData   Super;
  typedef RunData_        Self;


  explicit inline         RunData_

    ( const String&         context );


 public:

  Matrix                  vbuf;

  bool                    validMatrix;


 protected:

  virtual void            dofsChanged_  ();

};


//-----------------------------------------------------------------------
//   constructor
//-----------------------------------------------------------------------


inline XNonlinModule::RunData_::RunData_ ( const String& ctx ) :

  Super ( ctx )

{
  validMatrix = false;
}


//-----------------------------------------------------------------------
//   dofsChanged_
//-----------------------------------------------------------------------


void XNonlinModule::RunData_::dofsChanged_ ()
{
  if ( frozen )
  {
    Super::dofsChanged_ ();
  }
  else
  {
    validMatrix = false;
  }
}


//=======================================================================
//   class XNonlinModule::Work_
//=======================================================================


class XNonlinModule::Work_ : public ConHandler
{
 public:

                          Work_

    ( XNonlinModule&         module,
      const Properties&     globdat );

  inline                 ~Work_             ();

  void                    updateRscale

    ( const Properties&     globdat );

  void                    updateResidual    ();

  void                    updateMatrix

    ( const Properties&     globdat,
      bool                  getFint = true );

  void                    calcIncrement

    ( double                maxIncr );

  void                    doTotalUpdate

    ( const Properties&     globdat );

  bool                    checkConverged

    ( double                tol,
      const Properties&     globdat );

  inline void             reportProgress    ();


 public:

  RunData_&               rundat;

  Vector                  u;
  Vector                  du;
  Vector                  fext;
  Vector                  fint;
  Vector                  r;

  IdxVector               slaveDofs;

  idx_t                   iiter;
  double                  rscale;
  double                  dnorm;
  double                  rnorm;
  double                  rnorm0;
  double                  rnorm1;


 private:

  String                  myName_;
  Function*               updateCond_;

};


//-----------------------------------------------------------------------
//   constructor & destructor
//-----------------------------------------------------------------------


XNonlinModule::Work_::Work_

  ( XNonlinModule&      mod,
    const Properties&  globdat ) :

    rundat  ( *mod.rundat_ ),
    myName_ (  mod.getName() )

{
  idx_t  dofCount;
  idx_t  j;


  iiter  = 0;
  rscale = 0.0;
  dnorm  = 0.0;
  rnorm  = 1.0;
  rnorm0 = 1.0;
  rnorm1 = 1.0;

  updateCond_ = mod.updateCond_.get ();

  rundat.updateConstraints ( globdat );
  rundat.dofs->resetEvents ();

  rundat.frozen = true;
  dofCount      = rundat.dofs->dofCount ();

  StateVector::get ( u, rundat.dofs, globdat );

  j = 0;

  rundat.vbuf.resize ( dofCount, 4 );

  du  .ref ( rundat.vbuf[j++] );
  fext.ref ( rundat.vbuf[j++] );
  fint.ref ( rundat.vbuf[j++] );
  r   .ref ( rundat.vbuf[j++] );

  saveConstraints ( *rundat.cons );

  if ( ! (mod.options_ & DELTA_CONS) )
  {
    adjustConstraints ( *rundat.cons, u );
  }
}


inline XNonlinModule::Work_::~Work_ ()
{
  rundat.frozen = false;
}


//-----------------------------------------------------------------------
//   updateRscale
//-----------------------------------------------------------------------


void XNonlinModule::Work_::updateRscale

  ( const Properties&  globdat )

{
  using jive::model::ActionParams;

  Properties  params;
  double      rtmp;

  axpy ( r, fext, -1.0, fint );

  //System::out() << "updateRscale: fint = " << fint << "\n";
  //System::out() << "updateRscale: fext = " << fext << "\n";
  //System::out() << "updateRscale: r = " << r << "\n";

  rtmp   = std::sqrt( Float::EPSILON ) *

    max ( rundat.vspace->norm2( fext ),
          rundat.vspace->norm2( fint ) );

  rtmp   = max ( rtmp, rundat.vspace->norm2( r ) );
  rscale = max ( rtmp, rscale );

  params.set ( ActionParams::RESIDUAL,   r );
  params.set ( ActionParams::RES_SCALE,  rscale );
  params.set ( ActionParams::INT_VECTOR, fint );
  params.set ( ActionParams::EXT_VECTOR, fext );

  rundat.model->takeAction ( Actions::GET_RES_SCALE,
                             params, globdat );

  params.get ( rscale, ActionParams::RES_SCALE );
}


//-----------------------------------------------------------------------
//   updateResidual
//-----------------------------------------------------------------------


void XNonlinModule::Work_::updateResidual ()
{
  //System::out() << "Residual before eMD: " << r << "\n";
  axpy           ( r, fext, -1.0, fint );
  evalMasterDofs ( r, *rundat.cons );
  //System::out() << "Residual after eMD: " << r << "\n";
  //System::out() << "fint = " << fint << "\n";
  //System::out() << "fext = " << fext << "\n";

  rnorm1 = rnorm0;
  rnorm0 = rnorm;
  rnorm  = rundat.vspace->norm2 ( r );

  if ( rscale > 0.0 )
  {
    rnorm /= rscale;
  }
}


//-----------------------------------------------------------------------
//   updateMatrix
//-----------------------------------------------------------------------


void XNonlinModule::Work_::updateMatrix

  ( const Properties&  globdat,
    bool               getFint )

{
  bool  doUpdate;

  if ( ! rundat.validMatrix || ! updateCond_ )
  {
    doUpdate = true;
  }
  else
  {
    double  args[4] = { (double) iiter, rnorm, rnorm0, rnorm1 };
    double  result  = 0.0;

    try
    {
      result = updateCond_->getValue ( args );
    }
    catch ( Exception& ex )
    {
      ex.setContext ( rundat.context );
      throw;
    }

    doUpdate = (result * result >= 0.25);
  }

  if ( doUpdate )
  {
    rundat.updateMatrix ( fint, globdat );

    rundat.validMatrix = true;
  }
  else if ( getFint )
  {
    rundat.getIntVector ( fint, globdat );
  }

}


//-----------------------------------------------------------------------
//   calcIncrement
//-----------------------------------------------------------------------


void XNonlinModule::Work_::calcIncrement ( double maxIncr )
{
  using jem::isTiny;

  double  dnorm0 = dnorm;

  rundat.solver->solve ( du, r );

  //System::out() << "du = " << du << "\n";
  //System::out() << "r = " << r << "\n";

  dnorm = rundat.vspace->norm2 ( du );

  iiter++;

  if ( iiter <= 2 || isTiny( dnorm0 ) )
  {
    return;
  }

  if ( dnorm > maxIncr * dnorm0 )
  {
    double  scale = maxIncr * dnorm0 / dnorm;

    print ( System::info( myName_ ), rundat.context,
            " : scaling solution increment with factor ",
            rundat.nformat.print( scale ),
            endl );

    du *= scale;
  }
}


//-----------------------------------------------------------------------
//   doTotalUpdate
//-----------------------------------------------------------------------


void XNonlinModule::Work_::doTotalUpdate

  ( const Properties& globdat )

{
  rundat.updateModel  (       globdat );
  rundat.getIntVector ( fint, globdat );
  updateResidual      ();
}


//-----------------------------------------------------------------------
//   checkConverged
//-----------------------------------------------------------------------


bool XNonlinModule::Work_::checkConverged

  ( double             tol,
    const Properties&  globdat )

{
  using jive::model::ActionParams;

  Properties  params;
  bool        conv;


  conv = (rnorm <= tol);

  params.set ( ActionParams::RESIDUAL,  r );
  params.set ( ActionParams::RES_SCALE, rscale );
  params.set ( ActionParams::CONVERGED, conv );

  rundat.model->takeAction ( Actions::CHECK_CONVERGED,
                             params, globdat );

  params.find ( conv, ActionParams::CONVERGED );

  return conv;
}


//-----------------------------------------------------------------------
//   reportProgress
//-----------------------------------------------------------------------


inline void XNonlinModule::Work_::reportProgress ()
{
  print ( System::info( myName_ ), rundat.context,
          " : iter = "          , iiter,
          ", scaled residual = ", rundat.nformat.print( rnorm ),
          endl );

  System::info( myName_ ).flush ();
}


//=======================================================================
//   class XNonlinModule
//=======================================================================

//-----------------------------------------------------------------------
//   static data
//-----------------------------------------------------------------------


const char*  XNonlinModule::TYPE_NAME   = "XNonlin";

const int    XNonlinModule::LINE_SEARCH       = 1 << 0;
const int    XNonlinModule::DELTA_CONS        = 1 << 1;
const char*  XNonlinModule::DESPAIR_PROP      = "allowDespair";
const char*  XNonlinModule::MAX_ITER2_PROP    = "maxIter2";
const char*  XNonlinModule::NHOPE_PROP        = "nHope";
const char*  XNonlinModule::MAXLOOPLEVEL_PROP = "maxLoopLevel";
const char*  XNonlinModule::MAXRESIDUAL_PROP  = "maxResidual";


//-----------------------------------------------------------------------
//   constructor & destructor
//-----------------------------------------------------------------------


XNonlinModule::XNonlinModule ( const String& name ) :

  Super ( name )

{
  maxIter_   = 20;
  options_   = 0;
  tiny_      = jem::Limits<double>::TINY_VALUE;
  precision_ = 1.0e-6;
  maxIncr_   = 10.0;
  maxResidual_ = 1.e1;

  allowDespair_ = false;
}


XNonlinModule::~XNonlinModule ()
{}


//-----------------------------------------------------------------------
//   init
//-----------------------------------------------------------------------


Module::Status XNonlinModule::init

  ( const Properties&  conf,
    const Properties&  props,
    const Properties&  globdat )

{
  using jive::util::joinNames;

  rundat_ = nullptr;

  Ref<RunData_>  newdat = newInstance<RunData_> ( getContext() );
  String         name   = joinNames ( myName_, PropNames::SOLVER );

  newdat->init       ( globdat );
  newdat->initSolver ( name, precision_, conf, props, globdat );

  // Everything OK, so commit the changes

  rundat_.swap ( newdat );

  return OK;
}


//-----------------------------------------------------------------------
//   shutdown
//-----------------------------------------------------------------------


void XNonlinModule::shutdown ( const Properties& globdat )
{
  rundat_ = nullptr;
  return;

  System::info() << "XNonlinModule statistics ..."
    << "\n... total # of iterations:       " << nIterTot_ << "\n"; 

  if ( allowDespair_ )
  {
    System::info() << "\n..."
    << "\n... # of desperateModes:         " << nDespair_;

    if ( nDespair_ > 0 )
    {
    System::info() 
    << "\n-------- ( of which  " << nDFailIn_ << " kept oscillating.)"
    << "\n-------- (           " << nDMaxIter2_ << " didn't converge.)"
    << "\n-------- (      and  " << nDSucces_ << " were succesful,"
    << "\n------------  although " << nDFailAfter_ << " re-oscillated.)"
    << "\n... # of iters in desperateMode  " << nIterD_;
    }
  }
  System::info() << "\n\n";
}


//-----------------------------------------------------------------------
//   configure
//-----------------------------------------------------------------------


void XNonlinModule::configure

  ( const Properties&  props,
    const Properties&  globdat )

{
  using jem::maxOf;

  if ( props.contains( myName_ ) )
  {
    Properties  myProps = props.findProps ( myName_ );

    String      expr;
    bool        option;


    myProps.find ( maxIter_,   PropNames::MAX_ITER,
                   0,          maxOf( maxIter_ ) );
    myProps.find ( tiny_,      PropNames::TINY,
                   0.0,        1.0e20 );
    myProps.find ( precision_, PropNames::PRECISION,
                   0.0,        1.0e20 );
    myProps.find ( maxIncr_,   PropNames::MAX_INCR,
                   0.0,        1.0e20 );

    FuncUtils::configFunc  ( updateCond_, "i, r, r0, r1",
                             PropNames::UPDATE_COND,
                             myProps, globdat );

    if ( myProps.find( option, PropNames::LINE_SEARCH ) )
    {
      if ( option )
      {
        options_ |=  LINE_SEARCH;
      }
      else
      {
        options_ &= ~LINE_SEARCH;
      }
    }

    if ( myProps.find( option, PropNames::DELTA_CONS ) )
    {
      if ( option )
      {
        options_ |=  DELTA_CONS;
      }
      else
      {
        options_ &= ~DELTA_CONS;
      }
    }
    myProps.find ( maxResidual_, MAXRESIDUAL_PROP, 
                   0.0,        1.0e20 );

    myProps.find ( allowDespair_, DESPAIR_PROP );

    if ( allowDespair_ )
    {
      maxIter2_ = 100;
      
      myProps.find ( maxIter2_, MAX_ITER2_PROP,
                     maxIter_,  maxOf( maxIter2_ ) );

      nHope_    = 10;

      myProps.find ( nHope_,    NHOPE_PROP,
                     0,         maxIter2_ - maxIter_ );

      maxLoopLevel_ = 3;

      myProps.find ( maxLoopLevel_, MAXLOOPLEVEL_PROP,
                     0,         maxOf ( maxLoopLevel_ ) );
    }
  }

  if ( rundat_ != nullptr )
  {
    rundat_->solver->configure ( props );
  }
}


//-----------------------------------------------------------------------
//   getConfig
//-----------------------------------------------------------------------


void XNonlinModule::getConfig

  ( const Properties&  conf,
    const Properties&  globdat ) const

{
  Properties  myConf = conf.makeProps ( myName_ );


  myConf.set ( PropNames::MAX_ITER,  maxIter_   );
  myConf.set ( PropNames::TINY,      tiny_      );
  myConf.set ( PropNames::PRECISION, precision_ );
  myConf.set ( PropNames::MAX_INCR,  maxIncr_   );

  myConf.set ( PropNames::LINE_SEARCH,
               ((options_ & LINE_SEARCH) != 0)  );

  myConf.set ( PropNames::DELTA_CONS,
               ((options_ & DELTA_CONS)  != 0)  );

  FuncUtils::getConfig ( myConf, updateCond_,
                         PropNames::UPDATE_COND );

  myConf.set ( DESPAIR_PROP, allowDespair_ );

  if ( allowDespair_ )
  {
    myConf.set ( NHOPE_PROP,     nHope_    );
    myConf.set ( MAX_ITER2_PROP, maxIter2_ );
    myConf.set ( MAXLOOPLEVEL_PROP, maxLoopLevel_ );
  }

  if ( rundat_ != nullptr )
  {
    rundat_->solver->getConfig ( conf );
  }
}


//-----------------------------------------------------------------------
//   advance
//-----------------------------------------------------------------------


void XNonlinModule::advance ( const Properties& globdat )
{
  if ( rundat_ == nullptr )
  {
    notAliveError ( JEM_FUNC );
  }

  rundat_->advance ( globdat );
}

//-----------------------------------------------------------------------
//   run
//-----------------------------------------------------------------------


Module::Status XNonlinModule::run ( const Properties& globdat )
{
  using jem::System;

  Properties  info = SolverInfo::get ( globdat );

  while ( true )
  {
    info.clear ();

    advance ( globdat );

    try
    {
      solve ( info, globdat );
    }
    catch ( const jem::Exception& )
    {
      cancel ( globdat );
      throw;
    }

    break;

    //if ( commit( globdat ) )
    //{
    //  break;
    //}

    //print  ( System::info( myName_ ),
    //         "Solution rejected; re-trying\n" );

    //cancel ( globdat );
  }

  return OK;
}


//-----------------------------------------------------------------------
//   solve
//-----------------------------------------------------------------------


void XNonlinModule::solve

  ( const Properties&  info,
    const Properties&  globdat )

{
  using jive::util::Globdat;
  using jive::util::FuncUtils;

  if ( rundat_ == nullptr )
  {
    notAliveError ( JEM_FUNC );
  }

  RunData_&  d = * rundat_;

  Work_      w ( *this, globdat );

  bool       result;


  print ( System::info( myName_ ),
          "Starting the Newton-Raphson solver `",
          myName_, "\' ...\n" );

  if ( updateCond_ != nullptr )
  {
    try
    {
      FuncUtils::resolve ( *updateCond_, globdat );
    }
    catch ( Exception& ex )
    {
      ex.setContext ( getContext() );
      throw;
    }
  }

  d.getExtVector    ( w.fext, globdat );
  w.updateMatrix    ( globdat );
  w.updateRscale    ( globdat );
  w.calcIncrement   ( maxIncr_ );
  w.zeroConstraints ( *d.cons  );

  result = solve_   ( w, globdat );

  w.restoreConstraints ( *d.cons );

  info.set ( SolverInfo::CONVERGED,  result  );
  info.set ( SolverInfo::ITER_COUNT, w.iiter );
  info.set ( SolverInfo::RESIDUAL,   w.rnorm );

  if ( Globdat::hasVariable( myName_, globdat ) )
  {
    Properties  vars = Globdat::getVariables ( myName_, globdat );

    vars.set ( SolverInfo::CONVERGED,  result  );
    vars.set ( SolverInfo::ITER_COUNT, w.iiter );
    vars.set ( SolverInfo::RESIDUAL,   w.rnorm );
  }

  if ( ! result )
  {
    throw Exception (
      getContext (),
      String::format (
        "no convergence achieved in %d iterations; "
        "final residual: %e",
        w.iiter,
        w.rnorm
      )
    );
  }

  print ( System::info( myName_ ),
          "The Newton-Raphson solver converged in ",
          w.iiter, " iterations\n\n" );
}


//-----------------------------------------------------------------------
//   cancel
//-----------------------------------------------------------------------


void XNonlinModule::cancel ( const Properties& globdat )
{
  if ( rundat_ == nullptr )
  {
    notAliveError ( JEM_FUNC );
  }

  rundat_->cancel ( globdat );
}


//-----------------------------------------------------------------------
//   commit
//-----------------------------------------------------------------------


bool XNonlinModule::commit ( const Properties& globdat )
{
  if ( rundat_ == nullptr )
  {
    notAliveError ( JEM_FUNC );
  }

  return rundat_->commit ( globdat );
}


//-----------------------------------------------------------------------
//   setPrecision
//-----------------------------------------------------------------------


void XNonlinModule::setPrecision ( double eps )
{
  JEM_PRECHECK ( eps > 0.0 );

  precision_ = eps;
}


//-----------------------------------------------------------------------
//   getPrecision
//-----------------------------------------------------------------------


double XNonlinModule::getPrecision () const
{
  return precision_;
}


//-----------------------------------------------------------------------
//   setOptions
//-----------------------------------------------------------------------


void XNonlinModule::setOptions ( int options )
{
  options_ = options;
}


//-----------------------------------------------------------------------
//   getOptions
//-----------------------------------------------------------------------


int XNonlinModule::getOptions () const
{
  return options_;
}


//-----------------------------------------------------------------------
//   makeNew
//-----------------------------------------------------------------------


Ref<Module> XNonlinModule::makeNew

  ( const String&      name,
    const Properties&  conf,
    const Properties&  props,
    const Properties&  globdat )

{
  return newInstance<Self> ( name );
}

//-----------------------------------------------------------------------
//   declare
//-----------------------------------------------------------------------

void XNonlinModule::declare ()
{
  using jive::app::ModuleFactory;

  ModuleFactory::declare ( TYPE_NAME, & makeNew );
  ModuleFactory::declare ( CLASS_NAME, & makeNew );
}

//-----------------------------------------------------------------------
//   solve_
//-----------------------------------------------------------------------


bool XNonlinModule::solve_

  ( Work_&             w,
    const Properties&  globdat )

{
  using jem::util::Flex;

  RunData_&  d = w.rundat;

  axpy ( w.u, 1.0, w.du );

  d.updateModel  ( globdat );
  w.updateMatrix ( globdat );
  w.updateRscale ( globdat );

  print ( System::info( myName_ ), d.context,
          " : residual scale factor = ",
          d.nformat.print( w.rscale ), endl );

  // initialize data for desperateMode

  Flex<double>   oldRs;
  idx_t          descending = 0;
  idx_t          dIterCount = 0;
  double         switchRate;
  bool           kinked;
  Properties     params;

  oldRs.pushBack ( 1. );

  idx_t  maxIter       = maxIter_;
  bool desperateMode = false;

  d.model->takeAction ( "END_DESPAIR", params, globdat );

  if ( w.rscale <= tiny_ )
  {
    return true;
  }

  w.updateResidual ();

  do
  {
    if ( w.rnorm < oldRs.back() )
    {
      ++descending;
    }
    else
    {
      descending = 0;
    }

    if ( desperateMode )
    {
      ++dIterCount;
      ++nIterD_;
    }

    oldRs.pushBack ( w.rnorm );

    w.reportProgress ();

    if ( allowDespair_ && ( desperateMode  
         ? dIterCount > 3 
         : w.iiter + maxIter_ - maxIter > 3 ) )
         
    {
      // difference in residuals must be small ...

      switchRate = max ( ::fabs ( oldRs[w.iiter  ] - oldRs[w.iiter-2] ),
                         ::fabs ( oldRs[w.iiter-1] - oldRs[w.iiter-3] ) );

      // and residual must be going up and down

      kinked = ( ( oldRs[w.iiter  ] - oldRs[w.iiter-1] ) * 
                 ( oldRs[w.iiter-1] - oldRs[w.iiter-2] ) < 0. )
               &&
               ( ( oldRs[w.iiter-1] - oldRs[w.iiter-2] ) * 
                 ( oldRs[w.iiter-2] - oldRs[w.iiter-3] ) < 0. );

      for ( idx_t i = 3; i <= maxLoopLevel_; ++i )
      {
        if ( desperateMode
             ? dIterCount > 2*i-1
             : w.iiter + maxIter_ - maxIter > 2*i-1 )
        {
          // also loops of the type ABCABC, ABCDABCD, etc

          Vector diffs ( i );

          for ( idx_t j = 0; j < i; ++j )
            diffs[j] = ::fabs ( oldRs[w.iiter-j] - oldRs[w.iiter-i-j] );

          double rate = max ( diffs );

          if ( rate < .1 * switchRate )
          {
            switchRate = rate;
            kinked = true;
          }
        }
      }
    }
    else
    {
      switchRate = 10.;
      kinked = false;
    }

    if ( allowDespair_ )
    {
      //if ( ( ! desperateMode ) && switchRate < precision_ )
      if ( ( ! desperateMode ) && switchRate < precision_ && dIterCount == 0 )
      {
        // move to desperateMode (unless that has been tried already)

        if ( dIterCount > 0 ) 
        {
          System::info() << "  -- too bad, desperateMode didn't help\n";

          ++nDFailAfter_;

          return false;
        }

        System::info() << "  -- desperateMode\n";

        desperateMode = true;
        descending    = 0;
        dIterCount    = 0;
        maxIter       = maxIter2_;
        ++nDespair_;

        d.model->takeAction ( "DESPAIR", params, globdat );
      }
    }

    else if (0)
    //else if ( switchRate < precision_ * .5 )
    {
      System::info() << "  -- oscillating, while allowDespair = false" << endl;
     
      return false;
    }

    if ( w.checkConverged( precision_, globdat ) )
    {
      return true;
    }

    if ( desperateMode )
    {
      maxIter = maxIter2_ - nHope_ + descending;

      kinked &= ( dIterCount > 3 );

      if (0) // DEBUG
      //if ( descending == nHope_ )
      {
        System::info() << "  -- this ought to do it, end desperateMode \n";

        desperateMode = false;
        //descending    = 0;
        ++nDSucces_;

        d.model->takeAction ( "END_DESPAIR", params, globdat );

        maxIter = w.iiter + maxIter_;
      }

      else if ( switchRate < precision_ * .01 && kinked )
      {
        System::info() << "  -- oscillating, while in desperateMode\n";

        ++nDFailIn_;

        return false;
      }

      // If still descending, do not give up on this step yet

      else if ( w.iiter > maxIter2_ && descending == 0 )
      {
        return false;
      }
    }

    if ( !desperateMode && w.iiter > maxIter_ && descending == 0 )
    {
      return false;
    }

    if ( w.rnorm > maxResidual_ )
    {
      if ( options_ & LINE_SEARCH )
      {
        // Undo the last solution increment and execute the line
        // search procedure.

        axpy ( w.u, -1.0, w.du );

        d.updateModel ( globdat );
        lineSearch_   ( w, globdat );
      }
      else
      {
        return false;
      }
    }

    w.calcIncrement ( maxIncr_ );

    if ( w.iiter > 2 && (options_ & LINE_SEARCH) )
    {
      lineSearch_ ( w, globdat );
    }
    else
    {
      axpy ( w.u, 1.0, w.du );

      d.updateModel    ( globdat );
      w.updateMatrix   ( globdat );
      w.updateResidual ();
    }
  }
  while ( true );

  return false;
}


//-----------------------------------------------------------------------
//   lineSearch_
//-----------------------------------------------------------------------


void XNonlinModule::lineSearch_

  ( Work_&             w,
    const Properties&  globdat )

{
  using jem::min;

  const idx_t  NMAX     = 10;

  RunData_&    d        = w.rundat;

  double       minScale = 0.1;
  double       maxScale = 1.0;

  double       rmin, rmin0;
  double       rmax;
  double       r[NMAX + 1];

  double       s, s0, ds;
  double       a, b;

  Vector       u0;

  idx_t        i, j, n;


  rmax = max ( 0.5 * w.rnorm0, precision_ );

  n    = max ( JEM_IDX_C(4), 2 * w.iiter );
  n    = min ( n, NMAX );

  s    = maxScale;

  axpy ( w.u, s, w.du );

  d.updateModel    ( globdat );
  w.updateMatrix   ( globdat );
  w.updateResidual ();

  if ( w.rnorm < rmax )
  {
    return;
  }

  r[n] = w.rnorm;

  print ( System::info( myName_ ), w.rundat.context,
          " : starting line search ...\n" );

  u0.resize ( w.u.size() );

  axpy ( u0, w.u, -s, w.du );

  s    = minScale;

  axpy ( w.u, u0,  s, w.du );

  w.doTotalUpdate ( globdat );

  if ( w.rnorm < rmax )
  {
    goto success;
  }

  r[0] = w.rnorm;
  rmin = 10.0 * rmax;
  s0   = s;

  do
  {
    rmin0 = rmin;

    if ( r[0] < r[n] )
    {
      rmin = r[0];
      j    = 0;
      s0   = minScale;
    }
    else
    {
      rmin = r[n];
      j    = n;
      s0   = maxScale;
    }

    ds = (maxScale - minScale) / (double) n;

    for ( i = 1; i < n; i++ )
    {
      s    = minScale + (double) i * ds;

      axpy ( w.u, u0, s, w.du );

      w.doTotalUpdate ( globdat );

      r[i] = w.rnorm;

      if ( w.rnorm < rmax )
      {
        goto success;
      }

      if ( w.rnorm < rmin )
      {
        rmin = w.rnorm;
        j    = i;
        s0   = s;
      }
    }

    if      ( j == 0 )
    {
      maxScale = minScale + ds;
      r[n]     = r[1];
    }
    else if ( j == n )
    {
      minScale = maxScale - ds;
      r[0]     = r[n - 1];
    }
    else
    {
      s        = minScale + (double) j * ds;
      maxScale = s + ds;
      minScale = s - ds;
      r[0]     = r[j - 1];
      r[n]     = r[j + 1];

      // Try a quadratic approximation

      a =  0.5 * (r[n] + r[0] - 2.0 * r[j]) / (ds * ds);
      b =  0.5 * (r[n] - r[0]) / ds - 2.0 * s * a;
      s = -0.5 * b / a;

      axpy ( w.u, u0, s, w.du );

      w.doTotalUpdate ( globdat );

      if ( w.rnorm < rmax )
      {
        goto success;
      }

      if ( w.rnorm < rmin )
      {
        rmin = w.rnorm;
        s0   = s;
      }

      j = n / 2;
    }
  }
  while ( ds > 0.01 && rmin < 0.9 * rmin0 );

  axpy ( w.u, u0, s0, w.du );

  d.updateModel    ( globdat );
  w.updateMatrix   ( globdat );
  w.updateResidual ();

  return;

 success:

  print ( System::info( myName_ ), w.rundat.context,
          " : scale factor = ", d.nformat.print( s ),
          endl );

  if ( w.rnorm > precision_ )
  {
    w.updateMatrix ( globdat, false );
  }
}


JIVE_END_PACKAGE( implict )

