#ifndef DECLARE_H
#define DECLARE_H


void  declareModels                  ();
void  declareLayerModels             ();
void  declareModules                 ();

void  declareStressModel             ();
void  declareInputModule             ();
void  declareBCModel                 ();
void  declarePeriodicBCModel         ();
void  declareDispArclenModel         ();
void  declareSampleModel             ();

void  declareDenseLayer              ();
void  declareRecurrentLayer          ();
void  declareDropoutLayer            ();
void  declareMaterialLayer           ();

void  declareGroupInputModule        ();
void  declareGmshInputModule         ();
void  declareLaminateMeshModule      ();
void  declarePBCGroupInputModule     ();
void  declareLoadDispModel           ();
void  declareAdaptiveStepModule      ();
void  declareFlexArclenModule        ();
void  declareANNInputModule          ();
void  declareANNOutputModule         ();

#endif
