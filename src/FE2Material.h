/*
 *  Copyright (C) 2016 TU Delft. All rights reserved.
 *
 *  This class provides an interface between macro and microlevel
 *  of fe2 analysis. The interface is similar to that of the Material 
 *  from monoscale analysis
 *  
 *  a two-level homogenization FE model in 2D.  
 *
 *   - Small deformation theory
 *   - One RVE for all macroscopic Gauss points.
 *   - Linear and nonlinear RVE
 *   - linear boundary conditions on RVE's edges
 *   - periodic boundary conditions on RVE's edges
 *
 *  Useful references:
 *
 *  Nguyen, V. P.; Lloberas-Valls, O.; Stroeven, M.; Sluys, L. J.
 *  Computational homogenization for multiscale crack modelling.
 *  Implementational and computational aspects. IJNME, 89:192-226, 2011.
 *  
 *  Somer, D. D.; de Souza Neto, E. A.; Dettmer, W. G.; Peric, D.
 *  A sub-stepping scheme for multi-scale analysis of solids.
 *  CMAME, 198:1006-1016, 2009.
 *
 *  Changelog:
 *
 *  Author:   Frans van der Meer, f.p.vandermeer@tudelft.nl
 *  Date:     May 2015
 *  (based on code written by Erik Jan Lingen and Vinh Phu Nguyen)
 *
 *  Modified: Iuri Barcelos, i.barcelos@wmc.eu
 *  Date:     Oct 2015
 *  Transformed into a material for direct use with SolidModel in
 *  order to make it compatible with MultiphysicsModule.
 *  Added specific functions for interaction with MultiPhysicsModule
 *
 *  Modified: Iuri Barcelos, i.barcelos@wmc.eu
 *  Date:     Jul 2016
 *  Each micro model is an instance of a class. This was done in order
 *  to allow the use of pthread.
 *
 *  Modified: Iuri Barcelos, i.barcelos@wmc.eu
 *  Date:     Mar 2018
 *  This material now works with the reduced-order modeling techniques
 *  POD and ECM. Strength computation was removed. A history-like
 *  class called MicroState was created to handle substepping.
 *  The microCommit_ procedure in StressModel is now unnecessary.
 *
 *  Modified: Iuri Barcelos, i.barceloscarneiromrocha@tudelft.nl
 *  Date:     Aug 2019
 *
 *  Code cleaning. New class name
 *  Removed aging stuff
 *  Removed reliance on a scale distinction in StressModel
 *  
 */

#include <jem/base/String.h>
#include <jem/io/PrintWriter.h>
#include <jem/io/Logger.h>
#include <jem/util/Flex.h>

#include <jive/algebra/MatrixBuilder.h>
#include <jive/app/Module.h>
#include <jive/fem/ElementSet.h>
#include <jive/fem/NodeSet.h>
#include <jive/fem/NodeGroup.h>
#include <jive/geom/InternalShape.h>
#include <jive/model/Model.h>
#include <jive/solver/SkylineLU.h>
#include <jive/util/Assignable.h>
#include <jive/util/XDofSpace.h>
#include <jive/util/Constraints.h>

#include "utilities.h"
#include "Material.h"
#include "Bayesian.h"

using jem::Exception;
using jem::String;
using jem::Array;
using jem::Ref;
using jem::idx_t;
using jem::io::Logger;
using jem::io::PrintWriter;
using jem::io::Writer;
using jem::util::Flex;
using jem::util::Properties;

using jive::Vector;
using jive::Matrix;
using jive::IdxVector;
using jive::BoolVector;
using jive::app::Module;
using jive::algebra::MatrixBuilder;
using jive::fem::ElementSet;
using jive::fem::NodeSet;
using jive::geom::IShape;
using jive::model::Model;
using jive::solver::SkylineLU;
using jive::util::XDofSpace;
using jive::util::DofSpace;
using jive::util::Constraints;
using jive::util::XTable;
using jive::util::Assignable;


//=======================================================================
//   class FE2Material
//=======================================================================


class FE2Material : public Material,
		    public Bayesian

{
 public:

  typedef FE2Material  Self;

  static const char*        BOUNDARY_PROP;
  static const char*        MAX_SUBSTEPLEVEL_PROP;
  static const char*        VIEWPOINTS_PROP;
  static const char*        OPTITERHR_PROP;
  static const char*        USESAMP_PROP;
  static const char*        USEOUT_PROP;
  static const char*        DEBUG_PROP;
  static const char*        PERM_PROP;
  static const char*        ANMODEL_PROP;
  static const char*        DOF_NAMES[3];

                            FE2Material

    ( const String&           name,
      const Properties&       props,
      const Properties&       conf,
      const Properties&       globdat,
      const idx_t             rank );

  virtual void              configure

    ( const Properties&       props );

  virtual void              getConfig

    ( const Properties&       conf ) const;

  virtual void              createIntPoints

    ( const idx_t             np );

  virtual void              update

    ( Matrix&                 tangent,
      Vector&                 stress,
      const Vector&           strain,
      const idx_t             gp      );

  virtual bool              isLoading

    ( const idx_t             gp      ) const;

  virtual bool              isInelastic

    ( const idx_t             gp      ) const;

  virtual double            getDissipation

    ( const idx_t             gp      ) const;

  virtual void              stressAtPoint

    ( Vector&                 stress,
      const Vector&           strain,
      const idx_t             gp      );

  virtual void              getDissStress

    ( const Vector&           sstress,
      const Vector&           strain,
      const idx_t             gp      );

  virtual void              commit ();

  virtual void              commitOne

    ( const idx_t             ip      );

  virtual void              cancel ();

  virtual void              addTableColumns

    ( IdxVector&              jcols,
      XTable&                 table,
      const String&           name );

  virtual void           getHistory

    ( Vector&              hvals,
      const idx_t          mpoint );

  virtual Ref<Material>     clone () const;

  virtual void              setProps

    ( const Vector&           props );

  virtual Vector            getProps();

  virtual Vector            getFeatures

    ( const idx_t             ipoint );

  virtual idx_t             propCount ();

  virtual void              clearHist ();

 protected:

  virtual                  ~FE2Material  ();

  void                      completeStrain_

    ( const Vector&           mstrain,
      const Vector&           Mstrain ) const;

  void                      reduceStress_

    ( const Vector&           Mstress,
      const Vector&           mstress ) const;

  void                      reduceStiff_

    ( const Matrix&           Mstiff,
      const Matrix&           mstiff ) const;

 private:

  idx_t                   macroRank_;
  idx_t                   microRank_;
  idx_t                   mstrCount_;
  IdxVector               perm23_;
  Properties              props_;
  Properties              conf_;
  Properties              globdat_;
  idx_t                   iStep_;

  bool                    debug_;

  Ref<Writer>             macroOut_;
  Ref<Writer>             microOut_;

  Ref<Logger>             macroLog_;
  Ref<Logger>             microLog_;

  idx_t                   nupd_; // TEMP

  String                  anmodel_;

  class                   MicroModel : public Object 
  {
   public:
    
    typedef FE2Material       Super;

    enum                      BndConditionType { LINEAR, PERIODIC };


                              MicroModel 
      ( const idx_t       ip,
        const Properties& props,
	const Properties& conf                       );
              
	                     ~MicroModel            ();

    void                      update

      ( Matrix&                 mstiff,
        Vector&                 mstress,
        const Vector&           mstrain );

    void                      commit                 
    
      ( const Properties&       info    );

    void                      cancel                 ();

    bool                      isLoading              () const;

    bool                      isInelastic            () const;

    double                    getPointDissipation    ();

    void                      getPointStress

      ( Vector&                 mstress );

    void                      getPointStiff

      ( Matrix&                 mstiff  );

    void                      getCompl0

      ( Matrix&                 compl0 );

    idx_t                     getMicroRank           ();

    idx_t                     getTimeStep            ();

    Vector                    getFeatures            ();

    Ref<Material>             getMaterial            ();

    void                      setMaxSubStepLevel

      ( const idx_t             maxsubstep );

    void                      setTimeStep

      ( const idx_t             step );

    void                      setTime

      ( const double            time );

    void                      setPlaneStress

      ( const idx_t             psdof );

   private:

    class MicroState : public Object
    {
     public:
                              MicroState
	
	( const idx_t           strcount );

			      MicroState

	( const idx_t           strcount, 
	  const Ref<MicroState> state,
	  const Ref<MicroState> state0   );


      void                    setTime
        
	( const double          t        );

      void                    setStrain

        ( const Vector&         eps      );

      double                  getTime   ();

      void                    getStrain

        (       Vector&         eps      );

      bool                    hasTime   ();

     private:

      bool                    hasTime_;

      double                  time_;
      Vector                  strain_;
    };

    void                      calcInvArea_            ();

    void                      calcDMatrix_            ();

    void                      calcLinearDMatrix_      ();

    void                      calcPeriodicDMatrix_    ();

    void                      buildInnerAndOuterDofs_ ();

    void                      buildLinearBndDofs_

      ( const DofSpace&         microDofs );

    void                      buildCornerBndDofs_

      ( const DofSpace&         microDofs );

    void                      setState_
      
      ( const Ref<MicroState>   state );
   
    void                      setMicroCons_

      ( const Vector&           strain );

    void                      setMicroLinearCons_

      ( const Vector&           strain );

    void                      setMicroPeriodicCons_   ();

    void                      setMicroCornerCons_

      ( const Vector&           strain );

    void                      calcMicroStiff_

      ( const Matrix&           stiff,
	const Vector&           stress );

    void                      subStepping_

      ( const Exception&        ex,
        const Ref<MicroState>   state0,
	const Ref<MicroState>   state,
	const Properties&       params );

    void                      computeDissipation_     ();

   private:

    String                    myID_;

    Properties                props_;
    Properties                conf_;
    idx_t                     rank_;
    idx_t                     strCount_;
    IdxVector                 dofTypes_;
    Properties                data_;
    Ref<Module>               chain_;
    Ref<Model>                model_;
    Ref<Constraints>          cons_;
    Ref<Constraints>          cons2_;
    Assignable<NodeSet>       nodes_;
    IdxVector                 bndNodes_[6]; // indices of boundary nodes
    Ref<Module>               outp_;
    Ref<Module>               samp_;
    Ref<Module>               view_;
    Ref<Module>               nlin_;
    BndConditionType          bndCondition_;

    idx_t                     psDOF_;

    IdxVector                 innerDofs_;
    IdxVector                 outerDofs_; // dofs of boundary nodes
    IdxVector                 cornerNodes_;

    Matrix                    D_; // boundary matrix

    double                    v0Inv_; // inverse RVE area
    Vector                    dxRve_;

    Ref<MicroState>           state_;
    Ref<MicroState>           state0_;

    idx_t                     maxSubStepLevel_;
    idx_t                     subStepLevel_;
    bool                      firstTimeFlag_;
    Vector                    stress_;
    Matrix                    compl0_;
    Matrix                    tangent_;
    double                    dissipation_;

    idx_t                     optIterHr_;
    bool                      updHr_;

    bool                      useView_;
    bool                      useSamp_;

    idx_t                     iStep_;
    idx_t                     iIter_;

    bool                      isLoading_;
    bool                      isInelastic_;

    Ref<PrintWriter>          stiffWriter_;
  };
  
  // micro model vector

  Flex< Ref<MicroModel> >     models_;
};
