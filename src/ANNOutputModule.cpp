/*
 *  TU Delft 
 *
 *  Iuri Barcelos, July 2019
 *
 *  Output module for ANN data. Reads the TrainingData object, uses the
 *  trained network stored in the model to make predictions and prints
 *  the results to data files. It can also be used to print the network
 *  weights to a file.
 *
 *  This module should be used in the output UserconfModule. 
 *
 */

#include <jem/base/Error.h>
#include <jem/base/System.h>
#include <jem/base/limits.h>
#include <jem/base/Exception.h>
#include <jem/base/array/operators.h>
#include <jem/base/IllegalInputException.h>
#include <jem/base/array/utilities.h>
#include <jem/util/PropertyException.h>
#include <jem/util/StringUtils.h>
#include <jem/io/Writer.h>
#include <jem/io/PrintWriter.h>
#include <jem/io/FileWriter.h>
#include <jem/io/FileFlags.h>
#include <jem/numeric/algebra/utilities.h>
#include <jem/mp/MPException.h>
#include <jem/mp/MPIContext.h>
#include <jem/mp/Buffer.h>
#include <jem/mp/Status.h>
#include <jive/util/utilities.h>
#include <jive/util/Globdat.h>
#include <jive/model/Model.h>
#include <jive/model/Actions.h>
#include <jive/model/StateVector.h>
#include <jive/app/ModuleFactory.h>
#include <jive/util/DofSpace.h>
#include <jive/mp/Globdat.h>

#include "declare.h"
#include "LearningNames.h"

#include "ANNOutputModule.h"

using namespace jem;

using jem::util::PropertyException;
using jem::util::StringUtils;
using jem::IllegalInputException;
using jem::io::Writer;
using jem::io::PrintWriter;
using jem::io::FileWriter;
using jem::io::FileFlags;
using jem::mp::SendBuffer;
using jem::mp::RecvBuffer;

using jive::StringVector;
using jive::model::Model;
using jive::model::StateVector;
using jive::util::DofSpace;

//=======================================================================
//   class ANNOutputModule
//=======================================================================

//-----------------------------------------------------------------------
//   static data
//-----------------------------------------------------------------------

const char* ANNOutputModule::TYPE_NAME  = "ANNOutput";
const char* ANNOutputModule::FILENAME   = "filename";
const char* ANNOutputModule::FORMAT     = "format";
const char* ANNOutputModule::PRINT      = "print";
const char* ANNOutputModule::WRITEEVERY = "writeEvery";
const char* ANNOutputModule::RUNFIRST   = "runFirst";
const char* ANNOutputModule::VALSPLIT   = "valSplit";

//-----------------------------------------------------------------------
//   constructor
//-----------------------------------------------------------------------

ANNOutputModule::ANNOutputModule

  ( const String& name ) : Super ( name )

{}

ANNOutputModule::~ANNOutputModule ()
{}

//-----------------------------------------------------------------------
//   init
//-----------------------------------------------------------------------

Module::Status ANNOutputModule::init

  ( const Properties& conf,
    const Properties& props,
    const Properties& globdat )

{
  Properties myConf  = conf. makeProps ( myName_ );
  Properties myProps = props.findProps ( myName_ );

  Ref<TrainingData> data = TrainingData::get ( globdat, getContext() );
  Ref<DofSpace>     dofs = DofSpace::get     ( globdat, getContext() );

  bestWeights_.resize ( dofs->dofCount() );
  bestWeights_ = 0.0;

  best_         = jem::maxOf ( best_ );
  nsamples_     = data->sampleSize();
  writeEvery_   = 100;
  mimicXOut_    = false;
  printWts_     = false;
  printOuts_    = false;
  printInps_    = false;
  printXInps_   = false;

  String format ( "lines" );
  String print  ( "" ); 

  myProps.find ( format,      FORMAT     );
  myProps.find ( print,       PRINT      );
  myProps.find ( writeEvery_, WRITEEVERY );
  myProps.get  ( fname_,      FILENAME   );

  if ( !myProps.find ( nsamples_, RUNFIRST, 0, data->sampleSize() ) )
  {
    double valsplit = 0.0;

    if ( myProps.find ( valsplit, VALSPLIT, 0.0, 1.0 ) )
    {
      nsamples_ = valsplit * data->sampleSize();
    }
  }

  if ( format.equalsIgnoreCase ( "columns" ) )
  {
    format_ = COLUMNS;
  }
  else if ( format.equalsIgnoreCase ( "lines" ) )
  {
    format_ = LINES;
  }
  else
  {
    throw IllegalInputException ( getContext(),
      "Unknown format type." );
  }

  StringVector prints = StringUtils::split ( print, '|' );

  for ( idx_t i = 0; i < prints.size(); ++i )
  {
    String str = prints[i].stripWhite();

    if ( str.equalsIgnoreCase ( "inputs" ) )
    {
      printInps_ = true;
    }
    if ( str.equalsIgnoreCase ( "xinputs" ) )
    {
      printXInps_= true;
    }
    if ( str.equalsIgnoreCase ( "outputs" ) )
    {
      printOuts_ = true;
    }
    if ( str.equalsIgnoreCase ( "xoutputs" ) )
    {
      printOuts_ = true;
      mimicXOut_ = true;
    }
    if  ( str.equalsIgnoreCase ( "weights" ) )
    {
      printWts_ = true;
    }
  }

  if ( format_ == COLUMNS &&
       printInps_ && printOuts_ &&
       data->inputSize() != data->outputSize() )
  {
    throw IllegalInputException ( getContext(),
      "Column-wise printing of both inputs and outputs is only possible if inputSize == outputSize" );
  }

  mpx_ = jive::mp::Globdat::getMPContext ( globdat );

  if ( mpx_ == nullptr )
  {
    throw Error ( JEM_FUNC, String::format (
       "MPContext has not been found" ) );
  }

  idx_t rank = mpx_->myRank();
  idx_t size = mpx_->size();
  bool  mp   = ( size > 1 );

  if ( mp )
  {
    System::out() << "MP is true\n";
    bool  last = ( rank == size - 1 );

    idx_t load = max ( 1, nsamples_ / size );
    idx_t end  = max ( nsamples_ - rank*load, 0 );
    idx_t beg  = last ? 0 : max ( end - load, 0 );

    sampIds_.ref ( IdxVector ( iarray ( slice ( beg, end ) ) ) );
  }
  else
  {
    sampIds_.ref ( IdxVector ( iarray ( nsamples_ ) ) );
  }

  return OK;
}

//-----------------------------------------------------------------------
//   run
//-----------------------------------------------------------------------

Module::Status ANNOutputModule::run

  ( const Properties& globdat )

{
  using jive::util::Globdat;

  idx_t epoch;
  globdat.get ( epoch, Globdat::TIME_STEP );

  idx_t rank = mpx_->myRank();
  idx_t size = mpx_->size();
  bool  root = ( rank == 0 );
  bool  mp   = ( size > 1 );

  if ( !(epoch % writeEvery_) )
  {
    Ref<TrainingData> data = TrainingData::get ( globdat, getContext() );

    double lerr = 0.0, err = 0.0;

    Batch samples = data->getData ( sampIds_ );

    if ( nsamples_ > rank )
    {
      lerr = runSamples_ ( samples, globdat );
    }

    if ( mp )
    {
      mpx_->reduce ( RecvBuffer ( &err, 1 ),
		     SendBuffer ( &lerr, 1 ),
		     0,
		     jem::mp::SUM );
    }
    else
    {
      err = lerr;
    }

    if ( root )
    {
      mpx_->broadcast ( SendBuffer ( &err, 1 ) );
    }
    else
    {
      mpx_->broadcast ( RecvBuffer ( &err, 1 ), 0 );
    }

    if ( err < best_ )
    {
      best_      = err;
      bestEpoch_ = epoch;

      Ref<DofSpace> dofs = DofSpace::get     ( globdat, getContext() );

      Vector state;
      StateVector::get ( state, dofs, globdat );

      bestWeights_ = state;
      bestSamples_.ref ( samples );

      write_ ( globdat );
    }

    if ( root )
    {
      print ( System::info( myName_ ), getContext(), 
	": Epoch ", epoch, ", computing error for the first ", nsamples_, " samples\n" );
      print ( System::info( myName_ ), getContext(), 
	": Epoch ", epoch, ", mean error = ", err, "\n" );
      print ( System::info( myName_ ), getContext(), 
	": Lowest error up until now = ", best_, " (from epoch ", bestEpoch_, ")\n\n" );
    }

  }

  return OK;
}

//-----------------------------------------------------------------------
//   shutdown
//-----------------------------------------------------------------------

void ANNOutputModule::shutdown

  ( const Properties& globdat )

{
  write_ ( globdat );
}

//-----------------------------------------------------------------------
//   write_
//-----------------------------------------------------------------------

void ANNOutputModule::write_

  ( const Properties& globdat )

{
  if ( mpx_->myRank() == 0 )
  {
    print ( System::info( myName_ ), getContext(), 
      ", writing results...\n" );

    if ( printWts_ )
    {
      writeWeights_ ( globdat );
    }
  }

  if ( printInps_ || printOuts_ )
  {
    if ( format_ == LINES )
    {
      writeLines_   ( globdat ); 
    }
    else if ( format_ == COLUMNS )
    {
      writeCols_    ( globdat );
    }
  }
}

//-----------------------------------------------------------------------
//   runSamples_
//-----------------------------------------------------------------------

double ANNOutputModule::runSamples_

  ( const Batch&      samples,
    const Properties& globdat )

{
  Properties params;
  double error = 0.0, totalerror = 0.0;

  Ref<Model>        model = Model::get        ( globdat, getContext() );
  Ref<TrainingData> data  = TrainingData::get ( globdat, getContext() );
  Ref<Normalizer>   inl   = data->getInpNormalizer();
  Ref<Normalizer>   onl   = data->getOutNormalizer();

  idx_t ns   = samples[0]->batchSize();
  idx_t seq  = data->sequenceSize();

  for ( idx_t t = 0; t < seq; ++t )
  {
    params.erase ( LearningParams::STATE );

    if ( t > 0 )
    {
      params.set      ( LearningParams ::STATE,            samples[t-1]   );
    }

    params.set        ( LearningParams ::DATA,             samples[t]     );

    model->takeAction ( LearningActions::RECALL, params, globdat );

    //// DEBUG
    //if ( false )
    //{
    //  model->takeAction ( NeuralActions::GETJACOBIAN, params, globdat );
    //  for ( idx_t s = 0; s < ns; ++s )
    //  {
    //    idx_t os = data->outputSize();

    //    Matrix jac ( samples[t]->jacobian ( slice(s*os,(s+1)*os), ALL ) );

    //    //for ( idx_t row = 0; row < os; ++row )
    //    //{
    //    //  jac(row,ALL) *= inl->getJacobianFactor ( Vector() );
    //    //}

    //    System::out() << jac << "\n";
    //  }
    //}
  }

  for ( idx_t s = 0; s < ns; ++s )
  {
    error = 0.0;

    for ( idx_t t = 0; t < seq; ++t )
    {
      Vector out = onl->denormalize ( samples[t]->outputs(ALL,s) );
      Vector tar = onl->denormalize ( samples[t]->targets(ALL,s) );


      error += jem::numeric::norm2 ( tar - out );
    }
    totalerror += error / (double)seq;
  }

  return totalerror / (double)nsamples_;
}

//-----------------------------------------------------------------------
//   writeWeights_
//-----------------------------------------------------------------------

void ANNOutputModule::writeWeights_

  ( const Properties& globdat )

{
  String name = fname_ + ".net";

  Ref<TrainingData> data  = TrainingData::get ( globdat, getContext() );
  Ref<Normalizer>   inl   = data->getInpNormalizer();
  Ref<Normalizer>   onl   = data->getOutNormalizer();

  Ref<PrintWriter> fout = newInstance<PrintWriter> (
    newInstance<FileWriter> ( name, FileFlags::WRITE ) );

  fout->nformat.setFractionDigits ( 5 );

  for ( idx_t i = 0; i < bestWeights_.size(); ++i )
  {
    *fout << bestWeights_[i] << "\n";
  }

  inl->write ( "input"  );
  onl->write ( "output" );
}

//-----------------------------------------------------------------------
//   writeLines_
//-----------------------------------------------------------------------

void ANNOutputModule::writeLines_

  ( const Properties& globdat  )

{
  idx_t rank =   mpx_->myRank();
  bool  mp   = ( mpx_->size() > 1 );

  String name = mp ? fname_ + String ( rank ) + ".out" : fname_ + ".out";

  Ref<TrainingData> data  = TrainingData::get ( globdat, getContext() );
  Ref<Normalizer>   inl   = data->getInpNormalizer();
  Ref<Normalizer>   onl   = data->getOutNormalizer();
  Ref<Normalizer>   xnl   = data->getXInNormalizer();

  idx_t isz  = data->inputSize   ();
  idx_t xsz  = data->xinputSize  ();
  idx_t osz  = data->outputSize  ();
  idx_t seq  = data->sequenceSize();
  idx_t ns   = sampIds_.size     ();

  Ref<PrintWriter> fout = newInstance<PrintWriter> (
    newInstance<FileWriter> ( name, FileFlags::WRITE ) );

  fout->nformat.setFractionDigits ( 5 );

  for ( idx_t s = 0; s < ns; ++s )
  {
    for ( idx_t t = 0; t < seq; ++t )
    {
      Vector inp = inl->denormalize ( bestSamples_[t]->inputs (ALL,s) );
      Vector xin = xnl->denormalize ( bestSamples_[t]->xinputs(ALL,s) );
      Vector out = onl->denormalize ( bestSamples_[t]->outputs(ALL,s) );


      if ( printInps_ )
      {
        for ( idx_t i = 0_idx; i < isz; ++i )
	{
          *fout << inp[i] << " ";
	}
      }

      if ( printXInps_ )
      {
        for ( idx_t x = 0_idx; x < xsz; ++x )
	{
          *fout << xin[x] << " ";
	}
      }

      if ( printOuts_ )
      {
	for ( idx_t o = 0_idx; o < osz; ++o )
	{
	  *fout << out[o] << " ";
	}
      }

      *fout << "\n";
    }

    *fout << "\n";
  }
}

//-----------------------------------------------------------------------
//   writeCols_
//-----------------------------------------------------------------------

void ANNOutputModule::writeCols_

  ( const Properties& globdat  )

{
  String name;

  Ref<TrainingData> data  = TrainingData::get ( globdat, getContext() );
  Ref<Normalizer>   inl   = data->getInpNormalizer();
  Ref<Normalizer>   onl   = data->getOutNormalizer();

  idx_t osz  = data->outputSize();
  idx_t seq  = data->sequenceSize();
  idx_t ns   = sampIds_.size();

  for ( idx_t s = 0; s < ns; ++s )
  {
    name = fname_ + String ( sampIds_[s] ) + ".out";

    Ref<PrintWriter> fout = newInstance<PrintWriter> (
      newInstance<FileWriter> ( name, FileFlags::WRITE ) );

    fout->nformat.setFractionDigits ( 5 );

    for ( idx_t t = 0; t < seq; ++t )
    {
      Vector inp = inl->denormalize ( bestSamples_[t]->inputs (ALL,s) );
      Vector out = onl->denormalize ( bestSamples_[t]->outputs(ALL,s) );

      if ( mimicXOut_ )
      {
        *fout << "newXOutput" << String ( t ) << "\n";
      }

      for ( idx_t i = 0; i < osz; ++i )
      {
        if ( printInps_ )
	{
          *fout << inp[i] << " ";
	}

        if ( printOuts_ )
	{
	  *fout << out[i] << "\n";
	}
      }

      *fout << "\n";
    }
  }
}

//-----------------------------------------------------------------------
//   makeNew
//-----------------------------------------------------------------------

Ref<Module> ANNOutputModule::makeNew

  ( const String&      name,
    const Properties&  conf,
    const Properties&  props,
    const Properties&  globdat )

{
  return newInstance<Self> ( name );
}

//-----------------------------------------------------------------------
//   declareANNOutputModule
//-----------------------------------------------------------------------

void declareANNOutputModule ()
{
  using jive::app::ModuleFactory;

  ModuleFactory::declare ( ANNOutputModule::TYPE_NAME, & ANNOutputModule::makeNew );
}

