/*
 * 
 *  Copyright (C) 2009 TU Delft. All rights reserved.
 *  
 *  This pure virtual class implements functions for a material 
 *  model with stochastic properties
 *  
 *  Author: Iuri Rocha, i.barceloscarneiromrocha@tudelft.nl
 *  Date: June 2020
 *
 */

#ifndef BAYESIAN_H 
#define BAYESIAN_H

class Bayesian
{
 public:
  
  virtual void            setProps
    
    ( const Vector&         props    ) = 0;

  virtual Vector          getProps  () = 0;

  virtual Vector          getFeatures

    ( const idx_t           ipoint   ) = 0;

  virtual void            clearHist () = 0;

  virtual idx_t           propCount () = 0;
};

#endif
