/*
 *  TU Delft 
 *
 *  Iuri Barcelos, August 2018
 *
 *  BC model adapted from VEVPLoadModel. 
 *
 */

#ifndef BC_MODEL_H 
#define BC_MODEL_H

#include <jem/numeric/func/Function.h>
#include <jive/Array.h>
#include <jive/model/Model.h>
#include <jive/fem/NodeGroup.h>
#include <jive/model/Actions.h>
#include <jive/model/ModelFactory.h>
#include <jive/util/Assignable.h>
#include <jive/util/XDofSpace.h>
#include <jive/model/PointLoadModel.h>

using namespace jem;

using jem::numeric::Function;
using jem::util::Properties;
using jive::Vector;
using jive::Matrix;
using jive::IntVector;
using jive::IdxVector;
using jive::StringVector;
using jive::model::Model;
using jive::model::PointLoadModel;
using jive::fem::NodeGroup;
using jive::fem::NodeSet;
using jive::util::Assignable;
using jive::util::XDofSpace;
using jive::util::DofSpace;
using jive::util::Constraints;

class BCModel : public Model
{
 public:

  static const char* MODE_PROP;
  static const char* NODEGROUPS_PROP;
  static const char* DOFS_PROP;
  static const char* UNITVEC_PROP;
  static const char* SHAPE_PROP;
  static const char* STEP_PROP;
  static const char* ALENGROUP_PROP;
  static const char* LOAD_PROP;
  static const char* VALFILE_PROP;
  static const char* VALIDXS_PROP;
  static const char* MAXNORM_PROP;

  enum               Mode      { LOAD, DISP, ALEN };

                     BCModel

    ( const String&     name,
      const Properties& conf,
      const Properties& props,
      const Properties& globdat );

  virtual void       configure

    ( const Properties& props,
      const Properties& globdat );

  virtual void       getConfig

    ( const Properties& conf,
      const Properties& globdat )          const;

  virtual bool       takeAction

    ( const String&     action,
      const Properties& params,
      const Properties& globdat );

  static Ref<Model>  makeNew

    ( const String&     name,
      const Properties& conf,
      const Properties& props,
      const Properties& globdat );

 protected:
  
  virtual           ~BCModel ();

 private:

  void               applyLoads_

    ( const Vector&     fext,
      const Properties& globdat );

  void               applyDisps_

    ( const Properties& params,
      const Properties& globdat );

  void               connect_ ();

  void               dofsChanged_ ();

  void               initUnitLoad_ 
  
    ( const Properties& globdat );

  void               getUnitLoad_

    ( const Properties& params,
      const Properties& globdat );

  void               getArcFunc_

    ( const Properties& params,
      const Properties& globdat );

 private:

  Ref<Constraints>    cons_;
  Ref<DofSpace>       dofs_;
  Assignable<NodeSet> nodes_;

  Ref<Function>       shape_;

  idx_t               numgroups_;
  IntVector           idofs_;

  Vector              unitVec_;
  Vector              initVals_;

  StringVector        nodeGroups_;
  StringVector        dofTypes_;

  Mode                mode_;

  // Time control

  double              time_;
  double              time0_;
  double              dt_;

  // Arclen control

  bool                ulUpd_;
  Vector              unitLoad_;
  idx_t               master_;
  idx_t               alenGroup_;

  double              maxNorm_;

  IdxVector           masters_;
  Vector              signs_;

  Ref<PointLoadModel> pLoad_;

  // Table control

  bool                fileVals_;
  idx_t               step_;
  idx_t               step0_;

  Matrix              vals_;

};

#endif
