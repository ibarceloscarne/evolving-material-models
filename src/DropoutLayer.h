/*
 * Copyright (C) 2019 TU Delft. All rights reserved.
 *
 * Class that implements a dropout layer for regularization.
 * Values come from previous layer, activations are either
 * zero or a scaled-up version of the values. The choice of
 * which neurons are zeroed is done based on a dropout rate
 * and a uniform random distribution.
 *
 * Author: Iuri Barcelos, i.barceloscarneiromrocha@tudelft.nl
 * Date:   Aug 2019 
 * 
 */

#ifndef DENSELAYER_H
#define DENSELAYER_H

#include <jive/model/Model.h>
#include <jem/base/System.h>
#include <jem/util/Properties.h>
#include <jem/util/Timer.h>
#include <jive/util/Assignable.h>
#include <jive/util/XDofSpace.h>

#include "NeuralUtils.h"
#include "XAxonSet.h"
#include "XNeuronSet.h"

#include "NData.h"

using jem::String;
using jem::idx_t;
using jem::util::Properties;
using jem::util::Timer;

using jive::model::Model;
using jive::util::Assignable;
using jive::util::DofSpace;

using NeuralUtils::ActivationFunc;
using NeuralUtils::ActivationGrad;
using NeuralUtils::InitFunc;

typedef Array<idx_t,2> IdxMatrix;

class DropoutLayer : public Model
{
 public:
  
  static const char*   RATE;

                       DropoutLayer

    ( const String&      name,
      const Properties&  conf,
      const Properties&  props,
      const Properties&  globdat  );

  virtual void         configure

    ( const Properties&  props,
      const Properties&  globdat  );

  virtual void         getConfig

    ( const Properties&  conf,
      const Properties&  globdat  );

  virtual bool         takeAction

    ( const String&      action,
      const Properties&  params,
      const Properties&  globdat  );

 protected:

  virtual             ~DropoutLayer  ();

  virtual void         propagate_

    ( const double       rate,
      const Ref<NData>   data,
      const Properties&  globdat  );

  virtual void         backPropagate_

    ( const Ref<NData>   data,
            Vector&      grads,
      const Properties&  globdat  );

 private:
  
  IdxVector            iNeurons_;
  IdxVector            inpNeurons_;

  double               rate_;
};

#endif
