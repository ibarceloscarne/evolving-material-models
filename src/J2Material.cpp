
/*
 *  Copyright (C) 2018 TU Delft. All rights reserved.
 *  
 *  This class implements the material model based on
 *  classic Von Mises J2-theory.
 *  This class is a child material member of DamTLSMaterial
 *  classes. 
 *
 *  NB.: This class only works on Plane Stress assumptions.
 *       However, the architecture for Plane Strain and 3D
 *       is already defined. According to [1], it is possible
 *       to write the governing equations only related to 
 *       plastic multiplier.
 *  
 *  Author: Luiz A. T. Mororo, l.a.taumaturgomororo@tudelft.nl
 *  Date:   Nov 2018
 *
 *  [1] E.A. de Soiza Neto, D. Peric, D.R.J. Owen. Computational
 *      Methods for Plasticity: Theory and Applications. Jhon Wiley
 *      and Sons, 2008.
 *
 *  Modified: Iuri Rocha, i.rocha@tudelft.nl
 *  Date: Feb 2022
 *
 *  Adapted to ML use. Switched to linear hardening
 *
 */


#include <jem/base/array/tensor.h>
#include <jem/base/System.h>
#include <jem/base/limits.h>
#include <jem/base/Float.h>
#include <jem/base/Error.h>
#include <jem/base/PrecheckException.h>
#include <jem/base/array/tensor.h>
#include <jem/base/array/operators.h>
#include <jem/io/PrintWriter.h>
#include <jem/numeric/func/UserFunc.h>
#include <jem/numeric/algebra/matmul.h>
#include <jem/numeric/utilities.h>
#include <jem/numeric/algebra/LUSolver.h>
#include <jem/util/Properties.h>
#include <jive/util/FuncUtils.h>


#include "utilities.h"
#include "voigtUtilities.h"
#include "J2Material.h"


using namespace jem::literals;

using jem::Error;
using jem::newInstance;
using jem::numeric::matmul;
using jem::numeric::UserFunc;
using jem::numeric::LUSolver;
using jem::io::PrintWriter;
using jem::io::endl;


using jive::util::FuncUtils;


//======================================================================
//   class J2Material
//======================================================================


//-----------------------------------------------------------------------
//   static data
//-----------------------------------------------------------------------


const char*  J2Material::YOUNG_PROP       = "young";
const char*  J2Material::POISSON_PROP     = "poisson";
const char*  J2Material::YIELD_PROP       = "yield";
const char*  J2Material::HARD_PROP        = "hard";
const char*  J2Material::ANMODEL_PROP     = "anmodel";
const char*  J2Material::RM_TOLERANCE     = "rmTolerance";
const char*  J2Material::RM_MAX_ITER      = "rmMaxIter";


//-----------------------------------------------------------------------
//   constructors & destructor
//-----------------------------------------------------------------------


J2Material::J2Material 

  (  const idx_t       rank, 
     const Properties& globdat )

    : Material ( rank, globdat )

{
  rank_ = rank;
  globdat_ = globdat;

  e_  = 0.0;
  n_  = 0.0;
  y_  = 0.0;
  h_  = 0.0;

  rmTolerance_ = 1.e-10;
  rmMaxIter_   = 10;

  v61_.resize ( 6 );
  v62_.resize ( 6 );
}


J2Material::~J2Material 

  ()

{}


//-----------------------------------------------------------------------
//   configure
//-----------------------------------------------------------------------


void J2Material::configure 
  
  ( const Properties& props,
    const Properties& globdat )

{

  props_ = props;

  props.get  ( e_, YOUNG_PROP   );
  props.get  ( n_, POISSON_PROP );
  props.get  ( y_, YIELD_PROP   );
  props.get  ( h_, HARD_PROP    );

  props.get  ( anmodel_,     ANMODEL_PROP     );

  props.find ( rmTolerance_, RM_TOLERANCE     );
  props.find ( rmMaxIter_  , RM_MAX_ITER      );
  
  computeElasticStiff_();

  if ( rank_ == 2 )
  {
    // Compute the 3x3 matrix for 2D problems.

    comp2DStiffMat_ ();
  }

  if ( rank_ == 2 )
  {
    if      ( anmodel_ == "PLANE_STRAIN" )
    { 
      f_ = newInstance<YieldFunc3D_> ( e_, n_, y_, h_ );

      f_-> setRmSettings             ( rmTolerance_, rmMaxIter_ );
      f_-> setElasticStiff           ( stiff2D_ );
    }
    else if ( anmodel_ == "PLANE_STRESS" )
    {
      f_ = newInstance<YieldFuncPS_> ( e_, n_, y_, h_ );

      f_-> setRmSettings             ( rmTolerance_, rmMaxIter_ );
      f_-> setElasticStiff           ( stiff2D_ );
    }
    else
    {
      throw Error 
        ( JEM_FUNC, 
	  "It only works on P.Stress and P.Strain!" );
    }
  }
  else if ( rank_ == 3 )
  {
    f_ = newInstance<YieldFunc3D_> ( e_, n_, y_, h_ );

    f_-> setRmSettings             ( rmTolerance_, rmMaxIter_ );
    f_-> setElasticStiff           ( stiff_ );
  }
  else
  {
    throw Error 
      ( JEM_FUNC, 
      "This model only works on P.Stress, P.Strain and 3D!" );
  }

  // data-driven properties

  if ( props.find ( propNames_, "propNames" ) )
  {
    props.get ( propUpper_, "propUpper" );
    props.get ( propLower_, "propLower" );

    if ( propUpper_.size() != propNames_.size() ||
    	 propLower_.size() != propNames_.size()   )
    {
      throw Error ( JEM_FUNC, "setProps: Bounds inconsistent with property set" );
    }

    for ( idx_t p = 0_idx; p < propNames_.size(); ++p )
    {
      if ( !props_.contains ( propNames_[p] ) )
      {
        throw Error ( JEM_FUNC, "setProps: Unknown property" );
      }
    }

    latestProps_.resize ( propNames_.size() );
    latestProps_ = 0.0;
  }
}


//-----------------------------------------------------------------------
//   getConfig
//-----------------------------------------------------------------------


void J2Material::getConfig 
  
  ( const Properties& conf,
    const Properties& globdat ) const

{
  conf.set   ( YOUNG_PROP,   e_  );
  conf.set   ( POISSON_PROP, n_  );
  conf.set   ( YIELD_PROP,   y_  );
  conf.set   ( HARD_PROP,    h_  );

  conf.set   ( ANMODEL_PROP, anmodel_ );

  conf.set ( RM_TOLERANCE    , rmTolerance_ );
  conf.set ( RM_MAX_ITER     , rmMaxIter_   );
}


//-----------------------------------------------------------------------
//   update
//-----------------------------------------------------------------------


void J2Material::update

  (        Matrix& stiff,
           Vector& stress,
    const Vector&  strain,
    const idx_t    ipoint )

{
  // Get equivalent plastic strain (previous).

  double epspeq0 = preHist_[ipoint].epspeq;

  // Compute the trial elastic stress.

  //Vec6 eps   ( fill3DStrain_ ( strain ) ); 

  //Vec6 sigtr = tmatmul_ ( stiff_, (eps - preHist_[ipoint].epsp) );

  Vec6 epse6;
  epse6 = 0.0;
  
  if ( anmodel_ == "PLANE_STRESS" )
  {
    epse6[0] = strain[0] - preHist_[ipoint].epsp[0];
    epse6[1] = strain[1] - preHist_[ipoint].epsp[1];
    epse6[2] = -n_ / (1.-n_) * (epse6[0] + epse6[1]);
    epse6[3] = strain[2] - preHist_[ipoint].epsp[3];
  }
  else
  {
    epse6 = fill3DStrain_ ( strain ) - preHist_[ipoint].epsp;
  }

  Vec6 sigtr = tmatmul_ ( stiff_, epse6 );
  
  // Elastic predictor/return mapping scheme.

  Vec6   sig;
  double dgam;
  bool   loading = false;

  //System::out() << "eps " << eps << " epsp " << preHist_[ipoint].epsp << " trial stress " << sigtr << '\n';
  if ( f_->isLinear ( sigtr, epspeq0 ) )
  {
    //System::out() << "Elastic update with epspeq0 " << epspeq0 << '\n';
    // Linear domain.

    sig = sigtr;

    stiff = f_->getElastStiff ();

    // Update history.

    newHist_[ipoint].epsp        = preHist_[ipoint].epsp;
    newHist_[ipoint].epspeq      = preHist_[ipoint].epspeq;
    newHist_[ipoint].dissipation = preHist_[ipoint].dissipation;
  }
  else
  {
    // Plastic flow takes place.

    //System::out() << "Plastic update with epspeq0 " << epspeq0 << '\n';
    //System::out() << "Initial epspeq " << epspeq0 << '\n';
    try
    {
      // Classical return mapping scheme starting with dgam = 0.

      dgam = f_->findRoot ( 0. );
    }
    catch ( const Exception& ex ) 
    {
      handleException_ ( ex, strain, ipoint );

      // More robust return mapping scheme.

      double gmin, gmax, fmin, fmax;

      f_->findBounds      ( gmin, gmax, fmin, fmax );

      dgam = f_->findRoot ( gmin, gmax, fmin, fmax );
    }

    // Compute stress.

    f_->compSigUpdated ( sig, dgam, sigtr ); 

    // Compute plastic strain increment.

    Vec6 depsp;

    f_->compPlastStrInc ( depsp, dgam );

    // Compute elastoplastic tangent stiffness.

    stiff = f_->compTangStiff ( dgam );

    // Update history.

    double dG = dot ( sig, depsp );

    newHist_[ipoint].epsp        = preHist_[ipoint].epsp + depsp;
    newHist_[ipoint].epspeq      = f_->getEpspeq ();
    newHist_[ipoint].dissipation = preHist_[ipoint].dissipation + dG;

    loading = true; 

    //Vec6 epse;
    //epse = 0.;
    //f_->compElastStr ( epse );

    //Ref<PrintWriter> out = newInstance<PrintWriter> ( &System::out() );

    //out->nformat.setFractionDigits ( 10 );

    //*out << "Elastic strain 1" << epse6 - depsp << '\n';
    //*out << "Elastic strain 2" << epse << '\n';

    //*out << "isLinear, updated sig, updated epsp " << f_->isLinear ( sig, f_->getEpspeq() ) << " with sigma " << sig << '\n';

    //sigtr = tmatmul_ ( stiff_, epse );

    //*out << "isLinear, updated elastic strain " << f_->isLinear ( sigtr, f_->getEpspeq() ) << " with sigtr " << sigtr << '\n';

    //sigtr = tmatmul_ ( stiff_, epse6-depsp );

    //*out << "isLinear, updated elastic strain " << f_->isLinear ( sigtr, f_->getEpspeq() ) << " with sigtr " << sigtr << '\n';

    //*out << "Old depsp " << depsp << " new depsp " << epse6 - epse << '\n';

    //System::out() << "Updated sig " << sig << '\n';
  }

  newHist_[ipoint].loading = loading;

  reduce3DVector_ ( stress, sig );

  latestHist_ = &( newHist_ );
}

//-----------------------------------------------------------------------
//   stressAtPoint
//-----------------------------------------------------------------------

void J2Material::stressAtPoint

  ( Vector&           stress,
    const Vector&     strain,
    const idx_t       ipoint )

{
  Vec6 eps ( fill3DStrain_ ( strain ) );
  Vec6 epsp = (*latestHist_)[ipoint].epsp;
  Vec6 epse = eps - epsp;

  Vec6 sig = tmatmul_ ( stiff_, epse );

  reduce3DVector_ ( stress, sig );
}

//-----------------------------------------------------------------------
//  addTableColumns 
//-----------------------------------------------------------------------

void J2Material::addTableColumns

  ( IdxVector&     jcols,
    XTable&        table,
    const String&  name )

{
  // Check if the requested table is supported by this material.

  if ( name == "nodalStress" || name == "ipStress" )
  {
    if ( anmodel_ == "BAR" )
    {
      jcols.resize ( 1 );

      jcols[0_idx] = table.addColumn ( "s_xx" );
    }

    else if ( anmodel_ == "PLANE_STRESS" )
    {
      jcols.resize ( 3 );

      jcols[0_idx] = table.addColumn ( "s_xx" );
      jcols[1_idx] = table.addColumn ( "s_yy" );
      jcols[2_idx] = table.addColumn ( "s_xy" );
    }

    else if ( anmodel_ == "PLANE_STRAIN" )
    {
      jcols.resize ( 4 );

      jcols[0_idx] = table.addColumn ( "s_xx" );
      jcols[1_idx] = table.addColumn ( "s_yy" );
      jcols[2_idx] = table.addColumn ( "s_xy" );
      jcols[3_idx] = table.addColumn ( "s_zz" );
    }

    else if ( anmodel_ == "SOLID" )
    {
      jcols.resize ( 6 );

      jcols[0_idx] = table.addColumn ( "s_xx" );
      jcols[1_idx] = table.addColumn ( "s_yy" );
      jcols[2_idx] = table.addColumn ( "s_zz" );
      jcols[3_idx] = table.addColumn ( "s_xy" );
      jcols[4_idx] = table.addColumn ( "s_xz" );
      jcols[5_idx] = table.addColumn ( "s_yz" );
    }

    else
      throw Error ( JEM_FUNC, "Unexpected analysis model: " + anmodel_ );
  }
  else if ( name == "history" || name == "nodalHistory" )
  {
    jcols.resize ( 7 );

    jcols[0_idx] = table.addColumn ( "epsp_xx" );
    jcols[1_idx] = table.addColumn ( "epsp_yy" );
    jcols[2_idx] = table.addColumn ( "epsp_zz" );
    jcols[3_idx] = table.addColumn ( "epsp_xy" );
    jcols[4_idx] = table.addColumn ( "epsp_xz" );
    jcols[5_idx] = table.addColumn ( "epsp_yz" );
    jcols[6_idx] = table.addColumn ( "epspeq" );
  }
}


//-----------------------------------------------------------------------
//   commit
//-----------------------------------------------------------------------


void J2Material::commit 

  () 

{
  ( newHist_ ).swap ( preHist_ );

  latestHist_ =    &( preHist_ );

  //if ( h_ > 1.0 )
  //  System::out() << "J2Material committing with preHist " << preHist_[0_idx].epsp << '\n';
}

//-----------------------------------------------------------------------
//  commitOne
//-----------------------------------------------------------------------

void J2Material::commitOne

  ( const idx_t ipoint )

{
  preHist_[ipoint].epsp[0_idx]     = newHist_[ipoint].epsp[0_idx];
  preHist_[ipoint].epsp[1_idx]     = newHist_[ipoint].epsp[1_idx];
  preHist_[ipoint].epsp[2_idx]     = newHist_[ipoint].epsp[2_idx];
  preHist_[ipoint].epsp[3_idx]     = newHist_[ipoint].epsp[3_idx];
  preHist_[ipoint].epsp[4_idx]     = newHist_[ipoint].epsp[4_idx];
  preHist_[ipoint].epsp[5_idx]     = newHist_[ipoint].epsp[5_idx];
  preHist_[ipoint].epspeq      = newHist_[ipoint].epspeq;
  preHist_[ipoint].dissipation = newHist_[ipoint].dissipation;
  preHist_[ipoint].loading     = newHist_[ipoint].loading;
}

//-----------------------------------------------------------------------
//   clone
//-----------------------------------------------------------------------

Ref<Material> J2Material::clone () const

{
  // use default copy constructor

  return newInstance<J2Material> ( *this );
}


//-----------------------------------------------------------------------
//   createIntPoints
//-----------------------------------------------------------------------


void J2Material::createIntPoints

  ( const idx_t      ipcount )

{
  preHist_.reserve ( ipcount );
  newHist_.reserve ( ipcount );

  for ( idx_t i = 0; i < ipcount; i++ )
  {
    preHist_.pushBack ( Hist_ ( ) );
    newHist_.pushBack ( Hist_ ( ) );
  }

  latestHist_ = &preHist_;
}

//-----------------------------------------------------------------------
//   setProps
//-----------------------------------------------------------------------

void J2Material::setProps

  ( const Vector& props )

{
  if ( !propNames_.size() )
  {
    return;
  }

  Properties newProps ( props_.clone() );

  if ( props.size() != propNames_.size() )
  {
    throw Error ( JEM_FUNC, "J2Material::setProps: Inconsistent number of properties" );
  }

  for ( idx_t p = 0_idx; p < propNames_.size(); ++p )
  {
    double prop = propLower_[p] + ( propUpper_[p] - propLower_[p] ) * props[p];

    newProps.set ( propNames_[p], prop );
  }

  newProps.get  ( e_, YOUNG_PROP   );
  newProps.get  ( n_, POISSON_PROP );
  newProps.get  ( y_, YIELD_PROP   );
  newProps.get  ( h_, HARD_PROP    );

  computeElasticStiff_();

  if ( rank_ == 2 )
  {
    // Compute the 3x3 matrix for 2D problems.

    comp2DStiffMat_ ();
  }

  if ( rank_ == 2 )
  {
    if      ( anmodel_ == "PLANE_STRAIN" )
    { 
      f_ = newInstance<YieldFunc3D_> ( e_, n_, y_, h_ );

      f_-> setRmSettings             ( rmTolerance_, rmMaxIter_ );
      f_-> setElasticStiff           ( stiff2D_ );
    }
    else if ( anmodel_ == "PLANE_STRESS" )
    {
      f_ = newInstance<YieldFuncPS_> ( e_, n_, y_, h_ );

      f_-> setRmSettings             ( rmTolerance_, rmMaxIter_ );
      f_-> setElasticStiff           ( stiff2D_ );
    }
    else
    {
      throw Error 
        ( JEM_FUNC, 
	  "It only works on P.Stress and P.Strain!" );
    }
  }
  else if ( rank_ == 3 )
  {
    f_ = newInstance<YieldFunc3D_> ( e_, n_, y_, h_ );

    f_-> setRmSettings             ( rmTolerance_, rmMaxIter_ );
    f_-> setElasticStiff           ( stiff_ );
  }
  else
  {
    throw Error 
      ( JEM_FUNC, 
      "This model only works on P.Stress, P.Strain and 3D!" );
  }

  latestProps_ = props;
}

//-----------------------------------------------------------------------
//   getProps
//-----------------------------------------------------------------------

Vector J2Material::getProps ()

{
  Vector props ( propNames_.size() );
  props = 0.0;

  for ( idx_t p = 0; p < propNames_.size(); ++p )
  {
    props[p] = propLower_[p] + ( propUpper_[p] - propLower_[p] ) * latestProps_[p];
  }

  return props;
}

//-----------------------------------------------------------------------
//   getFeatures
//-----------------------------------------------------------------------

Vector J2Material::getFeatures

  ( const idx_t ipoint )

{
  Vector ret ( 7 );
  
  ret[0_idx] = (*latestHist_)[ipoint].epsp[0_idx];
  ret[1_idx] = (*latestHist_)[ipoint].epsp[1_idx];
  ret[2_idx] = (*latestHist_)[ipoint].epsp[2_idx];
  ret[3_idx] = (*latestHist_)[ipoint].epsp[3_idx];
  ret[4_idx] = (*latestHist_)[ipoint].epsp[4_idx];
  ret[5_idx] = (*latestHist_)[ipoint].epsp[5_idx];
  ret[6_idx] = (*latestHist_)[ipoint].epspeq;

  return ret;
}

//-----------------------------------------------------------------------
//   propCount
//-----------------------------------------------------------------------

idx_t J2Material::propCount ()

{
  return propNames_.size();
}

//-----------------------------------------------------------------------
//   clearHist
//-----------------------------------------------------------------------

void J2Material::clearHist ()

{
  for ( idx_t p = 0; p < preHist_.size(); ++p )
  {
    preHist_[p].epsp        = 0.0;
    preHist_[p].epspeq      = 0.0;
    preHist_[p].dissipation = 0.0;
    preHist_[p].loading     = false;

    newHist_[p].epsp        = 0.0;
    newHist_[p].epspeq      = 0.0;
    newHist_[p].dissipation = 0.0;
    newHist_[p].loading     = false;
  }
}

//-----------------------------------------------------------------------
//   getHistory
//-----------------------------------------------------------------------

void J2Material::getHistory

  ( Vector&        hvals,
    const idx_t    mpoint )

{
  (*latestHist_)[mpoint].toVector ( hvals );
}

//-----------------------------------------------------------------------
//   setHistory
//-----------------------------------------------------------------------

void J2Material::setHistory

  ( const Vector&  hvals,
    const idx_t    mpoint )

{
  preHist_[mpoint].epsp[0]     = hvals[0];
  preHist_[mpoint].epsp[1]     = hvals[1];
  preHist_[mpoint].epsp[2]     = hvals[2];
  preHist_[mpoint].epsp[3]     = hvals[3];
  preHist_[mpoint].epsp[4]     = hvals[4];
  preHist_[mpoint].epsp[5]     = hvals[5];
  preHist_[mpoint].epspeq      = hvals[6];

  latestHist_      = &preHist_;
  newHist_[mpoint] = preHist_[mpoint];
}

//-----------------------------------------------------------------------
//   getDissipationStress
//-----------------------------------------------------------------------


void J2Material::getDissipationStress

  ( const Vector&  sstar,
    const Vector&  strain,
    const idx_t    ipoint ) 

{
  sstar = 0.;
}


//-----------------------------------------------------------------------
//   tmatmul_
//-----------------------------------------------------------------------


Vec6 J2Material::tmatmul_

  ( const Matrix& mat,
    const Vec6&   t6 ) const

{
  t6_to_v6_ ( v61_, t6 );

  matmul    ( v62_, mat, v61_ );

  return v6_to_t6_ ( v62_ );
}

//-----------------------------------------------------------------------
//   fill3DStrain_
//-----------------------------------------------------------------------

Tuple<double,6> J2Material::fill3DStrain_

  ( const Vector&       v ) const

{
  Tuple<double,6> ret;

  if ( v.size() == 1 )
  {
    ret[0_idx] = v[0_idx];
    ret[1_idx] = 0.0;
    ret[2_idx] = 0.0;
    ret[3_idx] = 0.0;
    ret[4_idx] = 0.0;
    ret[5_idx] = 0.0;
  }

  else if ( v.size() == 3 )
  {
    double eps_zz = anmodel_ == "PLANE_STRESS"
                  ? -n_ / (1.-n_) * (v[0_idx]+v[1_idx])
		  : 0.;
    
    ret[0_idx] = v[0_idx];
    ret[1_idx] = v[1_idx];
    ret[2_idx] = eps_zz;
    ret[3_idx] = v[2_idx];
    ret[4_idx] = 0.0;
    ret[5_idx] = 0.0;
  }

  else if ( v.size() == 6 )
  {
    ret[0_idx] = v[0_idx];
    ret[1_idx] = v[1_idx];
    ret[2_idx] = v[2_idx];
    ret[3_idx] = v[3_idx];
    ret[4_idx] = v[4_idx];
    ret[5_idx] = v[5_idx];
  }

  else
    throw Error ( JEM_FUNC,
      "Unexpected strain vector size in fill3DStrain_ (J2Material)" );

  return ret;
}

//-----------------------------------------------------------------------
//   reduce3DVector_
//-----------------------------------------------------------------------

void J2Material::reduce3DVector_

  ( const Vector&          v,
    const Tuple<double,6>& t ) const 

{
  // reduce a full 3D tuple to a 2D or 3D vector

  if ( v.size() == 1 )
  {
    v[0_idx] = t[0_idx];
  }
  else if ( v.size() == 3 ) // Plane strain excl. s_zz
  {
    v[0_idx] = t[0_idx];
    v[1_idx] = t[1_idx];
    v[2_idx] = t[3_idx];
  }
  else if ( v.size() == 4 ) // Plane strain incl. s_zz (printing)
  {
    v[0_idx] = t[0_idx];
    v[1_idx] = t[1_idx];
    v[2_idx] = t[3_idx];
    v[3_idx] = t[2_idx];
  }
  else
  {
    v[0_idx] = t[0_idx];
    v[1_idx] = t[1_idx];
    v[2_idx] = t[2_idx];
    v[3_idx] = t[3_idx];
    v[4_idx] = t[4_idx];
    v[5_idx] = t[5_idx];
  }
}

//-----------------------------------------------------------------------
//   reduce3DMatrix_
//-----------------------------------------------------------------------

void J2Material::reduce3DMatrix_

  ( const Matrix&            m,
    const Tuple<double,6,6>& t ) const 

{
  if ( m.size(0) == 1 )
  {
    m = 0.0;

    m(0,0) = t(0,0);
  }
  else if ( m.size(0) == 3 )
  {
    if ( anmodel_ == "PLANE_STRAIN" )
    {
      select2DMatrix_ ( m, t );
    }
    else
    {
      double d;
      Tuple<double,6,6> tmp66 = t;
      LUSolver::invert ( tmp66, d );
      select2DMatrix_ ( m, tmp66 );
      LUSolver::invert ( m, d );
    }
  }
  else
  {
    for ( idx_t i = 0; i < 6; ++i )
    {
      for ( idx_t j = 0; j < 6; ++j )
      {
        m(i,j) = t(i,j);
      }
    }
  }
}

//-----------------------------------------------------------------------
//   select2DMatrix_
//-----------------------------------------------------------------------

void J2Material::select2DMatrix_

  ( const Matrix&            m,
    const Tuple<double,6,6>& t ) const 

{
  // selecting [xx, yy, xy] components from [xx, yy, zz, xy, xz, yz]

  for ( idx_t i = 0; i < 2; ++i )
  {
    m(i,2_idx) = t(i,3_idx);
    m(2_idx,i) = t(3_idx,i); 

    for ( idx_t j = 0; j < 2; ++j )
    {
      m(i,j) = t(i,j);
    }
  }
  m(2_idx,2_idx) = t(3_idx,3_idx);
}


//-----------------------------------------------------------------------
//   makeFunc_
//-----------------------------------------------------------------------


Ref<Function> J2Material::makeFunc_ 

  ( const Properties& props,
    const String&     name ) const

{
  String args = "x";
  props.find ( args, "args" );

  Ref<Function> func = FuncUtils::newFunc ( args, name, props, globdat_ );

  FuncUtils::resolve ( *func, globdat_ );

  return func;
}


//-----------------------------------------------------------------------
//   handleException_
//-----------------------------------------------------------------------


void J2Material::handleException_ 

  ( const Exception&      ex,
    const Vector&         strain,
    const idx_t           ipoint )

{
  using jem::newInstance;

  Ref<PrintWriter> out = newInstance<PrintWriter>
    ( &System::debug("j2mat") );

  out->nformat.setFractionDigits ( 10 );

  if ( ex.what().equals ( "No convergence" ) )
  {
    *out << "No convergence in return mapping algorithm " << endl;
  }
  else if ( ex.what().equals ( "nan" ) )
  {
    *out << "NaN detected in return mapping algorithm " << endl;
  }
  else if ( ex.what().equals ( "Negative dgam" ) )
  {
    *out << "Negative increment found" << endl;
  }
  else
  {
    *out << "Caught unkown exception: " << ex.what() << endl;
  }
}


//-----------------------------------------------------------------------
//   comp2DStiffMat_ 
//-----------------------------------------------------------------------


void J2Material::comp2DStiffMat_

  ()

{
  using jem::Error;

  const int    n = STRAIN_COUNTS[rank_];

  stiff2D_.resize ( n, n );
  stiff2D_ = 0.;

  if      ( rank_ == 2 && anmodel_ == "PLANE_STRAIN" )
  {
    const double a = e_ / ((1. + n_) * (1. - 2. * n_));
    const double b = .5 * (1. - 2. * n_);
    const double c = 1. - n_;

    stiff2D_ (0,0) = a * c;
    stiff2D_ (0,1) = stiff2D_ (1,0) = a * n_;
    stiff2D_ (1,1) = a * c;
    stiff2D_ (2,2) = a * b;
  }
  else if ( rank_ == 2 && anmodel_ == "PLANE_STRESS" )
  {
    const double a = e_ / (1. - n_ * n_);

    stiff2D_ (0,0) = a;
    stiff2D_ (0,1) = stiff2D_ (1,0) = a * n_;
    stiff2D_ (1,1) = a;
    stiff2D_ (2,2) = a * .5 * (1. - n_);
  }
  else
  {
    throw Error ( JEM_FUNC, "unexpected 2D material for local rank!" );
  }
}

//-----------------------------------------------------------------------
//   computeElasticStiff_
//-----------------------------------------------------------------------

void J2Material::computeElasticStiff_ () 

{
  const double d = (1.0 + n_) * (1.0 - 2.0*n_);

  stiff_.resize(6,6);
  stiff_ = 0.;

  stiff_(0,0) = stiff_(1,1) =
  stiff_(2,2) = e_*(1.0 - n_)/d;
  stiff_(0,1) = stiff_(1,0) =
  stiff_(0,2) = stiff_(2,0) =
  stiff_(1,2) = stiff_(2,1) = e_*n_/d;
  stiff_(3,3) = stiff_(4,4) =
  stiff_(5,5) = 0.5*e_/(1.0 + n_);
}

//=======================================================================
//   class J2Material::Hist_
//=======================================================================


//-----------------------------------------------------------------------
//   ElemtHist_ constructor
//-----------------------------------------------------------------------


J2Material::Hist_::Hist_ 
   
  ()
     
{
  epsp        = 0.;
  epspeq      = 0.;
  dissipation = 0.;
  loading     = false;
}

void J2Material::Hist_::toVector

  ( Vector& vec )

{
  vec.resize ( 7 );

  vec = 0.0;

  vec[0] = epsp[0];
  vec[1] = epsp[1];
  vec[2] = epsp[2];
  vec[3] = epsp[3];
  vec[4] = epsp[4];
  vec[5] = epsp[5];
  vec[6] = epspeq;
}


//=======================================================================
//   YieldFunc_ class
//=======================================================================


//-----------------------------------------------------------------------
//   constructors & destructor
//-----------------------------------------------------------------------


YieldFunc_::YieldFunc_

  ( const double        young,
    const double        poisson,
    const double        yield,
    const double        hard      )

  : y_ ( yield ), h_ ( hard ), rmTol_(0.), maxIter_(0)

{
  E_       = young;
  n_      = poisson;
  G_       = young / 2. / ( 1. + poisson );
  K_       = young / 3. / ( 1. - 2. * poisson );

  rankDep_ = 3;
}


YieldFunc_::~YieldFunc_

  ()

{}


//-----------------------------------------------------------------------
//   setRmSettings
//-----------------------------------------------------------------------


void YieldFunc_::setRmSettings 

  ( const double rmTolerance, 
    const idx_t  rmMaxIter )

{
  rmTol_   = rmTolerance;
  maxIter_ = rmMaxIter;
}


//-----------------------------------------------------------------------
//   setElasticStiff
//-----------------------------------------------------------------------


void YieldFunc_::setElasticStiff

  ( const Matrix& mat ) 

{
  idx_t nr = mat.size(0);
  idx_t nc = mat.size(1);

  JEM_ASSERT ( (nr == 3 && nc == 3) || (nr == 6 && nc == 6) );

  rankDep_ = nr;

  De_.resize ( nr, nc );

  for ( idx_t i = 0; i < nr; ++i )
  {
    for ( idx_t j = 0; j < nc; ++j )
    {
      De_(i,j) = mat(i,j);
    }
  }
}


//-----------------------------------------------------------------------
//   findRoot 
//-----------------------------------------------------------------------


double YieldFunc_::findRoot

  ( const double dgam0 ) 

{
  double dgam     = dgam0;
  double oldddgam = -1.;
  double oldcrit  = evalTrial_();

  for ( idx_t irm = 0; irm < maxIter_; )
  {
    // Evaluate the yield functions.

    double crit = eval_ ( dgam );

    // Compute the derivative of yield function with respect to dgam.
    // (residual derivative)

    double df   = evalDer_ ( dgam );

    if ( jem::numeric::abs( crit ) < rmTol_ )
    {
      break;
    }
    if ( ++irm == maxIter_ )
    {
      throw Exception ( JEM_FUNC, "No convergence" );
    }
    if ( jem::Float::isNaN ( df ) )
    {
      throw Exception ( JEM_FUNC, "nan" );
    }

    // Corrector.

    double ddgam = crit / df;

    // Divergence detection.

    if ( ddgam * oldddgam < 0. )
    {
      if ( oldcrit * crit < 0. && 
      
           jem::numeric::abs(ddgam) > jem::numeric::abs(oldddgam) )
      {
        // there might be an inflection point around the root
        // use linear interpolation rather than linearization

        ddgam = -oldddgam * crit / ( crit - oldcrit );
      }
    }

    // Set a new guess for dgam.

    dgam    -= ddgam;

    oldddgam = ddgam;
    oldcrit  = crit;
  }

  if ( dgam < -1.e-12 )
  {
    throw Exception ( JEM_FUNC, "Negative dgam" );
  }
  return dgam;
}


//-----------------------------------------------------------------------
//   findBounds 
//-----------------------------------------------------------------------


void YieldFunc_::findBounds

  ( double&  gmin,
    double&  gmax,
    double&  fmin,
    double&  fmax ) 

{
  gmin = 0.;
  gmax = -1.;
  fmax = -1.;

  fmin = evalTrial_();

  JEM_ASSERT ( fmin > 0. );

  double dgam = 1.e-16;

  while ( gmax < 0. )
  {
    double crit = eval_ ( dgam );

    if ( crit > 0. )
    {
      gmin = dgam;
      fmin = crit;
    }
    else
    {
      gmax = dgam;
      fmax = crit;
    }
    dgam *= 10.;
  }
}


//-----------------------------------------------------------------------
//   findRoot 
//-----------------------------------------------------------------------


double YieldFunc_::findRoot

  ( double  gmin,
    double  gmax,
    double  fmin,
    double  fmax )

{
  double dgam;

  try 
  {
    double dgam0 = estimateRoot_ ( gmin, gmax, fmin, fmax );

    dgam = findRoot ( dgam0 );
  }
  catch ( const Exception& ex )
  {
    improveBounds_ ( gmin, gmax, fmin, fmax );

    dgam = findRoot ( gmin, gmax, fmin, fmax );
  }

  return dgam;
}


//-----------------------------------------------------------------------
//   deviatoric_
//-----------------------------------------------------------------------


Vec6 YieldFunc_::deviatoric_

  ( const Vec6&   full,
    const double  p ) const

{
  Vec6 ret = full;

  ret[0] -= p;
  ret[1] -= p;
  ret[2] -= p;

  return ret;
}


//-----------------------------------------------------------------------
//   estimateRoot_ 
//-----------------------------------------------------------------------


double YieldFunc_::estimateRoot_

  ( const double  gmin,
    const double  gmax,
    const double  fmin,
    const double  fmax ) const

{
  // linear interpolation

  // fmin  o___ 
  //           ---___
  // phi=0 |----------o-----------|------
  //                      ---___
  // fmax  -                    --o
  //     gmin        ret        gmax

  return gmin + fmin * (gmax-gmin) / (fmin-fmax);
}


//-----------------------------------------------------------------------
//   improveBounds_ 
//-----------------------------------------------------------------------


void YieldFunc_::improveBounds_

  ( double&  gmin,
    double&  gmax,
    double&  fmin,
    double&  fmax )

{
  idx_t  np   = 10;
  double dg   = ( gmax - gmin ) / double ( np );
  double dgam = gmin;

  while ( dgam < gmax )
  {
    dgam += dg;

    double crit = eval_ ( dgam );

    if ( crit > 0. )
    { 
      gmin = dgam;
      fmin = crit;
    }
    else
    {
      gmax = dgam;
      fmax = crit;

      break;
    }
  }
}


//=======================================================================
//   YieldFunc3D_ class
//=======================================================================


//-----------------------------------------------------------------------
//   constructor & destructor
//-----------------------------------------------------------------------


YieldFunc3D_::YieldFunc3D_

  ( const double        young,
    const double        poisson,
    const double        yield,
    const double        hard     )

  : YieldFunc_ ( young, poisson, yield, hard )

{
  sigDtr_ = 0.;
  ptrial_ = 0.;
}

YieldFunc3D_::~YieldFunc3D_

  ()

{}


//-----------------------------------------------------------------------
//   isLinear 
//-----------------------------------------------------------------------


bool YieldFunc3D_::isLinear

  ( const Vec6&   sigtr,
    const double  epspeq0 )

{
  epspeq0_ = epspeq0; 

  StressInvariants inv  ( sigtr );

  // Hydrostatic stress tensor.

  ptrial_ = inv.getFirstInvariant () / 3.;

  // Deviatoric stress tensor.

  sigDtr_ = deviatoric_ ( sigtr, ptrial_ );

  // J2 term => J2 = 1/2 * (s:s).

  double J2 = inv.getJ2 ();

  ksitr_    = sqrt ( 3. * J2 );

  //System::out() << "evalTrial " << evalTrial_() << " rmTol " << rmTol_ << '\n';

  return evalTrial_() < rmTol_; 
}


//-----------------------------------------------------------------------
//   compSigUpdated 
//-----------------------------------------------------------------------


void YieldFunc3D_::compSigUpdated

  ( Vec6&        sig, 
    const double dgam, 
    const Vec6&  sigtr ) 

{
  double frac = 1. - dgam * 3. * G_ / ksitr_;

  sigD_ = frac * sigDtr_;

  sig   = frac * sigDtr_; 

  sig[0] += ptrial_;
  sig[1] += ptrial_;
  sig[2] += ptrial_;
}

//-----------------------------------------------------------------------
//   compElastStr
//-----------------------------------------------------------------------


void YieldFunc3D_::compElastStr

  ( Vec6&         epse )

{
  // Before calling getPlastStrInc(), it must call compSigUpdated() first
  // to compute sigD_ member.

  throw Error (JEM_FUNC,"Not implemented");
}

//-----------------------------------------------------------------------
//   compPlastStrInc 
//-----------------------------------------------------------------------


void YieldFunc3D_::compPlastStrInc

  ( Vec6&         depsp, 
    const double  dgam )

{
  // Before calling getPlastStrInc(), it must call compSigUpdated() first
  // to compute sigD_ member.

  double normS = normSig_ ( sigD_ );

  depsp = dgam * sqrt ( 1.5 ) * sigD_ / normS;

  depsp[3] *= 2.;
  depsp[4] *= 2.;
  depsp[5] *= 2.;
}


//-----------------------------------------------------------------------
//   compEquPlastInc 
//-----------------------------------------------------------------------


double YieldFunc3D_::compEquPlastInc

  ( const double  dgam ) 

{
  return dgam;
}


//-----------------------------------------------------------------------
//   compTangStiff 
//-----------------------------------------------------------------------


Matrix YieldFunc3D_::compTangStiff
  
  ( const double  dgam )

{
  // Unit plastic flow vector related to trial stress.

  double normStr = normSig_ ( sigDtr_ ); 

  Vec6   N       = sigDtr_ / normStr; 

  // Compute elastoplastic tangent matrix. 

  double fac1 = 2. * G_ * ( 1. - dgam * 3. * G_ / ksitr_ );

  double fac2 = 6. * G_ * G_ * ( dgam / ksitr_ - 1. / ( 3.*G_ + h_ ) );

  Tuple<double,6,6> NxN;
  matmul ( NxN, N, N );

  Tuple<double,6,6> IxI;
  IxI = 0.;
  for ( idx_t i = 0; i < 3; ++i) 
  {
    for ( idx_t j = 0; j < 3; ++j )
    {
      IxI(i,j) = 1.;
    }
  }

  Tuple<double,6,6> Id;
  Id = getIdMatrix_ ();

  Tuple<double,6,6> dmat6;
  dmat6 = fac1 * Id + fac2 * NxN + K_ * IxI;

  Matrix Dep ( 6, 6 );
  for ( idx_t i = 0; i < 6; ++i )
  {
    for ( idx_t j = 0; j < 6; ++j )
    {
      Dep(i,j) = dmat6(i,j);
    }
  }

  if ( rankDep_ == 3 )
  {
    Matrix Dep33 ( 3, 3 );
    m66_to_m33_ ( Dep33, Dep );

    return Dep33;
  }
  else
  {
    return Dep;
  }
}


//-----------------------------------------------------------------------
//   evalTrial_
//-----------------------------------------------------------------------


double YieldFunc3D_::evalTrial_

  () const

{
  double sigY = y_ + epspeq0_ * h_;

  return ksitr_ - sigY;
}


//-----------------------------------------------------------------------
//   eval_
//-----------------------------------------------------------------------


double YieldFunc3D_::eval_

  ( const double  dgam )

{
  epspeq_     = epspeq0_ + dgam; 

  double sigY = y_ + epspeq_ * h_;

  double fac  = evalKsi_ ( dgam );

  return fac - sigY;  
}


//-----------------------------------------------------------------------
//   evalDer_
//-----------------------------------------------------------------------


double YieldFunc3D_::evalDer_

  ( const double  dgam ) 

{
  // Residual derivative.

  return -3. * G_ - h_ ;
}


//-----------------------------------------------------------------------
//   evalKsi_
//-----------------------------------------------------------------------


double YieldFunc3D_::evalKsi_

  ( const double  dgam ) 

{
  return ksitr_ - 3. * G_ * dgam; 
}


//-----------------------------------------------------------------------
//   evalDerKsi_
//-----------------------------------------------------------------------


double YieldFunc3D_::evalDerKsi_

  ( const double dgam ) 

{
  return ksitr_ - 3. * G_;
}


//-----------------------------------------------------------------------
//   getIdMatrix_ 
//-----------------------------------------------------------------------


Tuple<double,6,6> YieldFunc3D_::getIdMatrix_

  () 

{
  Tuple<double,6,6> Id;

  Id = 0.;

  Id(0,0) =  2./3.; Id(0,1) = -1./3.; Id(0,2) = -1./3.;

  Id(1,0) = -1./3.; Id(1,1) =  2./3.; Id(1,2) = -1./3.; 

  Id(2,0) = -1./3.; Id(2,1) = -1./3.; Id(2,2) =  2./3.;

  Id(3,3) =  1./2.; 
  Id(4,4) =  1./2.;
  Id(5,5) =  1./2.;

  return Id;
}


//-----------------------------------------------------------------------
//   normSig_ 
//-----------------------------------------------------------------------


double YieldFunc3D_::normSig_

  ( const Vec6& sig ) const 

{
  // Compute the norm of 'stress' tensors.

  return sqrt ( sig[0]*sig[0] + sig[1]*sig[1] + sig[2]*sig[2] +

                2.*sig[3]*sig[3] + 2.*sig[4]*sig[4] + 2.*sig[5]*sig[5] );
}


//-----------------------------------------------------------------------
//   m66_to_m33_ 
//-----------------------------------------------------------------------


void YieldFunc3D_::m66_to_m33_

  ( const Matrix&  m33,
    const Matrix&  m66 ) const

{
  // Selecting [xx, yy, xy] components from [xx, yy, zz, xy, yz, xz]
  // This member is designed to P.Strain. 
 
  for ( idx_t i = 0; i < 2; ++i )
  {
    m33(i,2) = m66(i,3);
    m33(2,i) = m66(3,i);
 
    for ( idx_t j = 0; j < 2; ++j )
    {
      m33(i,j) = m66(i,j);
    }
  }
  m33(2,2) = m66(3,3);
}


//=======================================================================
//   YieldFuncPS_ class
//=======================================================================


//-----------------------------------------------------------------------
//   constructor & destructor
//-----------------------------------------------------------------------


YieldFuncPS_::YieldFuncPS_

  ( const double        young,
    const double        poisson,
    const double        yield,
    const double        hard    )

  : YieldFunc_ ( young, poisson, yield, hard )

{
  P_ = 0.;

  P_(0,0) = P_(1,1) =  2./3.;
  P_(0,1) = P_(1,0) = -1./3.;
  P_(2,2) = 2.;

  stress_ = 0.;
}

YieldFuncPS_::~YieldFuncPS_

  ()

{}


//-----------------------------------------------------------------------
//   isLinear 
//-----------------------------------------------------------------------


bool YieldFuncPS_::isLinear

  ( const Vec6&   sigtr,
    const double  epspeq0 )

{
  epspeq0_ = epspeq0; 

  a1tr_  = ( sigtr[0] + sigtr[1] ) * ( sigtr[0] + sigtr[1] );
  a2tr_  = ( sigtr[1] - sigtr[0] ) * ( sigtr[1] - sigtr[0] );
  a3tr_  = sigtr[3] * sigtr[3];

  ksitr_ = a1tr_/6. + 0.5*a2tr_ + 2.*a3tr_;

  //System::out() << "\n sigtr " << sigtr << " epspeq0 " << epspeq0_ << " a1tr_ " << a1tr_ << " a2tr_ " << a2tr_ << " a3tr_ " << a3tr_ << " ksitr_ " << ksitr_ << '\n';
  //System::out() << "evalTrial " << evalTrial_() << " rmTol " << rmTol_ << '\n';

  return evalTrial_() < rmTol_; 
}


//-----------------------------------------------------------------------
//   compSigUpdated 
//-----------------------------------------------------------------------


void YieldFuncPS_::compSigUpdated

  ( Vec6&        sig, 
    const double dgam, 
    const Vec6&  sigtr ) 

{
  Vec3 sigtr3;

  t6_to_t3_ ( sigtr3, sigtr );

  // Compute A matrix.

  Tuple<double,3,3> A ( getAMatrix_ ( dgam ) );

  // Compute the updated stress.

  stress_ = matmul ( A, sigtr3 );

  t3_to_t6_ ( sig, stress_ );
}

//-----------------------------------------------------------------------
//   compElastStr
//-----------------------------------------------------------------------


void YieldFuncPS_::compElastStr

  ( Vec6&         epse )

{
  // Before calling getPlastStrInc(), it must call compSigUpdated() first
  // to compute sigD_ member.

  Tuple<double,3,3> comp;
  comp = 0.0;

  double fac = E_ / ( 1. - n_ * n_ );

  comp(0,0) = comp(1,1) = fac * 1.0;
  comp(0,1) = comp(1,0) = fac * n_;
  comp(2,2) = fac * ( 1. - n_ ) / 2.;

  double d;
  LUSolver::invert(comp,d);

  Vec3 epse3;
  epse3 = 0.;
  epse3 = matmul ( comp, stress_ );

  ee3_to_ee6_ ( epse, epse3 );

  //epse[3] *= 2.0;
}


//-----------------------------------------------------------------------
//   compPlastStrInc 
//-----------------------------------------------------------------------


void YieldFuncPS_::compPlastStrInc

  ( Vec6&         depsp, 
    const double  dgam )

{
  // Before calling getPlastStrInc(), it must call compSigUpdated() first
  // to compute stress_ member.

  Vec3 depsp3;
  depsp3 = 0.;
  
  depsp3 = dgam * matmul ( P_, stress_ );

  ep3_to_ep6_ ( depsp, depsp3 );

  //depsp[3] *= 2.;
}


//-----------------------------------------------------------------------
//   compEquPlastInc 
//-----------------------------------------------------------------------


double YieldFuncPS_::compEquPlastInc

  ( const double  dgam ) 

{
  double fac = 2. * evalKsi_ ( dgam ) / 3.;  

  return dgam * sqrt ( fac );
}


//-----------------------------------------------------------------------
//   compTangStiff 
//-----------------------------------------------------------------------


Matrix YieldFuncPS_::compTangStiff
  
  ( const double  dgam )

{
  // Compute ksi for stress and stiffness.

  double k = dot ( matmul ( P_, stress_ ), stress_ );

  // Matrix E.

  Tuple<double,3,3> E;
  
  E = getEMatrix_ ( dgam );

  // Vector n.

  Vec3 n = matmul ( E, matmul(P_, stress_) );

  // Alpha.

  double fac   = dot ( matmul( stress_, P_ ), n );
  fac         += 2. * k * h_ / ( 3. - 2. * h_ * dgam );

  double alpha = 1. / fac;

  // Assemble the elastoplastic tangent matrix.

  Tuple<double,3,3> dmat3;
  
  dmat3 = E - alpha * matmul ( n, n.transpose() );

  Matrix D3 ( 3, 3 );

  t33_to_m33_ ( D3, dmat3 );

  return D3;
}


//-----------------------------------------------------------------------
//   evalTrial_
//-----------------------------------------------------------------------


double YieldFuncPS_::evalTrial_

  () const

{
  double sigY = y_ + epspeq0_ * h_;

  //System::out() << "J2Material::evalTrial sigY " << sigY << " epspeq0 " << epspeq0_ <<  " h " << h_ << " ksitr " << ksitr_ << " crit " << 0.5*ksitr_ - sigY*sigY/3. << '\n';

  return 0.5*ksitr_ - sigY*sigY/3.;
}


//-----------------------------------------------------------------------
//   eval_
//-----------------------------------------------------------------------


double YieldFuncPS_::eval_

  ( const double  dgam )

{
  double ksi  = evalKsi_ ( dgam );

  epspeq_     = epspeq0_ + dgam * sqrt ( 2. * ksi / 3. ); 

  double sigY = y_ + epspeq_ * h_;

  return 0.5*ksi - sigY*sigY/3.;  
}


//-----------------------------------------------------------------------
//   evalDer_
//-----------------------------------------------------------------------


double YieldFuncPS_::evalDer_

  ( const double  dgam ) 

{
  // Compute ksi. 

  double ksi = evalKsi_ ( dgam );

  // ksiDer.

  double ksiDer = evalDerKsi_ ( dgam );

  // sigY

  double sigY = y_ + epspeq_ * h_;

  // Hbar.

  double sqrtKsi = sqrt ( ksi );

  double Hbar = 2.*sigY * h_ * sqrt(2./3.) * 

               ( sqrtKsi + dgam*ksiDer/2./sqrtKsi );

  return 0.5*ksiDer - Hbar/3.;
}


//-----------------------------------------------------------------------
//   evalKsi_
//-----------------------------------------------------------------------


double YieldFuncPS_::evalKsi_

  ( const double  dgam ) 

{
  double f1  = 1. + E_ * dgam / 3. / ( 1. - n_ );
  double f2  = 1. + 2. * G_ * dgam;

  double f12 = f1 * f1 * 6.;
  double f22 = f2 * f2;

  return a1tr_ / f12 + ( 0.5*a2tr_ + 2.*a3tr_ ) / f22;
}


//-----------------------------------------------------------------------
//   evalDerKsi_
//-----------------------------------------------------------------------


double YieldFuncPS_::evalDerKsi_

  ( const double dgam ) 

{
  double fac = E_ / ( 1. - n_ );

  double f1  = 1. + fac * dgam / 3.;
  double f2  = 1. + 2.  * G_ * dgam;

  double f13 = f1 * f1 * f1 * 9.;
  double f23 = f2 * f2 * f2;

  return - a1tr_ * fac / f13 - 2. * G_ * ( a2tr_ + 4.*a3tr_ ) / f23; 
}


//-----------------------------------------------------------------------
//   getAMatrix_ 
//-----------------------------------------------------------------------


Tuple<double,3,3> YieldFuncPS_::getAMatrix_

  ( const double dgam ) const 

{
  double A11 = 3. * ( 1. - n_ ) / ( 3. * ( 1. - n_ ) + E_ * dgam );
  double A22 = 1. / ( 1. + 2. * G_ * dgam );
  double A33 = A22;

  Tuple<double,3,3> tmp;

  tmp = 0.;

  tmp(0,0) = .5 * ( A11 + A22 ); tmp(0,1) = .5 * ( A11 - A22 );
  tmp(1,0) = .5 * ( A11 - A22 ); tmp(1,1) = .5 * ( A11 + A22 );  
  tmp(2,2) = A33; 

  return tmp;
}


//-----------------------------------------------------------------------
//   getEMatrix_ 
//-----------------------------------------------------------------------


Tuple<double,3,3> YieldFuncPS_::getEMatrix_

  ( const double dgam ) const 

{
  double E11 = 3. * E_ / ( 3. * ( 1. - n_ ) + E_ * dgam );
  double E22 = 2. * G_ / ( 1 + 2. * G_ * dgam );
  double E33 = .5 * E22; 

  Tuple<double,3,3> tmp;

  tmp = 0.;

  tmp(0,0) = .5 * ( E11 + E22 ); tmp(0,1) = .5 * ( E11 - E22 );
  tmp(1,0) = .5 * ( E11 - E22 ); tmp(1,1) = .5 * ( E11 + E22 );  
  tmp(2,2) = E33; 

  return tmp;
}


//-----------------------------------------------------------------------
//   t33_to_m33_
//-----------------------------------------------------------------------


void YieldFuncPS_::t33_to_m33_
 
  ( const Matrix&             m,
    const Tuple<double,3,3>&  t ) const

{
  for ( idx_t i = 0; i < 3; ++i )
  {
    for ( idx_t j = 0; j < 3; ++j )
    {
      m(i,j) = t(i,j);
    }
  }
}

