/*
 * Copyright (C) 2019 TU Delft. All rights reserved.
 *
 * Utility functions for artificial neural networks.
 *
 * Author: Iuri Barcelos, i.barceloscarneiromrocha@tudelft.nl
 * Date:   May 2019
 * 
 */

#ifndef NEURALUTILS_H
#define NEURALUTILS_H

#include <jem/base/System.h>
#include <jem/base/Array.h>
#include <jem/base/Float.h>
#include <jem/base/array/tensor.h>
#include <jem/util/Properties.h>
#include <jem/numeric/algebra/utilities.h>
#include <jive/Array.h>
#include <jem/base/Tuple.h>

extern "C"
{
  #include  <math.h>
}

using namespace jem;

using jem::Array;
using jem::String;
using jem::util::Properties;

using jive::Vector;
using jive::Matrix;
using jive::IntVector;
using jive::IdxVector;
using jem::Tuple;
using jem::idx_t;

//-----------------------------------------------------------------------
//   typedefs
//-----------------------------------------------------------------------


namespace NeuralUtils
{
  typedef void (*ActivationFunc)

    ( Matrix& x );
    
  typedef void (*ActivationGrad)

    ( Matrix& x );

  typedef void (*ActivationHess)

    ( Matrix& x );

  typedef double (*LossFunc)

    ( const Matrix& pred,
      const Matrix& real );

  typedef void (*LossGrad)
   
    (       Matrix& pred,
      const Matrix& real );

  typedef void (*InitFunc)

    (       Matrix&     w, 
      const Properties& globdat );

  ActivationFunc getActivationFunc

    ( const String&    name );

  ActivationGrad getActivationGrad

    ( const String&    name );

  ActivationHess getActivationHess

    ( const String&    name );

  LossFunc       getLossFunc

    ( const String&    name );

  LossGrad       getLossGrad

    ( const String&    name );

  InitFunc       getInitFunc

    ( const String&    name );

  namespace Activations
  {
    inline void evalIdentityFunc

      ( Matrix& x )

    {}

    inline void evalSigmoidFunc

      ( Matrix& x )

    {
      x = 1. / ( 1. + exp(-x) );
    }

    inline void evalTanhFunc

      ( Matrix& x )

    {
      //x = 2. / ( 1. + exp(-2.*x) ) - 1.;
      x = tanh(x);
    }

    inline void evalReLUFunc

      ( Matrix& x )

    {
      TensorIndex i, j;
      x(i,j) = where ( x(i,j) > 0., x(i,j), 0. );
    }

    inline void evalLeakyReLUFunc

      ( Matrix& x )

    {
      TensorIndex i, j;
      x(i,j) = where ( x(i,j) >= 0., x(i,j), 0.01*x(i,j) ); 
    }

    inline void evalSoftPlusFunc

      ( Matrix& x )

    {
      x = log ( 1. + exp(x) );
    }

    inline void evalSeLUFunc

      ( Matrix& x )

    {
      TensorIndex i, j;
      x(i,j) = where ( x(i,j) > 0.0, 1.0507*x(i,j), 1.0507*(1.6733*exp(x(i,j))-1.6733) ); 
    }

    inline void evalIdentityGrad

      ( Matrix& x )

    {
      x = 1.;
    }

    inline void evalSigmoidGrad

      ( Matrix& x )

    {
      Matrix temp = x.clone();
      evalSigmoidFunc(temp);

      x = temp * ( 1. - temp );
    }

    inline void evalTanhGrad

      ( Matrix& x )

    {
      Matrix temp = x.clone();
      evalTanhFunc ( temp );

      x = 1. - temp * temp;
    }

    inline void evalReLUGrad

      ( Matrix& x )

    {
      TensorIndex i, j;

      x(i,j) = where ( x(i,j) > 0., 1., 0. );
    }

    inline void evalLeakyReLUGrad

      ( Matrix& x )

    {
      TensorIndex i, j;
      x(i,j) = where ( x(i,j) >= 0., 1., 0.01 );
    }

    inline void evalSoftPlusGrad

      ( Matrix& x )

    {
      evalSigmoidFunc ( x );
    }

    inline void evalSeLUGrad

      ( Matrix& x )

    {
      TensorIndex i, j;
      x(i,j) = where ( x(i,j) > 0.0, 1.0507, 1.0507*1.6733*exp(x(i,j)) );
    }

    inline void evalIdentityHess

      ( Matrix& x )

    {
      x = 0.0;
    }

    inline void evalSigmoidHess

      ( Matrix& x )

    {
      Matrix temp = x.clone();
      evalSigmoidFunc(temp);

      x = temp * ( 1. - temp ) * ( 1. - 2.*temp );
    }

    inline void evalTanhHess

      ( Matrix& x )

    {
      x = -2.0 * tanh(x)/cosh(x)/cosh(x);
    }

    inline void evalReLUHess

      ( Matrix& x )

    {
    }

    inline void evalLeakyReLUHess

      ( Matrix& x )

    {
    }

    inline void evalSoftPlusHess

      ( Matrix& x )

    {
    }

    inline void evalSeLUHess

      ( Matrix& x )

    {
      TensorIndex i, j;
      x(i,j) = where ( x(i,j) > 0.0, 0.0, 1.0507*1.6733*exp(x(i,j)) );
    }
  }

  namespace Losses
  {
    inline double evalSquaredErrorFunc

      ( const Matrix& pred,
	const Matrix& real )

    {
      return 0.5 * dot ( real - pred );
    }

    inline void evalSquaredErrorGrad

      (       Matrix& pred,
	const Matrix& real )

    {
      pred -= real;
    }
  }

  namespace Initializations
  {
	   void zeroInit

      (       Matrix&     w,
        const Properties& globdat );

           void glorotInit
 
      (       Matrix&     w,
        const Properties& globdat );

           void martensInit
 
      (       Matrix&     w,
        const Properties& globdat );

           void sutskeverInit

      (       Matrix&     w,
        const Properties& globdat );

           void orthoInit

      (       Matrix&     w,
        const Properties& globdat );

           void seluInit

      (       Matrix&     w,
        const Properties& globdat );
  }

  void           shuffle

    (       IdxVector&  vec,
      const Properties& globdat );
}

#endif

