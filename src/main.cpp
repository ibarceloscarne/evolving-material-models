
#include <jive/app/ChainModule.h>
#include <jive/app/OutputModule.h>
#include <jive/app/ReportModule.h>
#include <jive/app/Application.h>
#include <jive/app/InfoModule.h>
#include <jive/app/ControlModule.h>
#include <jive/app/declare.h>
#include <jive/geom/declare.h>
#include <jive/fem/declare.h>
#include <jive/mesh/declare.h>
#include <jive/fem/InitModule.h>
#include <jive/fem/InputModule.h>
#include <jive/fem/ShapeModule.h>
#include <jive/app/UserconfModule.h>
#include <jive/app/SampleModule.h>
#include <jive/model/declare.h>
#include <jive/gl/FemViewModule.h>
#include <jive/gl/DisplayModule.h>
#include <jive/gl/declare.h>
#include <jive/implict/declare.h>
#include <jive/implict/LinsolveModule.h>
#include <jive/implict/Park3Module.h>
#include <jive/implict/NewmarkModule.h>
#include <jive/implict/NonlinModule.h>

#include "declare.h"
#include "XOutputModule.h"

using namespace jem;

using jive::app::Application;
using jive::app::Module;
using jive::app::ChainModule;
using jive::app::ControlModule;
using jive::app::ReportModule;
using jive::app::InfoModule;
using jive::app::OutputModule;
using jive::app::SampleModule;
using jive::app::UserconfModule;
using jive::fem::InitModule;
using jive::fem::InputModule;
using jive::fem::ShapeModule;
using jive::gl::FemViewModule;
using jive::gl::DisplayModule;
using jive::implict::LinsolveModule;
using jive::implict::Park3Module;

using jive::implict::NewmarkModule;
using jive::implict::NonlinModule;

//-----------------------------------------------------------------------
//   mainModule
//-----------------------------------------------------------------------


Ref<Module> mainModule ()
{
  Ref<ChainModule>  chain = newInstance<ChainModule> ();

  // Register the required models/modules and other classes.

  declareModels                 ();
  declareLayerModels            ();
  declareModules                ();

  jive::fem  ::declareMBuilders ();
  jive::model::declareModels    ();
  jive::geom::declareIShapes    ();
  jive::geom::declareShapes     ();

  jive::implict::declareModules ();
  jive::app::declareModules     ();
  jive::gl::declareModules      ();
  jive::mesh::declareModules    ();

  // Define the main module chain.

  chain->pushBack ( newInstance<UserconfModule>     ( "userinput"    ) );
  chain->pushBack ( newInstance<ShapeModule>        ( "shape"        ) );
  chain->pushBack ( newInstance<InitModule>         ( "init"         ) );
  chain->pushBack ( newInstance<InfoModule>         ( "info"         ) );
  chain->pushBack ( newInstance<UserconfModule>     ( "usermodules"  ) );
  chain->pushBack ( newInstance<XOutputModule>      ( "xout"         ) );

  // Wrap the entire chain in a ControlModule.

  Ref<ControlModule>  ctrl =

    newInstance<ControlModule> ( "control", chain,
				 ControlModule::FG_MODE );

  return newInstance<ReportModule> ( "report", ctrl );
}


//-----------------------------------------------------------------------
//   main
//-----------------------------------------------------------------------


int main ( int argc, char** argv )
{
  return Application::exec ( argc, argv, & mainModule );
}
