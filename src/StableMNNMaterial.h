/*
 * Copyright (C) 2021 TU Delft. All rights reserved.
 *
 * Author: Iuri Barcelos, i.rocha@tudelft.nl
 * Date:   Nov 2021
 * 
 */

#ifndef STABLEMNN_MATERIAL_H
#define STABLEMNN_MATERIAL_H

#include <jive/app/Module.h>
#include <jive/model/Model.h>
#include <jive/util/Random.h>
#include <jem/util/Flex.h>

#include "Material.h"
#include "NData.h"
#include "Normalizer.h"
#include "Bayesian.h"

using jive::app::Module;
using jive::util::Random;
using jem::util::Flex;
using jive::BoolVector;

typedef Ref<Material>  MatRef;
typedef Ref<Bayesian>  BayRef;

//-----------------------------------------------------------------------
//   class StableMNNMaterial
//-----------------------------------------------------------------------

class StableMNNMaterial : public Material
{
 public:

  typedef Array< Ref<NData>, 1 > Batch;

  static const char*     WEIGHTS;
  static const char*     MATERIAL;
  static const char*     FEATUREEXTRACTOR;
  static const char*     FEATURES;

  static const char*     NETWORK;
  static const char*     STABILIZE;
  static const char*     LEARNINGRATE;
  static const char*     SGDEPOCHS;
  static const char*     RETRAINEVERY;
  static const char*     CRITTHRESHOLD;
  static const char*     CRITERIUM;
  static const char*     SGD;
  static const char*     MONITORLOSS;

  enum                      Criterium { DET, ACOUSTIC, DIAG };

  explicit               StableMNNMaterial

    ( const Properties&    props,
      const Properties&    conf,
      const idx_t          rank,
      const Properties&    globdat );

  virtual void           configure

    ( const Properties&    props,
      const Properties&    globdat );

  virtual void           getConfig

    ( const Properties&    conf,
      const Properties&    globdat )   const;

  virtual void           update

    ( Matrix&              stiff,
      Vector&              stress,
      const Vector&        strain,
      const idx_t          ipoint );

  virtual void           commit  ();

  virtual void           cancel  ();

  virtual void           stressAtPoint

    ( Vector&              stress,
      const Vector&        strain,
      const idx_t          ipoint );

  virtual void           addTableColumns

    ( IdxVector&           jcols,
      XTable&              table,
      const String&        name   );

 virtual void            getHistory

    ( Vector&              hvals,
      const idx_t          mpoint );
 
  virtual void           createIntPoints

    ( const idx_t           npoints );

  virtual Ref<Material>  clone ( ) const;

 protected:

  virtual               ~StableMNNMaterial();

 protected:

  idx_t                   rank_;

  Ref<Module>             encoder_;
  Ref<Module>             network_;
  Ref<Module>             sgd_;

  Properties              encData_;
  Properties              netData_;

  Ref<Normalizer>         nizer_;

  IdxVector               features_;
  IdxVector               perm_;
  bool                    perm23_;

  Array<MatRef>           mat_;
  Array<MatRef>           ext_;

  Array<BayRef>           bmat_;
  Array<BayRef>           bext_;

  Matrix                  newStrains_;
  Matrix                  preStrains_;
  Matrix                  initStiff_;
  BoolVector              isUnstable_;

  Flex<Matrix>            strains_;

  Properties              props_;
  Properties              conf_;
  Properties              globdat_;

  bool                    stabilize_;
  bool                    monitorLoss_;
  idx_t                   nepochs_;
  idx_t                   retrainEvery_;
  double                  lrate_;

  Criterium               crit_;
  double                  critThres_;
  double                  initCrit_;

  idx_t                   iIter_;

 protected:

  void                    initWeights_

    ( const String&         fname        );

  void                    writeWeights_

    ( const String&         fname        );

  void                    initEncoder_

    ( const Properties&     props,
      const Properties&     globdat      );

  void                    initNetwork_

    ( const Properties&     props,
      const Properties&     globdat      );

  void                    updateProps_  

    ( const Matrix&         strains      );

  void                    updateHist_  
  
    ( const idx_t           point        );

  Vector                  permutate_    ( const Vector& strain ); 

  Vector                  getProps_

    ( const Vector& strain,
      const idx_t   p                   );

  Vector                  getFeatures_

    ( const Vector& strain,
      const idx_t   p                   );

  double                  evalCrit_

    ( const Vector& strain,
      const Vector& props,
      const idx_t   p                   );

  double                  evalCrit_

    ( const Matrix& stiff               );

  void                    updateNetwork_ 
  
    ( const IdxVector&      points      );
};

#endif
