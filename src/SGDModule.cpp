/*
 *  TU Delft 
 *
 *  Iuri Rocha, Nov 2021
 *
 *  Simple SGD with fixed learning rate
 *
 */

#include <cstdlib>
#include <random>

#include <jem/base/Error.h>
#include <jem/base/limits.h>
#include <jem/base/Float.h>
#include <jem/base/System.h>
#include <jem/base/Exception.h>
#include <jem/base/ClassTemplate.h>
#include <jem/base/array/operators.h>
#include <jem/base/Thread.h>
#include <jem/base/Monitor.h>
#include <jem/io/Writer.h>
#include <jem/io/PrintWriter.h>
#include <jem/io/FileWriter.h>
#include <jem/io/FileReader.h>
#include <jem/io/FileInputStream.h>
#include <jem/io/FileFlags.h>
#include <jem/io/FileStream.h>
#include <jem/util/Event.h>
#include <jem/util/Flex.h>
#include <jem/numeric/algebra/utilities.h>
#include <jem/mp/MPException.h>
#include <jem/mp/Context.h>
#include <jem/mp/Buffer.h>
#include <jem/mp/Status.h>
#include <jive/util/utilities.h>
#include <jive/util/Globdat.h>
#include <jive/util/FuncUtils.h>
#include <jive/algebra/VectorSpace.h>
#include <jive/model/Actions.h>
#include <jive/model/StateVector.h>
#include <jive/app/ModuleFactory.h>
#include <jive/implict/Names.h>
#include <jive/implict/SolverInfo.h>
#include <jive/util/XDofSpace.h>
#include <jive/mp/Globdat.h>

#include "SGDModule.h"
#include "SolverNames.h"
#include "declare.h"
#include "TrainingData.h"
#include "LearningNames.h"
#include "XNeuronSet.h"

JEM_DEFINE_CLASS( jive::implict::SGDModule );


JIVE_BEGIN_PACKAGE( implict )


using jem::max;
using jem::newInstance;
using jem::Float;
using jem::System;
using jem::Exception;
using jem::Error;
using jem::Thread;
using jem::Monitor;
using jem::io::endl;
using jem::io::Writer;
using jem::io::PrintWriter;
using jem::io::FileWriter;
using jem::io::FileReader;
using jem::io::FileFlags;
using jem::io::FileStream;
using jem::numeric::axpy;
using jem::mp::MPException;
using jem::mp::SendBuffer;
using jem::mp::RecvBuffer;
using jem::mp::Status;

using jive::util::FuncUtils;
using jive::util::XDofSpace;
using jive::model::Actions;
using jive::model::StateVector;

//=======================================================================
//   class SGDModule
//=======================================================================

//-----------------------------------------------------------------------
//   static data
//-----------------------------------------------------------------------

const char* SGDModule::TYPE_NAME     = "SGD";
const char* SGDModule::SEED          = "seed";
const char* SGDModule::LEARNING_RATE = "learningRate";
const char* SGDModule::MINIBATCH     = "miniBatch";
const char* SGDModule::LOSSFUNC      = "loss";
const char* SGDModule::VALSPLIT      = "valSplit";
const char* SGDModule::SKIPFIRST     = "skipFirst";

//-----------------------------------------------------------------------
//   constructor & destructor
//-----------------------------------------------------------------------

SGDModule::SGDModule ( const String& name ) :

  Super ( name )

{
  alpha_ = 0.001;
  batchSize_ = 1;
  epoch_     = 0;
  iiter_     = 1;
  valSplit_  = 0.0;
  skipFirst_ = 0;

  mpi_   = false;
  mpx_   = nullptr;
}


SGDModule::~SGDModule ()
{}

//-----------------------------------------------------------------------
//   init
//-----------------------------------------------------------------------

Module::Status SGDModule::init

  ( const Properties&  conf,
    const Properties&  props,
    const Properties&  globdat )

{
  Ref<DofSpace>     dofs  = DofSpace::   get ( globdat, getContext() );
  Ref<Model>        model = Model::      get ( globdat, getContext() );
  Ref<TrainingData> data = TrainingData::get ( globdat, getContext() );

  // Parallelization stuff

  mpx_ = jive::mp::Globdat::getMPContext ( globdat );

  if ( mpx_ == nullptr )
  {
    throw Error ( JEM_FUNC, String::format (
       "MPContext has not been found" ) );
  }

  if ( mpx_->size() > 1 )
  {
    print ( System::info( myName_ ), getContext(), 
      ": Running in MP mode with ", mpx_->size(), " processes" );
    mpi_ = true;
  }

  if ( !mpi_ )
  {
    print ( System::info( myName_ ), getContext(), 
      ": Running in sequential mode" );
  }

  // Resize some stuff

  idx_t dc = dofs->dofCount();

  g_.resize     ( dc );
  g_ = 0.0;

  if ( mpi_ && mpx_->myRank() == 0 )
  {
    gt_.resize ( dc );
    gt_ = 0.0;
  }

  if ( valSplit_ > 0.0 && skipFirst_ == 0 )
  {
    skipFirst_ = valSplit_ * data->sampleSize();
  }

  return OK;
}

//-----------------------------------------------------------------------
//   shutdown
//-----------------------------------------------------------------------

void SGDModule::shutdown ( const Properties& globdat )
{
  bool root = ( !mpi_ || mpx_->myRank() == 0 );

  if ( root )
  {
    System::out() << "SGDModule statistics ..." 
      << "\n-- total # of epochs: " << epoch_ 
      << "\n-- total optimization time: " << total_ << ", of which"
      << "\n---- " << t5_ << " shuffling the dataset"
      << "\n---- " << t6_ << " updating weights"
      << "\n---- " << t1_ << " computing loss and grads, of which"
      << "\n------ " << t2_ << " allocating sample batches"
      << "\n------ " << t3_ << " propagating through the network"
      << "\n------ " << t4_ << " backpropagating through the network"
      << "\n\n";
  }
}

//-----------------------------------------------------------------------
//   configure
//-----------------------------------------------------------------------

void SGDModule::configure

  ( const Properties&  props,
    const Properties&  globdat )

{
  using jem::maxOf;

  if ( props.contains( myName_ ) )
  {
    Properties  myProps = props.findProps ( myName_ );

    myProps.get  ( lossName_,  LOSSFUNC  );

    func_  = NeuralUtils::getLossFunc ( lossName_ );
    grad_  = NeuralUtils::getLossGrad ( lossName_ );

    myProps.find ( alpha_,     LEARNING_RATE );
    myProps.find ( batchSize_, MINIBATCH     );

    myProps.find ( valSplit_,  VALSPLIT, 0.0, 1.0 );

    if ( valSplit_ == 0.0 )
    {
      myProps.find ( skipFirst_, SKIPFIRST, 0, maxOf ( skipFirst_ ) );
    }
  }
}

//-----------------------------------------------------------------------
//   getConfig
//-----------------------------------------------------------------------

void SGDModule::getConfig

  ( const Properties&  conf,
    const Properties&  globdat ) const

{
  Properties  myConf = conf.makeProps ( myName_ );

  myConf.set ( LOSSFUNC,      lossName_   );
  myConf.set ( LEARNING_RATE, alpha_      );
  myConf.set ( MINIBATCH,     batchSize_  );
  myConf.set ( VALSPLIT,      valSplit_   );
  myConf.set ( SKIPFIRST,     skipFirst_  );
}

//-----------------------------------------------------------------------
//   advance
//-----------------------------------------------------------------------

void SGDModule::advance ( const Properties& globdat )
{
  using jive::util::Globdat;

  globdat.set ( Globdat::TIME_STEP, ++epoch_ );
}

//-----------------------------------------------------------------------
//   solve
//-----------------------------------------------------------------------

void SGDModule::solve

  ( const Properties& info,
    const Properties& globdat )

{
  total_.start();

  if ( mpi_ )
  {
    mpSolve_ ( info, globdat );
  }
  else
  {
    solve_ ( info, globdat );
  }

  total_.stop();
}

//-----------------------------------------------------------------------
//   solveBatch
//-----------------------------------------------------------------------

void SGDModule::solveBatch

  ( const Properties&  info,
    const Properties&  globdat )

{
  Ref<TrainingData> data = TrainingData::get ( globdat, getContext() );
  Ref<DofSpace>     dofs = DofSpace::get     ( globdat, getContext() );
  Ref<Model>        model = Model::get       ( globdat, getContext() );

  Vector state;
  StateVector::get ( state, dofs, globdat );

  idx_t n = data->sampleSize();

  idx_t batch = 0;
  IdxVector valset, trainset;

  double loss;

  if ( skipFirst_ )
  {
    trainset.ref ( IdxVector ( iarray ( slice ( skipFirst_, n ) ) ) );
    n -= skipFirst_;
  }
  else
  {
    trainset.ref ( IdxVector ( iarray ( n ) ) );
  }

  NeuralUtils::shuffle ( trainset, globdat );

  loss = 0.0;

  batch = min ( n, batchSize_ );

  g_ = 0.0;

  loss += eval_ ( trainset[slice(n-batch,n)], true, globdat );    

  g_ /= (double)batch;

  t6_.start();

  state -= alpha_ * g_;

  //System::out() << "Change " << alpha_ * g_ << '\n';
  //System::out() << "alpha " << alpha_ << " grads " << jem::numeric::norm2 ( g_ ) << '\n';
  
  t6_.stop();

  model->takeAction ( LearningActions::UPDATE, Properties(), globdat );

  iiter_++;

  //System::out() << "\n";
  //print ( System::info( myName_ ), getContext(),
  //        " : Minibatch "          , iiter_,
  //        ", batch loss = ", loss / (double)batch, endl );
  //System::out() << "\n";
}

//-----------------------------------------------------------------------
//   getValiError
//-----------------------------------------------------------------------

double SGDModule::getValiError

  ( const Properties&  globdat )

{
  Properties params;

  Ref<TrainingData> data  = TrainingData::get ( globdat, getContext() );
  Ref<DofSpace>     dofs  = DofSpace::get     ( globdat, getContext() );
  Ref<Model>        model = Model::get       ( globdat, getContext() );

  Ref<Normalizer>   onl   = data->getOutNormalizer();

  IdxVector valiset;

  double error      = 0.0;
  double totalerror = 0.0;

  if ( !skipFirst_ )
  {
    return 0.0;
  }

  valiset.ref ( IdxVector ( iarray ( skipFirst_ ) ) );

  Batch samples = data->getData ( valiset );

  idx_t seq  = data->sequenceSize();

  for ( idx_t t = 0; t < seq; ++t )
  {
    params.erase ( LearningParams::STATE );

    if ( t > 0 )
    {
      params.set      ( LearningParams ::STATE,            samples[t-1]   );
    }

    params.set        ( LearningParams ::DATA,             samples[t]     );

    model->takeAction ( LearningActions::RECALL, params, globdat );
  }

  for ( idx_t s = 0; s < skipFirst_; ++s )
  {
    error = 0.0;

    for ( idx_t t = 0; t < seq; ++t )
    {
      Vector out = onl->denormalize ( samples[t]->outputs(ALL,s) );
      Vector tar = onl->denormalize ( samples[t]->targets(ALL,s) );

      error += jem::numeric::norm2 ( tar - out );
    }
    totalerror += error / (double)seq;
  }

  return totalerror / (double)skipFirst_;
}

//-----------------------------------------------------------------------
//   cancel
//-----------------------------------------------------------------------

void SGDModule::cancel ( const Properties& globdat )
{
}

//-----------------------------------------------------------------------
//   commit
//-----------------------------------------------------------------------

bool SGDModule::commit ( const Properties& globdat )
{
  return true;
}

//-----------------------------------------------------------------------
//   setPrecision
//-----------------------------------------------------------------------

void SGDModule::setPrecision ( double eps )
{
}

//-----------------------------------------------------------------------
//   getPrecision
//-----------------------------------------------------------------------

double SGDModule::getPrecision () const
{
  return 0.0;
}

//-----------------------------------------------------------------------
//   mpSolve_
//-----------------------------------------------------------------------

void SGDModule::mpSolve_

  ( const Properties& info,
    const Properties& globdat )

{
  idx_t rank = mpx_->myRank();
  idx_t size = mpx_->size();
  bool  root = ( rank == 0 );
  bool  last = ( rank == size - 1 );

  Ref<TrainingData> data = TrainingData::get ( globdat, getContext() );
  Ref<DofSpace>     dofs = DofSpace::get     ( globdat, getContext() );
  Ref<Model>        model = Model::get       ( globdat, getContext() );

  idx_t dc = dofs->dofCount();

  Vector state;
  StateVector::get ( state, dofs, globdat );

  idx_t n = data->sampleSize();

  idx_t batch = 0, load = 0, end = 0, beg = 0;

  IdxVector valset;   
  IdxVector trainset;

  if ( skipFirst_ )
  {
    print ( System::info( myName_ ), getContext(),
	    " : Epoch "          , epoch_,
	    ", skipping first ", skipFirst_, " samples" );
    trainset.ref ( IdxVector ( iarray ( slice ( skipFirst_, n ) ) ) );
    n -= skipFirst_;
  }
  else
  {
    trainset.ref ( IdxVector ( iarray ( n ) ) );
  }

  t5_.start();
  NeuralUtils::shuffle ( trainset, globdat );
  t5_.stop();

  double trainloss = 0.0;

  while ( n > 0 )
  {
    batch = min ( n, batchSize_    );
    load  = max ( 1, batch / size  );
    end   = max ( n - rank*load, 0 );
    beg   = last ? n - batch : max ( end - load, 0_idx );

    n    -= batch;

    g_ = 0.0;
    gt_ = 0.0;

    double localloss = 0.0;
    
    if ( batch > rank )
    {
      localloss = eval_ ( trainset[slice(beg,end)], true, globdat );    
    }

    double accloss = 0.0;

    mpx_->reduce ( RecvBuffer ( &accloss,   1 ),
                   SendBuffer ( &localloss, 1 ),
		   0,
		   jem::mp::SUM );

    mpx_->reduce ( RecvBuffer ( gt_.addr(), dc ),
		   SendBuffer ( g_. addr(), dc ),
		   0,
		   jem::mp::SUM );

    t6_.start();
    if ( root )
    {
      trainloss += accloss;

      gt_ /= (double)batch;

      state -= alpha_ * gt_;

      mpx_->broadcast ( SendBuffer ( state.addr(), dc ) );
    }

    if ( !root )
    {
      mpx_->broadcast ( RecvBuffer ( state.addr(), dc ), 0 );
    }

    model->takeAction ( LearningActions::UPDATE, Properties(), globdat );

    iiter_++;
    t6_.stop();
  }

  if ( root )
  {
    System::out() << "\n";
    print ( System::info( myName_ ), getContext(),
	    " : Epoch "          , epoch_,
	    ", training loss = ", trainloss / trainset.size(), endl );
    System::out() << "\n";
  }
}


//-----------------------------------------------------------------------
//   solve_
//-----------------------------------------------------------------------

void SGDModule::solve_

  ( const Properties&  info,
    const Properties&  globdat )

{
  Ref<TrainingData> data = TrainingData::get ( globdat, getContext() );
  Ref<DofSpace>     dofs = DofSpace::get     ( globdat, getContext() );
  Ref<Model>        model = Model::get       ( globdat, getContext() );

  Vector state;
  StateVector::get ( state, dofs, globdat );

  idx_t n = data->sampleSize();

  idx_t batch = 0;
  IdxVector valset, trainset;

  double loss;

  if ( skipFirst_ )
  {
    print ( System::info( myName_ ), getContext(),
	    " : Epoch "          , epoch_,
	    ", skipping first ", skipFirst_, " samples" );

    trainset.ref ( IdxVector ( iarray ( slice ( skipFirst_, n ) ) ) );
    n -= skipFirst_;
  }
  else
  {
    trainset.ref ( IdxVector ( iarray ( n ) ) );
  }

  t5_.start();
  NeuralUtils::shuffle ( trainset, globdat );
  t5_.stop();

  loss = 0.0;

  while ( n > 0 )
  {
    batch = min ( n, batchSize_ );

    g_ = 0.0;

    loss += eval_ ( trainset[slice(n-batch,n)], true, globdat );    

    g_ /= (double)batch;

    t6_.start();

    state -= alpha_ * g_;

    //System::out() << "Change " << alpha_ * g_ << '\n';
    //System::out() << "alpha " << alpha_ << " grads " << jem::numeric::norm2 ( g_ ) << '\n';
    
    t6_.stop();

    model->takeAction ( LearningActions::UPDATE, Properties(), globdat );

    iiter_++;

    n -= batch;
  }

  System::out() << "\n";
  print ( System::info( myName_ ), getContext(),
	  " : Epoch "          , epoch_,
	  ", training loss = ", loss / trainset.size(), endl );
  System::out() << "\n";
}

//-----------------------------------------------------------------------
//   eval_ 
//-----------------------------------------------------------------------

double SGDModule::eval_

  ( const IdxVector&  samples,
    const bool        dograds,
    const Properties& globdat )

{
  t1_.start();

  Properties params;
 
  Ref<Model>        model = Model::get        ( globdat, getContext() );
  Ref<TrainingData> tdata = TrainingData::get ( globdat, getContext() );

  double loss = 0.0;

  t2_.start();
  Batch b = tdata->getData ( samples );
  t2_.stop();

  
  for ( idx_t t = 0; t < tdata->sequenceSize(); ++t )
  {
    params.erase ( LearningParams::STATE );

    if ( t > 0 )
    {
      params.set      ( LearningParams ::STATE,             b[t-1]  );
    }

    params.set        ( LearningParams ::DATA,              b[t]    );

    t3_.start();
    model->takeAction ( LearningActions::PROPAGATE, params, globdat );
    t3_.stop();

    loss += func_ ( b[t]->outputs, b[t]->targets );
  }

  if ( dograds )
  {
    for ( idx_t t = tdata->sequenceSize() - 1; t >= 0; --t )
    {
      grad_ ( b[t]->outputs, b[t]->targets );

      params.erase ( LearningParams::STATE );

      if ( t > 0 )
      {
	params.set      ( LearningParams ::STATE,                 b[t-1]  );
      }

      params.set        ( LearningParams ::GRADS,                 g_       );
      params.set        ( LearningParams ::DATA,                  b[t]     );

      t4_.start();
      model->takeAction ( LearningActions::BACKPROPAGATE, params, globdat  );
      t4_.stop();

    }
  }

  t1_.stop();
  
  return loss;
}

//-----------------------------------------------------------------------
//   makeNew
//-----------------------------------------------------------------------


Ref<Module> SGDModule::makeNew

  ( const String&      name,
    const Properties&  conf,
    const Properties&  props,
    const Properties&  globdat )

{
  return newInstance<Self> ( name );
}

//-----------------------------------------------------------------------
//   declare
//-----------------------------------------------------------------------

void SGDModule::declare ()
{
  using jive::app::ModuleFactory;

  ModuleFactory::declare ( TYPE_NAME, & makeNew );
  ModuleFactory::declare ( CLASS_NAME, & makeNew );
}

JIVE_END_PACKAGE( implict )

