
/*
 *  Copyright (C) 2018 TU Delft. All rights reserved.
 *  
 *  This class implements the material model based on
 *  classic Von Mises J2-theory.
 *  This class is a child material member of DamTLSMaterial
 *  classes.
 *
 *  NB.: This class only works on Plane Stress assumptions.
 *       However, the architecture for Plane Strain and 3D
 *       is already defined. According to [1], it is possible
 *       to write the governing equations only related to 
 *       plastic multiplier.
 *  
 *  Author: Luiz A. T. Mororo, l.a.taumaturgomororo@tudelft.nl
 *  Date:   Nov 2018
 *
 *  [1] E.A. de Soiza Neto, D. Peric, D.R.J. Owen. Computational
 *      Methods for Plasticity: Theory and Applications. Jhon Wiley
 *      and Sons, 2008.
 *  
 *  Modified: Iuri Rocha, i.rocha@tudelft.nl
 *  Date: Feb 2022
 *
 *  Adapted to ML use. Switched to linear hardening
 *
 */


#ifndef J2_MATERIAL_H
#define J2_MATERIAL_H


#include <jem/base/Exception.h>
#include <jem/numeric/func/Function.h>
#include <jem/util/Flex.h>
#include <jive/Array.h>


#include "Material.h"
#include "Plasticity.h"
#include "Bayesian.h"
#include "Invariants.h"


using jem::Exception;
using jem::numeric::Function;
using jem::util::Flex;

using jive::StringVector;


//=======================================================================
//   foward-declaration
//=======================================================================
//
//   This class is defined to handle Plane Stress, Plane
//   Strain and 3D cases.


class YieldFunc_;


//=======================================================================
//   class J2Material
//=======================================================================


class J2Material : public Material,
                   public Plasticity,
		   public Bayesian
{
 public:

  static const char*        YOUNG_PROP;
  static const char*        POISSON_PROP;
  static const char*        YIELD_PROP;
  static const char*        HARD_PROP;
  static const char*        ANMODEL_PROP;
  static const char*        RM_TOLERANCE;
  static const char*        RM_MAX_ITER;

  typedef J2Material        Self;

  explicit                  J2Material

    ( const idx_t             rank,
      const Properties&       globdat );

  virtual void              configure

    ( const Properties&       props,
      const Properties&       globdat )                      override;

  virtual void              getConfig

    ( const Properties&       conf,
      const Properties&       globdat )                 const override;

  virtual void              update

    (       Matrix&           stiff,
            Vector&           stress,
      const Vector&           strain,
      const idx_t             ipoint )                     override;

  virtual void              stressAtPoint

    ( Vector&                 stress,
      const Vector&           strain,
      const idx_t             ipoint )                     override;

  virtual void              addTableColumns

    ( IdxVector&              jcols,
      XTable&                 table,
      const String&           name )                       override;

  virtual void              commit                ()       override;

  virtual void              commitOne

    ( const idx_t             ipoint )                     override;

  virtual Ref<Material>       clone ()               const override;

  virtual void              createIntPoints 

    ( const idx_t             ipcount )                    override;

  virtual void              getHistory

    (       Vector&           hvals,
      const idx_t             mpoint )                     override;

  virtual void                setHistory

    ( const Vector&             hvals,
      const idx_t               mpoint )                   override;

  virtual void              getDissipationStress
    
    ( const Vector&           sstar,
      const Vector&           strain,
      const idx_t             ipoint )                     override;

  virtual void                setProps

    ( const Vector&             props );

  virtual Vector              getProps();

  virtual Vector              getFeatures

    ( const idx_t               ipoint );

  virtual idx_t               propCount ();

  virtual void                clearHist ();

   protected:

  virtual                  ~J2Material            ();

 protected:

  class                     Hist_
  {
   public:
                              Hist_();

    void                      toVector ( Vector& vec );
    void                      print () const;

    Vec6                      epsp;       
    double                    epspeq;    
    double                    dissipation;
    bool                      loading;
  };

  Vec6                      tmatmul_

    ( const Matrix&           mat,
      const Vec6&             tuple )                const;

  inline void               t6_to_v6_

    ( const Vector&           t6,
      const Vec6&             v6 )                   const;

  inline void               v6_to_t6_

    (       Vec6&             t6,
      const Vector&           v6 )                   const;

  inline Vec6               v6_to_t6_

    ( const Vector&           v6 )                   const;

  Ref<Function>             makeFunc_

    ( const Properties&       props,
      const String&           name )                 const;

  void                      handleException_ 

    ( const Exception&        ex,
      const Vector&           strain,
      const idx_t             ipoint );

  void                      computeElasticStiff_  ();
  void                      comp2DStiffMat_       ();

  Tuple<double,6>             fill3DStrain_

    ( const Vector&             v3 ) const;

  void                        reduce3DVector_

    ( const Vector&             v,
      const Tuple<double,6>&    t ) const;

  void                        reduce3DMatrix_

    ( const Matrix&             m,
      const Tuple<double,6,6>&  t ) const;

  void                        select2DMatrix_

    ( const Matrix&             m,
      const Tuple<double,6,6>&  t ) const;

 protected: 

  // Need own rank, because base rank is always 3, for 3D stiff.

  idx_t                     j2Rank_;

  // Input properties.

  idx_t                     rank_;
  String                    anmodel_;

  double                    e_;
  double                    n_;
  double                    y_;
  double                    h_;
  double                    rmTolerance_;
  idx_t                     rmMaxIter_;

  Ref<YieldFunc_>           f_;

  // Hardening functions.

  Properties                props_;
  Properties                globdat_;

  // Preallocated arrays.

  Vector                    v61_;
  Vector                    v62_;

  // Hisotry.

  Flex<Hist_>               preHist_;
  Flex<Hist_>               newHist_;
  Flex<Hist_>*              latestHist_;

  IdxVector                 iElemHist_;

  // parameters for data-driven properties

  StringVector              propNames_;
  Vector                    propUpper_;
  Vector                    propLower_;
  Vector                    latestProps_;

 private:

  // stiffness matrix

  Matrix                    stiff_;
  Matrix                    stiff2D_;
};


//=======================================================================
//   class YieldFunc_ (Helper class)
//=======================================================================
//
//   General class to handle yield functions for all 
//   assumptions: Plane Stress, Plane Strain, 3D.

class YieldFunc_ : public Object
{
 public:

                            YieldFunc_  
      
    ( const double            young,
      const double            poisson,
      const double            yield,
      const double            hard    );

  void                      setRmSettings 

    ( const double            rmTolerance, 
      const idx_t             rmMaxIter );

  void                      setElasticStiff

    ( const Matrix&           mat );

  double                    findRoot 
      
    ( const double            dgam0 );

  void                      findBounds

    ( double&                 gmin,
      double&                 gmax,
      double&                 fmin,
      double&                 fmax );

  double                    findRoot

   ( double                   gmin,
     double                   gmax,
     double                   fmin,
     double                   fmax );

  virtual bool              isLinear

    ( const Vec6&             sigtr,
      const double            epspeq0 )                    = 0;

  virtual void              compSigUpdated

    ( Vec6&                   sig, 
      const double            dgam, 
      const Vec6&             sigtr )                      = 0;

  virtual void              compElastStr

    ( Vec6&                   epse )                       = 0;

  virtual void              compPlastStrInc

    ( Vec6&                   depsp, 
      const double            dgam )                       = 0;

  virtual double            compEquPlastInc

    ( const double            dgam )                       = 0;

  virtual Matrix            compTangStiff

    ( const double            dgam )                       = 0;

  inline double             getEpspeq             () const;
  inline Matrix             getElastStiff         () const;

 protected:

  virtual                  ~YieldFunc_            ();


  virtual double            evalTrial_            () const = 0;

  virtual double            eval_

    ( const double            dgam )                       = 0;

  virtual double            evalDer_

    ( const double            dgam )                       = 0;

  virtual double            evalKsi_

    ( const double            dgam )                       = 0; 

  virtual double            evalDerKsi_

    ( const double            dgam )                       = 0; 

  Vec6                      deviatoric_

    ( const Vec6&             full,
      const double            p )                    const;


  Properties                globdat_;

  Matrix                    De_;
  double                    rmTol_;
  double                    E_;
  double                    n_;
  double                    y_;
  double                    h_;
  double                    G_;
  double                    K_;

  double                    epspeq0_;
  double                    epspeq_;
  double                    ksitr_;

  idx_t                     maxIter_;
  idx_t                     rankDep_;  // rank of elastoplastic matrix

 private:

  double                    estimateRoot_

    ( const double            gmin,
      const double            gmax,
      const double            fmin,
      const double            fmax )                 const;

  void                      improveBounds_

    ( double&                 gmin,
      double&                 gmax,
      double&                 fmin,
      double&                 fmax );
};


//=======================================================================
//   class YieldFunc3D_ (Helper class)
//=======================================================================
//
//   This class deals with 3D and Plane Strain problems.

class YieldFunc3D_ : public YieldFunc_
{
 public:

                            YieldFunc3D_  
      
    ( const double            young,
      const double            poisson,
      const double            yield,
      const double            hard    );

  bool                      isLinear

    ( const Vec6&             sigtr,
      const double            epspeq0 )                    override;

  void                      compSigUpdated

    ( Vec6&                   sig, 
      const double            dgam, 
      const Vec6&             sigtr )                      override;

  virtual void              compElastStr

    ( Vec6&                   epse )                       override;

  void                      compPlastStrInc

    ( Vec6&                   depsp, 
      const double            dgam )                       override;

  double                    compEquPlastInc

    ( const double            dgam )                       override;

  Matrix                    compTangStiff

    ( const double            dgam )                       override;

 protected:

  virtual                  ~YieldFunc3D_          ();

  double                    evalTrial_            () const override;

  double                    eval_

    ( const double            dgam )                       override;

  double                    evalDer_

    ( const double            dgam )                       override; 

  double                    evalKsi_

    ( const double            dgam )                       override; 

  double                    evalDerKsi_

    ( const double            dgam )                       override; 

 
  Vec6                      sigDtr_;                 
  Vec6                      sigD_;
  double                    ptrial_;

 private:

  Tuple<double,6,6>         getIdMatrix_          ();

  double                    normSig_

    ( const Vec6&             sig )                  const;

  void                      m66_to_m33_     
  
    ( const Matrix&           m33,
      const Matrix&           m66 )                  const;
};


//=======================================================================
//   class YieldFuncPS_ (Helper class)
//=======================================================================
//
//   This class deals with Plane Stress problems.

class YieldFuncPS_ : public YieldFunc_
{
 public:

                            YieldFuncPS_  
      
    ( const double            young,
      const double            poisson,
      const double            yield,
      const double            hard    );

  bool                      isLinear

    ( const Vec6&             sigtr,
      const double            epspeq0 )                    override;

  void                      compSigUpdated

    ( Vec6&                   sig, 
      const double            dgam, 
      const Vec6&             sigtr )                      override;

  virtual void              compElastStr

    ( Vec6&                   epse )                       override;

  void                      compPlastStrInc

    ( Vec6&                   depsp, 
      const double            dgam )                       override;

  double                    compEquPlastInc

    ( const double            dgam )                       override;

  Matrix                    compTangStiff

    ( const double            dgam )                       override;

 protected:

  virtual                  ~YieldFuncPS_          ();

  double                    evalTrial_            () const override;

  double                    eval_

    ( const double            dgam )                       override;

  double                    evalDer_

    ( const double            dgam )                       override; 

  double                    evalKsi_

    ( const double            dgam )                       override; 

  double                    evalDerKsi_

    ( const double            dgam )                       override; 


  Tuple<double,3,3>         P_;
  Vec3                      stress_;
  double                    a1tr_;
  double                    a2tr_;
  double                    a3tr_;

 private:

  Tuple<double,3,3>         getAMatrix_

    ( const double            dgam )                 const; 

  Tuple<double,3,3>         getEMatrix_

    ( const double            dgam )                 const; 

  inline void               t6_to_t3_

    (       Vec3&             t3,
      const Vec6&             t6 )                   const;

  inline void               t3_to_t6_

    (       Vec6&             t6,
      const Vec3&             t3 )                   const;

  inline void               ep3_to_ep6_

    (       Vec6&             t6,
      const Vec3&             t3 )                   const;

  inline void               ee3_to_ee6_

    (       Vec6&             t6,
      const Vec3&             t3 )                   const;

  void                      t33_to_m33_

    ( const Matrix&           m,
      const Tuple<double,3,3>& 
                              t )                    const; 
};


//#######################################################################
//   Implementation
//#######################################################################


//-----------------------------------------------------------------------
//   J2Material::v6_to_t6_
//-----------------------------------------------------------------------


void J2Material::v6_to_t6_

  (       Vec6&   t6,
    const Vector& v6 ) const

{
  t6[0] = v6[0]; t6[1] = v6[1]; t6[2] = v6[2]; 
  t6[3] = v6[3]; t6[4] = v6[4]; t6[5] = v6[5]; 
}


Vec6 J2Material::v6_to_t6_

  ( const Vector& v6 ) const

{
  Vec6 t6;
  v6_to_t6_ ( t6, v6 );
  return t6;
}


//-----------------------------------------------------------------------
//   J2Material::t6_to_v6_
//-----------------------------------------------------------------------


void J2Material::t6_to_v6_

  ( const Vector& v6,
    const Vec6&   t6 ) const

{
  v6[0] = t6[0]; v6[1] = t6[1]; v6[2] = t6[2]; 
  v6[3] = t6[3]; v6[4] = t6[4]; v6[5] = t6[5]; 
}


//-----------------------------------------------------------------------
//   YieldFunc_::getEpspeq
//-----------------------------------------------------------------------


inline double YieldFunc_::getEpspeq

  () const

{
  return epspeq_;
}


//-----------------------------------------------------------------------
//   YieldFunc_::getElastStiff
//-----------------------------------------------------------------------


inline Matrix YieldFunc_::getElastStiff   

  () const

{
  return De_;
}


//-----------------------------------------------------------------------
//   YieldFuncPS_::t6_to_t3_ 
//-----------------------------------------------------------------------


void YieldFuncPS_::t6_to_t3_

  (       Vec3&  t3,
    const Vec6&  t6 ) const

{
  t3[0] = t6[0]; 
  t3[1] = t6[1]; 
  t3[2] = t6[3]; 
}


//-----------------------------------------------------------------------
//   YieldFuncPS_::t3_to_t6_ 
//-----------------------------------------------------------------------


void YieldFuncPS_::t3_to_t6_

  (       Vec6&  t6,
    const Vec3&  t3 ) const

{
  t6[0] = t3[0]; 
  t6[1] = t3[1]; 
  t6[2] = 0.; 
  t6[3] = t3[2]; 
  t6[4] = 0.; 
  t6[5] = 0.; 
}


//-----------------------------------------------------------------------
//   YieldFuncPS_::ep3_to_ep6_ 
//-----------------------------------------------------------------------
// This member takes into account the out-of-plane component of plastic
// strain.


void YieldFuncPS_::ep3_to_ep6_

  (       Vec6&  ep6,
    const Vec3&  ep3 ) const

{
  ep6[0] = ep3[0]; 
  ep6[1] = ep3[1]; 
  ep6[2] = -ep3[0]-ep3[1]; // out-of-plane component 
  ep6[3] = ep3[2]; 
  ep6[4] = 0.; 
  ep6[5] = 0.; 
}

//-----------------------------------------------------------------------
//   YieldFuncPS_::ee3_to_ee6_ 
//-----------------------------------------------------------------------
// This member takes into account the out-of-plane component of plastic
// strain.


void YieldFuncPS_::ee3_to_ee6_

  (       Vec6&  ee6,
    const Vec3&  ee3 ) const

{
  ee6[0] = ee3[0]; 
  ee6[1] = ee3[1]; 
  ee6[2] = -n_/(1.-n_) * (ee3[0]+ee3[1]); // out-of-plane component 
  ee6[3] = ee3[2]; 
  ee6[4] = 0.; 
  ee6[5] = 0.; 
}

#endif 
