/*
 * Copyright (C) 2021 TU Delft. All rights reserved.
 *  
 * This class implements the  export of mesh data for
 * visualization with ParaView
 *
 * Author: T. Gaertner (t.gartner@tudelft.nl)
 * Date: June 21
 *
 */

#include "ParaViewModule.h"

//=======================================================================
//   class ParaViewModule
//=======================================================================

//-----------------------------------------------------------------------
//   static data
//-----------------------------------------------------------------------

const char*   ParaViewModule::TYPE_NAME = "ParaView";
const char*   ParaViewModule::SPACING   = "    ";

//-----------------------------------------------------------------------
//   constructor
//-----------------------------------------------------------------------

ParaViewModule::ParaViewModule 

  ( const String&            name ) : Module ( name ) 

{
  // default Values for internal Variables
  nameFormat_       = "para_%d";
  fileType_         = "vtu";
  elemSets_.resize  ( 0 );
  setInfo_.resize   ( 0 );  
  report_intervall_ = 1;
}

//-----------------------------------------------------------------------
//   init
//-----------------------------------------------------------------------

Module::Status    ParaViewModule::init

(     const Properties&        conf,
      const Properties&        props,
      const Properties&        globdat      )

{
  Properties  myProps = props.findProps ( myName_ );
  Properties  myConf  = conf .makeProps ( myName_ );

  myProps.find( nameFormat_, "output_format" );
  myConf .set ( "output_format", nameFormat_ );  

  myProps.find( fileType_, "output_file" );
  myConf .set ( "output_file", fileType_ ); 

  myProps.find( report_intervall_, "reportIntervall" );
  myConf .set ( "reportIntervall", report_intervall_ );

  myProps.get ( elemSets_, "groups" );
  myConf .set ( "groups", elemSets_ );

  setInfo_.resize ( elemSets_.size() );

  for (idx_t igroup = 0; igroup < elemSets_.size(); igroup++)
  {
    Properties groupProps = myProps.getProps  ( elemSets_[igroup] );
    Properties groupConf  = myConf .makeProps ( elemSets_[igroup] );

    setInfo_[igroup].name = elemSets_[igroup];

    groupProps.get ( setInfo_[igroup].shape, "shape" );
    nameToVTKNum( setInfo_[igroup].shape );
    groupConf .set ( "shape", setInfo_[igroup].shape );
    
    groupProps.get ( setInfo_[igroup].dispData, "disps" );
    groupConf .set ( "disps", setInfo_[igroup].dispData );

    if (groupProps.find( setInfo_[igroup].dofData, "otherDofs" ))
        groupConf .set ( "otherDofs", setInfo_[igroup].dofData );
    
    groupProps.find( setInfo_[igroup].elemData, "el_data" );
    groupConf .set ( "el_data", setInfo_[igroup].elemData );
    
    groupProps.find( setInfo_[igroup].nodeData, "node_data" );
    groupConf .set ( "node_data", setInfo_[igroup].nodeData );
  }

  // construct the file folder, if it does not exist
  //boost::filesystem::path paraFolder = makeCString(nameFormat_).addr();
  //paraFolder.remove_filename();
  //boost::filesystem::create_directories( paraFolder );

  return OK;
}

//-----------------------------------------------------------------------
//   run
//-----------------------------------------------------------------------

Module::Status     ParaViewModule::run

    ( const Properties&        globdat )

{
  idx_t                   currentStep;
  String                  currentFile;

  // get the current timeStep and format the given format accordingly
  globdat.get ( currentStep, Globdat::TIME_STEP);
  currentFile = String::format ( makeCString(nameFormat_).addr(), currentStep/report_intervall_);
  currentFile = currentFile + "." + fileType_;

  // write everything to file
  if ( currentStep % report_intervall_ == 0) writeFile_ ( currentFile, globdat );

  return OK;
}

//-----------------------------------------------------------------------
//   writeFile_
//-----------------------------------------------------------------------
void      ParaViewModule::writeFile_

    ( const String&         fileName,
      const Properties&     globdat )

{
  Ref<Writer>       file_raw  = newInstance<FileWriter>  ( fileName );
  Ref<PrintWriter>  file_frmt = newInstance<PrintWriter> ( file_raw );

  // Output Formatting
  file_frmt->nformat.setScientific     ( true ); 
  file_frmt->nformat.setFractionDigits ( 8 );
  file_frmt->nformat.setFloatWidth     ( 15 ); //1sign + 1digit + 1decimal + 8digits + 1e + 1sign + 2digits
  file_frmt->setIndentWidth            ( 2 );

  // Write Header
  // see http://www.vtk.org/wp-content/uploads/2015/04/file-formats.pdf
  *file_frmt << "<?xml version=\"1.0\"?>" << endl;
  *file_frmt << "<VTKFile type=\"UnstructuredGrid\" version=\"0.1\">" << endl;
  file_frmt->incrIndentLevel ();
  *file_frmt << "<UnstructuredGrid>" << endl;
  file_frmt->incrIndentLevel ();

  // iterate over all the groups
  for (idx_t igroup = 0; igroup < elemSets_.size(); igroup++)
  {
    // get the current standings elements and corresponding nodes
    Assignable<ElementSet>  elems   = ElementSet::get ( globdat, getContext() );
    Assignable<ElementGroup>egroup  = ElementGroup::get ( elemSets_[igroup], elems, globdat, getContext() );    
    Assignable<NodeSet>     nodes   = elems.getNodes();

    // get the current displacements
    Ref<DofSpace>           dofs    = DofSpace::get ( globdat, getContext() );
    Vector                  disp;
    StateVector::get        ( disp, dofs, globdat );

    // get the current model
    Ref<Model>              model   = Model::get ( globdat, getContext() );

    writePiece_ ( file_frmt, nodes, elems, egroup, disp, dofs, model, globdat, setInfo_[igroup] );
  }

  file_frmt->decrIndentLevel ();
  *file_frmt << "</UnstructuredGrid>" << endl;
  file_frmt->decrIndentLevel ();
  *file_frmt << "</VTKFile>";

  // close the stream
  file_frmt->close ();
}

//-----------------------------------------------------------------------
//   writePiece_
//-----------------------------------------------------------------------

void       ParaViewModule::writePiece_

  ( const Ref<PrintWriter>&         file,
    const Assignable<NodeSet>&      points,
    const Assignable<ElementSet>&   cells,
    const Assignable<ElementGroup>& group,
    const Vector&                   disp,
    const Ref<DofSpace>&            dofs,
    const Ref<Model>&               model,
    const Properties&               globdat,
    const elInfo&                   info   )

{
  IdxVector groupNodes = group.getNodeIndices();

  jem::sort ( groupNodes );

  *file << "<Piece "
    << "NumberOfPoints=\"" << groupNodes.size() << "\" "
    << "NumberOfCells=\"" << group.size() << "\""
    << ">" << endl;
  file->incrIndentLevel ();

  // Write the points to the file
  *file << "<Points>" << endl;
  file->incrIndentLevel ();
  *file << "<DataArray type=\"Float32\" NumberOfComponents=\"" << 3 << "\">" << endl;
  file->incrIndentLevel ();

  for (idx_t inode = 0; inode < groupNodes.size(); inode++)
  //for ( idx_t inode = 0; inode < points.size(); ++inode )
  {
    Vector  coords ( points.rank() );
    points.getNodeCoords ( coords, groupNodes[inode] );
    //points.getNodeCoords ( coords, inode );

    *file << coords[0] << SPACING 
      << (points.rank() >= 2 ? coords[1] : 0.0) << SPACING
      << (points.rank() >= 3 ? coords[2] : 0.0) << SPACING
      << endl;
  }
  file->decrIndentLevel ();
  *file << "</DataArray>" << endl;
  file->decrIndentLevel ();
  *file << "</Points>" << endl;

  // Write the elements to the file
  IdxVector       offsets ( group.size () );
  IdxVector       types   ( group.size () );
  *file << "<Cells>" << endl;
  file->incrIndentLevel ();

  *file << "<DataArray type=\"Int32\" Name=\"connectivity\">" << endl;
  file->incrIndentLevel ();
  // iterate through the elements
  for (idx_t ie = 0; ie < group.size(); ie++)
  {
    idx_t ielem = group.getIndices()[ie];

    IdxVector     elNodes   ( cells.getElemNodeCount ( ielem ) );
    cells.getElemNodes( elNodes, ielem );
    
    IdxVector     paraNodes = gmsh2ParaNodeOrder ( elNodes, info.shape );

    offsets[ie] = elNodes.size();   
    types[ie] = nameToVTKNum( info.shape );

    for (idx_t inode = 0; inode < elNodes.size(); inode++)
    {
      *file << jem::binarySearch ( paraNodes[inode], groupNodes ) << SPACING;
    }    
    *file << endl;
  }
  file->decrIndentLevel ();
  *file << "</DataArray>" << endl;

  *file << "<DataArray type=\"Int32\" Name=\"offsets\">" << endl;
  file->incrIndentLevel ();
  for (idx_t ielem = 0; ielem < group.size(); ielem++)
  {
    *file << sum( offsets[SliceFromTo(0, ielem+1)] ) << endl;
  }
  file->decrIndentLevel ();
  *file << "</DataArray>" << endl;

  *file << "<DataArray type=\"UInt8\" Name=\"types\">" << endl;
  file->incrIndentLevel ();
  for (idx_t ielem = 0; ielem < group.size(); ielem++)
  {
    *file << types[ielem] << endl;
  }
  file->decrIndentLevel ();
  *file << "</DataArray>" << endl;

  file->decrIndentLevel ();
  *file << "</Cells>" << endl;

  // Write the pointdata to the file
  *file << "<PointData>" << endl;
  file->incrIndentLevel ();

  // Start by writing Displacements
  IdxVector   iDofs           ( info.dispData.size() );  
  IdxVector   iDisps          ( info.dispData.size() );
  //Matrix      disp_mat        ( groupNodes.size(), info.dispData.size() );
  Matrix      disp_mat        ( groupNodes.size(), 3 );

  disp_mat = 0.0;

  for (idx_t idof = 0; idof < iDisps.size(); idof++)
  {
    iDofs[idof] = dofs->findType ( info.dispData[idof] );
  }

  for (idx_t ipoint = 0; ipoint < groupNodes.size(); ipoint++)
  {
    dofs->getDofIndices( iDisps, groupNodes[ipoint], iDofs ); 
    disp_mat(ipoint, slice(0,info.dispData.size())) = disp[iDisps];
  }

  writeDataArray_ ( file, disp_mat, "Float32", "Displacement" );

  // Next write other Dofs
  if (info.dofData.size() > 0)
  {
    iDofs.resize           ( info.dofData.size() );  
    iDisps.resize          ( info.dofData.size() );
    disp_mat .resize       ( groupNodes.size(), info.dofData.size() );

    for (idx_t idof = 0; idof < iDisps.size(); idof++)
    {
      iDofs[idof] = dofs->findType ( info.dofData[idof] );
    }

    for (idx_t ipoint = 0; ipoint < groupNodes.size(); ipoint++)
    {
      dofs->getDofIndices( iDisps, groupNodes[ipoint], iDofs ); 
      disp_mat(ipoint, ALL) = disp[iDisps];
    }

    writeDataArray_ ( file, disp_mat, "Float32", "otherDofs" );
  }

  // iterate through all other data ( see OutputModule for more advanced features )

  for (idx_t iPtDatum = 0; iPtDatum < info.nodeData.size(); iPtDatum++)
  {        
    using jive::model::ActionParams;
    Properties     params     ( "actionParams" );
    
    Ref<ItemSet>   pointSet   = ItemSet::get   ( "nodes", globdat, getContext() );

    Ref<XTable>    datumTable = newInstance<SparseTable>( "paraView", pointSet );
    Vector         weights    ( datumTable->rowCount() );

    weights = 0.;

    params.set ( ActionParams::TABLE,         datumTable );
    params.set ( ActionParams::TABLE_NAME,    info.nodeData[iPtDatum] );
    params.set ( ActionParams::TABLE_WEIGHTS, weights );

    model->takeAction ( Actions::GET_TABLE, params, globdat );

    params.erase ( ActionParams::TABLE_NAME );
    params.erase ( ActionParams::TABLE );
    params.erase ( ActionParams::TABLE_WEIGHTS );

    weights = where ( abs( weights ) < Limits<double>::TINY_VALUE,
			1.0, 1.0 / weights );
    datumTable->scaleRows ( weights );

    const idx_t ncols = datumTable->columnCount();
    const idx_t nrows = groupNodes.size();
    Matrix      mat     ( nrows, ncols );

    mat = 0.0;

    IdxVector   icols   ( jem::iarray ( ncols ) );
    IdxVector   irows   ( groupNodes.clone() );

    datumTable->getBlock ( mat, irows, icols );
    writeDataArray_      ( file, mat, "Float32", info.nodeData[iPtDatum] );

    //writeDataArray_ ( file, datumTable, "Float32", info.nodeData[iPtDatum] ); 
  }
  
  file->decrIndentLevel ();
  *file << "</PointData>" << endl;
  
  // Write the cell data to the file
  *file << "<CellData>" << endl;
  file->incrIndentLevel ();

  // iterate through all desired cell (element) data ( see OutputModule for more advanced features )
  // IR: disabled for now, might not work for models with multiple element groups

  //for (idx_t iElDatum = 0; iElDatum < info.elemData.size(); iElDatum++)
  //{    
  //  using jive::model::ActionParams;
  //  
  //  Ref<ItemSet>   cellSet   = ItemSet::get   ( "elements", globdat, getContext() );

  //  Ref<XTable>    datumTable = newInstance<SparseTable>( "paraView", cellSet );
  //  Vector         weights    ( datumTable->rowCount() );
  //  Properties     params     ( "actionParams" );

  //  weights = 0.;

  //  params.set ( ActionParams::TABLE_NAME,    info.elemData[iElDatum] );
  //  params.set ( ActionParams::TABLE,         datumTable );
  //  params.set ( ActionParams::TABLE_WEIGHTS, weights );

  //  model->takeAction ( Actions::GET_TABLE, params, globdat );

  //  params.erase ( ActionParams::TABLE_NAME );
  //  params.erase ( ActionParams::TABLE );
  //  params.erase ( ActionParams::TABLE_WEIGHTS );

  //  weights = where ( abs( weights ) < Limits<double>::TINY_VALUE,
  //                      1.0, 1.0 / weights );
  //  datumTable->scaleRows ( weights );

  //  writeDataArray_ ( file, datumTable, "Float32", info.elemData[iElDatum] );   
  //}

  file->decrIndentLevel ();
  *file << "</CellData>" << endl;

  file->decrIndentLevel ();
  *file << "</Piece>" << endl;
}


//-----------------------------------------------------------------------
//   writeDataArray
//-----------------------------------------------------------------------

void      ParaViewModule::writeDataArray_

( const Ref<PrintWriter>&       file,
  const Matrix&                 data,
  const String&                 type,
  const String&                 name   )
{
  *file << "<DataArray type=\"" << type << "\" Name=\"" << name << "\" NumberOfComponents=\"" << data.shape()[1] << "\">" << endl;
  file->incrIndentLevel ();
  
  for (idx_t iRow = 0; iRow < data.shape()[0]; iRow++)
  {
    for (idx_t iColumn = 0; iColumn < data.shape()[1]; iColumn++)
    {
      *file << (float)data(iRow, iColumn) << SPACING; // float since ParaView can only read single precision floats
    }
    *file << endl;
  } 

  file->decrIndentLevel ();
  *file << "</DataArray>" << endl;
}

void      ParaViewModule::writeDataArray_

( const Ref<PrintWriter>&       file,
  const Ref<XTable>&            data,
  const String&                 type,
  const String&                 name   )
{
  const idx_t columns = data->columnCount();
  const idx_t rows    = data->size()/columns;
  Matrix      mat     ( rows, columns  );

  IdxVector   icols   ( columns );
  IdxVector   irows   ( rows );

  for ( idx_t icol = 0; icol<columns; icol++) icols[icol] = icol;
  for ( idx_t irow = 0; irow<rows;    irow++) irows[irow] = irow;

  data->getBlock      ( mat, irows, icols );
  writeDataArray_     ( file, mat, type, name );
}

void      ParaViewModule::writeDataArray_

( const Ref<PrintWriter>&       file,
  const Vector&                 data,
  const String&                 type,
  const String&                 name   )
{
  Matrix  mat   ( data.size(), 1 );

  mat ( ALL, 0 ) = data;

  writeDataArray_ ( file, mat, type, name );
}

//-----------------------------------------------------------------------
//   makeNew
//-----------------------------------------------------------------------

Ref<Module>      ParaViewModule::makeNew

  ( const String&      name,
    const Properties&  conf,
    const Properties&  props,
    const Properties&  globdat            )

{
  return newInstance<ParaViewModule> ( name );
}

//-----------------------------------------------------------------------
//   declare
//-----------------------------------------------------------------------

void              ParaViewModule::declare ()
{
  using jive::app::ModuleFactory;

  ModuleFactory::declare ( TYPE_NAME, & makeNew );
}

//-----------------------------------------------------------------------
//   nameToVTKNum
//-----------------------------------------------------------------------

idx_t             ParaViewModule::nameToVTKNum

    ( const String&      name               )
{
  idx_t cellType = 0;

    // 1D elements
  if (name == "Line2")
  {
    cellType = 3;
  }
  else if (name == "Line3")
  {
    cellType = 21;
  }
  else if (name == "Line4")
  {
    cellType = 35;
  }
  // 2D elements
  else if (name == "Quad4")
  {
    cellType = 9;
  }
  else if (name == "Quad8")
  {
    cellType = 23;
  }
  else if (name == "Triangle3")
  {
    cellType = 5;
  }
  else if (name == "Triangle6")
  {
    cellType = 22;
  }
  // 3D elements
  else if (name == "Hex8")
  {
    cellType = 12;
  }
  else if (name == "Hex20")
  {
    cellType = 25;
  }
  else if (name == "Tet4")
  {
    cellType = 10;
  }
  else if (name == "Tet10")
  {
    cellType = 24;
  }
  else
  {
    throw IllegalArgumentException ( "ParaViewModule", "Model type " + name + " unkown" );
  }

  return cellType;
}

//-----------------------------------------------------------------------
//   gmsh2ParaNodeOrder
//-----------------------------------------------------------------------

IdxVector             ParaViewModule::gmsh2ParaNodeOrder

    ( const IdxVector    elNodes,
      const String&      name               )
{
  IdxVector paraViewNodes ( elNodes.size() );

  // paraview : http://www.vtk.org/wp-content/uploads/2015/04/file-formats.pdf ---> pages 9 and 10
  // gmsh     : http://gmsh.info/doc/texinfo/gmsh.html  ---> section 9.3


  /*
  1D elements


          Line2:                  Line3:
  Gmsh:
      0----------1 --> u      0-----2-----1

  ParaView:

      0----------1 --> u      0-----1-----2

  */

  if (name == "Line2")
    {
      paraViewNodes[0] = elNodes[0];
      paraViewNodes[1] = elNodes[1];
    }
  else if (name == "Line3")
    {
      paraViewNodes[0] = elNodes[0];
      paraViewNodes[1] = elNodes[2];
      paraViewNodes[2] = elNodes[1];
    }
  else if (name == "Line4")
   {     
      paraViewNodes[0] = elNodes[0];
      paraViewNodes[1] = elNodes[3];
      paraViewNodes[2] = elNodes[1];      
      paraViewNodes[3] = elNodes[2];
   }

  /*
  2D elements


  Quadrangle:            Quadrangle8:            Quadrangle9:
  Gmsh:
        v
        ^
        |
  3-----------2          3-----6-----2           3-----6-----2
  |     |     |          |           |           |           |
  |     |     |          |           |           |           |
  |     +---- | --> u    7           5           7     8     5
  |           |          |           |           |           |
  |           |          |           |           |           |
  0-----------1          0-----4-----1           0-----4-----1

  ParaView:
        v
        ^
        |
  3-----------2          6-----5-----4           6-----5-----4
  |     |     |          |           |           |           |
  |     |     |          |           |           |           |
  |     +---- | --> u    7           3           7     8     3
  |           |          |           |           |           |
  |           |          |           |           |           |
  0-----------1          0-----1-----2           0-----1-----2
  */
  else if (name == "Quad4")
    {
      paraViewNodes[0] = elNodes[0];
      paraViewNodes[1] = elNodes[1];
      paraViewNodes[2] = elNodes[2];
      paraViewNodes[3] = elNodes[3];
    }
  else if (name == "Quad8")
    {
      paraViewNodes[0] = elNodes[0];
      paraViewNodes[1] = elNodes[2];
      paraViewNodes[2] = elNodes[4];
      paraViewNodes[3] = elNodes[6];
      paraViewNodes[4] = elNodes[1];
      paraViewNodes[5] = elNodes[3];
      paraViewNodes[6] = elNodes[5];
      paraViewNodes[7] = elNodes[7];
    }
  else if (name == "Quad9")
    {
      paraViewNodes[0] = elNodes[0];
      paraViewNodes[1] = elNodes[2];
      paraViewNodes[2] = elNodes[4];
      paraViewNodes[3] = elNodes[6];
      paraViewNodes[4] = elNodes[1];
      paraViewNodes[5] = elNodes[3];
      paraViewNodes[6] = elNodes[5];
      paraViewNodes[7] = elNodes[7];
      paraViewNodes[8] = elNodes[8];
    }

  /*
  Triangle:               Triangle6:          Triangle9/10:          Triangle12/15:

  Gmsh:

  v
  ^                                                                   2
  |                                                                   | \
  2                       2                    2                      9   8
  |`\                     |`\                  | \                    |     \
  |  `\                   |  `\                7   6                 10 (14)  7
  |    `\                 5    `4              |     \                |         \
  |      `\               |      `\            8  (9)  5             11 (12) (13) 6
  |        `\             |        `\          |         \            |             \
  0----------1 --> u      0-----3----1         0---3---4---1          0---3---4---5---1

  ParaView:

  v
  ^                                                                   8
  |                                                                   | \
  2                       4                    6                      9   7
  |`\                     |`\                  | \                    |     \
  |  `\                   |  `\                7   5                 10 (14)  6
  |    `\                 5    `3              |     \                |         \
  |      `\               |      `\            8  (9)  4             11 (12) (13) 5
  |        `\             |        `\          |         \            |             \
  0----------1 --> u      0-----1----2         0---1---2---3          0---1---2---3---4

  */
  else if (name == "Triangle3")
    {
      paraViewNodes[0] = elNodes[0];
      paraViewNodes[1] = elNodes[1];
      paraViewNodes[2] = elNodes[2];
    }
  else if (name == "Triangle6")
    {
      paraViewNodes[0] = elNodes[0];
      paraViewNodes[1] = elNodes[2];
      paraViewNodes[2] = elNodes[4];
      paraViewNodes[3] = elNodes[1];
      paraViewNodes[4] = elNodes[3];
      paraViewNodes[5] = elNodes[5];
    }

  /*

  3D elements

  Hexahedron8:            Hexahedron20:          Hexahedron27:

  Gmsh:
          v
  3----------2            3----13----2           3----13----2
  |\     ^   |\           |\         |\          |\         |\
  | \    |   | \          | 15       | 14        |15    24  | 14
  |  \   |   |  \         9  \       11 \        9  \ 20    11 \
  |   7------+---6        |   7----19+---6       |   7----19+---6
  |   |  +-- |-- | -> u   |   |      |   |       |22 |  26  | 23|
  0---+---\--1   |        0---+-8----1   |       0---+-8----1   |
    \  |    \  \  |         \  17      \  18       \ 17    25 \  18
    \ |     \  \ |         10 |        12|        10 |  21    12|
      \|      w  \|           \|         \|          \|         \|
      4----------5            4----16----5           4----16----5

  ParaView:


  */

  else if (name == "Hex8")
    {
      paraViewNodes[0] = elNodes[0];
      paraViewNodes[1] = elNodes[1];
      paraViewNodes[2] = elNodes[3];
      paraViewNodes[3] = elNodes[2];
      paraViewNodes[4] = elNodes[4];
      paraViewNodes[5] = elNodes[5];
      paraViewNodes[6] = elNodes[7];
      paraViewNodes[7] = elNodes[6];
    }
  else if (name == "Hex20")
    {
      paraViewNodes[0] = elNodes[0];
      paraViewNodes[1] = elNodes[2];
      paraViewNodes[2] = elNodes[4];
      paraViewNodes[3] = elNodes[6];
      paraViewNodes[4] = elNodes[12];
      paraViewNodes[5] = elNodes[14];
      paraViewNodes[6] = elNodes[16];
      paraViewNodes[7] = elNodes[18];
      paraViewNodes[8] = elNodes[1];
      paraViewNodes[9] = elNodes[3];
      paraViewNodes[10] = elNodes[5];
      paraViewNodes[11] = elNodes[7];
      paraViewNodes[12] = elNodes[13];
      paraViewNodes[13] = elNodes[15];
      paraViewNodes[14] = elNodes[17];
      paraViewNodes[15] = elNodes[19];
      paraViewNodes[16] = elNodes[8];
      paraViewNodes[17] = elNodes[9];
      paraViewNodes[18] = elNodes[10];
      paraViewNodes[19] = elNodes[11];
    }

  /*

        Tetrahedron4:                         Tetrahedron10:

  Gmsh:
                      v
                    .
                  ,/
                /
              2                                     2
            / | \                                 / | \
          ,/  |  `\                             ,/  |  `\
        ,/    '.   `\                         ,6    '.   `5
      ,/       |     `\                     ,/       8     `\
    ,/         |       `\                 ,/         |       `\
  0-----------'.--------1 --> u         0--------4--'.--------1
    `\.         |      ,/                 `\.         |      ,/
      `\.      |    ,/                      `\.      |    ,9
          `\.   '. ,/                           `7.   '. ,/
            `\. |/                                `\. |/
                `3                                    `3
                  `\.
                      ` w

  ParaView:

  */

  else if (name == "Tet4")
    {
      paraViewNodes[0] = elNodes[0];
      paraViewNodes[1] = elNodes[1];
      paraViewNodes[2] = elNodes[2];
      paraViewNodes[3] = elNodes[3];
    }
  else if (name == "Tet10")
    {
      paraViewNodes[0] = elNodes[0];
      paraViewNodes[1] = elNodes[1];
      paraViewNodes[2] = elNodes[2];
      paraViewNodes[3] = elNodes[3];
      paraViewNodes[4] = elNodes[4];
      paraViewNodes[5] = elNodes[5];
      paraViewNodes[6] = elNodes[6];
      paraViewNodes[7] = elNodes[8]; // here
      paraViewNodes[8] = elNodes[7]; // here
    }
  else
    {
      throw IllegalArgumentException ( "Model type " + name + " unkown" );
    }

  return paraViewNodes;
}
