/*
 * Copyright (C) 2021 TU Delft. All rights reserved.
 *
 *  Material shell that takes stress/strain/history observations
 *  from anchor models and prints them to a file.
 *
 * Author: Iuri Rocha, i.rocha@tudelft.nl
 * Date:   Apr 2021
 *
 */

#include <jem/base/Error.h>
#include <jem/base/System.h>
#include <jem/base/Array.h>
#include <jem/util/Properties.h>
#include <jem/numeric/algebra/matmul.h>
#include <jem/numeric/algebra/utilities.h>
#include <jem/numeric/algebra/LUSolver.h>
#include <jem/numeric/algebra/Cholesky.h>
#include <jem/io/FileReader.h>
#include <jem/io/PrintWriter.h>
#include <jem/io/FileWriter.h>
#include <jem/io/FileFlags.h>
#include <jem/io/PatternLogger.h>
#include <jive/model/Model.h>
#include <jive/model/StateVector.h>
#include <jive/app/UserconfModule.h>
#include <jive/app/InfoModule.h>
#include <jive/fem/InitModule.h>
#include <jive/fem/ElementSet.h>
#include <jive/fem/NodeSet.h>
#include <jive/util/Random.h>
#include <jive/util/Globdat.h>
#include <jive/util/utilities.h>
#include <jive/util/DofSpace.h>
#include <jive/util/Assignable.h>
#include <jive/implict/SolverInfo.h>
#include <jive/algebra/FlexMatrixBuilder.h>
#include <jive/algebra/SparseMatrixObject.h>

#include "ObserverMaterial.h"

#include "utilities.h"
#include "LearningNames.h"
#include "SolverNames.h"
#include "MelroMaterial.h"
#include "NData.h"
#include "NeuralUtils.h"

using namespace jem;

using jem::numeric::norm2;
using jem::numeric::matmul;
using jem::numeric::LUSolver;
using jem::numeric::Cholesky;
using jem::io::FileReader;
using jem::io::FileWriter;
using jem::io::PrintWriter;
using jem::io::PatternLogger;
using jem::io::FileFlags;

using jive::Cubix;

using jive::model::Model;
using jive::model::StateVector;
using jive::app::ChainModule;
using jive::app::UserconfModule;
using jive::app::InitModule;
using jive::app::InfoModule;
using jive::util::Random;
using jive::util::Globdat;
using jive::util::joinNames;
using jive::util::DofSpace;
using jive::util::Assignable;
using jive::fem::ElementSet;
using jive::fem::NodeSet;
using jive::algebra::FlexMatrixBuilder;

//=================================================================================================
//   class ObserverMaterial
//=================================================================================================

//-------------------------------------------------------------------------------------------------
//   static data
//-------------------------------------------------------------------------------------------------

const char* ObserverMaterial::MATERIAL         = "material";
const char* ObserverMaterial::FEATUREEXTRACTOR = "featureExtractor";
const char* ObserverMaterial::OUTFILE          = "outFile";
const char* ObserverMaterial::WRITEHIST        = "writeHist";
const char* ObserverMaterial::RSEED            = "rseed";
const char* ObserverMaterial::SHUFFLE          = "shuffle";

//-------------------------------------------------------------------------------------------------
//   constructor and destructor
//-------------------------------------------------------------------------------------------------

ObserverMaterial::ObserverMaterial

  ( const Properties& props,
    const Properties& conf,
    const idx_t       rank,
    const Properties& globdat )

  : Material ( rank, globdat )

{
  JEM_PRECHECK ( rank >= 1 && rank <= 3 );

  const idx_t  STRAIN_COUNTS[4] = { 0, 1, 3, 6 };

  strCount_      = STRAIN_COUNTS[rank];
  rank_          = rank;
  props_         = props;
  conf_          = conf;
  globdat_       = globdat;
  file_          = "";
  out_           = nullptr;
  shuffle_       = false;
  writeHist_    = false;
  writeFeatures_ = false;

  child_    = newMaterial ( MATERIAL, conf, props, globdat );
}

ObserverMaterial::~ObserverMaterial ()

{
  IdxVector points ( iarray ( hist_.size() ) );

  if ( shuffle_ )
  {
    NeuralUtils::shuffle ( points, globdat_ );
  }

  for ( idx_t p = 0_idx; p < hist_.size(); ++p )
  {
    idx_t point = points[p];

    for ( idx_t t = 0_idx; t < hist_[point].strains.size(); ++t )
    {
      if ( writeFeatures_ )
      {
	for ( idx_t f = 0_idx; f < hist_[point].features[t].size(); ++f )
	{
	  *out_ << hist_[point].features[t][f] << " ";
	}
      }

      for ( idx_t s = 0_idx; s < strCount_; ++s )
      {
        *out_ << hist_[point].strains[t][s] << " ";
      }

      for ( idx_t s = 0_idx; s < strCount_; ++s )
      {
        *out_ << hist_[point].stresses[t][s] << " ";
      }

      if ( writeHist_ )
      {
        for ( idx_t m = 0_idx; m < hist_[point].history[t].size(); ++m )
	{
          *out_ << hist_[point].history[t][m] << " ";
	}
      }

      *out_ << times_[t] << '\n';
    }

    *out_ << '\n';
  }
}

//-------------------------------------------------------------------------------------------------
//   configure
//-------------------------------------------------------------------------------------------------

void ObserverMaterial::configure

  ( const Properties& props,
    const Properties& globdat )

{
  // Printing

  props.get ( file_, OUTFILE );

  out_ = newInstance<PrintWriter> ( newInstance<FileWriter> ( file_ ) );

  out_->setPageWidth ( 1000000000 );

  props.find ( writeHist_, WRITEHIST );

  // Shuffling

  props.find ( shuffle_, SHUFFLE );

  if ( shuffle_ )
  {
    props.get ( rseed_, RSEED );

    Ref<Random> rng = Random::get ( globdat );

    rng->restart ( rseed_ );
  }

  // Configure child

  child_->configure ( props_.findProps ( MATERIAL ), globdat_ );
}

//-------------------------------------------------------------------------------------------------
//   getConfig
//-------------------------------------------------------------------------------------------------

void ObserverMaterial::getConfig

  ( const Properties& conf,
    const Properties& globdat ) const
    
{
  conf.set ( OUTFILE,          file_                                 );
  conf.set ( FEATUREEXTRACTOR, props_.findProps ( FEATUREEXTRACTOR ) );

  // Get child config

  child_->getConfig ( conf.makeProps ( MATERIAL ), globdat );
}

//-------------------------------------------------------------------------------------------------
//  createIntPoints 
//-------------------------------------------------------------------------------------------------

void ObserverMaterial::createIntPoints

  ( const idx_t       npoints )

{
  child_->createIntPoints ( npoints );

  //strains_. pushBack ( Vector ( strCount_ ), npoints );
  //stresses_.pushBack ( Vector ( strCount_ ), npoints );

  for ( idx_t p = 0_idx; p < npoints; ++p )
  {
    strains_ .pushBack ( Vector ( strCount_ ) );
    stresses_.pushBack ( Vector ( strCount_ ) );

    hist_.pushBack ( Hist_() );

    if ( props_.contains ( FEATUREEXTRACTOR ) )
    {
      writeFeatures_ = true;

      hist_[p].extractor = newMaterial ( FEATUREEXTRACTOR, Properties(), props_, globdat_ );
      hist_[p].extractor->configure ( props_.findProps ( FEATUREEXTRACTOR ), globdat_ );
      hist_[p].extractor->createIntPoints ( 1 );

      props_.get(extRank_, joinNames(FEATUREEXTRACTOR,"rank"));

      if ( extRank_ < rank_ )
      {
        System::out() << "Observing 3D strains with 2D extractor\n";
      }
    }
  }
}

//-------------------------------------------------------------------------------------------------
//  update 
//-------------------------------------------------------------------------------------------------

void ObserverMaterial::update

  ( Matrix&       stiff,
    Vector&       stress,
    const Vector& strain,
    const idx_t   ipoint )
{
  // Update using nested material

  child_->update ( stiff, stress, strain, ipoint );

  // Store strains and stresses for observation purposes

  strains_ [ipoint] = strain;
  stresses_[ipoint] = stress;
}

//-------------------------------------------------------------------------------------------------
//  cancel
//-------------------------------------------------------------------------------------------------

void ObserverMaterial::cancel ()

{
  child_->cancel();
}

//-------------------------------------------------------------------------------------------------
//  commit
//-------------------------------------------------------------------------------------------------

void ObserverMaterial::commit ()

{
  const idx_t  STRAIN_COUNTS[4] = { 0, 1, 3, 6 };

  child_->commit();

  // Store converged observations

  double time;

  globdat_.get ( time, Globdat::TIME );

  times_.pushBack ( time );

  Matrix stiff  ( strCount_, strCount_ );
  Vector stress ( strCount_            );

  stiff = 0.0; stress = 0.0;

  for ( idx_t p = 0; p < hist_.size(); ++p )
  {
    hist_[p].strains .pushBack ( strains_ [p].clone() );
    hist_[p].stresses.pushBack ( stresses_[p].clone() );

    if ( writeFeatures_ )
    {
      Vector extStrain ( STRAIN_COUNTS[extRank_] );
      extStrain = 0.0;

      if ( extRank_ < rank_ )
      {
        extStrain[0] = strains_[p][0];
        extStrain[1] = strains_[p][1];
        extStrain[2] = strains_[p][3];
      }
      else if ( extRank_ > rank_ )
      {
        extStrain[0] = strains_[p][0];
        extStrain[1] = strains_[p][1];
        extStrain[3] = strains_[p][2];
      }
      else
      {
        extStrain = strains_[p];
      }

      Matrix stiff  ( extStrain.size(), extStrain.size() );
      Vector stress ( extStrain.size() );
      stiff = 0.0; stress = 0.0;

      hist_[p].extractor->update ( stiff, stress, extStrain, 0_idx );
      hist_[p].extractor->commit ();

      BayRef bay = dynamicCast<Bayesian> ( hist_[p].extractor );
      hist_[p].features.pushBack ( bay->getFeatures ( 0_idx ).clone() );
    }

    if ( writeHist_ )
    {
      Vector hvals;
      child_->getHistory ( hvals, p );

      hist_[p].history.pushBack ( hvals.clone() );
    }
  }
}

//-------------------------------------------------------------------------------------------------
//  CheckCommit
//-------------------------------------------------------------------------------------------------

void ObserverMaterial::checkCommit 

  ( const Properties& params )

{
  child_->checkCommit ( params );
}

//-------------------------------------------------------------------------------------------------
//  stressAtPoint 
//-------------------------------------------------------------------------------------------------

void ObserverMaterial::stressAtPoint

  ( Vector&       stress,
    const Vector& strain,
    const idx_t   ipoint )

{
  child_->stressAtPoint ( stress, strain, ipoint );
}

//-------------------------------------------------------------------------------------------------
//  clone 
//-------------------------------------------------------------------------------------------------

Ref<Material> ObserverMaterial::clone ( ) const

{
  return newInstance<ObserverMaterial> ( *this );
}

//-------------------------------------------------------------------------------------------------
//  addTableColumns 
//-------------------------------------------------------------------------------------------------

void ObserverMaterial::addTableColumns

  ( IdxVector&     jcols,
    XTable&        table,
    const String&  name )

{
  child_->addTableColumns ( jcols, table, name );
}

//-------------------------------------------------------------------------------------------------
//  getHistory
//-------------------------------------------------------------------------------------------------

void ObserverMaterial::getHistory

  ( Vector&           hvals,
    const idx_t       mpoint ) 

{
  child_->getHistory ( hvals, mpoint );
}

//-------------------------------------------------------------------------------------------------
//  Hist_
//-------------------------------------------------------------------------------------------------

ObserverMaterial::Hist_::Hist_ ()
{
}

ObserverMaterial::Hist_::~Hist_ ()
{
}

  
