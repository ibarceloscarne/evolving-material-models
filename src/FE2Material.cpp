/*
 *  Copyright (C) 2016 TU Delft. All rights reserved.
 *
 *  This class provides an interface between macro and microlevel
 *  of fe2 analysis. The interface is similar to that of the Material 
 *  from monoscale analysis
 *  
 *  a two-level homogenization FE model in 2D.  
 *
 *   - Small deformation theory
 *   - One RVE for all macroscopic Gauss points.
 *   - Linear and nonlinear RVE
 *   - linear boundary conditions on RVE's edges
 *   - periodic boundary conditions on RVE's edges
 *
 *  Useful references:
 *
 *  Nguyen, V. P.; Lloberas-Valls, O.; Stroeven, M.; Sluys, L. J.
 *  Computational homogenization for multiscale crack modelling.
 *  Implementational and computational aspects. IJNME, 89:192-226, 2011.
 *  
 *  Somer, D. D.; de Souza Neto, E. A.; Dettmer, W. G.; Peric, D.
 *  A sub-stepping scheme for multi-scale analysis of solids.
 *  CMAME, 198:1006-1016, 2009.
 *
 *  Changelog:
 *
 *  Author:   Frans van der Meer, f.p.vandermeer@tudelft.nl
 *  Date:     May 2015
 *  (based on code written by Erik Jan Lingen and Vinh Phu Nguyen)
 *
 *  Modified: Iuri Barcelos, i.barcelos@wmc.eu
 *  Date:     Oct 2015
 *  Transformed into a material for direct use with SolidModel in
 *  order to make it compatible with MultiphysicsModule.
 *  Added specific functions for interaction with MultiPhysicsModule
 *
 *  Modified: Iuri Barcelos, i.barcelos@wmc.eu
 *  Date:     Jul 2016
 *  Each micro model is an instance of a class. This was done in order
 *  to allow the use of pthread.
 *
 *  Modified: Iuri Barcelos, i.barcelos@wmc.eu
 *  Data:     Mar 2018
 *  This material now works with the reduced-order modeling techniques
 *  POD and ECM. Strength computation was removed. A history-like
 *  class called MicroState was created to handle substepping.
 *  The microCommit_ procedure in StressModel is now unnecessary.
 *
 *  Modified: Iuri Barcelos, i.barceloscarneiromrocha@tudelft.nl
 *  Date:     Aug 2019
 *
 *  Code cleaning, new class name
 *  Removed aging stuff
 *  Removed reliance on a scale distinction in StressModel
 *  Removed all custom MultiActions
 *
 */

#include <jem/util/ArrayBuffer.h>
#include <jem/util/PropertyException.h>
#include <jem/base/Array.h>
#include <jem/base/array/tensor.h>
#include <jem/base/array/operators.h>
#include <jem/base/array/utilities.h>
#include <jem/base/Error.h>
#include <jem/base/System.h>
#include <jem/util/StringUtils.h>
#include <jem/util/Timer.h>
#include <jem/util/Dictionary.h>
#include <jem/numeric/algebra/MatmulChain.h>
#include <jem/numeric/algebra/matmul.h>
#include <jem/numeric/algebra/utilities.h>
#include <jem/numeric/algebra/LUSolver.h>
#include <jem/numeric/sparse/matmul.h>
#include <jem/numeric/sparse/SparseMatrix.h>
#include <jem/numeric/func/Function.h>
#include <jem/numeric/func/UserFunc.h>
#include <jem/io/Writer.h>
#include <jem/io/PrintWriter.h>
#include <jem/io/FileWriter.h>
#include <jem/io/FileReader.h>
#include <jem/io/FileFlags.h>
#include <jem/io/PatternLogger.h>

#include <jive/util/utilities.h>
#include <jive/util/Globdat.h>
#include <jive/util/Printer.h>
#include <jive/geom/Geometries.h>
#include <jive/geom/declare.h>
#include <jive/geom/IShapeFactory.h>
#include <jive/algebra/AbstractMatrix.h>
#include <jive/algebra/MatrixBuilder.h>
#include <jive/algebra/SparseMatrixObject.h>
#include <jive/algebra/MBuilderParams.h>
#include <jive/algebra/ConstrainedMatrix.h>
#include <jive/solver/SkylineLU.h>
#include <jive/solver/StdConstrainer.h>
#include <jive/implict/SolverInfo.h>
#include <jive/model/StateVector.h>
#include <jive/model/Actions.h>
#include <jive/model/ModelFactory.h>
#include <jive/model/declare.h>
#include <jive/femodel/declare.h>
#include <jive/fem/FEMatrixBuilder.h>
#include <jive/fem/ElementGroup.h>
#include <jive/fem/Globdat.h>
#include <jive/fem/declare.h>
#include <jive/util/FuncUtils.h>
#include <jive/app/ChainModule.h>
#include <jive/app/ControlModule.h>
#include <jive/app/OutputModule.h>
#include <jive/app/ReportModule.h>
#include <jive/app/Application.h>
#include <jive/app/InfoModule.h>
#include <jive/app/SampleModule.h>
#include <jive/app/UserconfModule.h>
#include <jive/app/ExitModule.h>
#include <jive/fem/NodeGroup.h>
#include <jive/fem/ElementSet.h>
#include <jive/fem/InputModule.h>
#include <jive/fem/InitModule.h>
#include <jive/fem/ShapeModule.h>
#include <jive/fem/FEMatrixBuilder.h>
#include <jive/gl/FemViewModule.h>
#include <jive/implict/NonlinModule.h>
#include <jive/solver/SparseLU.h>
#include <jive/algebra/ConstrainedMatrix.h>

#include "FE2Material.h"
#include "PBCGroupInputModule.h"
#include "SolverNames.h"
#include "XNonlinModule.h"
#include "LearningNames.h"

using namespace jem;

using jem::util::PropertyException;
using jem::util::ArrayBuffer;
using jem::util::StringUtils;
using jem::newInstance;
using jem::util::Dict;
using jem::util::DictEnum;
using jem::util::Properties;
using jem::numeric::MatmulChain;
using jem::numeric::matmul;
using jem::numeric::Function;
using jem::numeric::UserFunc;
using jem::numeric::LUSolver;
using jem::io::Writer;
using jem::io::PrintWriter;
using jem::io::FileWriter;
using jem::io::FileReader;
using jem::io::FileFlags;
using jem::io::endl;
using jem::io::Logger;
using jem::io::PatternLogger;

using jive::util::FuncUtils;
using jive::fem::NodeGroup;
using jive::fem::ElementGroup;
using jive::fem::Globdat;
using jive::geom::Geometries;
using jive::implict::SolverInfo;
using jive::model::StateVector;
using jive::model::Actions;
using jive::app::Module;
using jive::app::ChainModule;
using jive::app::Application;
using jive::app::ExitModule;
using jive::app::Module;
using jive::app::ChainModule;
using jive::app::OutputModule;
using jive::app::InfoModule;
using jive::app::ControlModule;
using jive::app::ReportModule;
using jive::app::SampleModule;
using jive::app::UserconfModule;
using jive::fem::InputModule;
using jive::fem::InitModule;
using jive::fem::ShapeModule;
using jive::fem::NodeGroup;
using jive::fem::ElementSet;
using jive::gl::FemViewModule;
using jive::implict::NonlinModule;

using jive::implict::XNonlinModule;

//=======================================================================
//   class FE2Material
//=======================================================================

//-----------------------------------------------------------------------
//   static members
//-----------------------------------------------------------------------

const char* FE2Material::BOUNDARY_PROP         = "bcType";
const char* FE2Material::MAX_SUBSTEPLEVEL_PROP = "maxSubStepLevel";
const char* FE2Material::VIEWPOINTS_PROP       = "viewPoints";
const char* FE2Material::USESAMP_PROP          = "sample";
const char* FE2Material::USEOUT_PROP           = "output";
const char* FE2Material::DEBUG_PROP            = "debug";
const char* FE2Material::PERM_PROP             = "permutation";
const char* FE2Material::ANMODEL_PROP          = "anmodel";
const char* FE2Material::DOF_NAMES[3]          = { "dx", "dy", "dz" };

//-----------------------------------------------------------------------
//   constructors & destructor
//-----------------------------------------------------------------------

FE2Material::FE2Material

     ( const String&     name,
       const Properties& props,
       const Properties& conf,
       const Properties& globdat,
       const idx_t       rank )

     : Material ( name, props, conf, rank )

{
  iStep_     = 0;
  microRank_ = 0;
  macroRank_ = rank;
  anmodel_   = "";

  debug_     = false;

  nupd_ = 0;

  globdat_   = globdat;

  Properties  myProps = props.getProps  ( name );
  Properties  myConf  = conf .makeProps ( name );

  props_     = myProps;
  conf_      = myConf;

  configure ( myProps );
  getConfig ( myConf );
}

FE2Material::~FE2Material ()
{}

//-----------------------------------------------------------------------
//   allocPoints
//-----------------------------------------------------------------------

void FE2Material::createIntPoints

  ( const idx_t np )

{
  macroOut_ = &System::out();
  microOut_ = newInstance<PrintWriter> (
              newInstance<FileWriter> ( "micro.log" ) );

  macroLog_ = newInstance<PatternLogger> ( macroOut_, "*.info" );
  microLog_ = newInstance<PatternLogger> ( microOut_, "*.info" );

  // Activate micro logger

  macroLog_->flush ( );
  System::setLogger     ( microLog_ );
  System::setOutStream  ( microOut_ );
  System::setWarnStream ( microOut_ );

  // Create the micro models

  for ( idx_t ip = 0; ip < np; ip++ )
  {
    Ref<MicroModel>  newModel = newInstance<MicroModel> 
      ( ip, props_, conf_ );
    
    if ( !microRank_ )
    {
      microRank_ = newModel->getMicroRank ( );
    }
    else
    {
      JEM_PRECHECK ( microRank_ == newModel->getMicroRank ( ) );
    }

    models_.pushBack ( newModel );

    if ( debug_ )
    {
      System::out() << "Created micromodel " << ip+1 << "\n";
    }
  }

  JEM_PRECHECK ( microRank_ == macroRank_ || 
               ( microRank_ == 3 && macroRank_ == 2 ) );

  if ( microRank_ > macroRank_ )
  {
    if ( debug_ )
    {
      System::out() << "Performing plane strain analysis on 3D micromodel"
	<< endl;
    }

    perm23_.resize ( 3 );

    perm23_[0] = 0;   // micro_xx <-> macro_xx
    perm23_[1] = 1;   // micro_yy <-> macro_yy
    perm23_[2] = 3;   // micro_xy <-> macro_xy 

    props_.find ( perm23_, PERM_PROP );

    props_.get ( anmodel_, ANMODEL_PROP );

    if ( anmodel_ == "PLANE_STRESS" )
    {
      idx_t psdof = -1;

      if ( !testany ( perm23_ == 0 ) )
      {
        psdof = 3;
      }
      else if ( !testany ( perm23_ == 1 ) )
      {
        psdof = 7;
      }
      else if ( !testany ( perm23_ == 2 ) )
      {
        psdof = 11;
      }
      else
      {
        throw Error ( JEM_FUNC, "FE2Material: Could not determine out-of-plane DOF based on permutation vector" );
      }

      for ( idx_t ip = 0; ip < np; ++ip )
      {
        models_[ip]->setPlaneStress ( psdof );
      }
    }
    else if ( anmodel_ != "PLANE_STRAIN" )
    {
      throw Error ( JEM_FUNC, "FE2Material: Incompatible analysis model for rank-mismatched FE2" );
    }
  }

  mstrCount_ = STRAIN_COUNTS[microRank_];

  // Activate macro logger

  microLog_->flush();
  System::setLogger     ( macroLog_ );
  System::setOutStream  ( macroOut_ );
  System::setWarnStream ( macroOut_ );
}

//-----------------------------------------------------------------------
//   configure
//-----------------------------------------------------------------------

void FE2Material::configure

  ( const Properties&  props )

{
  props.find ( debug_,     DEBUG_PROP     );
}

//-----------------------------------------------------------------------
//   getConfig
//-----------------------------------------------------------------------

void FE2Material::getConfig

  ( const Properties&  conf ) const

{
  conf.set ( DEBUG_PROP, debug_ );
}

//-----------------------------------------------------------------------
//   update
//----------------------------------------------------------------------

void FE2Material::update

     ( Matrix&       tangent,
       Vector&       stress,
       const Vector& strain,
       const idx_t   gp )
{
  //macroLog_->flush ( );
  //System::setLogger     ( microLog_ );
  //System::setOutStream  ( microOut_ );
  //System::setWarnStream ( microOut_ );

  nupd_++;

  double time = 0.;
  
  if ( globdat_.find ( time, Globdat::TIME ) )
    models_[gp]->setTime ( time );

  Vector  mstrain ( mstrCount_ );
  Vector  mstress ( mstrCount_ );
  Matrix  mstiff  ( mstrCount_, mstrCount_ );

  completeStrain_ ( mstrain, strain );

  models_[gp]->update ( mstiff, mstress, mstrain );

  reduceStress_ ( stress, mstress );
  reduceStiff_ ( tangent, mstiff );

  //microLog_->flush();
  //System::setLogger     ( macroLog_ );
  //System::setOutStream  ( macroOut_ );
  //System::setWarnStream ( macroOut_ );

  if ( debug_ )
  {
    Ref<PrintWriter> out = newInstance<PrintWriter>
      ( &System::out() );
    out->nformat.setFractionDigits ( 10 );
    *out << "FE2Material: point " << gp << "\n";
    *out << "FE2Material: strain = " << strain << "\n";
    *out << "FE2Material: stress = " << stress << "\n";
    *out << "FE2Material: stiff =\n" << tangent << "\n";
  }
}

//-----------------------------------------------------------------------
//   isLoading
//----------------------------------------------------------------------

bool FE2Material::isLoading

  ( const idx_t gp ) const

{
  return models_[gp]->isLoading(); 
}

//-----------------------------------------------------------------------
//   isInelastic
//----------------------------------------------------------------------

bool FE2Material::isInelastic

  ( const idx_t gp ) const

{
  return models_[gp]->isInelastic(); 
}

//-----------------------------------------------------------------------
//   getDissipation
//----------------------------------------------------------------------

double FE2Material::getDissipation

   ( const idx_t   gp ) const

{
  // get the latest stored dissipation (stored in commit)

  return models_[gp]->getPointDissipation ( );
}

//-----------------------------------------------------------------------
//   stressAtPoint
//----------------------------------------------------------------------

void FE2Material::stressAtPoint

   ( Vector&       stress,
     const Vector& strain,
     const idx_t   gp )

{
  // get the latest stored stress (stored in update)

  Vector mstress ( mstrCount_ );

  models_[gp]->getPointStress ( mstress );

  reduceStress_ ( stress, mstress );
}

//-----------------------------------------------------------------------
//   getDissStress
//----------------------------------------------------------------------

void FE2Material::getDissStress

   ( const Vector& sstress,
     const Vector& strain,
     const idx_t   gp )

{
  Vector     mstrain  ( mstrCount_ );
  Vector     straine  ( mstrCount_ );
  Vector     strainp  ( mstrCount_ );
  Vector     strainv  ( mstrCount_ );
  Vector     msstress ( mstrCount_ );

  Matrix     compl0  ( mstrCount_, mstrCount_ );

  models_[gp]->getCompl0 ( compl0 );

  completeStrain_ ( mstrain, strain );

  // Get the latest stored stress and stiffness

  Vector     mstress ( mstrCount_ );
  Matrix     mstiff ( mstrCount_, mstrCount_ );

  models_[gp]->getPointStress ( mstress );
  models_[gp]->getPointStiff  ( mstiff  );

  matmul    ( straine, compl0, mstress );
  strainp   = mstrain - straine;
  strainv   = 2. * strainp - mstrain;
  matmul    ( msstress, mstiff.transpose(), strainv );
  msstress += mstress;

  reduceStress_ ( sstress, msstress );
}

//----------------------------------------------------------------------
//   commit
//----------------------------------------------------------------------

void  FE2Material::commit  ()

{
  Properties  info = SolverInfo::get ( globdat_ );
  
  for ( idx_t ip = 0; ip < models_.size(); ++ip )
  {
    models_[ip]->commit ( info );

    models_[ip]->setTimeStep ( iStep_ + 1 );
  }
  iStep_++;

  System::out() << "Total update count " << nupd_ << '\n'; 
}

//----------------------------------------------------------------------
//   commitOne
//----------------------------------------------------------------------

void FE2Material::commitOne

  ( const idx_t ip )

{
  Properties info = SolverInfo::get ( globdat_ );

  models_[ip]->commit ( info );
}

//----------------------------------------------------------------------
//   cancel
//----------------------------------------------------------------------

void  FE2Material::cancel ()

{
  for ( idx_t ip = 0; ip < models_.size(); ++ip )
  {
    models_[ip]->cancel ( ); 
  }
}

//-----------------------------------------------------------------------
//   addTableColumns
//-----------------------------------------------------------------------

void FE2Material::addTableColumns

  ( IdxVector&    jcols,
    XTable&       table,
    const String& name )

{
  // Check if the requested table is supported by this material

  if ( name == "nodalStress" )
  {
    if ( microRank_ == 2 )
    {
      jcols.resize ( 3 );

      jcols[0] = table.addColumn ( "s_xx" );
      jcols[1] = table.addColumn ( "s_yy" );
      jcols[2] = table.addColumn ( "s_xy" );
    }
    else
    {
      jcols.resize ( 6 );

      jcols[0] = table.addColumn ( "s_xx" );
      jcols[1] = table.addColumn ( "s_yy" );
      jcols[2] = table.addColumn ( "s_zz" );
      jcols[3] = table.addColumn ( "s_xy" );
      jcols[4] = table.addColumn ( "s_xz" );
      jcols[5] = table.addColumn ( "s_yz" );
    }
  }
}

//-----------------------------------------------------------------------
//   clone 
//-----------------------------------------------------------------------

Ref<Material> FE2Material::clone () const
{
  return newInstance<FE2Material> ( *this );
}

//-----------------------------------------------------------------------
//   setProps
//-----------------------------------------------------------------------

void FE2Material::setProps

  ( const Vector& props )

{
  Ref<Material> mat = models_[0]->getMaterial();

  Ref<Bayesian> bay = dynamicCast<Bayesian> ( mat );

  bay->setProps ( props );

  //throw Error ( JEM_FUNC, "FE2Material::setProps not yet implemented" );
}

//-----------------------------------------------------------------------
//   getProps
//-----------------------------------------------------------------------

Vector FE2Material::getProps ()

{
  Ref<Material> mat = models_[0]->getMaterial();

  Ref<Bayesian> bay = dynamicCast<Bayesian> ( mat );

  return bay->getProps ( );

  //throw Error ( JEM_FUNC, "FE2Material::getProps not yet implemented" );
}

//-----------------------------------------------------------------------
//   getFeatures
//-----------------------------------------------------------------------

Vector FE2Material::getFeatures 

  ( const idx_t ipoint )

{
  Ref<Material> mat = models_[0]->getMaterial();

  Ref<Bayesian> bay = dynamicCast<Bayesian> ( mat );

  return bay->getFeatures (ipoint );
  
  //return models_[ipoint]->getFeatures();
}

//-----------------------------------------------------------------------
//   propCount
//-----------------------------------------------------------------------

idx_t FE2Material::propCount ()

{
  //throw Error ( JEM_FUNC, "FE2Material::propCount not yet implemented" );
  
  Ref<Material> mat = models_[0]->getMaterial();

  Ref<Bayesian> bay = dynamicCast<Bayesian> ( mat );

  return bay->propCount( );
}

//-----------------------------------------------------------------------
//   getHistory
//-----------------------------------------------------------------------

void FE2Material::getHistory

  (       Vector& hvals,
    const idx_t   mpoint )

{
  Ref<Material> mat = models_[0]->getMaterial();

  mat->getHistory ( hvals, mpoint );
}

//-----------------------------------------------------------------------
//   clearHist
//-----------------------------------------------------------------------

void FE2Material::clearHist ()

{
  throw Error ( JEM_FUNC, "FE2Material::clearHist not yet implemented" );
}

//-----------------------------------------------------------------------
//   completeStrain_
//-----------------------------------------------------------------------

void  FE2Material::completeStrain_

  ( const Vector&  mstrain,
    const Vector&  Mstrain ) const

{
  // function that adds missing strain components in case microRank=3
  // and macroRank=2

  if ( microRank_ == macroRank_ )
  {
    mstrain = Mstrain;
  }
  else
  {
    mstrain = 0.;
    mstrain[perm23_] = Mstrain;
  }
}

//-----------------------------------------------------------------------
//   reduceStress_
//-----------------------------------------------------------------------

void  FE2Material::reduceStress_

  ( const Vector&  Mstress,
    const Vector&  mstress ) const

{
  // function that removes out of plane stress components 
  // in case microRank=3 and macroRank=2

  if ( microRank_ == macroRank_ )
  {
    Mstress = mstress;
  }
  else
  {
    Mstress = mstress[perm23_];
  }
}

//-----------------------------------------------------------------------
//   reduceStiff_
//-----------------------------------------------------------------------

void  FE2Material::reduceStiff_

  ( const Matrix&  Mstiff,
    const Matrix&  mstiff ) const

{
  // function that removes out of plane stress components 
  // in case microRank=3 and macroRank=2

  if ( microRank_ == macroRank_ )
  {
    Mstiff = mstiff;
  }
  else
  {
    if ( anmodel_ == "PLANE_STRESS" )
    {
      double d;
      Matrix tmp = mstiff;
      LUSolver::invert ( tmp, d );
      Mstiff = mstiff(perm23_,perm23_);
      LUSolver::invert ( Mstiff, d );
    }
    else
    {
      // plane strain

      Mstiff = mstiff(perm23_,perm23_);
    }
  }
}

//=======================================================================
//   class MicroModel 
//=======================================================================

//-----------------------------------------------------------------------
//   MicroModel 
//-----------------------------------------------------------------------

FE2Material::MicroModel::MicroModel 

  ( const idx_t       ip,
    const Properties& props,
    const Properties& conf   )

{
  props_  = props;
  conf_   = conf;
  myID_   = String( ip );

  iStep_           = 0;
  maxSubStepLevel_ = 0;
  firstTimeFlag_   = true;

  isLoading_       = false;
  isInelastic_     = false;

  outp_            = nullptr;
  samp_            = nullptr;
  view_            = nullptr;
  //nlin_            = nullptr;

  psDOF_ = -1;

  bool usesamp = false;
  bool useview = false;
  bool useoutp = false;

  String boundary, view ( "none" );

  props.find ( maxSubStepLevel_, Super::MAX_SUBSTEPLEVEL_PROP );
  props.find ( usesamp,          Super::USESAMP_PROP          );
  props.find ( useoutp,          Super::USEOUT_PROP           );
  props.get  ( boundary,         Super::BOUNDARY_PROP         ); 

  try
  {
    props.find ( view, Super::VIEWPOINTS_PROP );

    if ( view == "all" )
    {
      useview = true;
    }
  }
  catch ( const PropertyException& ex )
  {
    IdxVector viewpoints;
    props.get ( viewpoints, Super::VIEWPOINTS_PROP );

    if ( testany ( viewpoints == ip ) )
    {
      useview = true;
    }
  }

  if ( boundary == "linear" )
  {
    bndCondition_ = LINEAR;
  }
  else
  {
    bndCondition_ = PERIODIC;
  }

  if ( useview )
  {
    view_ = newInstance<FemViewModule> ( "micro.view" );
  }

  if ( usesamp )
  {
    samp_ = newInstance<SampleModule> ( "micro.sample" );

    props_.set ( "micro.sample.file", "s" + myID_ + ".dat" );

    samp_->configure ( props_, data_        );
    samp_->getConfig ( conf_, data_         );
    samp_->init      ( conf_, props_, data_ );
  }

  if ( useoutp )
  {
    outp_ = newInstance<SampleModule> ( "micro.output" );

    props_.set ( "micro.output.file", "o" + myID_ + ".out" );

    outp_->configure ( props_, data_        );
    outp_->getConfig ( conf_, data_         );
    outp_->init      ( conf_, props_, data_ );
  }

  Assignable<ElementSet> mesh;
  Ref<ChainModule>       chain;
  Ref<DofSpace>          dofs;

  data_ = Globdat::newInstance ( "microPoint" + myID_ );

  Globdat::getVariables ( data_ );

  // NB: Here we MUST use XNonlin because it does not commit during run()
  //     otherwise micromodels should know their scale and avoid committing
  //     before macro convergence.

  nlin_ = newInstance<XNonlinModule> ( "micro.nonlin" );

  chain = newInstance<ChainModule>   ( "micro.chain"  );

  chain->pushBack ( newInstance<UserconfModule> ( "micro.userinput"  ));
  chain->pushBack ( newInstance<ShapeModule>    ( "micro.shape"      ));
  chain->pushBack ( newInstance<InitModule>     ( "micro.init"       ));
  chain->pushBack ( newInstance<InfoModule>     ( "micro.info"       ));

  chain->pushBack ( nlin_ );

  if ( useview )
  {
    chain->pushBack ( view_ );
  }

  chain_ = chain;

  chain_->configure ( props_, data_         );
  chain_->getConfig ( conf_,  data_         );
  chain_->init      ( conf_,  props_, data_ );

  String context = "MicroModel constructor";

  model_    = Model::get ( data_, context );
  mesh      = ElementSet::get ( data_, context );
  nodes_    = mesh.getNodes ( );
  rank_     = nodes_.rank ( );
  strCount_ = STRAIN_COUNTS[rank_];

  // Initialize MicroState objects

  state0_ = newInstance<MicroState> ( strCount_ );
  state_  = newInstance<MicroState> ( strCount_ );

  // Get constraints

  dofs   = DofSpace::get ( nodes_.getData(), data_, context );
  cons_  = Constraints::get ( dofs, data_ );
  cons2_ = newInstance<Constraints> ( dofs ); 

  // Add dof types

  dofTypes_.resize ( rank_ );

  for ( idx_t i = 0; i < rank_; ++i )
  {
    dofTypes_[i] = dofs->findType ( DOF_NAMES[i] );
  }

  if ( testany ( dofTypes_ == -1 ) )
  {
    throw Error ( JEM_FUNC, 
      "Error creating new MicroModel: invalid DOF type" ); 
  }

  // Get edge nodes
  
  for ( idx_t i = 0; i < 2*rank_; ++i )
  {
    NodeGroup edge = NodeGroup::get
      ( PBCGroupInputModule::EDGES[i], nodes_, data_, context );

    IdxVector inodes = edge.getIndices ( );
    bndNodes_[i].resize ( inodes.size ( ) );
    bndNodes_[i] = inodes;
  }

  // Get corner nodes

  cornerNodes_.resize ( rank_+1 );

  for ( idx_t i = 0; i < rank_+1; ++i )
  {
    NodeGroup corner = NodeGroup::find
      ( PBCGroupInputModule::CORNERS[i], nodes_, data_ );

    IdxVector inodes = corner.getIndices ( );

    JEM_PRECHECK ( inodes.size() == 1 );

    cornerNodes_[i] = inodes[0];
  }

  // Compute RVE area

  calcInvArea_ ();

  // Compute the boundary matrix

  calcDMatrix_ ();

  // Determine boundary dofs and interior dofs

  buildInnerAndOuterDofs_ ();

  // Initialise some variables

  stress_.resize  ( strCount_ );
  compl0_.resize  ( strCount_, strCount_ );
  tangent_.resize ( strCount_, strCount_ );

  stress_ = 0.;
  compl0_ = 0.;
  tangent_ = 0.;
}

//-----------------------------------------------------------------------
//  ~MicroModel 
//-----------------------------------------------------------------------

FE2Material::MicroModel::~MicroModel ()

{}

//-----------------------------------------------------------------------
//   update
//----------------------------------------------------------------------

void FE2Material::MicroModel::update

  ( Matrix&       mstiff,
    Vector&       mstress,
    const Vector& mstrain )

{
  Properties  microParams;

  subStepLevel_ = 0;

  // Set current model state

  state_->setStrain ( mstrain );

  setState_ ( state_ );

  // Solve the micro model.

  try
  {
    chain_->run ( data_ );
  }
  catch ( const Exception& ex )
  {
    System::out() << "Micromodel " << myID_ << " with strain " << mstrain << " did not converge\n";

    try
    {
      subStepping_ ( ex, state0_, state_, microParams );
    }
    catch ( const Exception& ex )
    {
      System::out() << "Maximum SubstepLevel has been reached.\n" 
	<< "...Continuing with non-converged solution!" << endl;

      throw;
    }
  }

  // Calculate the micro stress-strain matrix.

  calcMicroStiff_ ( mstiff, mstress );

  if ( firstTimeFlag_ )
  {
    // store compl0 in first step

    double d;
    compl0_ = mstiff;
    jem::numeric::LUSolver::invert ( compl0_, d );
    firstTimeFlag_ = false;
  }

  // Get loading/inelastic flags

  idx_t lcount = 0;
  idx_t ucount = 0;

  isLoading_ = false;

  model_->takeAction ( SolverNames::GET_MAT_STATE, microParams, data_ );

  microParams.get ( lcount,       SolverNames::LOADINGCOUNT   );
  microParams.get ( ucount,       SolverNames::UNLOADINGCOUNT );
  microParams.get ( isInelastic_, SolverNames::INELASTIC      );

  if ( ( lcount > 0 && ucount == 0 ) || ( lcount > 0 && lcount / ucount > 1.0 ) )
  {
    isLoading_ = true;
  }

  // Store values and clean up

  stress_  = mstress;
  tangent_ = mstiff;

  cons_->clear();
  cons2_->clear();

  iIter_++;
}

//----------------------------------------------------------------------
//   commit
//----------------------------------------------------------------------

void FE2Material::MicroModel::commit 

  ( const Properties& info )

{
  Properties params;

  iIter_ = 0;

  data_.set ( Globdat::TIME_STEP, iStep_ );

  // signal the micro model that a macro commit was reached

  //params.set ( "micromodel",  myID_ );

  //model_->takeAction ( Actions::COMMIT, params, data_ );

  Ref<XNonlinModule> xnlin = dynamicCast<XNonlinModule> ( nlin_ );

  xnlin->commit ( data_ );

  // compute dissipation in the micro model

  computeDissipation_ ( );

  // store model state

  state_.swap ( state0_ );

  // write micro output to files

  if ( samp_ != nullptr )
  {
    samp_->run ( data_ );
  }

  if ( outp_ != nullptr )
  {
    outp_->run ( data_ );
  }
}

//----------------------------------------------------------------------
//   cancel
//----------------------------------------------------------------------

void  FE2Material::MicroModel::cancel ()

{
  Properties params;

  model_->takeAction 
    ( Actions::CANCEL, params, data_ );
}

//-----------------------------------------------------------------------
//   isLoading
//----------------------------------------------------------------------

bool FE2Material::MicroModel::isLoading () const

{
  return isLoading_;
}

//-----------------------------------------------------------------------
//   isInelastic
//----------------------------------------------------------------------

bool FE2Material::MicroModel::isInelastic () const

{
  return isInelastic_;
}

//-----------------------------------------------------------------------
//   getPointDissipation
//----------------------------------------------------------------------

double FE2Material::MicroModel::getPointDissipation ()

{
  return dissipation_;
}

//-----------------------------------------------------------------------
//   getPointStress
//----------------------------------------------------------------------

void FE2Material::MicroModel::getPointStress

  ( Vector& mstress )

{
  mstress = stress_;
}

//-----------------------------------------------------------------------
//   getPointStiff
//----------------------------------------------------------------------

void FE2Material::MicroModel::getPointStiff

  ( Matrix& mstiff )
{
  mstiff = tangent_;
}

//-----------------------------------------------------------------------
//   getCompl0
//----------------------------------------------------------------------

void FE2Material::MicroModel::getCompl0

  ( Matrix& compl0 )
{
  compl0 = compl0_;
}

//-----------------------------------------------------------------------
//   getMicroRank
//----------------------------------------------------------------------

idx_t FE2Material::MicroModel::getMicroRank ()

{
  return rank_;
}

//-----------------------------------------------------------------------
//   getTimeStep
//----------------------------------------------------------------------

idx_t FE2Material::MicroModel::getTimeStep ()

{
  return iStep_;
}

//-----------------------------------------------------------------------
//   getMaterial
//----------------------------------------------------------------------

Ref<Material> FE2Material::MicroModel::getMaterial ()

{
  Properties params;

  model_->takeAction ( "getMaterialObj", params, data_ );

  Ref<Material> mat;

  params.get ( mat, "materialObj" );

  return mat;
}

//-----------------------------------------------------------------------
//   getFeatures
//----------------------------------------------------------------------

Vector FE2Material::MicroModel::getFeatures ()

{
  Properties params;

  model_->takeAction ( LearningActions::GETFEATURES, params, data_ );

  Vector features;

  params.get ( features, LearningParams::FEATURES );

  return features;
}

//-----------------------------------------------------------------------
//   setTimeStep
//----------------------------------------------------------------------

void FE2Material::MicroModel::setTimeStep

  ( idx_t step )
{
  iStep_ = step;
}

//-----------------------------------------------------------------------
//   setTime
//----------------------------------------------------------------------

void FE2Material::MicroModel::setTime
  
  ( const double time )
{
  state_->setTime ( time );
}

//-----------------------------------------------------------------------
//   setPlaneStress
//----------------------------------------------------------------------

void FE2Material::MicroModel::setPlaneStress
  
  ( const idx_t psdof )
{
  psDOF_ = psdof;
}

//=======================================================================
//   class MicroState
//=======================================================================

//-----------------------------------------------------------------------
//   MicroState
//-----------------------------------------------------------------------

FE2Material::MicroModel::MicroState::MicroState 

  ( const idx_t strcount )

{
  strain_.resize ( strcount );

  strain_ = 0.;
  time_   = 0.;

  hasTime_   = false;
}

//-----------------------------------------------------------------------
//   MicroState
//-----------------------------------------------------------------------

FE2Material::MicroModel::MicroState::MicroState

  ( const idx_t strcount,
    const Ref<MicroState> state,
    const Ref<MicroState> state0 )

{
  double v, v0;

  strain_.resize ( strcount );

  strain_  = 0.0;
  time_    = 0.0;
  hasTime_ = false;

  // Process strain

  Vector eps  ( strcount );
  Vector eps0 ( strcount );
  Vector deps ( strcount );
  eps = 0.0;
  eps0 = 0.0;
  deps = 0.0;

  state->getStrain  ( eps  );
  state0->getStrain ( eps0 );

  deps = 0.5 * ( eps - eps0 );

  System::out() << "MicroState constructor: eps0 = " << eps0 << " eps = " << eps << " deps = " << deps << "\n";

  strain_ = eps0 + deps;

  // Process time

  if ( state->hasTime() )
  {
    v  = state->getTime();
    v0 = state0->getTime(); 

    time_ = v0 + 0.5 * ( v - v0 );
    hasTime_ = true;
  }
}

//-----------------------------------------------------------------------
//   setTime
//-----------------------------------------------------------------------

void FE2Material::MicroModel::MicroState::setTime

  ( const double t )

{
  time_ = t;

  hasTime_ = true;
}

//-----------------------------------------------------------------------
//   setStrain
//-----------------------------------------------------------------------

void FE2Material::MicroModel::MicroState::setStrain

  ( const Vector& eps )

{
  strain_ = eps.clone();
}

//-----------------------------------------------------------------------
//   getTime
//-----------------------------------------------------------------------

double FE2Material::MicroModel::MicroState::getTime ()

{
  return time_;
}

//-----------------------------------------------------------------------
//   getStrain
//-----------------------------------------------------------------------

void FE2Material::MicroModel::MicroState::getStrain

  ( Vector& eps )

{
  eps = strain_.clone();
}

//-----------------------------------------------------------------------
//   hasTime
//-----------------------------------------------------------------------

bool FE2Material::MicroModel::MicroState::hasTime ()

{
  return hasTime_;
}

//-----------------------------------------------------------------------
//   calcInvArea_
//-----------------------------------------------------------------------

void FE2Material::MicroModel::calcInvArea_ ()
{
  // compute area (or volume) based on corner node coordinates

  dxRve_.resize  ( rank_ );

  Vector x0 ( rank_ );
  Vector x1 ( rank_ );

  nodes_.getNodeCoords ( x0, cornerNodes_[0] );

  for ( idx_t i = 0; i < rank_; ++i )
  {
    nodes_.getNodeCoords ( x1, cornerNodes_[i+1] );

    dxRve_[i] = x1[i] - x0[i];
  }
  v0Inv_ = 1. / product ( dxRve_ );
}

//-----------------------------------------------------------------------
//   calcDMatrix_
//-----------------------------------------------------------------------

void FE2Material::MicroModel::calcDMatrix_ ()
{
  switch ( bndCondition_ )
  {
    case PERIODIC:

      calcPeriodicDMatrix_();
      break;

    case LINEAR:

      calcLinearDMatrix_  ();
      break;
  }
}

//-----------------------------------------------------------------------
//   calcLinearDMatrix_
//-----------------------------------------------------------------------

void FE2Material::MicroModel::calcLinearDMatrix_ ()
{
  JEM_ASSERT2 ( rank_ == 2, 
      "linear boundary conditions only implemented for 2D rve" );

  idx_t nodeCount = 0;

  for ( idx_t i = 0; i < 4; i++ )
  {
    IdxVector  inodes = bndNodes_[i];

    nodeCount += inodes.size ();
  }

  // adjust nodeCount due to duplicatition at corner nodes

  nodeCount -= 4;

  D_.resize ( 3, 2 * nodeCount ); 
  D_ = 0.;

  outerDofs_.resize ( 2 * nodeCount );

  // compute D_

  idx_t id, ii = 0;

  Vector  nCoords(2);

  ArrayBuffer<idx_t> doneNodes;

  for ( idx_t i = 0; i < 4; i++ )
  {
    IdxVector  inodes = bndNodes_[i];

    for ( idx_t in = 0; in < inodes.size(); in++ )
    {
      id   = inodes[in];

      if ( testany ( id == doneNodes.toArray() ) )
      {
        continue;
      }

      nodes_.getNodeCoords ( nCoords, id );

      D_(0, 2*ii  ) = nCoords[0];
      D_(1, 2*ii+1) = nCoords[1];
      D_(2, 2*ii  ) = 0.5 * nCoords[1]; 
      D_(2, 2*ii+1) = 0.5 * nCoords[0];

      ii++;

      doneNodes.pushBack ( id );
    }
  }
}

//-----------------------------------------------------------------------
//   calcPeriodicDMatrix_
//-----------------------------------------------------------------------

void FE2Material::MicroModel::calcPeriodicDMatrix_ ()
{
  idx_t nCornerDofs = rank_ * cornerNodes_.size();

  outerDofs_.resize ( nCornerDofs );
  D_.resize ( strCount_, nCornerDofs ); 

  // compute D_   0x 0y  xx  xy  yx  yy
  //          xx [ 0, 0, dx,  0,  0,  0 ]
  // 2D: D_ = yy [ 0, 0,  0,  0,  0, dy ]
  //          xy [ 0, 0,  0, dx, dy,  0 ]/2

  //              0x 0y 0z  xx  xy  xz  yx  yy  yz  zx  zy  zz 
  //          xx [ 0, 0, 0, dx,  0,  0,  0,  0,  0,  0,  0,  0 ]
  //          yy [ 0, 0, 0,  0,  0,  0,  0, dy,  0,  0,  0,  0 ]
  // 3D: D_ = zz [ 0, 0, 0,  0,  0,  0,  0,  0,  0,  0,  0, dz ]
  //          xy [ 0, 0, 0,  0, dx,  0, dy,  0,  0,  0,  0,  0 ]/2

  //          zx [ 0, 0, 0,  0,  0, dx,  0,  0,  0, dz,  0,  0 ]/2
  //          yz [ 0, 0, 0,  0,  0,  0,  0,  0, dy,  0, dz,  0 ]/2

  D_ = 0.;

  if ( rank_ == 2 )
  {
    D_(0,2) = dxRve_[0];
    D_(1,5) = dxRve_[1];
    D_(2,3) = dxRve_[0];
    //D_(2,4) = dxRve_[1] * .5;
  }
  else
  {
    D_(0,3)  = dxRve_[0];
    D_(1,7)  = dxRve_[1];
    D_(2,11) = dxRve_[2];

    D_(3,4)  = dxRve_[0];
    D_(5,8)  = dxRve_[1];
    D_(4,5)  = dxRve_[0];

    ////D_(3,4)  = dxRve_[0] * .5;
    //D_(3,6)  = dxRve_[1] * .5;
    ////D_(4,8)  = dxRve_[1] * .5;
    //D_(5,10) = dxRve_[2] * .5;
    ////D_(5,5)  = dxRve_[0] * .5;
    //D_(4,9)  = dxRve_[2] * .5;
  }
  System::out() << " D_ matrix \n" << D_ << "\n";
}

//-----------------------------------------------------------------------
//   buildInnerAndOuterDofs_ 
//-----------------------------------------------------------------------

void  FE2Material::MicroModel::buildInnerAndOuterDofs_ ()
{
  using jive::BoolVector;

  Ref<DofSpace>  microDofs = cons_->getDofSpace ();

  const idx_t    dofCount  = microDofs->dofCount ();

  BoolVector     dofMask   ( dofCount );

  idx_t          i, j;

  // Determine the boundaryDofs vector.

  switch ( bndCondition_ )
  {
    case PERIODIC:

      buildCornerBndDofs_ ( *microDofs );
      break;

    case LINEAR:

      buildLinearBndDofs_ ( *microDofs );
      break;
  }

  // Then, determine the inner dofs

  dofMask = true;

  select ( dofMask, outerDofs_ ) = false;

  innerDofs_.resize ( count( dofMask ) );

  for ( i = j = 0; i < dofCount; i++ )
  {
    if ( dofMask[i] )
    {
      innerDofs_[j++] = i;
    }
  }

  JEM_ASSERT ( j == innerDofs_.size() );
}

//-----------------------------------------------------------------------
//   buildLinearBndDofs_
//-----------------------------------------------------------------------

void  FE2Material::MicroModel::buildLinearBndDofs_

  ( const DofSpace& microDofs )
{
  idx_t xtype  = dofTypes_[0] ;
  idx_t ytype  = dofTypes_[1] ;

  idx_t ixdof, iydof;
  idx_t id, ii = 0;

  ArrayBuffer<idx_t> doneNodes;

  for ( idx_t i = 0; i < 4; i++ )
  {
    IdxVector  inodes = bndNodes_[i];

    for ( idx_t  in = 0; in < inodes.size(); in++ )
    {
      id   = inodes[in];

      if ( testany ( doneNodes.toArray() == id ) )
      {
        continue;
      }

      ixdof = microDofs.getDofIndex ( id, xtype );
      iydof = microDofs.getDofIndex ( id, ytype );

      outerDofs_[2*ii  ] = ixdof;
      outerDofs_[2*ii+1] = iydof;

      ii++;
      doneNodes.pushBack ( id );
    }
  }
}

//-----------------------------------------------------------------------
//   buildCornerBndDofs_
//-----------------------------------------------------------------------

void  FE2Material::MicroModel::buildCornerBndDofs_

  ( const DofSpace& microDofs )

{
  idx_t k = 0;

  for ( idx_t i = 0; i < cornerNodes_.size(); i++ )
  {
    idx_t inode = cornerNodes_[i];

    for ( idx_t j = 0; j < rank_; ++j )
    {
      outerDofs_[k++] = microDofs.getDofIndex ( inode, dofTypes_[j] );
    }
  }
  JEM_ASSERT ( k == outerDofs_.size() );
}

//-----------------------------------------------------------------------
//   setState_
//-----------------------------------------------------------------------

void FE2Material::MicroModel::setState_

  ( const Ref<MicroState> state )

{
  Properties microParams;

  // Set strain

  Vector strain ( strCount_ );
  strain = 0.0;

  state->getStrain ( strain );

  setMicroCons_ ( strain );

  //if ( psDOF_ != -1 )
  //{
  //  cons_->eraseConstraint ( outerDofs_[psDOF_] );
  //  //cons2_->eraseConstraint ( outerDofs_[psDOF_] );
  //}

  // Set time and time step

  data_.set ( Globdat::TIME_STEP, iStep_ );

  if ( state->hasTime() )
  {
    data_.set ( Globdat::TIME, state->getTime() );
  }
}

//-----------------------------------------------------------------------
//   setMicroCons_
//-----------------------------------------------------------------------

void FE2Material::MicroModel::setMicroCons_ 

  ( const Vector& strain )

{
  switch ( bndCondition_ )
  {
    case LINEAR:

      setMicroLinearCons_ ( strain );
      break;

    case PERIODIC:

      setMicroCornerCons_ ( strain );

      setMicroPeriodicCons_ ();

      break;
  }

  //if ( psDOF_ != -1 )
  //{
  //  cons_->eraseConstraint ( outerDofs_[psDOF_] );
  //  cons2_->eraseConstraint ( outerDofs_[psDOF_] );
  //}
}

//-----------------------------------------------------------------------
//   setMicroLinearCons_
//-----------------------------------------------------------------------

void FE2Material::MicroModel::setMicroLinearCons_ 

  ( const Vector& strain )

{
  Ref<DofSpace>  microDofs = cons_->getDofSpace ();

  idx_t xtype  = dofTypes_[0] ;
  idx_t ytype  = dofTypes_[1] ;

  idx_t ixdof, iydof;
  idx_t id;

  double xval, yval;

  Vector      nCoords ( rank_ );
  ArrayBuffer<idx_t> doneNodes;

  for ( idx_t i = 0; i < 4; i++ )
  {
    IdxVector  inodes = bndNodes_[i];

    // Set the correct constraints.

    for ( idx_t in = 0; in < inodes.size(); in++ )
    {
      id   = inodes[in];

      if ( testany ( doneNodes.toArray() == id ) )
      {
        continue;
      }

      nodes_.getNodeCoords ( nCoords, id );

      ixdof = microDofs->getDofIndex ( id, xtype );
      iydof = microDofs->getDofIndex ( id, ytype );

      xval  = nCoords[0] * strain[0] + 0.5 * nCoords[1] * strain[2];
      yval  = nCoords[1] * strain[1] + 0.5 * nCoords[0] * strain[2];

      cons_->addConstraint ( ixdof, xval );
      cons_->addConstraint ( iydof, yval );

      doneNodes.pushBack ( id );
    }
  }
}

//-----------------------------------------------------------------------
//   setMicroCornerCons_
//-----------------------------------------------------------------------

void FE2Material::MicroModel::setMicroCornerCons_ 

  ( const Vector& strain )

{
  Ref<DofSpace>  microDofs = cons_->getDofSpace ();

  Vector  values ( outerDofs_.size() );

  matmul ( values, D_.transpose(), strain );

  //System::out() << "setMicroCornerCons strain " << strain << " values " << values << '\n';

  // prescribed displacement on the three corner nodes

  for ( idx_t i = 0; i < outerDofs_.size(); ++i )
  {
    if ( i != psDOF_ )
    {
      cons_->addConstraint ( outerDofs_[i], values[i] );
    }
  }
}

//-----------------------------------------------------------------------
//   setMicroPeriodicCons_
//-----------------------------------------------------------------------

void FE2Material::MicroModel::setMicroPeriodicCons_ ()

{
  Ref<DofSpace>  dofs = cons_->getDofSpace ();

  IdxVector  idofs(2);
  Vector     coefs(2);

  coefs = 1.;

  // loop over faces
  for ( idx_t ix = 0; ix < rank_; ix++ )
  {
    IdxVector  inodes  ( bndNodes_[ix*2] );
    IdxVector  jnodes  ( bndNodes_[ix*2+1] );
    idx_t      imaster = cornerNodes_[ix+1];

    // loop over nodes of edges

    for ( idx_t  in = 0; in < inodes.size(); in++ )
    {
      if ( jnodes[in] != imaster )
      {
        // loop over dof types

        for ( idx_t jx = 0; jx < rank_; ++jx )
        {
          // master dofs: opposite node and relevant corner node

          idofs[0] = dofs->getDofIndex ( inodes[in], dofTypes_[jx] );
          idofs[1] = dofs->getDofIndex ( imaster,    dofTypes_[jx] );

          // dofs of dependent (slave) node jd

          idx_t jdof = dofs->getDofIndex ( jnodes[in], dofTypes_[jx] );

          // add constraint

          cons_ ->addConstraint ( jdof, idofs, coefs );
          cons2_->addConstraint ( jdof, idofs, coefs );
        }
      }
    }
  }
}

//-----------------------------------------------------------------------
//   calcMicroStiff_
//-----------------------------------------------------------------------

void FE2Material::MicroModel::calcMicroStiff_ 

  ( const Matrix& stiff,
    const Vector& stress )
{
  using jive::model::Actions;
  using jive::model::ActionParams;
  using jive::algebra::AbstractMatrix;
  using jive::solver::ConstrainedMatrix;
  using jive::util::evalSlaveDofs;
  using jive::util::evalMasterDofs;
  using jive::util::joinNames;
  using jive::fem::FEMatrixBuilder;

  Properties           microParams;

  String context = "calcMicroStiff_";
  String parent  = props_.getName ( );

  Assignable<ElementSet> mesh = ElementSet::get ( data_, context );
  Ref<DofSpace>          dofs = cons_->getDofSpace ();

  Ref<FEMatrixBuilder>     mB   = newInstance<FEMatrixBuilder>
                           ( "microMBuilder", mesh, dofs );

  Ref<SkylineLU>         solver = newInstance<SkylineLU>
                           ( joinNames ( parent, "micro.solver" ),
			     mB->getMatrix (),
			     cons_ );

  solver->configure ( props_ );
  solver->getConfig ( conf_ );

  Vector               strIncr ( strCount_ );

  Ref<AbstractMatrix>  microMatrix = mB->getMatrix ();
  const idx_t          dofCount    = dofs->dofCount  ();

  Vector               fint ( dofCount );

  Matrix               unit ( strCount_, strCount_ );

  TensorIndex i,j;
  unit(i,j) = where ( i==j, 1., 0. );

  // Set homogeneous constraints for the micro model.

  strIncr = 0.;
  setMicroCons_ ( strIncr );

  // Assemble the tangent micro stiffness matrix and compute the internal
  // force vector.

  mB->setToZero ();

  fint = 0.0;

  microParams.set ( ActionParams::MATRIX0,    mB   );
  microParams.set ( ActionParams::INT_VECTOR, fint );

  model_->takeAction ( Actions::GET_MATRIX0,
		       microParams, data_ );

  mB->updateMatrix ();

  // Assemble the micro strain-stress stiffness matrix, column by column.

  const idx_t bndDofCount = D_.size(1);

  Vector    f      ( bndDofCount );
  Vector    h      ( bndDofCount );
  Vector    rhs1   ( dofCount );
  Vector    rhs2   ( dofCount );
  Vector    result ( dofCount );
  Vector    solut  ( dofCount );

  Matrix    Dt         = D_.transpose ();

  // Build the modified matrix K^* = C^T * K * C

  microMatrix = newInstance<ConstrainedMatrix> (
    microMatrix,
    cons2_
  );

  for ( idx_t j = 0; j < strCount_; j++ )
  {
    f  = matmul ( Dt, unit(ALL,j) );

    // rhs1 = [0 f]^T

    rhs1 = 0.;
    select ( rhs1, outerDofs_ ) = f;

    microMatrix->matmul ( result, rhs1 );

    h = select ( result, outerDofs_ );

    // compute Kaa^{-1}g

    rhs2 = 0.;
    select ( rhs2, innerDofs_ ) = select ( result, innerDofs_ );

    solver->solve ( solut, rhs2 );

    microMatrix->matmul ( result, solut );

    f = h - select ( result, outerDofs_ );

    stiff(ALL,j) = matmul ( D_, f );
  }

  // homogenized tangent moduli matrix and
  // homogenized stress.

  stiff *= v0Inv_;

  // do not forget the internal force at independent
  // dofs is increased by an amount = C^T * fint(dependent dof)

  evalMasterDofs   ( fint, *cons2_ );
  h       = select ( fint, outerDofs_ ) ;
  stress  = matmul ( D_, h );
  stress *= v0Inv_;

  mB->setToZero();
  mB->clear();
  solver->clear();
} 

//-----------------------------------------------------------------------
//   subStepping_
//-----------------------------------------------------------------------

void FE2Material::MicroModel::subStepping_ 

  ( const Exception&      ex,
    const Ref<MicroState> state0,
    const Ref<MicroState> state,
    const Properties&     microParams  )
{
  Properties  info = SolverInfo::get ( data_ );
  bool        conv = true;

  info.find ( conv, SolverInfo::CONVERGED );

  //if ( conv )
  //{
  //  throw ex;
  //}

  System::info() << "Micromodel " << myID_ << ": Micro-BVP did not converge:\n"
    << ex.name  () << "\n" << ex.where () << "\n" << ex.what  () << "\n\n";

  if ( subStepLevel_ == maxSubStepLevel_ )
  {
    throw ex;
  }

  ++subStepLevel_;
  System::out() << "Micromodel " << myID_ << ": Substepping into level " << subStepLevel_ << endl;

  // 1. First interval

  Ref<MicroState> stateHalf = newInstance<MicroState> ( strCount_, state, state0 );

  setState_ ( stateHalf );
  
  Vector eps2 ( strCount_ );
  stateHalf->getStrain ( eps2 );

  System::info() << "Micromodel " << myID_ << ": 1st half substepping of level " << subStepLevel_ << endl;
  
  try
  {
    chain_->run ( data_ );
  }
  catch ( const Exception& ex1 )
  {
    subStepping_ ( ex1, state0, stateHalf, microParams );
  }

  System::info() << "Micromodel " << myID_ << ": Solving first interval of level " << subStepLevel_ << "... done!\n";

  // 2. Second interval

  setState_ ( state );
  
  Vector eps3 ( strCount_ );
  state->getStrain ( eps3 );

  System::info() << "Micromodel " << myID_ << ": 2nd half substepping of level " << subStepLevel_ << endl;

  try
  {
    chain_->run ( data_ );
  }
  catch ( const Exception& ex2 )
  {
    subStepping_ ( ex2, stateHalf, state, microParams );
  }
  
  System::info() << "Micromodel " << myID_ << ": Solving second interval of level " << subStepLevel_ << "... done!\n";
  --subStepLevel_;
}

//-----------------------------------------------------------------------
//   computeDissipation_
//----------------------------------------------------------------------

void FE2Material::MicroModel::computeDissipation_ ()

{
  Properties dissParams;
  double     value;

  // let micro model integrate dissipation over its domain

  model_->takeAction ( "GET_DISSIPATION", dissParams, data_ );

  Ref<Dict>  d = dissParams.getContents();

  // collect multiple contributions given by multiple models

  dissipation_ = 0.;

  for ( Ref<DictEnum> e = d->enumerate(); !e->atEnd(); e->toNext() )
  {
    dissParams.find ( value, e->getKey() );
    dissipation_ += value;
  }
  dissipation_ *= v0Inv_; // store volume average
}

