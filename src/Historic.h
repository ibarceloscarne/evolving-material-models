/*
 * 
 *  Copyright (C) 2017 Knowledge Centre WMC / TU Delft. All rights reserved.
 *  
 *  This pure virtual class implements functions for a material 
 *  model with history
 *  
 *  Author: Iuri Barcelos, i.barcelos@wmc.eu
 *  Date: November 2017
 *
 */

#ifndef HISTORIC_H 
#define HISTORIC_H 

#include <jem/base/Array.h>
#include <jive/Array.h>

using jem::idx_t;
using jive::Vector;

class Historic
{
 public:

  virtual Vector          getHist

    ( const idx_t           ipoint ) = 0;

  virtual Vector          getHist2

    ( const idx_t           ipoint ) = 0;

  virtual void            setHist

    ( const Vector&         hist,
      const idx_t           ipoint ) = 0;

  virtual idx_t           histCount () = 0;

  virtual double          getTotalDissipation () = 0;
};

#endif
