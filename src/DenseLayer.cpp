/*
 * Copyright (C) 2019 TU Delft. All rights reserved.
 *
 * Class that implements a fully-connected neural layer. Each
 * of its neurons will connect themselves with every neuron of
 * the previous layer (towards the input layer). Note that this
 * does not guarantee fully connectivity to the next layer.
 *
 * Author: Iuri Barcelos, i.barceloscarneiromrocha@tudelft.nl
 * Date:   May 2019
 * 
 */

#include <jem/base/Error.h>
#include <jem/base/System.h>
#include <jem/base/Float.h>
#include <jem/numeric/algebra/matmul.h>
#include <jem/util/Timer.h>
#include <jive/Array.h>
#include <jive/model/Actions.h>
#include <jive/model/ModelFactory.h>
#include <jive/model/StateVector.h>
#include <jive/util/error.h>

#include "DenseLayer.h"
#include "LearningNames.h"

using jem::Error;
using jem::numeric::matmul;
using jem::util::Timer;

using jive::IdxVector;
using jive::util::XDofSpace;
using jive::util::sizeError;
using jive::model::StateVector;

//-----------------------------------------------------------------------
//   static data
//-----------------------------------------------------------------------

const char* DenseLayer::SIZE           = "size";
const char* DenseLayer::ACTIVATION     = "activation";
const char* DenseLayer::INITIALIZATION = "init";
const char* DenseLayer::USEBIAS        = "useBias";

//-----------------------------------------------------------------------
//   constructor & destructor
//-----------------------------------------------------------------------

DenseLayer::DenseLayer

  ( const String&      name,
    const Properties&  conf,
    const Properties&  props,
    const Properties&  globdat ) : Model ( name )

{
  String func ( "identity" );

  size_ = 0;

  inputLayer_  = false;
  outputLayer_ = false;
  mirrored_    = false;
  useBias_     = true;

  Properties myProps = props.getProps ( myName_ );
  Properties myConf  = conf.makeProps ( myName_ );

  System::out() << "myProps " << myProps << "\n";
  myProps.get  ( size_, SIZE );
  myProps.find ( func, ACTIVATION );
  myProps.find ( useBias_, USEBIAS );

  myConf. set ( SIZE, size_ );
  myConf. set ( ACTIVATION, func );
  myConf. set ( USEBIAS, useBias_ );

  func_ = NeuralUtils::getActivationFunc ( func );
  grad_ = NeuralUtils::getActivationGrad ( func );
  hess_ = NeuralUtils::getActivationHess ( func );

  func = "glorot";

  myProps.find ( func, INITIALIZATION );
  myConf.set   ( INITIALIZATION, func );

  init_ = NeuralUtils::getInitFunc ( func );

  iNeurons_.resize ( size_ );
  iNeurons_ = 0;

  Ref<XAxonSet>   aset = XAxonSet  ::get ( globdat, getContext()    );
  Ref<XNeuronSet> nset = XNeuronSet::get ( globdat, getContext()    );
  Ref<XDofSpace>  dofs = XDofSpace:: get ( aset->getData(), globdat );

  for ( idx_t in = 0; in < size_; ++in )
  {
    iNeurons_[in] = nset->addNeuron();
  }

  weightType_ = dofs->findType ( LearningNames::WEIGHTDOF );
  biasType_   = dofs->findType ( LearningNames::BIASDOF   );

  Ref<Model> layer;

  if ( myProps.find ( layer, LearningParams::IMAGE ) )
  {
    Ref<DenseLayer> image = dynamicCast<DenseLayer> ( layer );

    if ( image == nullptr )
    {
      throw Error ( JEM_FUNC, "Broken symmetry" );
    }

    globdat.get ( inpNeurons_, LearningParams::PREDECESSOR );

    idx_t presize = inpNeurons_.size();

    iInpWts_.resize ( size_, presize );
    iInpWts_ = -1;

    IdxMatrix imagewts;
    image->getInpDofs ( imagewts );

    iInpWts_ = imagewts.transpose();

    weights_.resize ( size_, inpNeurons_.size() );
    weights_ = 0.0;

    Vector state;

    if ( useBias_ )
    {
      iBiases_.resize ( size_ );
      biases_. resize ( size_ );
      iBiases_ = -1;
      biases_  = 0.0;

      IdxVector newbaxons = aset->addAxons ( size_ );
      dofs->addDofs ( newbaxons, biasType_   );

      System::out() << "Mirrored bias: Creating " << size_ << " axons\n";

      StateVector::get ( state, dofs, globdat );

      dofs->getDofIndices ( iBiases_, newbaxons, biasType_ );
      state[iBiases_] = 0.0;
    }

    StateVector::get ( state, dofs, globdat );

    for ( idx_t in = 0; in < size_; ++in )
    {
      weights_(in,ALL) = state[iInpWts_(in,ALL)];
    }

    mirrored_ = true;
  }
  else if ( globdat.find ( inpNeurons_, LearningParams::PREDECESSOR ) )
  {
    idx_t presize = inpNeurons_.size();

    IdxVector idofs ( presize );

    
    iInpWts_.resize ( size_, presize );
    iInpWts_ = -1;

    weights_.resize ( size_, inpNeurons_.size() );
    weights_ = 0.0;

    System::out() << "DenseLayer::init\n";
    init_ ( weights_, globdat );

    Vector state;

    IdxVector newaxons  = aset->addAxons ( presize * size_ );
    System::out() << "Normal inpWeights: Creating " << presize * size_ << " axons\n";

    dofs->addDofs ( newaxons,  weightType_ );

    StateVector::get ( state, dofs, globdat );

    for ( idx_t in = 0; in < size_; ++in )
    {
      IdxVector myaxons ( newaxons[slice (in*presize,(in+1)*presize )] );

      dofs->getDofIndices ( idofs,  myaxons,   weightType_ );
      iInpWts_(in,ALL) = idofs;
      state[idofs] = weights_(in,ALL);
    }

    if ( useBias_ )
    {
      iBiases_.resize ( size_ );
      biases_. resize ( size_ );
      iBiases_ = -1;
      biases_  = 0.0;

      IdxVector newbaxons = aset->addAxons ( size_ );
      dofs->addDofs ( newbaxons, biasType_   );
      System::out() << "Normal biases: Creating " << size_ << " axons\n";

      StateVector::get ( state, dofs, globdat );

      dofs->getDofIndices ( iBiases_, newbaxons, biasType_ );
      state[iBiases_] = 0.0;
    }
  }
  //else
  //{
  //  inputLayer_ = true;
  //}

  myProps.find ( inputLayer_, LearningNames::FIRSTLAYER );
  myProps.find ( outputLayer_, LearningNames::LASTLAYER );

  System::out() << "Input? " << inputLayer_ << "\n";
  System::out() << "Output? " << outputLayer_ << "\n";

  axons_   = aset;
  neurons_ = nset;
  dofs_    = dofs;

  globdat.set ( LearningParams::PREDECESSOR, iNeurons_.clone() );
}

DenseLayer::~DenseLayer ()
{}

//-----------------------------------------------------------------------
//   configure
//-----------------------------------------------------------------------

void DenseLayer::configure

  ( const Properties& props,
    const Properties& globdat )

{
  //outputLayer_ = inputLayer_ ? false :
  //               mirrored_   ? min ( iInpWts_ ) == 0 :
  //               max ( max ( iBiases_ ), max ( iInpWts_ ) ) == dofs_->dofCount() - 1;

}

//-----------------------------------------------------------------------
//   getConfig
//-----------------------------------------------------------------------

void DenseLayer::getConfig

  ( const Properties& conf,
    const Properties& globdat )

{
}

//-----------------------------------------------------------------------
//   takeAction
//-----------------------------------------------------------------------

bool DenseLayer::takeAction

  ( const String&     action,
    const Properties& params,
    const Properties& globdat )

{
  using jive::model::Actions;

  //if ( action == Actions::SHUTDOWN )
  //{
  //  System::out() << "Layer " << myName_ << " statistics:\n";
  //  System::out() << "Time spent propagating " << ptot_ << "\n";
  //  System::out() << "... matmul " << pmmul_ << "\n";
  //  System::out() << "... func   " << pfunc_ << "\n";
  //  System::out() << "... bias   " << pbias_ << "\n";
  //  System::out() << "... allocs " << palloc_ << "\n";
  //  System::out() << "Time spent backpropagating " << bptot_ << "\n";
  //  System::out() << "... matmul " << bpmmul_ << "\n";
  //  System::out() << "... func   " << bpfunc_ << "\n";
  //  System::out() << "... bias  "  << bpbias_ << "\n";
  //  System::out() << "... alloc "  << bpalloc_ << "\n";
  //  System::out() << '\n';

  //  return true;
  //}

  if ( action == LearningActions::UPDATE )
  {
    if ( !inputLayer_ )
    {
      update_ ( globdat );
    }

    return true;
  }

  if ( action == LearningActions::PROPAGATE ||
       action == LearningActions::RECALL       )
  {
    Ref<NData> data;
    params.get ( data, LearningParams::DATA );

    if ( inputLayer_ )
    {
      if ( data->inpSize() != size_ )
      {
	System::out() << "Size error: " << size_ << " " << data->inpSize() << '\n';
        sizeError ( JEM_FUNC, "Neural input", size_, data->inpSize() );
      }

      data->init ( neurons_->size() );

      select ( data->values,      iNeurons_, ALL ) = data->inputs;
      select ( data->activations, iNeurons_, ALL ) = data->inputs;

      return true;
    }

    propagate_ ( data, globdat );

    if ( outputLayer_ )
    {
      if ( data->outSize() != size_ )
      {
        sizeError ( JEM_FUNC, "Neural output", size_, data->outSize() );
      }

      data->outputs = select ( data->activations, iNeurons_, ALL );
    }

    return true;
  }

  if ( action == LearningActions::FORWARDJAC )
  {

    Ref<NData> data, rdata;
    params.get ( data,  LearningParams::DATA  );
    params.get ( rdata, LearningParams::RDATA );

    if ( inputLayer_ )
    {
      rdata->init ( neurons_->size() );

      rdata->inputs += select ( data->deltas, iNeurons_, ALL );

      select ( rdata->values, iNeurons_, ALL )      = rdata->inputs;
      select ( rdata->activations, iNeurons_, ALL ) = rdata->inputs;

      return true;
    }

    forwardJacobian_ ( data, rdata, globdat );

    return true;
  }

  if ( action == LearningActions::BACKPROPAGATE )
  {
    Ref<NData> data;
    params.get ( data, LearningParams::DATA );

    Vector grads;
    params.get ( grads, LearningParams::GRADS );

    if ( outputLayer_ )
    {
      select ( data->deltas, iNeurons_, ALL ) = data->outputs;
    }

    if ( !inputLayer_ )
    {
      backPropagate_ ( data, grads, globdat );
    }

    return true;
  }

  if ( action == LearningActions::BACKWARDJAC )
  {
  
    Ref<NData> data, rdata;
    params.get ( data,  LearningParams::DATA  );
    params.get ( rdata, LearningParams::RDATA );

    Vector grads;
    params.get ( grads, LearningParams::GRADS );

    if ( outputLayer_ )
    {
      select ( rdata->deltas, iNeurons_, ALL ) = 0.0;
    }

    if ( !inputLayer_ )
    {
      backJacobian_ ( data, rdata, grads, globdat );
    }

    return true;
  }

  if ( action == LearningActions::GETJACOBIAN )
  {
    Ref<NData> data;
    params.get ( data, LearningParams::DATA );

    if ( !inputLayer_ )
    {
      getJacobian_ ( data, globdat );
    }

    return true;
  }

  return false;
}

//-----------------------------------------------------------------------
//   getInpDofs
//-----------------------------------------------------------------------

void DenseLayer::getInpDofs

  (       IdxMatrix& dofs )

{
  dofs.ref ( iInpWts_ );
}

//-----------------------------------------------------------------------
//   update_
//-----------------------------------------------------------------------

void DenseLayer::update_

  ( const Properties& globdat )

{
  Vector state;
  StateVector::get ( state, dofs_, globdat );

  if ( useBias_ )
  {
    biases_ = state[iBiases_];
  }

  for ( idx_t in = 0; in < size_; ++in )
  {
    weights_(in,ALL) = state[iInpWts_(in,ALL)];
  }
}

//-----------------------------------------------------------------------
//   propagate_
//-----------------------------------------------------------------------

void DenseLayer::propagate_

  ( const Ref<NData>  data,
    const Properties& globdat  )

{
  ptot_.start();

  palloc_.start();
  Matrix netValues ( size_, data->batchSize() );
  netValues = 0.0;

  Matrix acts        ( select ( data->activations, inpNeurons_, ALL ) );
  palloc_.stop();

  pmmul_.start();
  netValues = matmul ( weights_, acts );
  pmmul_.stop();


  pbias_.start();
  if ( useBias_ )
  {
    for ( idx_t is = 0; is < data->batchSize(); ++is )
    {
      netValues(ALL,is) += biases_;
    }
  }
  pbias_.stop();

  select ( data->values, iNeurons_, ALL ) = netValues;

  pfunc_.start();
  func_ ( netValues );
  pfunc_.stop();

  //if ( true )
  //{
  //  System::out() << "Input " << acts << " weights " << weights_ << " bias " << biases_ << " output " << netValues << '\n';
  //}

  select ( data->activations, iNeurons_, ALL ) = netValues;

  ptot_.stop();
}

//-----------------------------------------------------------------------
//   backPropagate_
//-----------------------------------------------------------------------

void DenseLayer::backPropagate_

  ( const Ref<NData>   data,
          Vector&      grads,
    const Properties&  globdat  )

{
  bptot_.start();
  bpalloc_.start();
  Matrix derivs ( select ( data->values, iNeurons_, ALL ) );
  bpalloc_.stop();

  bpfunc_.start();
  grad_ ( derivs );

  //select ( data->deltas, iNeurons_, ALL ) *= derivs;
  bpfunc_.stop();

  bpalloc_.start();
  Matrix acts        ( select ( data->activations, inpNeurons_, ALL ) );

  //Matrix deltas ( select ( data->deltas, iNeurons_, ALL ) );
  Matrix deltas ( select ( data->deltas, iNeurons_, ALL ) * derivs );

  Matrix gradmat ( size_, inpNeurons_.size() );
  gradmat = 0.0;
  bpalloc_.stop();

  bpmmul_.start();
  gradmat = matmul ( deltas, acts.transpose() );
  bpmmul_.stop();

  for ( idx_t in = 0; in < size_; ++in )
  {
    grads[iInpWts_(in,ALL)] += gradmat     (in,ALL);
    
    bpbias_.start();
    if ( useBias_ )
    {
      grads[iBiases_[in]]     += sum ( deltas(in,ALL) );
    }
    bpbias_.stop();
  }
  
  bpmmul_.start();
  // NB: 08-10-19 - was +=, removed the +
  select ( data->deltas, inpNeurons_, ALL ) = matmul ( weights_.transpose(), deltas );  
  bpmmul_.stop();
  bptot_.stop();
}

//-----------------------------------------------------------------------
//   forwardJacobian_
//-----------------------------------------------------------------------

void DenseLayer::forwardJacobian_

  ( const Ref<NData>  data,
    const Ref<NData>  rdata,
    const Properties& globdat  )

{
  Matrix netValues ( size_, data->batchSize() );
  netValues = 0.0;

  Matrix acts ( select ( rdata->activations, inpNeurons_, ALL ) );

  netValues = matmul ( weights_, acts );

  select ( rdata->values, iNeurons_, ALL ) = netValues;

  Matrix vals ( select ( data->values, iNeurons_, ALL ) );

  grad_ ( vals );

  select ( rdata->activations, iNeurons_, ALL ) = vals * netValues;

  //System::out() << "J-forward vals" << netValues << "\n";
  //System::out() << "J-forward activation " << vals*netValues << "\n";
}

//-----------------------------------------------------------------------
//   backJacobian_
//-----------------------------------------------------------------------

void DenseLayer::backJacobian_

  ( const Ref<NData>   data,
    const Ref<NData>   rdata,
          Vector&      grads,
    const Properties&  globdat  )

{
  Matrix derivs  ( select ( data->values, iNeurons_, ALL ) );
  Matrix derivs2 ( select ( data->values, iNeurons_, ALL ) );

  grad_ ( derivs  );
  hess_ ( derivs2 );

  //System::out() << "fp " << derivs << " fpp " << derivs2 << "\n";

  Matrix predeltas ( select ( data->deltas, iNeurons_, ALL ) );
  Matrix deltas    ( select ( data->deltas, iNeurons_, ALL ) * derivs );
  Matrix rdeltas   ( select ( rdata->deltas, iNeurons_, ALL ) );
  Matrix rvalues   ( select ( rdata->values, iNeurons_, ALL ) );

  rdeltas = rdeltas*derivs + predeltas*derivs2*rvalues;

  //System::out() << "rdeltas " << rdeltas << "\n";

  Matrix acts    ( select ( data->activations,  inpNeurons_, ALL ) );
  Matrix racts   ( select ( rdata->activations, inpNeurons_, ALL ) );

  Matrix gradmat ( size_, inpNeurons_.size() );
  gradmat = 0.0;

  gradmat = matmul ( rdeltas, acts.transpose() ) + matmul ( deltas, racts.transpose() );

  for ( idx_t in = 0; in < size_; ++in )
  {
    grads[iInpWts_(in,ALL)] += gradmat (in,ALL);
    
    if ( useBias_ )
    {
      grads[iBiases_[in]] += sum ( rdeltas(in,ALL) );
    }
  }
  
  select ( rdata->deltas, inpNeurons_, ALL ) = matmul ( weights_.transpose(), rdeltas );  
}

//-----------------------------------------------------------------------
//   getJacobian_
//-----------------------------------------------------------------------

void DenseLayer::getJacobian_

  ( const Ref<NData>  data,
    const Properties& globdat )

{
  idx_t bsize = data->batchSize();
  idx_t jsize = data->outSize();

  if ( jacobian_.size(0) != jsize * bsize )
  {
    jacobian_.resize ( jsize*bsize, inpNeurons_.size() );
    jacobian_ = 0.0;
  }

  Matrix derivs ( select ( data->values, iNeurons_, ALL ) );

  grad_ ( derivs );

  if ( outputLayer_ )
  {
    for ( idx_t b = 0; b < bsize; ++b )
    {
      for ( idx_t i = 0; i < size_; ++i )
      {
        jacobian_(i+b*jsize,ALL) = weights_(i,ALL) * derivs(i,b);
      }
    }
  }
  else
  {
    for ( idx_t b = 0; b < bsize; ++b )
    {
      for ( idx_t i = 0; i < size_; ++i )
      {
        data->jacobian ( slice(b*jsize,(b+1)*jsize), i ) *= derivs(i,b);
      }
    }

    jacobian_ = matmul ( data->jacobian, weights_ );
  }

  data->jacobian.ref ( jacobian_ );
}

//=======================================================================
//   related functions
//=======================================================================

//-----------------------------------------------------------------------
//   newDenseLayer
//-----------------------------------------------------------------------


Ref<Model>            newDenseLayer

  ( const String&       name,
    const Properties&   conf,
    const Properties&   props,
    const Properties&   globdat )

{
  // Return an instance of the class.

  return newInstance<DenseLayer> ( name, conf, props, globdat );
}

//-----------------------------------------------------------------------
//   declareDenseLayer
//-----------------------------------------------------------------------

void declareDenseLayer ()
{
  using jive::model::ModelFactory;

  // Register the StressModel with the ModelFactory.

  ModelFactory::declare ( "Dense", newDenseLayer );
}

