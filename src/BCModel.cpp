/*
 *  TU Delft 
 *
 *  Iuri Barcelos, August 2018
 *
 *  BC model adapted from VEVPLoadModel. 
 *
 */

#include <jem/base/System.h>
#include <jem/base/Error.h>
#include <jem/base/array/operators.h>
#include <jem/base/array/select.h>
#include <jem/numeric/algebra/utilities.h>
#include <jem/numeric/utilities.h>
#include <jem/numeric/func/UserFunc.h>
#include <jem/util/ArrayBuffer.h>
#include <jem/util/Event.h>
#include <jem/io/FileReader.h>
#include <jive/util/error.h>
#include <jive/util/Printer.h>
#include <jive/util/utilities.h>
#include <jive/util/Constraints.h>
#include <jive/util/Globdat.h>
#include <jive/util/FuncUtils.h>
#include <jive/model/Actions.h>
#include <jive/model/ModelFactory.h>
#include <jive/implict/ArclenActions.h>
#include <jive/model/StateVector.h>
#include <jive/implict/SolverInfo.h>

#include <math.h>

#include "declare.h"
#include "BCModel.h"
#include "SolverNames.h"

using jem::util::ArrayBuffer;
using jem::io::endl;
using jem::io::FileReader;
using jem::numeric::UserFunc;
using jive::IntVector;
using jive::model::Actions;
using jive::model::ActionParams;
using jive::util::FuncUtils;
using jive::util::Globdat;
using jive::model::StateVector;
using jive::implict::ArclenActions;
using jive::implict::ArclenParams;
using jive::implict::SolverInfo;

//=======================================================================
//   class BCModel
//=======================================================================


//-----------------------------------------------------------------------
//   static data
//-----------------------------------------------------------------------

const char* BCModel::MODE_PROP        = "mode";
const char* BCModel::NODEGROUPS_PROP  = "nodeGroups";
const char* BCModel::DOFS_PROP        = "dofs";
const char* BCModel::UNITVEC_PROP     = "unitVec";
const char* BCModel::SHAPE_PROP       = "shape";
const char* BCModel::STEP_PROP        = "step";
const char* BCModel::ALENGROUP_PROP   = "arclenGroup";
const char* BCModel::LOAD_PROP        = "loads";      
const char* BCModel::VALFILE_PROP     = "valFile";
const char* BCModel::MAXNORM_PROP     = "maxNorm";

//-----------------------------------------------------------------------
//   constructor & destructor
//-----------------------------------------------------------------------

BCModel::BCModel

  ( const String&     name,
    const Properties& conf,
    const Properties& props,
    const Properties& globdat ):

    Model ( name )

{
  time_  = 0.0;
  time0_ = 0.0;

  dt_   = 1.;
  
  unitVec_    = 0.;
  initVals_   = 0.;

  mode_      = LOAD;

  ulUpd_     = false;
  master_    = 0;
  alenGroup_ = 0;

  maxNorm_   = jem::maxOf ( maxNorm_ );

  pLoad_     = nullptr;

  nodes_ = NodeSet::find    ( globdat );
  dofs_  = XDofSpace::get   ( nodes_.getData(), globdat );
  cons_  = Constraints::get ( dofs_, globdat );

  fileVals_ = false;
  step_     = 0;
  step0_    = 0;

  configure ( props, globdat );
}

BCModel::~BCModel ()
{}

//-----------------------------------------------------------------------
//   configure
//-----------------------------------------------------------------------

void BCModel::configure

  ( const Properties& props,
    const Properties& globdat )

{
  if ( props.contains( myName_ ) )
  {
    Properties myProps = props.getProps ( myName_ );

    String mode;
    myProps.get ( mode, MODE_PROP );

    if ( mode == "load" )
      mode_ = LOAD;
    else if ( mode == "disp" )
      mode_ = DISP;
    else if ( mode == "arclen" )
      mode_ = ALEN;
    else
      throw IllegalInputException ( JEM_FUNC,
	    "Invalid BC mode. Choose either 'load', 'disp' or 'arclen'." );

    myProps.get  ( nodeGroups_, NODEGROUPS_PROP );
    numgroups_ = nodeGroups_.size ( );

    myProps.get  ( dofTypes_,  DOFS_PROP      );
    myProps.get  ( unitVec_,   UNITVEC_PROP   );

    if ( mode_ == ALEN )
    {
      myProps.get ( alenGroup_, ALENGROUP_PROP );
      myProps.find ( maxNorm_, MAXNORM_PROP );

      if ( alenGroup_ == -1 )
      {
	Properties loadProps;

	if ( myProps.find ( loadProps, LOAD_PROP ) )
	{
	  if ( pLoad_ == nullptr )
	  {
	    pLoad_ = newInstance<PointLoadModel> ( LOAD_PROP );
	    pLoad_->configure ( myProps, globdat );
	  }
	}
	else
	{
          throw IllegalInputException ( JEM_FUNC,
	    "Invalid arclength mode. Either specify a group or give PointLoadModel data." );
	}
      }
    }

    String args = "t";
  
    shape_  = FuncUtils::newFunc ( args, SHAPE_PROP, myProps, globdat );
    FuncUtils::resolve ( *shape_, globdat );

    initVals_.resize ( numgroups_ );
    initVals_ = 0.0;

    if ( dofTypes_.size() != numgroups_ || unitVec_.size() != numgroups_ )
      throw IllegalInputException ( JEM_FUNC,
	    "nodeGroups, dofTypes and unitVector must have the same size." );

    myProps.get  ( dt_,  STEP_PROP  );

    if ( mode_ == DISP )
    {
      myProps.find ( maxNorm_, MAXNORM_PROP );

      ArrayBuffer<idx_t> mbuf;

      for ( idx_t g = 0_idx; g < numgroups_; ++g )
      {
	Assignable<NodeGroup> group = NodeGroup::get ( nodeGroups_[g], nodes_, globdat, "");

	idx_t nn = group.size();

	IdxVector inodes ( nn );
	inodes = group.getIndices ();

	idx_t itype = dofs_->findType ( dofTypes_[g] );

	for ( idx_t i = 0_idx; i < nn; ++i )
	{
	  idx_t idof  = dofs_->getDofIndex ( inodes[i], itype );

	  mbuf.pushBack ( idof );
	}
      }

      masters_.ref ( mbuf.toArray() );
    }

    String fname;

    if ( myProps.find ( fname, VALFILE_PROP ) )
    {
      fileVals_ = true;

      System::out() << 
        "BCModel: Reading values directly from file, overriding shape\n";

      if ( mode_ != DISP )
      {
        throw Error ( JEM_FUNC,
	  "BCModel: Reading values from file only supported for DISP mode" );
      }

      Ref<FileReader> in = newInstance<FileReader> ( fname );

      idx_t nsteps = in->parseInt();

      vals_.resize ( nsteps, numgroups_ );
      vals_ = 0.0;

      for ( idx_t i = 0_idx; i < nsteps; ++i )
      {
      	for ( idx_t j = 0_idx; j < numgroups_; ++j )
	{
	  vals_(i,j) = in->parseFloat();
	}
      }
    }
  }
}

//-----------------------------------------------------------------------
//   getConfig
//-----------------------------------------------------------------------

void BCModel::getConfig

  ( const Properties& conf,
    const Properties& globdat ) const

{
  Properties myConf = conf.makeProps ( myName_ );

  String mode;

  if ( mode_ == LOAD )
    mode = "load";
  else if ( mode_ == DISP )
    mode = "disp";
  else
    mode = "undefined";

  myConf.set ( MODE_PROP, mode              );
  myConf.set ( NODEGROUPS_PROP, nodeGroups_ );
  myConf.set ( DOFS_PROP, dofTypes_         );
  myConf.set ( UNITVEC_PROP, unitVec_       );
  myConf.set ( STEP_PROP, dt_               );

  if ( pLoad_ != nullptr )
  {
    pLoad_->getConfig ( myConf, globdat );
  }

  myConf.set ( MAXNORM_PROP, maxNorm_ );

  FuncUtils::getConfig ( myConf, shape_, SHAPE_PROP   ); 
}


//-----------------------------------------------------------------------
//   takeAction
//-----------------------------------------------------------------------

bool BCModel::takeAction

  ( const String&     action,
    const Properties& params,
    const Properties& globdat )

{
  if ( action == ArclenActions::GET_UNIT_LOAD && mode_ == ALEN )
  {
    getUnitLoad_ ( params, globdat );

    return true;
  }

  if ( action == ArclenActions::GET_ARC_FUNC && mode_ == ALEN )
  {
    getArcFunc_ ( params, globdat );

    return true;
  }

  if ( action == SolverNames::SET_STEP_SIZE )
  {
    params.get ( dt_, SolverNames::STEP_SIZE );

    return true;
  }

  if ( action == Actions::GET_EXT_VECTOR && mode_ == LOAD )
  {
    Vector f;

    params.get ( f, ActionParams::EXT_VECTOR );

    applyLoads_ ( f, globdat );

    return true;
  }

  if ( action == Actions::GET_CONSTRAINTS )
  {
    if ( mode_ == LOAD )
      return false;

    applyDisps_ ( params, globdat );

    return true;
  }

  if ( action == SolverNames::CHECK_COMMIT && 
       mode_ != LOAD                          )
       
  {
    Vector u;

    StateVector::get ( u, dofs_, globdat );

    double norm = 0.0;

    if ( mode_ == ALEN )
    {
      norm = (pLoad_ == nullptr) ? fabs(u[master_]) : jem::numeric::norm2 ( u[masters_] );
    }
    else
    {
      norm = jem::numeric::norm2 ( u[masters_] );
    }

    System::out() << "BCModel: Current displacement norm " << norm << "\n";
    System::out() << "BCModel: Max norm " << maxNorm_ << '\n';

    if ( norm > maxNorm_ )
    {
      System::out() << "BCModel: Maximum displacement norm reached. Terminating analysis.\n";

      params.set ( SolverNames::TERMINATE, "sure" );
    }

    if ( fileVals_ )
    {
      if ( step_ == vals_.size(0) )
      {
	System::out() << "BCModel: End of value table reached. Terminating analysis.\n";

	params.set ( SolverNames::TERMINATE, "sure" );
      }
    }

    return true;
  }

  if ( action == Actions::COMMIT )
  {
    time0_ = time_;

    if ( fileVals_ )
    {
      step0_ = step_;
    }

    double lambda;

    Properties info = SolverInfo::get ( globdat );

    if ( info.find ( lambda, SolverInfo::LOAD_SCALE ) )
    {
      Properties  myVars = Globdat::getVariables ( myName_, globdat );

      myVars.set ( "lambda", lambda );
    }

    return true;
  }

  if ( action == Actions::ADVANCE )
  {
    time_ = time0_ + dt_;

    globdat.set ( Globdat::TIME, time_ );

    if ( fileVals_ )
    {
      step_ = step0_ + 1;
    }

    return true;
  }

  if ( action == Actions::CANCEL )
  {
    time_ = time0_;

    if ( fileVals_ )
    {
      step_ = step0_;
    }

    return true;
  }

  return false;
}

//-----------------------------------------------------------------------
//   applyLoads_
//-----------------------------------------------------------------------

void BCModel::applyLoads_

  ( const Vector&     fext,
    const Properties& globdat )

{
  idx_t                 nn;
  Assignable<NodeGroup> group;
  IdxVector             itype ( 1 );
  IdxVector             inodes;
  IdxVector             idofs;
  String                context;

  for ( idx_t ig = 0; ig < numgroups_; ig++ )
  {
    group = NodeGroup::get ( nodeGroups_[ig], nodes_, globdat, context );

    nn = group.size();

    inodes.resize ( nn );
    idofs.resize ( nn );
    inodes = group.getIndices ();

    itype[0] = dofs_->findType ( dofTypes_[ig] );

    double loadfunc = 0.;
    double time = 0.;
    globdat.get ( time, Globdat::TIME );

    loadfunc = shape_->eval ( time );

    double val = unitVec_[ig] * loadfunc / nn;

    dofs_->findDofIndices ( idofs, inodes, itype );

    select ( fext, idofs ) += val;

    // Set load variable (for printing)

    Properties  myVars = Globdat::getVariables ( myName_, globdat );

    myVars.set ( "load", loadfunc );

  }
}

//-----------------------------------------------------------------------
//   applyDisps_
//-----------------------------------------------------------------------

void BCModel::applyDisps_

  ( const Properties& params,
    const Properties& globdat )

{
  idx_t                 nn;
  Assignable<NodeGroup> group;
  IntVector             inodes;
  String                context;

  Vector state;

  for ( idx_t ig = 0; ig < numgroups_; ig++ )
  {
    group = NodeGroup::get ( nodeGroups_[ig], nodes_, globdat, context );

    nn = group.size();

    inodes.resize ( nn );
    inodes = group.getIndices ();

    idx_t itype = dofs_->findType ( dofTypes_[ig] );
    idx_t idof  = dofs_->getDofIndex ( inodes[0], itype );

    if ( mode_ == DISP || ig != alenGroup_ )
    {
      if ( fileVals_ )
      {
	System::out() << "Step " << step_ << " Indexing with " << min ( vals_.size(0)-1, step_-1 ) << '\n';

        double val = vals_ ( min ( vals_.size(0)-1, step_-1 ), ig );  

	System::out() << "Setting group " << ig << " to " << val << '\n';

	cons_->addConstraint ( idof, val );
      }
      else
      {
	double time = 0.;
	globdat.get ( time, Globdat::TIME );

	double loadfunc = shape_->eval ( time );

	double val = unitVec_[ig] * loadfunc + initVals_[ig];

	if ( unitVec_[ig] == 0.0 )
	  cons_->addConstraint ( idof );
	else
	  cons_->addConstraint ( idof, val );
      }
    }

    for ( idx_t in = 1; in < nn; in++ )
    {
      idx_t jdof = dofs_->getDofIndex ( inodes[in], itype );

      cons_->addConstraint( jdof, idof, 1. );
    }
  }

  cons_->compress();
}

//-----------------------------------------------------------------------
//   connect_
//-----------------------------------------------------------------------

void BCModel::connect_ ()

{
  using jem::util::connect;

  connect ( dofs_->newSizeEvent,  this, &BCModel::dofsChanged_ );
  connect ( dofs_->newOrderEvent, this, &BCModel::dofsChanged_ );

  dofs_->resetEvents ();
}

//-----------------------------------------------------------------------
//   dofsChanged_
//-----------------------------------------------------------------------

void BCModel::dofsChanged_ ()

{
  ulUpd_ = false;
}

//-----------------------------------------------------------------------
//   initUnitLoad_
//-----------------------------------------------------------------------

void BCModel::initUnitLoad_  

  ( const Properties& globdat )

{
  String                context;

  unitLoad_.resize ( dofs_->dofCount() );
  unitLoad_ = 0.0;

  if ( pLoad_ == nullptr )
  {
    Assignable<NodeGroup> group = 
      NodeGroup::get ( nodeGroups_[alenGroup_], nodes_, globdat, context );

    idx_t nn = group.size();

    IntVector inodes;
    inodes.resize ( nn );
    inodes = group.getIndices ();

    idx_t itype = dofs_->findType ( dofTypes_[alenGroup_] );

    master_  = dofs_->getDofIndex ( inodes[0], itype );

    unitLoad_[master_] = 1.0;
  }
  else
  {
    Properties params;
    params.set ( ActionParams::SCALE_FACTOR, 1.      );
    params.set ( ActionParams::EXT_VECTOR, unitLoad_ );

    pLoad_->takeAction ( Actions::GET_EXT_VECTOR, params, globdat );

    ArrayBuffer<idx_t>  mbuf;
    ArrayBuffer<double> sbuf;

    for ( idx_t idof = 0; idof < dofs_->dofCount(); ++idof )
    {
      if ( jem::numeric::abs ( unitLoad_[idof] ) > 0.0 )
      {
        mbuf.pushBack ( idof );
	sbuf.pushBack ( unitLoad_[idof] / jem::numeric::abs ( unitLoad_[idof] ) );
      }
    }

    masters_.ref ( mbuf.toArray() );
    signs_.  ref ( sbuf.toArray() );

    //double max = 0.0;

    //for ( idx_t idof = 0; idof < dofs_->dofCount(); ++idof )
    //{
    //  double val = jem::numeric::abs ( unitLoad_[idof] );

    //  if ( val > max )
    //  {
    //    max = val;
    //    master_ = idof;
    //  }
    //}
  }

  dofs_->resetEvents ();

  ulUpd_ = true;
}

//-----------------------------------------------------------------------
//   getUnitLoad_
//-----------------------------------------------------------------------

void BCModel::getUnitLoad_
  
  ( const Properties& params,
    const Properties& globdat )

{
  if ( !ulUpd_ )
    initUnitLoad_ ( globdat );

  Vector f;

  params.get ( f, ArclenParams::UNIT_LOAD );

  if ( f.size() != unitLoad_.size() )
    throw Error ( JEM_FUNC, "unit load vector mismatch." );

  f = unitLoad_;
}

//-----------------------------------------------------------------------
//   getArcFunc_
//-----------------------------------------------------------------------

void BCModel::getArcFunc_

  ( const Properties& params,
    const Properties& globdat )

{
  double phi, jac11, time, loadfunc, val;

  Vector u, jac10;

  StateVector::get ( u, dofs_, globdat );

  globdat.get ( time, Globdat::TIME );

  loadfunc = shape_->eval ( time );

  params.get ( jac10, ArclenParams::JACOBIAN10 );
 
  jac11 = 0.0;
  jac10 = 0.0;

  if ( alenGroup_ != -1 )
  {
    val = unitVec_[alenGroup_] * loadfunc + initVals_[alenGroup_];

    phi   = u[master_] - val;

    jac10[master_] = 1.0;
  }
  else
  {
    //double norm = jem::numeric::norm2 ( u[masters_] );

    phi = dot ( u[masters_], signs_ ) - loadfunc;
    jac10[masters_] = signs_;

    //phi = sum ( u[masters_] ) - loadfunc;
    //jac10[masters_] = 1.0;

    //System::out() << "u[masters] = " << u[masters_] << "\n";
    //System::out() << "sum " << sum(u[masters_]) << "\n";
    //System::out() << "loadfunc " << loadfunc << "\n";

    //phi = norm - loadfunc; 

    //for ( idx_t im = 0; im < masters_.size(); ++im )
    //{
    //  idx_t idof = masters_[im];

    //  if ( norm > 0.0 )
    //    jac10[idof] = u[idof] / norm;
    //  else
    //    jac10[idof] = 1.0;
    //}

    // dbg

    //phi = u[masters_[0]] - loadfunc;
    //jac10 = 0.0;
    //jac10[masters_[0]] = 1.0;
  }

  params.set ( ArclenParams::ARC_FUNC,   phi   );
  params.set ( ArclenParams::JACOBIAN11, jac11 );
}

//-----------------------------------------------------------------------
//   makeNew
//-----------------------------------------------------------------------

Ref<Model> BCModel::makeNew

  ( const String&     name,
    const Properties& conf,
    const Properties& props,
    const Properties& globdat )

{
  return newInstance<BCModel> ( name, conf, props, globdat );
}

//=======================================================================
//   related functions
//=======================================================================

//-----------------------------------------------------------------------
//   declareBCModel
//-----------------------------------------------------------------------

void declareBCModel ()
{
  using jive::model::ModelFactory;

  ModelFactory::declare ( "BC", 
                          & BCModel::makeNew );
}
