/*
 * Copyright (C) 2019 TU Delft. All rights reserved.
 *
 * Class that implements a fully-connected recurrent neural layer.
 * Neurons are connected with the previous and next layers as well
 * as with its own state at the time when forward propagation
 * occurs. This layer can be stacked with other RecurrentLayers
 * as well as with DenseLayers. 
 * 
 * This is not LSTM or GRU, be mindful of the representational 
 * limitations of a conventional RNN layer.
 *
 * Author: Iuri Barcelos, i.barceloscarneiromrocha@tudelft.nl
 * Date:   June 2019
 * 
 */

#include <jem/base/System.h>
#include <jem/base/Float.h>
#include <jem/base/Error.h>
#include <jem/numeric/algebra/matmul.h>
#include <jive/Array.h>
#include <jive/model/ModelFactory.h>
#include <jive/model/StateVector.h>
#include <jive/util/error.h>

#include "RecurrentLayer.h"
#include "LearningNames.h"

using jem::numeric::matmul;

using jive::IdxVector;
using jive::util::XDofSpace;
using jive::util::sizeError;
using jive::model::StateVector;

//-----------------------------------------------------------------------
//   static data
//-----------------------------------------------------------------------

const char* RecurrentLayer::SIZE       = "size";
const char* RecurrentLayer::ACTIVATION = "activation";
const char* RecurrentLayer::INPINIT    = "inpInit";
const char* RecurrentLayer::RECINIT    = "recInit";

//-----------------------------------------------------------------------
//   constructor & destructor
//-----------------------------------------------------------------------

RecurrentLayer::RecurrentLayer

  ( const String&      name,
    const Properties&  conf,
    const Properties&  props,
    const Properties&  globdat ) : Model ( name )

{
  String func ( "identity" );

  size_ = 0;

  Properties myProps = props.getProps ( myName_ );
  Properties myConf  = conf.makeProps ( myName_ );

  myProps.get  ( size_, SIZE );
  myProps.find ( func, ACTIVATION );

  myConf. set ( SIZE, size_ );
  myConf. set ( ACTIVATION, func );

  func_ = NeuralUtils::getActivationFunc ( func );
  grad_ = NeuralUtils::getActivationGrad ( func );

  func = "glorot";

  myProps.find ( func, INPINIT );
  myConf. set  ( INPINIT, func );

  inpInit_ = NeuralUtils::getInitFunc ( func );

  func = "orthogonal";

  myProps.find ( func, RECINIT );
  myConf. set  ( RECINIT, func );

  recInit_ = NeuralUtils::getInitFunc ( func );

  iNeurons_.resize ( size_ );
  iNeurons_ = 0;

  Ref<XAxonSet>   aset = XAxonSet  ::get ( globdat, getContext()    );
  Ref<XNeuronSet> nset = XNeuronSet::get ( globdat, getContext()    );
  Ref<XDofSpace>  dofs = XDofSpace:: get ( aset->getData(), globdat );

  for ( idx_t in = 0; in < size_; ++in )
  {
    iNeurons_[in] = nset->addNeuron();
  }

  weightType_ = dofs->findType ( LearningNames::WEIGHTDOF );
  biasType_   = dofs->findType ( LearningNames::BIASDOF   );

  if ( myProps.contains ( LearningParams::IMAGE ) )
  {
    throw Error ( JEM_FUNC,
      "RNN layers do not currently support symmetry" );
  }

  if ( globdat.find ( inpNeurons_, LearningParams::PREDECESSOR ) )
  {
    idx_t presize = inpNeurons_.size();

    IdxVector idofs ( presize );
    IdxVector imems ( size_ );

    iBiases_.   resize ( size_          );
    iInits_.    resize ( size_          );
    iInpWts_.   resize ( size_, presize );
    iMemWts_.   resize ( size_, size_   );
    biases_.    resize ( size_          );
    inits_.     resize ( size_          );
    inpWeights_.resize ( size_, presize );
    memWeights_.resize ( size_, size_   );

    iBiases_    = -1; iInits_     = -1;
    iInpWts_    = -1; iMemWts_    = -1;
    biases_     = 0.; inits_      = 0.;
    inpWeights_ = 0.; memWeights_ = 0.;

    inpInit_ ( inpWeights_, globdat );
    recInit_ ( memWeights_, globdat );

    Vector state;

    IdxVector newinpaxons = aset->addAxons ( presize * size_ );
    IdxVector newrecaxons = aset->addAxons ( size_ * size_   );
    IdxVector newbiaaxons = aset->addAxons ( size_           );
    IdxVector newiniaxons = aset->addAxons ( size_           );

    dofs->addDofs ( newinpaxons, weightType_ );
    dofs->addDofs ( newrecaxons, weightType_ );
    dofs->addDofs ( newbiaaxons, biasType_   );
    dofs->addDofs ( newiniaxons, biasType_   );

    StateVector::get ( state, dofs, globdat );

    dofs->getDofIndices ( iBiases_, newbiaaxons, biasType_ );
    dofs->getDofIndices ( iInits_,  newiniaxons, biasType_ );

    state[iBiases_] = 0.0;
    state[iInits_]  = 0.0;

    for ( idx_t in = 0; in < size_; ++in )
    {
      IdxVector myinpaxons ( newinpaxons[slice(in*presize,(in+1)*presize)] );

      dofs->getDofIndices ( idofs, myinpaxons, weightType_ );
      iInpWts_(in,ALL) = idofs;
      state[idofs] = inpWeights_(in,ALL);

      IdxVector myrecaxons ( newrecaxons[slice(in*size_,(in+1)*size_)] );

      dofs->getDofIndices ( imems, myrecaxons, weightType_ );
      iMemWts_(in,ALL) = imems;
      state[imems] = memWeights_(in,ALL);
    }
  }

  bool inputLayer  = false;
  bool outputLayer = false;

  myProps.find ( inputLayer,  LearningNames::FIRSTLAYER );
  myProps.find ( outputLayer, LearningNames::LASTLAYER  );

  if ( inputLayer || outputLayer )
  {
    throw Error ( JEM_FUNC, "Recurrent layers must be hidden" );
  }

  axons_   = aset;
  neurons_ = nset;
  dofs_    = dofs;

  globdat.set ( LearningParams::PREDECESSOR, iNeurons_.clone() );
}

RecurrentLayer::~RecurrentLayer ()
{}

//-----------------------------------------------------------------------
//   configure
//-----------------------------------------------------------------------

void RecurrentLayer::configure

  ( const Properties& props,
    const Properties& globdat )

{
  if ( iInits_[size_-1] == dofs_->dofCount() - 1 )
  {
    throw Error ( JEM_FUNC, "Recurrent layers must be hidden" );
  }
}

//-----------------------------------------------------------------------
//   getConfig
//-----------------------------------------------------------------------

void RecurrentLayer::getConfig

  ( const Properties& conf,
    const Properties& globdat )

{
}

//-----------------------------------------------------------------------
//   takeAction
//-----------------------------------------------------------------------

bool RecurrentLayer::takeAction

  ( const String&     action,
    const Properties& params,
    const Properties& globdat )

{
  if ( action == LearningActions::UPDATE )
  {
    update_ ( globdat );

    return true;
  }

  if ( action == LearningActions::PROPAGATE ||
       action == LearningActions::RECALL       )
  {
    Ref<NData> data;
    Ref<NData> state = nullptr;

    params.get  ( data,  LearningParams::DATA  );
    params.find ( state, LearningParams::STATE ); 

    propagate_ ( data, state, globdat );

    return true;
  }

  if ( action == LearningActions::BACKPROPAGATE )
  {
    Vector grads;

    Ref<NData> data;
    Ref<NData> state = nullptr;

    params.get  ( data,  LearningParams::DATA  );
    params.get  ( grads, LearningParams::GRADS );
    params.find ( state, LearningParams::STATE );

    backPropagate_ ( data, state, grads, globdat );

    return true;
  }

  if ( action == LearningActions::GETJACOBIAN )
  {
    Ref<NData> data;
    params.get ( data, LearningParams::DATA );

    getJacobian_ ( data, globdat );

    return true;
  }

  return false;
}

//-----------------------------------------------------------------------
//   update_
//-----------------------------------------------------------------------

void RecurrentLayer::update_

  ( const Properties& globdat )

{
  Vector state;
  StateVector::get ( state, dofs_, globdat );

  biases_ = state[iBiases_];
  inits_  = state[iInits_];

  for ( idx_t in = 0; in < size_; ++in )
  {
    inpWeights_(in,ALL) = state[iInpWts_(in,ALL)];
    memWeights_(in,ALL) = state[iMemWts_(in,ALL)];
  }
}

//-----------------------------------------------------------------------
//   propagate_
//-----------------------------------------------------------------------

void RecurrentLayer::propagate_

  ( const Ref<NData>  data,
    const Ref<NData>  state,
    const Properties& globdat  )

{
  Matrix netValues ( size_, data->batchSize() );
  netValues = 0.0;

  Matrix acts        ( select ( data->activations, inpNeurons_, ALL ) );

  Matrix memory ( size_, data->batchSize() );
  memory = 0.0;

  if ( state == nullptr )
  {
    for ( idx_t is = 0; is < data->batchSize(); ++is )
    {
      memory(ALL,is) = inits_;
    }
  }
  else
  {
    memory = select ( state->activations, iNeurons_, ALL );
  }

  netValues  = matmul ( inpWeights_, acts   );
  netValues += matmul ( memWeights_, memory );

  for ( idx_t is = 0; is < data->batchSize(); ++is )
  {
    netValues(ALL,is) += biases_;
  }

  select ( data->values, iNeurons_, ALL ) = netValues;

  func_ ( netValues );

  select ( data->activations, iNeurons_, ALL ) = netValues;
}

//-----------------------------------------------------------------------
//   backPropagate_
//-----------------------------------------------------------------------

void RecurrentLayer::backPropagate_

  (    
    const Ref<NData>   data,
    const Ref<NData>   state,
          Vector&      grads,
    const Properties&  globdat  )

{
  Matrix derivs ( select ( data->values, iNeurons_, ALL ) );

  grad_ ( derivs );

  select ( data->deltas, iNeurons_, ALL ) *= derivs;

  Matrix acts        ( select ( data->activations, inpNeurons_, ALL ) );
  Matrix deltas      ( select ( data->deltas, iNeurons_, ALL        ) );
  Matrix memory      ( size_, data->batchSize()                       );
  Matrix memgrads    ( size_, size_                                   );
  Matrix memdeltas   ( size_, data->batchSize()                       );
  Matrix gradmat     ( size_, inpNeurons_.size()                      );

  memory  = 0.0; memgrads = 0.0; memdeltas = 0.0;
  gradmat = 0.0;

  if ( state == nullptr )
  {
    for ( idx_t is = 0; is < data->batchSize(); ++is )
    {
      memory(ALL,is) = inits_;
    }
  }
  else
  {
    memory = select ( state->activations, iNeurons_, ALL );
  }

  gradmat  = matmul ( deltas, acts.transpose()   );
  memgrads = matmul ( deltas, memory.transpose() );

  for ( idx_t in = 0; in < size_; ++in )
  {
    grads[iInpWts_(in,ALL)] += gradmat(in,ALL);
    grads[iBiases_[in]]     += sum ( deltas(in,ALL) );
    grads[iMemWts_(in,ALL)] += memgrads(in,ALL);
  }

  select ( data->deltas, inpNeurons_, ALL ) += 
    matmul ( inpWeights_.transpose(), deltas );  

  memdeltas = matmul ( memWeights_.transpose(), deltas );

  if ( state == nullptr )
  {
    for ( idx_t in = 0; in < size_; ++in )
    {
      grads[iInits_[in]] += sum ( memdeltas(in,ALL) );
    }
  }
  else
  {
    select ( state->deltas, iNeurons_, ALL ) = memdeltas;
  }
}

//-----------------------------------------------------------------------
//   getJacobian_
//-----------------------------------------------------------------------

void RecurrentLayer::getJacobian_

  ( const Ref<NData>  data,
    const Properties& globdat )

{
  idx_t bsize = data->batchSize();
  idx_t jsize = data->outSize();

  if ( jacobian_.size(0) != jsize * bsize )
  {
    jacobian_.resize ( jsize*bsize, inpNeurons_.size() );
    jacobian_ = 0.0;
  }

  Matrix derivs ( select ( data->values, iNeurons_, ALL ) );

  grad_ ( derivs );

  for ( idx_t b = 0; b < bsize; ++b )
  {
    for ( idx_t i = 0; i < size_; ++i )
    {
      data->jacobian ( slice(b*jsize,(b+1)*jsize), i ) *= derivs(i,b);
    }
  }

  jacobian_ = matmul ( data->jacobian, inpWeights_ );

  data->jacobian.ref ( jacobian_ );
}

//=======================================================================
//   related functions
//=======================================================================

//-----------------------------------------------------------------------
//   newRecurrentLayer
//-----------------------------------------------------------------------


Ref<Model>            newRecurrentLayer

  ( const String&       name,
    const Properties&   conf,
    const Properties&   props,
    const Properties&   globdat )

{
  // Return an instance of the class.

  return newInstance<RecurrentLayer> ( name, conf, props, globdat );
}

//-----------------------------------------------------------------------
//   declareRecurrentLayer
//-----------------------------------------------------------------------

void declareRecurrentLayer ()
{
  using jive::model::ModelFactory;

  // Register the StressModel with the ModelFactory.

  ModelFactory::declare ( "Recurrent", newRecurrentLayer );
}

