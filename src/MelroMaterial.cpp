/*
 * 
 *  Copyright (C) 2014 TU Delft. All rights reserved.
 *  
 *  This class implements the material model for polymers
 *  from Melro et al. (2013)
 *  
 *  Author: F.P. van der Meer, f.p.vandermeer@tudelft.nl
 *  Date: October 2014
 *
 *  Modified: Iuri Barcelos, i.barcelos@wmc.eu
 *  Date: February 2016
 *  Damage part implemented. Slight modifications made
 *  in order to use it with IsotropicMaterial instead
 *  of with HookeMaterial.
 *
 *  Modified: Iuri Barcelos, i.barcelos@wmc.eu
 *  Date: February/March 2017
 * 
 *  Model expanded to fatigue by cyclic accumulation of
 *  plastic strain. Fracture envelope is now a function
 *  of the plastic strain in order to make fatigue failure
 *  possible.
 *
 *  Modified: Iuri Barcelos, i.barceloscarneiromrocha@tudelft.nl
 *  Date: August 2019
 *
 *  Cleanup, back to pure plasticity. Removal of fatigue and
 *  water stuff. For neural paper #1
 *
 *  Modified: Iuri Barcelos, i.barceloscarneiromrocha@tudelft.nl
 *  Date: January 2020
 *
 *  Support for 1D bar models with zero Poisson. For GP paper #1
 *
 */

#include <math.h>

#include <jem/base/limits.h>
#include <jem/base/Float.h>
#include <jem/base/Error.h>
#include <jem/base/PrecheckException.h>
#include <jem/base/array/tensor.h>
#include <jem/base/array/operators.h>
#include <jem/base/tuple/operators.h>
#include <jem/base/StringBuffer.h>
#include <jem/io/Writer.h>
#include <jem/io/PrintWriter.h>
#include <jem/io/FileWriter.h>
#include <jem/io/FileFlags.h>
#include <jem/numeric/func/UserFunc.h>
#include <jem/util/Properties.h>
#include <jem/numeric/algebra/matmul.h>
#include <jem/numeric/algebra/LUSolver.h>
#include <jem/numeric/utilities.h>
#include <jem/base/System.h>
#include <jive/util/FuncUtils.h>
#include <jive/util/Globdat.h>

#include "utilities.h"
#include "MelroMaterial.h"

using namespace jem;
using namespace jem::literals;
//using jem::numeric::abs;
using jem::StringBuffer;
using jem::numeric::matmul;
using jem::numeric::LUSolver;
using jem::numeric::UserFunc;
using jem::io::Writer;
using jem::io::PrintWriter;
using jem::io::FileWriter;
using jem::io::FileFlags;
using jem::io::endl;
using jive::util::FuncUtils;
using jive::util::Globdat;

//=======================================================================
//   class MelroMaterial
//=======================================================================

//-----------------------------------------------------------------------
//   static data
//-----------------------------------------------------------------------

const char* MelroMaterial::E_PROP                   = "E";
const char* MelroMaterial::NU_PROP                  = "nu";
const char* MelroMaterial::AREA_PROP                = "area";
const char* MelroMaterial::ANMODEL_PROP             = "anmodel";
const char* MelroMaterial::PLASTIC_POISSON_PROP     = "poissonP";
const char* MelroMaterial::RM_TOLERANCE_PROP        = "rmTolerance";
const char* MelroMaterial::RM_MAXITER_PROP          = "rmMaxIter";
const char* MelroMaterial::SIGMAT_PROP              = "sigmaT";
const char* MelroMaterial::SIGMAC_PROP              = "sigmaC";

//-----------------------------------------------------------------------
//   constructors & destructor
//-----------------------------------------------------------------------

MelroMaterial::MelroMaterial 

  ( idx_t rank, const Properties& globdat )

  : Material ( rank, globdat )

{
  globdat_ = globdat;
  rank_ = rank;

  JEM_PRECHECK ( rank_ >= 1 && rank_ <= 3 );

  // Elastic properties
  
  e_       = 1.0;
  nu_      = 0.0;

  // Plasticity properties

  poissonP_     = 0.;
  rmTolerance_  = 1.e-10;
  rmMaxIter_    = 10;

  // Array resizing

  v61_.resize ( 6 );
  v62_.resize ( 6 );

  nupd_ = 0;
}


MelroMaterial::~MelroMaterial ()
{}

//-----------------------------------------------------------------------
//   configure
//-----------------------------------------------------------------------

void MelroMaterial::configure

  ( const Properties& props,
    const Properties& globdat )

{
  props_ = props;

  props.get  ( e_, E_PROP );
  props.get  ( nu_, NU_PROP );
  props.get  ( anmodel_, ANMODEL_PROP );

  computeElasticStiff_( );

  props.find ( rmTolerance_, RM_TOLERANCE_PROP );
  props.get  ( poissonP_, PLASTIC_POISSON_PROP );
  props.find ( rmMaxIter_, RM_MAXITER_PROP );
  
  Ref<Function> sigmaC = makeFunc_ ( props, SIGMAC_PROP );
  Ref<Function> sigmaT = makeFunc_ ( props, SIGMAT_PROP );

  y_ = newInstance<YieldFunc_> 
       ( e_, nu_, poissonP_, sigmaC, sigmaT );

  y_-> setRmSettings ( rmTolerance_, rmMaxIter_ );

  // Check analysis model (problem type)

  if ( anmodel_ == "PLANE_STRESS" )
    throw Error ( JEM_FUNC, 
      "plasticity for plane stress not implemented" );

  if ( anmodel_ == "BAR" && ( nu_ != 0.0 || poissonP_ != 0.0 ) )
    throw Error ( JEM_FUNC,
      "1D plasticity only implemented for zero Poisson's ratio" );

  // data-driven properties 

  stringProps_.resize ( 2 );
  stringProps_[0] = "sigmaT";
  stringProps_[1] = "sigmaC";

  if ( props.find ( propNames_, "propNames" ) )
  {
    props.get ( propUpper_, "propUpper" );
    props.get ( propLower_, "propLower" );

    if ( propUpper_.size() != propNames_.size() ||
         propLower_.size() != propNames_.size()   )
    {
      throw Error ( JEM_FUNC, "setProps: Bounds inconsistent with property set" );
    }

    for ( idx_t p = 0_idx; p < propNames_.size(); ++p )
    {
      if ( !props_.contains ( propNames_[p] ) )
      {
        throw Error ( JEM_FUNC, "setProps: Unknown property" );
      }
    }

    latestProps_.resize ( propNames_.size() );
    latestProps_ = 0.0;
  }
}

//-----------------------------------------------------------------------
//   makeFunc_
//-----------------------------------------------------------------------

Ref<Function> MelroMaterial::makeFunc_ 

  ( const Properties& props,
    const String&     name ) const

{
  String args = "x";
  props.find ( args, "args" );
  
  Ref<Function> func = FuncUtils::newFunc ( args, name, props, globdat_ );

  FuncUtils::resolve ( *func, globdat_ );

  return func;
}

//-----------------------------------------------------------------------
//   getConfig
//-----------------------------------------------------------------------

void MelroMaterial::getConfig 

  ( const Properties& conf,
    const Properties& globdat ) const

{
  conf.set   ( E_PROP, e_ );
  conf.set   ( NU_PROP, nu_ );
  conf.set   ( ANMODEL_PROP, anmodel_ );

  conf.set ( RM_MAXITER_PROP, rmMaxIter_ );
  conf.set ( RM_TOLERANCE_PROP, rmTolerance_ );
  conf.set ( PLASTIC_POISSON_PROP, poissonP_ );
  
  FuncUtils::getConfig ( conf, y_->getSigmaCFunc(), "sigmaC" );
  FuncUtils::getConfig ( conf, y_->getSigmaTFunc(), "sigmaT" );
}

//-----------------------------------------------------------------------
//  hasThermal 
//-----------------------------------------------------------------------

bool MelroMaterial::hasThermal ()
{
  return false;
}

//-----------------------------------------------------------------------
//  hasSwelling 
//-----------------------------------------------------------------------

bool MelroMaterial::hasSwelling ()
{
  return false;
}

//-----------------------------------------------------------------------
//   update
//-----------------------------------------------------------------------

void MelroMaterial::update

    ( Matrix&               stiff,
      Vector&               stress,
      const Vector&         strain,
      idx_t                 ipoint )

{
  nupd_++;

  Mat66  dmat;
  Vec6   eps   ( fill3DStrain_ ( strain ) ); 
  bool   loading = false;
  double dgam;

  Vec6   sig;

  double epspeq0 = preHist_[ipoint].epspeq; 
  Vec6   epsel ( eps - preHist_[ipoint].epsp );
  Vec6   sigtr   = matmul ( elStiff_, epsel );

  StressInvariants invpl ( sigtr );

  if ( y_->isPlastic ( invpl, epspeq0 ) )
  {
    try
    {
      // classical return mapping scheme starting with dgam = 0.

      dgam = y_->findRoot ( 0. );
    }
    catch ( const Exception& ex )
    {
      handleException_ ( ex, strain, ipoint );

      // more robust return mapping scheme

      double gmin, gmax, fmin, fmax;

      y_->findBounds ( gmin, gmax, fmin, fmax );

      dgam = y_->findRoot ( gmin, gmax, fmin, fmax );
    }

    // compute stress

    double ptr = invpl.getFirstInvariant() / 3.;
    Vec6   sigDtr = deviatoric ( sigtr, ptr );

    double p    = ptr    / y_->getZetaP();
    Vec6   sigD = sigDtr / y_->getZetaS();

    sig     = sigD;
    sig[0] += p;
    sig[1] += p;
    sig[2] += p;

    // compute plastic strain increment 

    double alphaP23 = y_->getAlpha() * p * 2./3.;

    Vec6 mvec;

    mvec[0]  = 3. * sigD[0] + alphaP23;
    mvec[1]  = 3. * sigD[1] + alphaP23;
    mvec[2]  = 3. * sigD[2] + alphaP23;
    mvec[3]  = 6. * sigD[3];  // engng strain factor 2
    mvec[4]  = 6. * sigD[4];  
    mvec[5]  = 6. * sigD[5]; 

    Vec6 depsp = dgam * mvec;

    // consistent tangent

    double betam, pbm, hfac, beta, phi, rho, chi, psi, xi, omega;

    y_->getTangentParameters 
      ( betam, pbm, hfac, beta, phi, rho, chi, psi, xi, omega, dgam );

    Tuple<double,6,6> dmde = aI4_plus_bII_ ( betam, pbm );

    Vec6 hvec = hfac * matmul ( dmde, mvec );

    dmat = aI4_plus_bII_ ( beta, phi-beta/3. );

    for ( idx_t i = 0; i < 6; ++i )
    {
      for ( idx_t j = 0; j < 6; ++j )
      {
        dmat(i,j) -= chi * sigDtr[i] * sigDtr[j];
        dmat(i,j) -= omega * sigDtr[i] * hvec[j];
      }
      for ( idx_t j = 0; j < 3; ++j )
      {
        dmat(i,j) -= rho * sigDtr[i];
        dmat(j,i) -= psi * sigDtr[i];
        dmat(j,i) -= xi  * hvec[i];
      }
    }

    // update history

    double dG = dot ( sig, depsp );
    if ( dG < 0.0 ) System::out() << "Negative dissipation bad!\n";
    newHist_[ipoint].epsp = preHist_[ipoint].epsp + depsp;
    newHist_[ipoint].epspeq = y_->getEpspeq();
    newHist_[ipoint].dissipation = preHist_[ipoint].dissipation + dG;
    loading = true;

    //System::out() << "Plastic update with epsp0 " << preHist_[ipoint].epsp << " epsp " << newHist_[ipoint].epsp << " eps " << eps << " sigma " << sig << '\n';
  }
  else 
  {
    sig = sigtr;
    dmat = elStiff_;

    newHist_[ipoint].epsp        = preHist_[ipoint].epsp;
    newHist_[ipoint].epspeq      = preHist_[ipoint].epspeq;
    newHist_[ipoint].dissipation = preHist_[ipoint].dissipation;
  }

  StressInvariants invsig ( sig );

  //if ( newHist_[ipoint].epspeq == 0.0 || loading )
  if ( true ) 
  {
    newHist_[ipoint].sigI1 = invsig.getFirstInvariant();
    newHist_[ipoint].sigJ2 = invsig.getJ2();
  }
  else
  {
    newHist_[ipoint].sigI1 = preHist_[ipoint].sigI1;
    newHist_[ipoint].sigJ2 = preHist_[ipoint].sigJ2;
  }

  newHist_[ipoint].loading = loading;
  reduce3DVector_ ( stress, sig );

  latestHist_ = &newHist_;

  if ( desperateMode_ )
  {
    if ( ! useSecant_[ipoint] )
    {
      bool switches = ( loading != preHist_[ipoint].loading );
 
      if ( switches != hasSwitched_[ipoint] )
      {
	useSecant_[ipoint] = true;
      }
    }
    if ( useSecant_[ipoint] )
    {
      dmat = elStiff_;
    }
  }

  reduce3DMatrix_ ( stiff, dmat );
  //System::out() << "Point " << ipoint << " preEpspeq " << preHist_[ipoint].epspeq << " newEpspeq " << newHist_[ipoint].epspeq << '\n';
  //if ( newHist_[ipoint].epspeq > 0.0 && newHist_[ipoint].epspeq == preHist_[ipoint].epspeq )
  //{
  //  System::out() << "Unloading\n";
  //}
  //System::out() << "Point " << ipoint << " newEpspeq " << newHist_[ipoint].epspeq << " stiff " << dmat << '\n';
  //System::out() << "dmat " << dmat << " stiff " << stiff << '\n';
  //Vector hist;
  //getHistory ( hist, ipoint );
  //System::out() << hist << '\n';
}

//-----------------------------------------------------------------------
//   stressAtPoint
//-----------------------------------------------------------------------

void MelroMaterial::stressAtPoint

  ( Vector&           stress,
    const Vector&     strain,
    const idx_t       ipoint )

{
  Vec6 eps ( fill3DStrain_ ( strain ) );
  Vec6 epsp = (*latestHist_)[ipoint].epsp;
  Vec6 epse = eps - epsp;

  Vec6 sig = matmul ( elStiff_, epse );

  reduce3DVector_ ( stress, sig );
}

//-----------------------------------------------------------------------
//  addTableColumns 
//-----------------------------------------------------------------------

void MelroMaterial::addTableColumns

  ( IdxVector&     jcols,
    XTable&        table,
    const String&  name )

{
  // Check if the requested table is supported by this material.

  if ( name == "nodalStress" || name == "ipStress" )
  {
    if ( anmodel_ == "BAR" )
    {
      jcols.resize ( 1 );

      jcols[0] = table.addColumn ( "s_xx" );
    }

    else if ( anmodel_ == "PLANE_STRESS" )
    {
      jcols.resize ( 3 );

      jcols[0] = table.addColumn ( "s_xx" );
      jcols[1] = table.addColumn ( "s_yy" );
      jcols[2] = table.addColumn ( "s_xy" );
    }

    else if ( anmodel_ == "PLANE_STRAIN" )
    {
      jcols.resize ( 4 );

      jcols[0] = table.addColumn ( "s_xx" );
      jcols[1] = table.addColumn ( "s_yy" );
      jcols[2] = table.addColumn ( "s_xy" );
      jcols[3] = table.addColumn ( "s_zz" );
    }

    else if ( anmodel_ == "SOLID" )
    {
      jcols.resize ( 6 );

      jcols[0] = table.addColumn ( "s_xx" );
      jcols[1] = table.addColumn ( "s_yy" );
      jcols[2] = table.addColumn ( "s_zz" );
      jcols[3] = table.addColumn ( "s_xy" );
      jcols[4] = table.addColumn ( "s_xz" );
      jcols[5] = table.addColumn ( "s_yz" );
    }

    else
      throw Error ( JEM_FUNC, "Unexpected analysis model: " + anmodel_ );
  }
  else if ( name == "history" || name == "nodalHistory" )
  {
    jcols.resize ( 7 );

    jcols[0] = table.addColumn ( "epsp_xx" );
    jcols[1] = table.addColumn ( "epsp_yy" );
    jcols[2] = table.addColumn ( "epsp_zz" );
    jcols[3] = table.addColumn ( "epsp_xy" );
    jcols[4] = table.addColumn ( "epsp_xz" );
    jcols[5] = table.addColumn ( "epsp_yz" );
    jcols[6] = table.addColumn ( "epspeq" );
    //jcols[7] = table.addColumn ( "dissipation" );
    //jcols[8] = table.addColumn ( "loading" );
  }
}

//-----------------------------------------------------------------------
//   handleException_
//-----------------------------------------------------------------------

void MelroMaterial::handleException_ 

  ( const Exception&      ex,
    const Vector&         strain,
    const idx_t           ipoint )

{
  Ref<PrintWriter> out = newInstance<PrintWriter>
    ( &System::debug("melro") );

  out->nformat.setFractionDigits ( 10 );

  if ( ex.what().equals ( "No convergence" ) )
  {
    *out << "No convergence in return mapping algorithm " << 
            "at point " << ipoint << endl;
  }
  else if ( ex.what().equals ( "nan" ) )
  {
    *out << "NaN detected in return mapping algorithm " << endl;
  }
  else if ( ex.what().equals ( "Negative dgam" ) )
  {
    *out << "Negative increment found for point " << ipoint << endl;
  }
  else
  {
    *out << "Caught unkown exception: " << ex.what() << endl;
  }
  *out << "epsp " << preHist_[ipoint].epsp << endl;
  *out << "epspeq0 " << preHist_[ipoint].epspeq << endl;
  *out << "strain " << strain << endl << endl;;
}

//-----------------------------------------------------------------------
//   getDissipationStress
//-----------------------------------------------------------------------

void MelroMaterial::getDissipationStress

    ( const Vector&   sstar,
      const Vector&   strain,
      const idx_t     ipoint ) 

{
  Vec6   eps   ( fill3DStrain_ ( strain ) ); 
  Vec6   sstar6;
  sstar6 = 0.;

  Mat66 dmat;

  // NB, use preHist. After commit, this contains the new values,
  // after cancel the old ones. In both cases the starting values
  // for the next solve

  double epspeq = preHist_[ipoint].epspeq;
  Vec6   epsel ( eps - preHist_[ipoint].epsp );
  Vec6   sig = matmul ( elStiff_, epsel );

  StressInvariants inv ( sig );

  if ( y_->isPlastic ( inv, epspeq ) )
  {
    double p = inv.getFirstInvariant() /3.;
    Vec6   sigD = deviatoric ( sig, p );

    double beta, phi, rho, chi, psi;
    y_->getTangentParameters ( beta, phi, rho, chi, psi );

    dmat = aI4_plus_bII_ ( beta, phi-beta/3. );

    for ( idx_t i = 0; i < 6; ++i )
    {
      for ( idx_t j = 0; j < 6; ++j )
      {
        dmat(i,j) -= chi * sigD[i] * sigD[j];
      }
      for ( idx_t j = 0; j < 3; ++j )
      {
        dmat(i,j) -= rho * sigD[i];
        dmat(j,i) -= psi * sigD[i];
      }
    }

    // As in fpm16ejmsol
    Vec6 epstmp = 2.*preHist_[ipoint].epsp - eps;
    sstar6 = sig + matmul ( dmat.transpose(), epstmp );

    // As in rocha17ijnme
    //double phiF, rhoF, chiF, psiF;
    //y_->getPGradientParameters ( phiF, rhoF, chiF, psiF );
    //Mat66 f = aI4_plus_bII_ ( 0., phiF );

    //for ( idx_t i = 0; i < 6; ++i )
    //{
    //  for ( idx_t j = 0; j < 6; ++j )
    //  {
    //    f(i,j) += chiF * sigD[i] * sigD[j];
    //  }
    //  for ( idx_t j = 0; j < 3; ++j )
    //  {
    //    f(i,j) += rhoF * sigD[i];
    //    f(j,i) += psiF * sigD[i];
    //  }
    //}

    //for ( idx_t i = 3; i < 6; ++i )
    //  for ( idx_t j = 0; j < 6; ++j )
    //      f(i,j) *= 2.;

    //sstar6 = matmul ( f.transpose(), sig ) + matmul ( dmat.transpose(), epsp );
  }
  else 
  {
    dmat = elStiff_;
    sstar6 = matmul ( dmat.transpose(), preHist_[ipoint].epsp );
  }

  sstar = 0.;
  reduce3DVector_ ( sstar, sstar6 );
}

//-----------------------------------------------------------------------
//   setProps
//-----------------------------------------------------------------------

void MelroMaterial::setProps

  ( const Vector& props )

{
  if ( !propNames_.size() )
  {
    return;
  }

  int    bufsize = 128;
  double padding = 1.e10;

  Properties newProps ( props_.clone() );

  if ( props.size() != propNames_.size() )
  {
    throw Error ( JEM_FUNC, "MelroMaterial::setProps: Inconsistent number of properties" );
  }

  for ( idx_t p = 0_idx; p < propNames_.size(); ++p )
  {
    double prop = propLower_[p] + ( propUpper_[p] - propLower_[p] ) * props[p];

    if ( testany ( propNames_[p] == stringProps_ ) )
    {
      char buf[bufsize];
      snprintf ( buf,  bufsize, "%f", padding * prop );
      newProps.set ( propNames_[p], String ( buf ) + "/" + String ( padding ) );
    }
    else 
    {
      newProps.set ( propNames_[p], prop );
    }
  }

  if ( newProps.contains ( "sigcFac" ) )
  {
    double fac = 1.0;
    newProps.get ( fac, "sigcFac" );

    String sigt;
    newProps.get ( sigt, SIGMAT_PROP );

    char buf[bufsize];
    snprintf ( buf,  bufsize, "%f", padding * fac );
    newProps.set ( SIGMAC_PROP, String ( buf ) + "/" + String ( padding ) + "*" + sigt );
  }

  newProps.get  ( e_, E_PROP );
  newProps.get  ( nu_, NU_PROP );
  newProps.get  ( poissonP_, PLASTIC_POISSON_PROP );

  Ref<Function> cfunc = makeFunc_ ( newProps, SIGMAC_PROP );
  Ref<Function> tfunc = makeFunc_ ( newProps, SIGMAT_PROP );

  computeElasticStiff_  ();

  y_->setYieldFuncs     ( cfunc, tfunc );
  y_->setElasticProps   ( e_, nu_ );
  y_->setPlasticPoisson ( poissonP_ );

  latestProps_ = props;
}

//-----------------------------------------------------------------------
//   getProps
//-----------------------------------------------------------------------

Vector MelroMaterial::getProps ()

{
  Vector props ( propNames_.size() );
  props = 0.0;

  for ( idx_t p = 0; p < propNames_.size(); ++p )
  {
    props[p] = propLower_[p] + ( propUpper_[p] - propLower_[p] ) * latestProps_[p];
  }

  return props;
}

//-----------------------------------------------------------------------
//   getFeatures
//-----------------------------------------------------------------------

Vector MelroMaterial::getFeatures

  ( const idx_t ipoint )

{
  Vector ret ( 9 );
  
  ret[0] = (*latestHist_)[ipoint].epsp[0];
  ret[1] = (*latestHist_)[ipoint].epsp[1];
  ret[2] = (*latestHist_)[ipoint].epsp[2];
  ret[3] = (*latestHist_)[ipoint].epsp[3];
  ret[4] = (*latestHist_)[ipoint].epsp[4];
  ret[5] = (*latestHist_)[ipoint].epsp[5];
  ret[6] = (*latestHist_)[ipoint].epspeq;
  ret[7] = (*latestHist_)[ipoint].sigI1;
  ret[8] = (*latestHist_)[ipoint].sigJ2;

  //Vector ret ( 1 );
  //
  //ret[0] = preHist_[ipoint].epspeq;

  return ret;
}

//-----------------------------------------------------------------------
//   propCount
//-----------------------------------------------------------------------

idx_t MelroMaterial::propCount ()

{
  return propNames_.size();
}

//-----------------------------------------------------------------------
//   clearHist
//-----------------------------------------------------------------------

void MelroMaterial::clearHist ()

{
  for ( idx_t p = 0; p < preHist_.size(); ++p )
  {
    preHist_[p].epsp        = 0.0;
    preHist_[p].epspeq      = 0.0;
    preHist_[p].dissipation = 0.0;
    preHist_[p].sigI1       = 0.0;
    preHist_[p].sigJ2       = 0.0;
    preHist_[p].loading     = false;

    newHist_[p].epsp        = 0.0;
    newHist_[p].epspeq      = 0.0;
    newHist_[p].dissipation = 0.0;
    newHist_[p].sigI1       = 0.0;
    newHist_[p].sigJ2       = 0.0;
    newHist_[p].loading     = false;
  }
}

//-----------------------------------------------------------------------
//   getHistory
//-----------------------------------------------------------------------

void MelroMaterial::getHistory

  ( Vector&        hvals,
    const idx_t    mpoint )

{
  (*latestHist_)[mpoint].toVector ( hvals );
}

//-----------------------------------------------------------------------
//   setHistory
//-----------------------------------------------------------------------

void MelroMaterial::setHistory

  ( const Vector&  hvals,
    const idx_t    mpoint )

{
  preHist_[mpoint].epsp[0]     = hvals[0];
  preHist_[mpoint].epsp[1]     = hvals[1];
  preHist_[mpoint].epsp[2]     = hvals[2];
  preHist_[mpoint].epsp[3]     = hvals[3];
  preHist_[mpoint].epsp[4]     = hvals[4];
  preHist_[mpoint].epsp[5]     = hvals[5];
  preHist_[mpoint].epspeq      = hvals[6];
  //preHist_[mpoint].dissipation = hvals[8];
  //preHist_[mpoint].loading     = hvals[9]; 

  latestHist_      = &preHist_;
  newHist_[mpoint] = preHist_[mpoint];

  //Vector hist;
  //preHist_[mpoint].toVector ( hist );
  //System::out() << "Setting hist to " << preHist_[mpoint].toVector() << '\n';
}

//-----------------------------------------------------------------------
//   computeElasticStiff_
//-----------------------------------------------------------------------

void MelroMaterial::computeElasticStiff_ () 

{
  const double d = (1.0 + nu_) * (1.0 - 2.0*nu_);

  elStiff_ = 0.;

  elStiff_(0,0) = elStiff_(1,1) =
  elStiff_(2,2) = e_*(1.0 - nu_)/d;
  elStiff_(0,1) = elStiff_(1,0) =
  elStiff_(0,2) = elStiff_(2,0) =
  elStiff_(1,2) = elStiff_(2,1) = e_*nu_/d;
  elStiff_(3,3) = elStiff_(4,4) =
  elStiff_(5,5) = 0.5*e_/(1.0 + nu_);
}

//-----------------------------------------------------------------------
//   aI4_plus_bII_
//-----------------------------------------------------------------------

Tuple<double,6,6> MelroMaterial::aI4_plus_bII_ 

  ( const double        a,
    const double        b ) const

{
  // make 6x6 matrix with a*I_4^s+b*II in Voigt notation

  Mat66 ret;
  ret = 0.;

  ret(0,0) += a+b;  ret(0,1) += b;    ret(0,2) += b;
  ret(1,0) += b;    ret(1,1) += a+b;  ret(1,2) += b;
  ret(2,0) += b;    ret(2,1) += b;    ret(2,2) += a+b;
  ret(3,3) += .5*a;
  ret(4,4) += .5*a;
  ret(5,5) += .5*a;

  return ret;
}

//-----------------------------------------------------------------------
//   fill3DStrain_
//-----------------------------------------------------------------------

Tuple<double,6> MelroMaterial::fill3DStrain_

  ( const Vector&       v ) const

{
  Tuple<double,6> ret;

  if ( v.size() == 1 )
  {
    ret[0] = v[0];
    ret[1] = 0.0;
    ret[2] = 0.0;
    ret[3] = 0.0;
    ret[4] = 0.0;
    ret[5] = 0.0;
  }

  else if ( v.size() == 3 )
  {
    // NB(Iuri): Plane stress not handled.
    
    ret[0] = v[0];
    ret[1] = v[1];
    ret[2] = 0.;
    ret[3] = v[2];
    ret[4] = 0.;
    ret[5] = 0.;
  }

  else if ( v.size() == 6 )
  {
    ret[0] = v[0];
    ret[1] = v[1];
    ret[2] = v[2];
    ret[3] = v[3];
    ret[4] = v[4];
    ret[5] = v[5];
  }

  else
    throw Error ( JEM_FUNC,
      "Unexpected strain vector size in fill3DStrain_ (MelroMaterial)" );

  return ret;
}

//-----------------------------------------------------------------------
//   reduce3DVector_
//-----------------------------------------------------------------------

void MelroMaterial::reduce3DVector_

  ( const Vector&          v,
    const Tuple<double,6>& t ) const 

{
  // reduce a full 3D tuple to a 2D or 3D vector

  if ( v.size() == 1 )
  {
    v[0] = t[0];
  }
  else if ( v.size() == 3 ) // Plane strain excl. s_zz
  {
    v[0] = t[0];
    v[1] = t[1];
    v[2] = t[3];
  }
  else if ( v.size() == 4 ) // Plane strain incl. s_zz (printing)
  {
    v[0] = t[0];
    v[1] = t[1];
    v[2] = t[3];
    v[3] = t[2];
  }
  else
  {
    v[0] = t[0];
    v[1] = t[1];
    v[2] = t[2];
    v[3] = t[3];
    v[4] = t[4];
    v[5] = t[5];
  }
}

//-----------------------------------------------------------------------
//   reduce3DMatrix_
//-----------------------------------------------------------------------

void MelroMaterial::reduce3DMatrix_

  ( const Matrix&            m,
    const Tuple<double,6,6>& t ) const 

{
  if ( m.size(0) == 1 )
  {
    m = 0.0;

    m(0,0) = t(0,0);
  }
  else if ( m.size(0) == 3 )
  {
    if ( anmodel_ == "PLANE_STRAIN" )
    {
      select2DMatrix_ ( m, t );
    }
    else
    {
      double d;
      Mat66 tmp66 = t;
      LUSolver::invert ( tmp66, d );
      select2DMatrix_ ( m, tmp66 );
      LUSolver::invert ( m, d );
    }
  }
  else
  {
    for ( idx_t i = 0; i < 6; ++i )
    {
      for ( idx_t j = 0; j < 6; ++j )
      {
        m(i,j) = t(i,j);
      }
    }
  }
}

//-----------------------------------------------------------------------
//   select2DMatrix_
//-----------------------------------------------------------------------

void MelroMaterial::select2DMatrix_

  ( const Matrix&            m,
    const Tuple<double,6,6>& t ) const 

{
  // selecting [xx, yy, xy] components from [xx, yy, zz, xy, xz, yz]

  for ( idx_t i = 0; i < 2; ++i )
  {
    m(i,2_idx) = t(i,3_idx);
    m(2_idx,i) = t(3_idx,i); 

    for ( idx_t j = 0; j < 2; ++j )
    {
      m(i,j) = t(i,j);
    }
  }
  m(2_idx,2_idx) = t(3_idx,3_idx);
}

//-----------------------------------------------------------------------
//  commitOne
//-----------------------------------------------------------------------

void MelroMaterial::commitOne

  ( const idx_t ipoint )

{
  preHist_[ipoint].epsp[0]     = newHist_[ipoint].epsp[0];
  preHist_[ipoint].epsp[1]     = newHist_[ipoint].epsp[1];
  preHist_[ipoint].epsp[2]     = newHist_[ipoint].epsp[2];
  preHist_[ipoint].epsp[3]     = newHist_[ipoint].epsp[3];
  preHist_[ipoint].epsp[4]     = newHist_[ipoint].epsp[4];
  preHist_[ipoint].epsp[5]     = newHist_[ipoint].epsp[5];
  preHist_[ipoint].epspeq      = newHist_[ipoint].epspeq;
  preHist_[ipoint].sigI1       = newHist_[ipoint].sigI1;
  preHist_[ipoint].sigJ2       = newHist_[ipoint].sigJ2;
  preHist_[ipoint].dissipation = newHist_[ipoint].dissipation;
  preHist_[ipoint].loading     = newHist_[ipoint].loading;
}

//-----------------------------------------------------------------------
//  commit
//-----------------------------------------------------------------------

void MelroMaterial::commit () 

{
  newHist_.swap ( preHist_ );

  latestHist_ = &preHist_;

  //System::out() << "MelroMaterial committing with preHist " << preHist_[0_idx].epsp << " " << preHist_[0_idx].epspeq << '\n';

  //for ( idx_t p = 0; p < preHist_.size(); ++p )
  //  if ( isInelastic(p) && !preHist_[p].loading )
  //    System::out() << "MelroMaterial: Unloading point\n";
  //System::out() << "Total update count " << nupd_ << '\n';
}

//-----------------------------------------------------------------------
//   clone
//-----------------------------------------------------------------------

Ref<Material> MelroMaterial::clone () const

{
  // use default copy constructor

  return newInstance<MelroMaterial> ( *this );
}

//-----------------------------------------------------------------------
//   deviatoric
//-----------------------------------------------------------------------

Vec6 MelroMaterial::deviatoric

  ( const Vec6&   full,
    const double  p ) const

{
  Vec6 ret = full;

  ret[0] -= p;
  ret[1] -= p;
  ret[2] -= p;

  return ret;
}

//-----------------------------------------------------------------------
//   giveHistory
//-----------------------------------------------------------------------

double MelroMaterial::giveHistory

  ( const idx_t ip ) const
{
  //return (*latestHist_)[ip].epspeq;
  return newHist_[ip].epspeq;
}

//-----------------------------------------------------------------------
//   createIntPoints
//-----------------------------------------------------------------------

void MelroMaterial::createIntPoints

  ( idx_t npoints )

{
  for ( idx_t i = 0; i < npoints; ++i )
  {
    preHist_.pushBack     ( Hist_() );
    newHist_.pushBack     ( Hist_() );
  }

  latestHist_ = &preHist_;

  useSecant_.resize ( preHist_.size() );
  useSecant_ = false;
}

//-----------------------------------------------------------------------
//   hasCrackBand 
//-----------------------------------------------------------------------

bool MelroMaterial::hasCrackBand ()

{
  return false;
}

//-----------------------------------------------------------------------
//   giveDissipation
//-----------------------------------------------------------------------

double MelroMaterial::giveDissipation ( const idx_t ipoint ) const
{   
  return (*latestHist_)[ipoint].dissipation;
}

//-----------------------------------------------------------------------
//   Hist_ constructor
//-----------------------------------------------------------------------

MelroMaterial::Hist_::Hist_ () : epspeq ( 0. ), dissipation ( 0. )
{
  epsp = 0.;
  epspeq = 0.;
  sigI1 = 0.;
  sigJ2 = 0.;
  dissipation = 0.;
  loading = false;
}

//-----------------------------------------------------------------------
//   Hist_ print function
//-----------------------------------------------------------------------

void MelroMaterial::Hist_::print () const

{
  System::out() << "epsp " << epsp << ", epspeq " << epspeq << endl;
}

// -------------------------------------------------------------------
//  Hist_::toVector
// -------------------------------------------------------------------

inline void MelroMaterial::Hist_::toVector

 ( Vector&  vec )

{
  //vec.resize ( 9 );
  vec.resize ( 7 );

  vec = 0.0;

  vec[0]  = epsp[0];
  vec[1]  = epsp[1];
  vec[2]  = epsp[2];
  vec[3]  = epsp[3];
  vec[4]  = epsp[4];
  vec[5]  = epsp[5];
  vec[6]  = epspeq;
  //vec[7]  = dissipation;
  //vec[8]  = loading;
}

//-----------------------------------------------------------------------
//   YieldFunc_ constructor
//-----------------------------------------------------------------------

MelroMaterial::YieldFunc_::YieldFunc_

  ( const double        young,
    const double        poisson,
    const double        poissonP,
    const Ref<Function> sigC,
    const Ref<Function> sigT )

  : sigmaC_ ( sigC ), sigmaT_ ( sigT ), rmTol_(0.), maxIter_(0)

{

  G_ = young / 2. / ( 1. + poisson );
  K_ = young / 3. / ( 1. - 2. * poisson );
  alpha_ = ( 4.5 - 9. * poissonP ) / ( 1. + poissonP );
  alpha2_ = alpha_ * alpha_;
  Ka_ = K_ * alpha_;
  nuPFac_ = sqrt ( 1. / ( 1. + 2. * poissonP * poissonP ) );
}

//-----------------------------------------------------------------------
//   YieldFunc_::setRmSettings
//-----------------------------------------------------------------------

void MelroMaterial::YieldFunc_::setRmSettings 

  ( const double rmTolerance, 
    const idx_t  rmMaxIter )

{
  rmTol_ = rmTolerance;
  maxIter_ = rmMaxIter;
}

//-----------------------------------------------------------------------
//   YieldFunc_::setElasticProps
//-----------------------------------------------------------------------

void MelroMaterial::YieldFunc_::setElasticProps

  ( const double young, 
    const double poisson )

{
  G_ = young / 2. / ( 1. + poisson );
  K_ = young / 3. / ( 1. - 2. * poisson );
  Ka_ = K_ * alpha_;
}

//-----------------------------------------------------------------------
//   YieldFunc_::setPlasticPoisson
//-----------------------------------------------------------------------

void MelroMaterial::YieldFunc_::setPlasticPoisson

  ( const double poissonP )

{
  alpha_ = ( 4.5 - 9. * poissonP ) / ( 1. + poissonP );
  alpha2_ = alpha_ * alpha_;
  Ka_ = K_ * alpha_;
  nuPFac_ = sqrt ( 1. / ( 1. + 2. * poissonP * poissonP ) );
}

//-----------------------------------------------------------------------
//   YieldFunc_::setYieldFuncs
//-----------------------------------------------------------------------

void MelroMaterial::YieldFunc_::setYieldFuncs

  ( const Ref<Function> sigC,
    const Ref<Function> sigT )

{
  sigmaC_ = sigC;
  sigmaT_ = sigT;
}

//-----------------------------------------------------------------------
//   YieldFunc_::isPlastic
//-----------------------------------------------------------------------

bool MelroMaterial::YieldFunc_::isPlastic

  ( const Invariants&  inv,
    const double       epspeq0 )

{
  // update all variables that are constant inside the return 
  // mapping scheme

  epspeq0_ = epspeq0;
  
  j2tr_ = inv.getJ2();
  i1tr_ = inv.getFirstInvariant();
  j2fac_ = 18.*j2tr_;
  i1fac_ = 4.*alpha2_*i1tr_*i1tr_ / 27.;

  // evaluate failure criterion for the trial state

  double crit = evalTrial_();

  double sigC = sigmaC_->eval ( epspeq0_ );
  double sigT = sigmaT_->eval ( epspeq0_ );

  return (crit/sigC/sigT) > -rmTol_;
}

//-----------------------------------------------------------------------
//   YieldFunc_::findRoot
//-----------------------------------------------------------------------

double MelroMaterial::YieldFunc_::findRoot 

  ( const double dgam0 )

{
  double dgam = dgam0;
  double oldddgam = -1.;
  double oldcrit = evalTrial_();

  for ( idx_t irm = 0; irm < maxIter_; )
  {
    double crit = eval_( dgam );

    double normalized = jem::numeric::abs(crit) / ( sigC_*sigT_  );

    if ( normalized < rmTol_ )
    {
      // System::out() << " return mapping converged in " << irm 
      // << " iterations " << normalized << endl;
      break;
    }
    if ( ++irm == maxIter_ )
    {
      throw Exception ( JEM_FUNC, "No convergence" );
    }
    if ( jem::Float::isNaN(dcdg_) )
    {
      throw Exception ( JEM_FUNC, "nan" );
    }

    double ddgam = crit / dcdg_;

    if ( ddgam * oldddgam < 0. )
    {
      // divergence detection
      if ( oldcrit * crit < 0. && jem::numeric::abs(ddgam) > jem::numeric::abs(oldddgam) )
      {
        // there might be an inflection point around the root
        // use linear interpolation rather than linearization

        // System::out() << "divergence prevention" << endl;
        ddgam = -oldddgam * crit / ( crit - oldcrit );
      }
    }

    dgam -= ddgam;

    oldddgam = ddgam;
    oldcrit = crit;
  }

  if ( dgam < -1.e-12 )
  {
    throw Exception ( JEM_FUNC, "Negative dgam" );
  }

  return dgam;
}

//-----------------------------------------------------------------------
//   YieldFunc_::findBounds
//-----------------------------------------------------------------------

void  MelroMaterial::YieldFunc_::findBounds

  (       double&      gmin,
          double&      gmax,
          double&      fmin,
          double&      fmax )

{
  gmin = 0.;
  gmax = -1.;
  fmax = -1.;

  fmin = evalTrial_();

  JEM_PRECHECK ( fmin > 0. );

  double dgam = 1.e-16;

  while ( gmax < 0. )
  {
    double crit = eval_( dgam );

    if ( crit > 0. )
    {
      gmin = dgam;
      fmin = crit;
    }
    else
    {
      gmax = dgam;
      fmax = crit;
    }
    dgam *= 10.;
  }
}

//-----------------------------------------------------------------------
//   YieldFunc_::improveBounds_
//-----------------------------------------------------------------------

void  MelroMaterial::YieldFunc_::improveBounds_

  (       double&      gmin,
          double&      gmax,
          double&      fmin,
          double&      fmax )

{
  idx_t np = 10;
  double dg = ( gmax - gmin ) / double(np);
  double dgam = gmin;

  while ( dgam < gmax )
  {
    dgam += dg;
    double crit = eval_( dgam );

    if ( crit > 0. )
    { 
      gmin = dgam;
      fmin = crit;
    }
    else
    {
      gmax = dgam;
      fmax = crit;
      break;
    }
  }
}

//-----------------------------------------------------------------------
//   YieldFunc_::findRoot (with bounds)
//-----------------------------------------------------------------------

double MelroMaterial::YieldFunc_::findRoot

  (       double       gmin,
          double       gmax,
          double       fmin,
          double       fmax )

{
  double dgam;
  try 
  {
    double dgam0 = estimateRoot_ ( gmin, gmax, fmin, fmax );

    dgam = findRoot ( dgam0 );
  }
  catch ( const Exception& ex )
  {
    improveBounds_ ( gmin, gmax, fmin, fmax );

    dgam = findRoot ( gmin, gmax, fmin, fmax );
  }

  return dgam;
}

//-----------------------------------------------------------------------
//   YieldFunc_::estimateRoot_
//-----------------------------------------------------------------------

double MelroMaterial::YieldFunc_::estimateRoot_

  ( const double       gmin,
    const double       gmax,
    const double       fmin,
    const double       fmax ) const

{
  // linear interpolation

  // fmin  o___ 
  //           ---___
  // phi=0 |----------o-----------|------
  //                      ---___
  // fmax  -                    --o
  //     gmin        ret        gmax

  return gmin + fmin * (gmax-gmin) / (fmin-fmax);
}

    
//-----------------------------------------------------------------------
//   YieldFunc_::evalTrial_
//-----------------------------------------------------------------------

double MelroMaterial::YieldFunc_::evalTrial_ () const

{
  // fast version of eval, without computing all dgam dependent variables

  double sigC = sigmaC_->eval ( epspeq0_ );
  double sigT = sigmaT_->eval ( epspeq0_ );
  double sigct = sigC - sigT;

  return 6.*j2tr_ + 2.*i1tr_*sigct - 2.*sigC*sigT;
}

//-----------------------------------------------------------------------
//   YieldFunc_::eval_
//-----------------------------------------------------------------------

double MelroMaterial::YieldFunc_::eval_

  ( const double dgam )

{
  setDGam_ ( dgam );

  return 6.*j2tr_/zS2_ + 2.*i1tr_*sigct_/zetaP_ - 2.*sigC_*sigT_;
}

//-----------------------------------------------------------------------
//   YieldFunc_::resetDGam
//-----------------------------------------------------------------------

void MelroMaterial::YieldFunc_::resetDGam ()

{
  // update all variables that are a function of dgam, using dgam=0.

  zetaS_ = 1.;
  zetaP_ = 1.;

  zS2_ = 1.;
  zP2_ = 1.;

  double j2fac = 18.*j2tr_;
  double i1fac = 4.*alpha2_*i1tr_*i1tr_ / 27.;
  double sqrtA = sqrt ( j2fac + i1fac );

  depspeq_  = 0.;
  epspeq_ = epspeq0_;

  setYieldStress_ ( epspeq_ );

  gjzs3_ = G_ * j2tr_;
  kaizp2_ = Ka_ * i1tr_;

  double depedg = nuPFac_ * sqrtA;

  double dsigCe = HC_ * depedg;
  double dsigTe = HT_ * depedg;

  dcdg_ = 2.*i1tr_*(dsigCe-dsigTe) - 4.*kaizp2_*sigct_
          - 72.*gjzs3_ - 2.*(sigC_*dsigTe+sigT_*dsigCe);

}

//-----------------------------------------------------------------------
//   YieldFunc_::setDGam_
//-----------------------------------------------------------------------

void MelroMaterial::YieldFunc_::setDGam_

  ( const double dgam ) 

{
  // update all variables that are a function of dgam

  zetaS_ = 1. + 6. * G_ * dgam;
  zetaP_ = 1. + 2. * Ka_ * dgam;

  zS2_ = zetaS_*zetaS_;
  zP2_ = zetaP_*zetaP_;

  double j2fac = 18.*j2tr_;
  double i1fac = 4.*alpha2_*i1tr_*i1tr_ / 27.;
  double sqrtA = sqrt ( j2fac / zS2_ + i1fac / zP2_ );

  depspeq_  = nuPFac_ * dgam * sqrtA;
  epspeq_ = epspeq0_ + depspeq_;

  setYieldStress_ ( epspeq_ );

  gjzs3_ = G_ * j2tr_ / zS2_ / zetaS_;
  kaizp2_ = Ka_ * i1tr_ / zP2_;

  // NB adding an additional I1tr in the final term of depedg
  // (Melro's paper is incorrect here)

  double depedg = nuPFac_ * ( sqrtA - .5 * dgam / sqrtA * 
      ( 216.*gjzs3_ + 16.*alpha2_*kaizp2_*i1tr_/27./zetaP_ ) );

  double dsigCe = HC_ * depedg;
  double dsigTe = HT_ * depedg;

  dcdg_ = 2.*i1tr_*(dsigCe-dsigTe)/zetaP_ - 4.*kaizp2_*sigct_
          - 72.*gjzs3_ - 2.*(sigC_*dsigTe+sigT_*dsigCe);
}

//-----------------------------------------------------------------------
//   YieldFunc_::setYieldStress_
//-----------------------------------------------------------------------

void MelroMaterial::YieldFunc_::setYieldStress_

  ( const double epspeq )

{
  sigC_ = sigmaC_->eval ( epspeq );
  sigT_ = sigmaT_->eval ( epspeq );

  HC_ = sigmaC_->deriv ( epspeq );
  HT_ = sigmaT_->deriv ( epspeq );

  sigct_ = ( sigC_ - sigT_ );
}


//-----------------------------------------------------------------------
//   YieldFunc_::getTangentParameters
//-----------------------------------------------------------------------

void MelroMaterial::YieldFunc_::getTangentParameters 

  (       double&  betam, 
          double&  pbm, 
          double&  hfac, 
          double&  beta, 
          double&  phi, 
          double&  rho, 
          double&  chi, 
          double&  psi, 
          double&  xi, 
          double&  omega, 
    const double   dgam )

{
  setDGam_ ( dgam );

  betam = 6. * G_ / zetaS_;
  pbm = 2.* Ka_/3./zetaP_ - betam/3.;

  if ( jem::numeric::abs(depspeq_)>1.e-20 )
  {
    hfac = 2. * ( ( HC_ - HT_ ) * i1tr_ / zetaP_ - (sigC_*HT_+sigT_*HC_) ) 
                * dgam*dgam*nuPFac_*nuPFac_/depspeq_;
  }
  else
  {
    hfac =  0.;
  }

  double eta  = -dcdg_;
  beta = 2. * G_ / zetaS_;
  phi  = K_ / zetaP_ * ( 1. - 4. * kaizp2_ * sigct_ / eta );
  rho  = 36. * K_ * G_ * sigct_ / eta / zS2_ / zetaP_;
  chi  = 72. * G_ * G_ / eta / zS2_ / zS2_;
  psi  = 8. * G_ * kaizp2_  / eta / zS2_;
  xi   = 2. * kaizp2_ / eta / 3.;
  omega = 6. * G_ / zS2_ / eta;
}

//-----------------------------------------------------------------------
//   YieldFunc_::getTangentParameters
//-----------------------------------------------------------------------

void MelroMaterial::YieldFunc_::getTangentParameters

  (       double&  beta, 
          double&  phi, 
          double&  rho, 
          double&  chi, 
          double&  psi )

{
  // fast version of getTangentParameters for dgam = 0

  resetDGam();

  double eta = -dcdg_;

  beta = 2. * G_;
  phi  = K_ * ( 1. - 4. * kaizp2_ * sigct_ / eta );
  rho = 36. * K_ * G_ * sigct_ / eta;
  chi = 72. * G_ * G_ / eta;
  psi = 8. * G_ * kaizp2_ / eta;
}

//-----------------------------------------------------------------------
//   YieldFunc_::getPGradientParameters
//-----------------------------------------------------------------------

void MelroMaterial::YieldFunc_::getPGradientParameters

  (       double& phiF,
          double& rhoF,
          double& chiF,
          double& psiF ) 

{
  resetDGam();

  double eta = -dcdg_;

  phiF = 4. * kaizp2_ * sigct_ / 3. / eta;
  rhoF = 18. * K_ * sigct_ / eta;
  chiF = 36. * G_ / eta;
  psiF = 8. * G_ * alpha_ * i1tr_ / 3. / eta;
}


// purgatory

//if ( true )
//  {
//    if ( ! (nz % 2) )
//    {
//      throw Error 
//	( JEM_FUNC, "MelroMaterial::setProps: Number of props must be odd" ); 
//    }
//
//    snprintf ( buf, 128, "%f", props[0]*100000.0 );
//    sigt = String ( buf );
//    sigt = sigt + "/100000.0";
//
//    for ( idx_t z = 1; z < props.size(); ++z )
//    {
//      if ( z % 2 )
//      {
//	snprintf ( buf, 128, "%f", props[z]*100000.0 );
//	sigt = sigt + "+" + String ( buf ) + "/100000.0";
//      }
//      else
//      {
//	snprintf ( buf, 128, "%f", props[z]*100000.0 );
//	sigt = sigt + "*exp(x/" + String ( buf ) + "*100000.0)";
//      }
//    }
//  }
//  else if ( false )
//  {
//    sigt = String ( props[0] );
//
//    //sigt = sigt + "-33.6*exp(x/-0.003407)";
//
//    for ( idx_t z = 1; z < props.size(); ++z )
//    {
//      sigt = sigt + "+" + String ( props[z] ) + "*x^" + String((double)z);
//    }
//  }
//  else
//  {
//    sigt = String ( "100.0" );
//
//    preHist_[0].epsp[0] = props[0];
//    preHist_[0].epsp[1] = props[1];
//    preHist_[0].epsp[2] = props[2];
//    preHist_[0].epsp[3] = props[3];
//    preHist_[0].epsp[4] = props[4];
//    preHist_[0].epsp[5] = props[5];
//    preHist_[0].epspeq  = props[6]*props[6];
//
//    newHist_[0].epsp[0] = props[0];
//    newHist_[0].epsp[1] = props[1];
//    newHist_[0].epsp[2] = props[2];
//    newHist_[0].epsp[3] = props[3];
//    newHist_[0].epsp[4] = props[4];
//    newHist_[0].epsp[5] = props[5];
//    newHist_[0].epspeq  = props[6]*props[6];
//
//  }
//
