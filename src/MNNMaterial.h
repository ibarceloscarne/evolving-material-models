/*
 * Copyright (C) 2021 TU Delft. All rights reserved.
 *
 * Author: Iuri Barcelos, i.rocha@tudelft.nl
 * Date:   May 2021
 * 
 */

#ifndef MNN_MATERIAL_H
#define MNN_MATERIAL_H

#include <jive/app/Module.h>
#include <jive/model/Model.h>

#include "Material.h"
#include "NData.h"
#include "Normalizer.h"
#include "Bayesian.h"

using jive::app::Module;

typedef Ref<Material>  MatRef;
typedef Ref<Bayesian>  BayRef;

//-----------------------------------------------------------------------
//   class MNNMaterial
//-----------------------------------------------------------------------

class MNNMaterial : public Material
{
 public:

  typedef Array< Ref<NData>, 1 > Batch;

  static const char*     NETWORK;
  static const char*     WEIGHTS;
  static const char*     NORMALIZER;
  static const char*     MATERIAL;
  static const char*     FEATUREEXTRACTOR;

  explicit               MNNMaterial

    ( const Properties&    props,
      const Properties&    conf,
      const idx_t          rank,
      const Properties&    globdat );

  virtual void           configure

    ( const Properties&    props,
      const Properties&    globdat );

  virtual void           getConfig

    ( const Properties&    conf,
      const Properties&    globdat )   const;

  virtual void           update

    ( Matrix&              stiff,
      Vector&              stress,
      const Vector&        strain,
      const idx_t          ipoint );

  virtual void           commit  ();

  virtual void           stressAtPoint

    ( Vector&              stress,
      const Vector&        strain,
      const idx_t          ipoint );

  virtual void           addTableColumns

    ( IdxVector&           jcols,
      XTable&              table,
      const String&        name   );

 virtual void            getHistory

    ( Vector&              hvals,
      const idx_t          mpoint );
 
  virtual void           createIntPoints

    ( const idx_t           npoints );

  virtual Ref<Material>  clone ( ) const;

 protected:

  virtual               ~MNNMaterial();

 protected:

  idx_t                   rank_;

  Ref<Module>             network_;
  Properties              netData_;
  Ref<Normalizer>         nizer_;

  Batch                   data_;

  IdxVector               perm_;
  bool                    perm23_;

  Array<MatRef>           mat_;
  Array<MatRef>           ext_;

  Array<BayRef>           bmat_;
  Array<BayRef>           bext_;

  Matrix                  iPointStrain_;

  Properties              props_;
  Properties              conf_;
  Properties              globdat_;

 protected:

  void                    initWeights_

    ( const String&         fname        );

  void                    updateProps_  ();
  void                    updateProps_  ( idx_t p );
};

#endif
