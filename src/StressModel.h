/*
 *  TU Delft / Knowledge Centre WMC
 *
 *  Simple class for stress analysis.
 *
 *  Author: Iuri Barcelos, i.barcelos@wmc.eu
 *  Date: August 2015
 *
 *  Modified: Iuri Barcelos, i.barcelos@wmc.eu
 *  Date: October 2015
 *  Incorporation of MacroModel and MicroModel routines
 *  written by Frans van der Meer, Erik Jan Lingen and
 *  Vinh Phu Nguyen. The two-scale homogenization model
 *  can then be used by specifying MacroMicroInterface
 *  as the material for the macroscopic model. Likewise,
 *  the same model will be used in the microscale with
 *  a conventional material. This adaptation was performed
 *  in order to make the fe2 model compatible with existing
 *  implementation of the MultiPhysicsModule. Please refer
 *  to the original implementations of MacroModel and
 *  MicroModel for more details.
 *
 *  Modified: Iuri Barcelos, February 2016
 *
 *  Implementation of characteristic length calculation
 *  for crack band method. In this implementation, the
 *  calculated lengths are kept in the material object
 *  in order to avoid overloading the update and
 *  stressAtPoint functions.
 *
 *  Modified: Iuri Barcelos, March 2018
 *
 *  Code cleanup after implementation of POD and ECM
 *
 *  Modified: Iuri Barcelos, August 2018
 *
 *  Further cleaning for adaptive ROM
 *
 *  Modified: Iuri Barcelos, November 2018
 *
 *  Added tools for domain-based adaptive ECM
 *
 */

#ifndef STRESS_MODEL_H
#define STRESS_MODEL_H

#include <jem/base/Thread.h>
#include <jem/base/Monitor.h>
#include <jem/numeric/func/Function.h>
#include <jive/util/XTable.h>
#include <jive/util/XDofSpace.h>
#include <jive/util/Assignable.h>
#include <jive/util/utilities.h>
#include <jive/util/StdPointSet.h>
#include <jive/model/Model.h>
#include <jive/geom/InternalShape.h>
#include <jive/fem/ElementGroup.h>
#include <jive/algebra/MatrixBuilder.h>
#include <jem/io/Writer.h>
#include <jem/io/PrintWriter.h>

#include "Material.h"
#include "DispWriter.h"

using namespace jem;

using jem::util::Properties;
using jem::numeric::Function;
using jive::Vector;
using jive::IdxVector;
using jive::Matrix;
using jive::util::XTable;
using jive::util::XDofSpace;
using jive::util::Assignable;
using jive::util::XPointSet;
using jive::model::Model;
using jive::geom::IShape;
using jive::fem::ElementGroup;
using jive::fem::NodeSet;
using jive::fem::ElementSet;
using jive::algebra::MatrixBuilder;

using jem::io::Writer;
using jem::io::PrintWriter;

//-----------------------------------------------------------------------
//   class StressModel
//-----------------------------------------------------------------------

class StressModel : public Model
{
 public:

  static const char*        DOF_TYPE_NAMES[3];
  static const char*        SHAPE_PROP;
  static const char*        MATERIAL_PROP;
  static const char*        THICKNESS_PROP;
  static const char*        WRITESTATE_PROP;
  static const char*        WRITESTRAINS_PROP;
  static const char*        WRITESTRESSES_PROP;
  static const char*        WRITEIPCOORDS_PROP;

  enum                      Mode { FULL, POD, ECM };

                            StressModel

    ( const String&         name,
      const Properties&     conf,
      const Properties&     props,
      const Properties&     globdat );

  virtual void              configure

    ( const Properties&     props,
      const Properties&     globdat );

  virtual void              getConfig
   
    ( const Properties&     conf,
      const Properties&     globdat ) const;

  virtual bool              takeAction

    ( const String&         action,
      const Properties&     params,
      const Properties&     globdat );

 protected:

  virtual                  ~StressModel ();

 private:

  void                      initDofs_

    ( const Properties&       globdat );

  void                      getMatrix_

    ( const Vector&           fint,
      Ref<MatrixBuilder>      mbld,
      const Properties&       globdat );

  void                      getDissForce_

    ( const Vector&           fDiss,
      const Vector&           disp );

  bool                      getTable_

    ( const Properties&       params,
      const Properties&       globdat );

  Ref<PrintWriter>          initWriter_

    ( const Properties&       params,
      const String            name ) const;

  void                      printIps_

    ( const Properties&       params,
      const Properties&       globdat );

  void                      printState_

    ( const Properties&       params,
      const Properties&       globdat );

  void                      printStrains_

    ( const Properties&       params,
      const Properties&       globdat );

  void                      printStresses_

    ( const Properties&       params,
      const Properties&       globdat );

  void                      printNodalStresses_

    (       XTable&           table,
      const String&           tablename,
      const Vector&           weights,
      const Properties&       globdat );

  void                      printNodalHistory_

    (       XTable&           table,
      const String&           tablename,
      const Vector&           weights,
      const Properties&       globdat );

  void                      printClusters_

    (       XTable&           table,
      const String&           tablename,
      const Vector&           weights,
      const Properties&       globdat );

  void                      printFullDofs_

    (       XTable&           table,
      const String&           tablename,
      const Vector&           weights,
      const Properties&       globdat );

  void                      getShapeGrads_

    ( const Matrix&           b,
      const Matrix&           g );

  void                      get1DShapeGrads_

    ( const Matrix&           b,
      const Matrix&           g );

  void                      get2DShapeGrads_

    ( const Matrix&           b,
      const Matrix&           g );

  void                      get3DShapeGrads_

    ( const Matrix&           b,
      const Matrix&           g );

  void                      getDissipation_ 
  
    ( const Properties&       params ) const;

  void                      initCharLength_ ();

  Vector                    getFeatures_ ();

 private:

  String                    myTag_;

  Properties                props_;

  Assignable<ElementGroup>  egroup_;
  Assignable<ElementSet>    elems_;
  Assignable<NodeSet>       nodes_;

  idx_t                     rank_;
  idx_t                     nodeCount_;
  idx_t                     numElem_;
  idx_t                     ipCount_;

  Ref<IShape>               shape_;
  Ref<Material>             material_;

  Ref<XDofSpace>            dofs_;
  IdxVector                 dofTypes_;
  idx_t                     dofCount_;

  idx_t                     strCount_;

  double                    thickness_;
  Ref<Function>             thickFunc_;

  DispWriter                dw_;

  // Writers

  Ref<PrintWriter>          stateOut_;
  Ref<PrintWriter>          strainOut_;
  Ref<PrintWriter>          stressOut_;
  Ref<PrintWriter>          coordOut_;

  DispWriter                stateWriter_;
  DispWriter                epsWriter_;
  DispWriter                sigWriter_;

  bool                      writeState_;
  bool                      writeSnaps_;
  bool                      writeStrains_;
  bool                      writeStresses_;

  Matrix                    epss_;
  Matrix                    sigs_;
  Matrix                    ipcoords_;

};

#endif
