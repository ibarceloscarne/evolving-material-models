/*
 * Copyright (C) 2021 TU Delft. All rights reserved.
 *
 * Author: Iuri Barcelos, i.rocha@tudelft.nl
 * Date:   May 2021
 * 
 */

#include <jem/base/Error.h>
#include <jem/base/System.h>
#include <jem/base/Array.h>
#include <jem/util/Properties.h>
#include <jem/numeric/algebra/matmul.h>
#include <jem/numeric/algebra/utilities.h>
#include <jem/io/FileReader.h>
#include <jive/model/Model.h>
#include <jive/model/StateVector.h>
#include <jive/app/ChainModule.h>
#include <jive/app/UserconfModule.h>
#include <jive/app/InfoModule.h>
#include <jive/fem/InitModule.h>
#include <jive/fem/ElementSet.h>
#include <jive/fem/NodeSet.h>
#include <jive/util/Globdat.h>
#include <jive/util/utilities.h>
#include <jive/util/DofSpace.h>
#include <jive/util/Assignable.h>

#include "MNNMaterial.h"

#include "utilities.h"
#include "LearningNames.h"

using namespace jem;

using jem::io::FileReader;

using jive::model::Model;
using jive::model::StateVector;
using jive::app::ChainModule;
using jive::app::UserconfModule;
using jive::app::InitModule;
using jive::app::InfoModule;
using jive::util::Globdat;
using jive::util::joinNames;
using jive::util::DofSpace;
using jive::util::Assignable;
using jive::fem::ElementSet;
using jive::fem::NodeSet;

//=======================================================================
//   class MNNMaterial
//=======================================================================

//-----------------------------------------------------------------------
//   static data
//-----------------------------------------------------------------------

const char* MNNMaterial::NETWORK          = "network";
const char* MNNMaterial::WEIGHTS          = "weights";
const char* MNNMaterial::NORMALIZER       = "normalizer";
const char* MNNMaterial::MATERIAL         = "material";
const char* MNNMaterial::FEATUREEXTRACTOR = "featureExtractor";

//-----------------------------------------------------------------------
//   constructor and destructor
//-----------------------------------------------------------------------

MNNMaterial::MNNMaterial

  ( const Properties& props,
    const Properties& conf,
    const idx_t       rank,
    const Properties& globdat )

  : Material ( rank, globdat )

{

  rank_    = rank;
  props_   = props;
  conf_    = conf;
  globdat_ = globdat;

  JEM_PRECHECK ( rank_ >= 1 && rank_ <= 3 );

  data_. resize ( 0 );

  // Check for permutation

  Assignable<ElementSet> eset;
  Assignable<NodeSet>    nset;
  eset = ElementSet::get ( globdat, "MNNMaterial" );
  nset = eset.getNodes ( );

  idx_t meshrank = nset.rank ( );

  if ( meshrank < rank_ )
  {
    perm23_ = true;

    System::out() << "Performing plane strain analysis on 3D network\n";

    perm_.resize ( 3 );

    perm_[0] = 0;   
    perm_[1] = 1;   
    perm_[2] = 3;   
  }
  else
  {
    perm23_ = false;
  }
}

MNNMaterial::~MNNMaterial ()
{}

//-----------------------------------------------------------------------
//   configure
//-----------------------------------------------------------------------

void MNNMaterial::configure

  ( const Properties& props,
    const Properties& globdat )

{
  String fname;

  Ref<ChainModule> chain;

  netData_ = Globdat::newInstance ( NETWORK );

  Globdat::getVariables ( netData_ );

  chain = newInstance<ChainModule> ( joinNames ( NETWORK, "chain" ) );

  chain->pushBack ( newInstance<UserconfModule> ( joinNames ( NETWORK, "userinput"   ) ) );
  chain->pushBack ( newInstance<InitModule>     ( joinNames ( NETWORK, "init"        ) ) );
  chain->pushBack ( newInstance<InfoModule>     ( joinNames ( NETWORK, "info"        ) ) );

  chain->configure ( props,  netData_        );
  chain->getConfig ( conf_,  netData_        );
  chain->init      ( conf_,  props, netData_ );

  network_ = chain;

  props.get ( fname, WEIGHTS );
  conf_.set ( WEIGHTS, fname );

  initWeights_ ( fname );

  props.get ( fname, NORMALIZER );
  conf_.set ( NORMALIZER, fname );

  nizer_ = newNormalizer ( fname );

  if ( perm23_ )
  {
    System::out() << "Performing plane strain analysis on 3D network\n";

    perm_.resize ( 3 );

    perm_[0] = 0;   
    perm_[1] = 1;   
    perm_[2] = 3;   
  }
}

//-----------------------------------------------------------------------
//   getConfig
//-----------------------------------------------------------------------

void MNNMaterial::getConfig

  ( const Properties& conf,
    const Properties& globdat ) const
    
{}

//-----------------------------------------------------------------------
//  createIntPoints 
//-----------------------------------------------------------------------

void MNNMaterial::createIntPoints

  ( const idx_t       npoints )

{
  const idx_t STRAIN_COUNTS[4] = { 0, 1, 3, 6 };

  idx_t size = STRAIN_COUNTS[rank_];

  iPointStrain_.resize ( size, npoints );
  iPointStrain_ = 0.0;

  mat_. resize ( npoints );
  ext_. resize ( npoints );
  bmat_.resize ( npoints );
  bext_.resize ( npoints );

  for ( idx_t p = 0; p < npoints; ++p )
  {
    mat_[p] = newMaterial ( MATERIAL, Properties(), props_, globdat_ );
    mat_[p]->configure ( props_.findProps ( MATERIAL ), globdat_ );
    mat_[p]->createIntPoints ( 1 );

    ext_[p] = newMaterial ( FEATUREEXTRACTOR, Properties(), props_, globdat_ );
    ext_[p]->configure ( props_.findProps ( FEATUREEXTRACTOR ), globdat_ );
    ext_[p]->createIntPoints ( 1 );

    bmat_[p] = dynamicCast<Bayesian> ( mat_[p] );
    bext_[p] = dynamicCast<Bayesian> ( ext_[p] );
  }

  updateProps_();
}

//-----------------------------------------------------------------------
//  update 
//-----------------------------------------------------------------------

void MNNMaterial::update

  ( Matrix&       stiff,
    Vector&       stress,
    const Vector& strain,
    const idx_t   ipoint )
{
  //updateProps_();
  //updateProps_( ipoint );

  mat_[ipoint]->update ( stiff, stress, strain, 0_idx );

  for ( idx_t i = 0_idx; i < strain.size(); ++i )
  {
    if ( stiff(i,i) < 0.0 )
    {
      System::out() << "MNNMaterial: softening point " << ipoint << '\n';
    }
  }

  iPointStrain_(ALL,ipoint) = strain;
  //System::out() << "Update strain " << strain << " stress " << stress << '\n';
}

//-----------------------------------------------------------------------
//  commit
//-----------------------------------------------------------------------

void MNNMaterial::commit ()

{
  const idx_t STRAIN_COUNTS[4] = { 0, 1, 3, 6 };

  idx_t size = STRAIN_COUNTS[rank_];

  Vector stress ( size );
  Matrix stiff  ( size, size );
  stress = 0.0; stiff = 0.0;

  updateProps_();

  for ( idx_t p = 0_idx; p < iPointStrain_.size(); ++p )
  {
    mat_[p]->update ( stiff, stress, iPointStrain_(ALL,p), 0_idx );
    mat_[p]->commit();
    //System::out() << "Latest strain norm " << jem::numeric::norm2(iPointStrain_(ALL,p)) << '\n';
  }
}

//-----------------------------------------------------------------------
//  stressAtPoint 
//-----------------------------------------------------------------------

void MNNMaterial::stressAtPoint

  ( Vector&       stress,
    const Vector& strain,
    const idx_t   ipoint )
{
  mat_[ipoint]->stressAtPoint ( stress, strain, 0_idx );
}

//-----------------------------------------------------------------------
//  clone 
//-----------------------------------------------------------------------

Ref<Material> MNNMaterial::clone ( ) const

{
  return newInstance<MNNMaterial> ( *this );
}

//-----------------------------------------------------------------------
//  addTableColumns 
//-----------------------------------------------------------------------

void MNNMaterial::addTableColumns

  ( IdxVector&     jcols,
    XTable&        table,
    const String&  name )

{
  mat_[0_idx]->addTableColumns ( jcols, table, name );
}

//-----------------------------------------------------------------------
//  getHistory
//-----------------------------------------------------------------------

void MNNMaterial::getHistory

  ( Vector&        hvals,
    const idx_t    mpoint )

{
  mat_[mpoint]->getHistory ( hvals, 0_idx );
}

//-----------------------------------------------------------------------
//  initWeights_
//-----------------------------------------------------------------------

void MNNMaterial::initWeights_

  ( const String&     fname )

{
  String context ( "MNNMaterial::initWeights_" );

  Ref<DofSpace> dofs  = DofSpace::get ( netData_, context );
  Ref<Model>    model = Model::   get ( netData_, context );

  Ref<FileReader> in    = newInstance<FileReader> ( fname );

  idx_t dc = dofs->dofCount();

  Vector wts ( dc );
  wts = 0.0;

  for ( idx_t i = 0; i < dc; ++i )
  {
    wts[i] = in->parseDouble();
  }

  StateVector::store ( wts, dofs, netData_ );

  model->takeAction ( LearningActions::UPDATE, Properties(), netData_ );
}

//-----------------------------------------------------------------------
//  updateProps_
//-----------------------------------------------------------------------

void MNNMaterial::updateProps_ ()

{
  const idx_t STRAIN_COUNTS[4] = { 0, 1, 3, 6 };

  idx_t size = STRAIN_COUNTS[rank_];

  Properties params;

  String context ( "MNNMaterial::commit" );
  
  Ref<Model> model = Model::get ( netData_, context );

  Vector strain ( size );
  Vector stress ( size );
  Matrix stiff  ( size, size );

  strain = 0.0; stress = 0.0; stiff = 0.0;

  for ( idx_t p = 0_idx; p < iPointStrain_.size(); ++p )
  {
    if ( perm23_ )
    {
      strain[perm_] = iPointStrain_(ALL,p);
    }
    else
    {
      strain = iPointStrain_(ALL,p);
    }

    ext_[p]->update ( stiff, stress, strain, 0_idx );
    ext_[p]->commit ();

    Vector features = bext_[p]->getFeatures ( 0_idx );

    Ref<NData> data = newInstance<NData> ( 1, features.size(), bmat_[p]->propCount() );

    data->inputs(ALL,0) = nizer_->normalize ( features );

    params.set ( LearningParams::DATA, data );

    model->takeAction ( LearningActions::PROPAGATE, params, netData_ );

    bmat_[p]->setProps ( data->outputs(ALL,0) );

    //System::out() << "Probing network with inputs " << features << '\n';
    //System::out() << "Setting properties of point " << p << " to " << data->outputs(ALL,0) << '\n';
  }
}

//-----------------------------------------------------------------------
//  updateProps_
//-----------------------------------------------------------------------

void MNNMaterial::updateProps_ ( idx_t p )

{
  const idx_t STRAIN_COUNTS[4] = { 0, 1, 3, 6 };

  idx_t size = STRAIN_COUNTS[rank_];

  Properties params;

  String context ( "MNNMaterial::commit" );
  
  Ref<Model> model = Model::get ( netData_, context );

  Vector strain ( size );
  Vector stress ( size );
  Matrix stiff  ( size, size );

  strain = 0.0; stress = 0.0; stiff = 0.0;

  if ( perm23_ )
  {
    strain[perm_] = iPointStrain_(ALL,p);
  }
  else
  {
    strain = iPointStrain_(ALL,p);
  }

  ext_[p]->update ( stiff, stress, strain, 0_idx );

  Vector features = bext_[p]->getFeatures ( 0_idx );

  Ref<NData> data = newInstance<NData> ( 1, features.size(), bmat_[p]->propCount() );

  data->inputs(ALL,0) = nizer_->normalize ( features );

  params.set ( LearningParams::DATA, data );

  model->takeAction ( LearningActions::PROPAGATE, params, netData_ );

  bmat_[p]->setProps ( data->outputs(ALL,0) );

  //System::out() << "Probing network with inputs " << features << '\n';
  //System::out() << "Setting properties of point " << p << " to " << data->outputs(ALL,0) << '\n';
}
