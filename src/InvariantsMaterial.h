#ifndef INVARIANTS_MATERIAL_H
#define INVARIANTS_MATERIAL_H

#include "Material.h"
#include "Bayesian.h"
#include "Invariants.h"

#include <jem/util/Flex.h>

using jem::util::Flex;

//-----------------------------------------------------------------------
//   class InvariantsMaterial
//-----------------------------------------------------------------------

class InvariantsMaterial : public Material,
                           public Bayesian
{
 public:

  static const char*     E_PROP;
  static const char*     NU_PROP;
  static const char*     ANMODEL_PROP;
  static const char*     AREA_PROP;

  explicit               InvariantsMaterial

    ( const idx_t          rank,
      const Properties&    globdat );

  virtual void           configure

    ( const Properties&    props,
      const Properties&    globdat );

  virtual void           getConfig

    ( const Properties&    conf,
      const Properties&    globdat )   const;

  virtual void           update

    ( Matrix&              stiff,
      Vector&              stress,
      const Vector&        strain,
      const idx_t          ipoint );

  virtual void           commit      ();

  virtual void           stressAtPoint

    ( Vector&              stress,
      const Vector&        strain,
      const idx_t          ipoint );

  virtual void           addTableColumns

    ( IdxVector&           jcols,
      XTable&              table,
      const String&        name );
  
  virtual void           createIntPoints

    ( const idx_t           npoints );

  virtual Ref<Material>  clone ( ) const;

  virtual void           setProps 
 
    ( const Vector&         props   );

  virtual Vector          getProps() {};

  virtual Vector         getFeatures

    ( const idx_t           ipoint  );

  virtual idx_t          propCount ();

  virtual void           clearHist ();

 protected:

  virtual               ~InvariantsMaterial();

 protected:

  class                  Hist_
  {
   public:
                           Hist_();

    double                 i1;    
    double                 i2;    
    double                 i3;    
    double                 j2;
    double                 j3;
  };

 protected:

  idx_t                   rank_;
  double                  e_;
  double                  nu_;
  double                  area_;
  Matrix                  stiffMatrix_;

  Flex<Hist_>             preHist_;
  Flex<Hist_>             newHist_;

  String                  anmodel_;

 protected:
  
  void                    computeStiffMatrix_     ( );

  Tuple<double,6>         fill3DStrain_

    ( const Vector&         v3 ) const;
};

#endif
