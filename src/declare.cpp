
#include "declare.h"

#include <jive/fem/InputModule.h>
#include <jive/app/ModuleFactory.h>
#include <jive/model/ModelFactory.h>

#include "XNonlinModule.h"
#include "AdamModule.h"
#include "SGDModule.h"
#include "ANNModel.h"
#include "ParaViewModule.h"

//-----------------------------------------------------------------------
//   declareModels
//-----------------------------------------------------------------------


void declareModels ()
{
  declareStressModel                        ();
  declareBCModel                            ();
  declarePeriodicBCModel                    ();
  declareLoadDispModel                      ();
  jive::model::ANNModel::declare            ();
}

void declareLayerModels()
{
  declareDenseLayer                         ();
  declareRecurrentLayer                     ();
  declareDropoutLayer                       ();
  declareSampleModel                        ();
  declareMaterialLayer                      ();
}

void declareModules()
{
  declareInputModule                        ();
  declareGroupInputModule                   ();
  declareGmshInputModule                    ();
  declareLaminateMeshModule                 ();
  declarePBCGroupInputModule                ();
  declareAdaptiveStepModule                 ();
  declareANNInputModule                     ();
  declareANNOutputModule                    ();

  jive::implict::XNonlinModule::declare     ();
  jive::implict::AdamModule::declare        ();
  jive::implict::SGDModule::declare         ();

  ParaViewModule::declare                   ();
}

void declareInputModule ()
{
  using jive::app::ModuleFactory;
  using jive::fem::InputModule;

  ModuleFactory::declare ( "Input",
  			 & InputModule::makeNew );
}
