/*
 * Copyright (C) 2019 TU Delft. All rights reserved.
 *
 * A model shell for SampleModule. Use this to write non-
 * converged data. Writing only happens when someone
 * calls the SolverNames::SUBSAMPLE action. This should go
 * at the end of the model list.
 *
 * Author: Iuri Barcelos, i.barceloscarneiromrocha@tudelft.nl
 * Date:   Sep 2019
 * 
 */

#include <jem/base/System.h>
#include <jem/base/Error.h>
#include <jive/util/utilities.h>
#include <jive/model/Actions.h>
#include <jive/model/ModelFactory.h>

#include "SampleModel.h"
#include "SolverNames.h"
#include "declare.h"

using jem::io::endl;
using jive::model::Actions;
using jive::model::ActionParams;

//=======================================================================
//   class SampleModel
//=======================================================================

//-----------------------------------------------------------------------
//   constructor & destructor
//-----------------------------------------------------------------------

SampleModel::SampleModel

  ( const String&     name,
    const Properties& conf,
    const Properties& props,
    const Properties& globdat ):

    Model ( name )

{
  sampler_ = newInstance<SampleModule> ( name );

  sampler_->configure ( props, globdat        );
  sampler_->getConfig ( conf,  globdat        );
  sampler_->init      ( conf,  props, globdat );
}

SampleModel::~SampleModel ()
{}

//-----------------------------------------------------------------------
//   configure
//-----------------------------------------------------------------------

void SampleModel::configure

  ( const Properties& props,
    const Properties& globdat )

{
  sampler_->configure ( props, globdat );
}

//-----------------------------------------------------------------------
//   getConfig
//-----------------------------------------------------------------------

void SampleModel::getConfig

  ( const Properties& conf,
    const Properties& globdat ) const

{
  sampler_->getConfig ( conf, globdat );
}

//-----------------------------------------------------------------------
//   takeAction
//-----------------------------------------------------------------------

bool SampleModel::takeAction

  ( const String&     action,
    const Properties& params,
    const Properties& globdat )

{
  if ( action == SolverNames::SUBSAMPLE )
  {
    sampler_->run ( globdat );

    return true;
  }

  return false;
}

//-----------------------------------------------------------------------
//   makeNew
//-----------------------------------------------------------------------

Ref<Model> SampleModel::makeNew

  ( const String&     name,
    const Properties& conf,
    const Properties& props,
    const Properties& globdat )

{
  return newInstance<SampleModel> ( name, conf, props, globdat );
}

//=======================================================================
//   related functions
//=======================================================================

//-----------------------------------------------------------------------
//   declareSampleModel
//-----------------------------------------------------------------------

void declareSampleModel ()
{
  using jive::model::ModelFactory;

  ModelFactory::declare ( "Sample", 
                          & SampleModel::makeNew );
}
