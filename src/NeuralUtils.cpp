/*
 * Copyright (C) 2019 TU Delft. All rights reserved.
 *
 * Utility functions for artificial neural networks.
 *
 * Author: Iuri Barcelos, i.barceloscarneiromrocha@tudelft.nl
 * Date:   May 2019
 * 
 */

#include <cstdlib>

#include <jem/base/System.h>
#include <jem/base/array/select.h>
#include <jem/base/array/utilities.h>
#include <jem/base/array/operators.h>
#include <jem/base/PrecheckException.h>
#include <jem/base/Error.h>
#include <jem/base/Float.h>
#include <jem/numeric/utilities.h>
#include <jem/numeric/algebra/matmul.h>
#include <jem/numeric/algebra/utilities.h>

#include <jive/util/Random.h>

#include "NeuralUtils.h"
#include "utilities.h"

using namespace jem;
using namespace NeuralUtils;

using jem::numeric::matmul;
using jem::numeric::norm2;
using jem::ALL;

using jive::util::Random;

//-----------------------------------------------------------------------
//   getActivationFunc
//-----------------------------------------------------------------------

ActivationFunc NeuralUtils::getActivationFunc

  ( const String& name )

{
  if      ( name.equalsIgnoreCase ( "identity" ) ||
            name.equalsIgnoreCase ( "linear"   )    )
  {
    return & NeuralUtils::Activations::evalIdentityFunc;
  }
  else if ( name.equalsIgnoreCase ( "sigmoid" ) )
  {
    return & NeuralUtils::Activations::evalSigmoidFunc;
  }
  else if ( name.equalsIgnoreCase ( "tanh"    ) )
  {
    return & NeuralUtils::Activations::evalTanhFunc;
  }
  else if ( name.equalsIgnoreCase ( "relu" ) )
  {
    return & NeuralUtils::Activations::evalReLUFunc;
  }
  else if ( name.equalsIgnoreCase ( "leakyrelu" ) )
  {
    return & NeuralUtils::Activations::evalLeakyReLUFunc;
  }
  else if ( name.equalsIgnoreCase ( "softplus" ) )
  {
    return & NeuralUtils::Activations::evalSoftPlusFunc;
  }
  else if ( name.equalsIgnoreCase ( "selu" ) )
  {
    return & NeuralUtils::Activations::evalSeLUFunc;
  }
  else
  {
    throw Error ( JEM_FUNC, "Unknown activation function." );
  }

  return 0;
}

//-----------------------------------------------------------------------
//   getActivationGrad
//-----------------------------------------------------------------------

ActivationGrad NeuralUtils::getActivationGrad

  ( const String& name )

{
  if      ( name.equalsIgnoreCase ( "identity" ) ||
            name.equalsIgnoreCase ( "linear"   )    )
  {
    return & NeuralUtils::Activations::evalIdentityGrad;
  }
  else if ( name.equalsIgnoreCase ( "sigmoid" ) )
  {
    return & NeuralUtils::Activations::evalSigmoidGrad;
  }
  else if ( name.equalsIgnoreCase ( "tanh"    ) )
  {
    return & NeuralUtils::Activations::evalTanhGrad;
  }
  else if ( name.equalsIgnoreCase ( "relu" ) )
  {
    return & NeuralUtils::Activations::evalReLUGrad;
  }
  else if ( name.equalsIgnoreCase ( "leakyrelu" ) )
  {
    return & NeuralUtils::Activations::evalLeakyReLUGrad;
  }
  else if ( name.equalsIgnoreCase ( "softplus" ) )
  {
    return & NeuralUtils::Activations::evalSoftPlusGrad;
  }
  else if ( name.equalsIgnoreCase ( "selu" ) )
  {
    return & NeuralUtils::Activations::evalSeLUGrad;
  }
  else
  {
    throw Error ( JEM_FUNC, "Unknown activation function." );
  }

  return 0;
}

//-----------------------------------------------------------------------
//   getActivationHess
//-----------------------------------------------------------------------

ActivationGrad NeuralUtils::getActivationHess

  ( const String& name )

{
  if      ( name.equalsIgnoreCase ( "identity" ) ||
            name.equalsIgnoreCase ( "linear"   )    )
  {
    return & NeuralUtils::Activations::evalIdentityHess;
  }
  else if ( name.equalsIgnoreCase ( "sigmoid" ) )
  {
    return & NeuralUtils::Activations::evalSigmoidHess;
  }
  else if ( name.equalsIgnoreCase ( "tanh"    ) )
  {
    return & NeuralUtils::Activations::evalTanhHess;
  }
  else if ( name.equalsIgnoreCase ( "relu" ) )
  {
    return & NeuralUtils::Activations::evalReLUHess;
  }
  else if ( name.equalsIgnoreCase ( "leakyrelu" ) )
  {
    return & NeuralUtils::Activations::evalLeakyReLUHess;
  }
  else if ( name.equalsIgnoreCase ( "softplus" ) )
  {
    return & NeuralUtils::Activations::evalSoftPlusHess;
  }
  else if ( name.equalsIgnoreCase ( "selu" ) )
  {
    return & NeuralUtils::Activations::evalSeLUHess;
  }
  else
  {
    throw Error ( JEM_FUNC, "Unknown activation function." );
  }

  return 0;
}

//-----------------------------------------------------------------------
//   getLossFunc
//-----------------------------------------------------------------------

LossFunc NeuralUtils::getLossFunc

  ( const String& name )

{
  if ( name.equalsIgnoreCase ( "squarederror" ) )
  {
    return & NeuralUtils::Losses::evalSquaredErrorFunc;
  }
  else
  {
    throw Error ( JEM_FUNC, "Unknown loss function." );
  }

  return 0;
}

//-----------------------------------------------------------------------
//   getLossGrad
//-----------------------------------------------------------------------

LossGrad NeuralUtils::getLossGrad

  ( const String& name )

{
  if ( name.equalsIgnoreCase ( "squarederror" ) )
  {
    return & NeuralUtils::Losses::evalSquaredErrorGrad;
  }
  else
  {
    throw Error ( JEM_FUNC, "Unknown loss function." );
  }

  return 0;
}

//-----------------------------------------------------------------------
//   getInitFunc
//-----------------------------------------------------------------------

InitFunc NeuralUtils::getInitFunc

  ( const String& name )

{
  if      ( name.equalsIgnoreCase  ( "zeros" ) )
  {
    return & NeuralUtils::Initializations::zeroInit;
  }
  else if ( name.equalsIgnoreCase ( "glorot" ) )
  {
    return & NeuralUtils::Initializations::glorotInit;
  }
  else if ( name.equalsIgnoreCase ( "selu" ) )
  {
    return & NeuralUtils::Initializations::seluInit;
  }
  else
  {
    throw Error ( JEM_FUNC, "Unknown initialization function." );
  }

  return 0;
}

//-----------------------------------------------------------------------
//   zeroInit
//-----------------------------------------------------------------------

void NeuralUtils::Initializations::zeroInit

  (       Matrix&     w,
    const Properties& globdat )

{
  w = 0.0;
}

//-----------------------------------------------------------------------
//   glorotInit
//-----------------------------------------------------------------------

void NeuralUtils::Initializations::glorotInit

  (       Matrix&     w,
    const Properties& globdat )

{
  // Variance-preserving weight initialization
  // Glorot, X. and Bengio, Y. (2010). Understanding the difficulty of
  // training deep feedforward neural networks. In: Proceedings of 
  // AISTATS 2010, 9:249-256.

  Ref<Random> generator = Random::get ( globdat );

  idx_t nrow = w.size(0);
  idx_t ncol = w.size(1);

  double fac = sqrt ( 6. / ( nrow + ncol ) );

  for ( idx_t i = 0; i < nrow; ++i )
  {
    for ( idx_t j = 0; j < ncol; ++j )
    {
      double rand = 2.0*generator->next() - 1.0;
      w(i,j) = rand * fac;
    }
  }
}

//-----------------------------------------------------------------------
//   seluInit
//-----------------------------------------------------------------------

void NeuralUtils::Initializations::seluInit

  (       Matrix& w,
    const Properties& globdat )

{
  // Self-normalizing network initialization
  // Klambauer, G., Unterthiner, T., Mayr, A. (2017).
  // Self-normalizing Neural Networks, arXiv:1706.02515v5

  Ref<Random> generator = Random::get ( globdat );

  idx_t nrow = w.size(0);
  idx_t ncol = w.size(1);

  double fac = sqrt ( 1. / ncol );

  for ( idx_t i = 0_idx; i < nrow; ++i )
  {
    for ( idx_t j = 0_idx; j < ncol; ++j )
    {
      w(i,j) = fac * generator->nextGaussian();
    }
  }
}

//-----------------------------------------------------------------------
//   shuffle
//-----------------------------------------------------------------------

void NeuralUtils::shuffle

  ( IdxVector&        vec,
    const Properties& globdat )

{
  // Fisher-Yates shuffling algorithm

  Ref<Random> generator = Random::get ( globdat );

  int  temp;
  jem::lint rand;  

  int size = vec.size();

  while ( size > 1 )
  {
    rand = generator->next ( size - 1 );
    size--;

    temp = vec[size];
    vec[size] = vec[rand];
    vec[rand] = temp;
  }
}
