/*
 * Copyright (C) 2022 TU Delft. All rights reserved.
 *
 * Author: Iuri Rocha, i.rocha@tudelft.nl
 * Date:   Jul 2022
 * 
 */

#include <jem/base/Error.h>
#include <jem/base/System.h>
#include <jem/base/Array.h>
#include <jem/util/Properties.h>
#include <jem/util/Flex.h>
#include <jem/util/ArrayBuffer.h>
#include <jem/numeric/algebra/matmul.h>
#include <jem/numeric/algebra/utilities.h>
#include <jem/numeric/algebra/LUSolver.h>
#include <jem/io/FileWriter.h>
#include <jem/io/FileReader.h>
#include <jem/io/FileInputStream.h>
#include <jem/io/FileFlags.h>
#include <jem/io/PrintWriter.h>
#include <jive/model/Model.h>
#include <jive/model/StateVector.h>
#include <jive/app/ChainModule.h>
#include <jive/app/UserconfModule.h>
#include <jive/app/InfoModule.h>
#include <jive/fem/InitModule.h>
#include <jive/fem/ElementSet.h>
#include <jive/fem/NodeSet.h>
#include <jive/util/Globdat.h>
#include <jive/util/utilities.h>
#include <jive/util/DofSpace.h>
#include <jive/util/Assignable.h>
#include <jive/implict/SolverInfo.h>

#include "StableMNNMaterial.h"

#include "utilities.h"
#include "LearningNames.h"
#include "TrainingData.h"
#include "SGDModule.h"

using namespace jem;

using jem::io::FileWriter;
using jem::io::FileReader;
using jem::io::FileInputStream;
using jem::io::FileFlags;
using jem::io::PrintWriter;
using jem::numeric::LUSolver;
using jem::util::Flex;
using jem::util::ArrayBuffer;

using jive::model::Model;
using jive::model::StateVector;
using jive::app::ChainModule;
using jive::app::UserconfModule;
using jive::app::InitModule;
using jive::app::InfoModule;
using jive::util::Globdat;
using jive::util::joinNames;
using jive::util::DofSpace;
using jive::util::Assignable;
using jive::fem::ElementSet;
using jive::fem::NodeSet;
using jive::StringVector;

using jive::implict::SGDModule;
using jive::implict::SolverInfo;

//=======================================================================
//   class StableMNNMaterial
//=======================================================================

//-----------------------------------------------------------------------
//   static data
//-----------------------------------------------------------------------


const char* StableMNNMaterial::WEIGHTS          = "weights";
const char* StableMNNMaterial::MATERIAL         = "material";
const char* StableMNNMaterial::FEATUREEXTRACTOR = "featureExtractor";
const char* StableMNNMaterial::FEATURES         = "features";
const char* StableMNNMaterial::NETWORK          = "network";
const char* StableMNNMaterial::STABILIZE        = "stabilize";
const char* StableMNNMaterial::LEARNINGRATE     = "learningRate";
const char* StableMNNMaterial::SGDEPOCHS        = "epochs";
const char* StableMNNMaterial::CRITTHRESHOLD    = "critThreshold";
const char* StableMNNMaterial::RETRAINEVERY     = "retrainEvery";
const char* StableMNNMaterial::CRITERIUM        = "criterium";
const char* StableMNNMaterial::SGD              = "sgd";
const char* StableMNNMaterial::MONITORLOSS      = "monitorLoss";

//-----------------------------------------------------------------------
//   constructor and destructor
//-----------------------------------------------------------------------

StableMNNMaterial::StableMNNMaterial

  ( const Properties& props,
    const Properties& conf,
    const idx_t       rank,
    const Properties& globdat )

  : Material ( rank, globdat )

{

  rank_    = rank;
  props_   = props;
  conf_    = conf;
  globdat_ = globdat;

  JEM_PRECHECK ( rank_ >= 1 && rank_ <= 3 );

  // Check for permutation

  Assignable<ElementSet> eset;
  Assignable<NodeSet>    nset;
  eset = ElementSet::get ( globdat, "StableMNNMaterial" );
  nset = eset.getNodes ( );

  idx_t meshrank = nset.rank ( );

  if ( meshrank < rank_ )
  {
    perm23_ = true;

    System::out() << "Performing plane strain analysis on 3D network\n";

    perm_.resize ( 3 );

    perm_[0] = 0;   
    perm_[1] = 1;   
    perm_[2] = 3;   
  }
  else
  {
    perm23_ = false;
  }

  // Stabilization parameters

  stabilize_   = false;
  monitorLoss_ = false;

  lrate_        = 1.e-5;
  nepochs_      = 10;
  retrainEvery_ = 1;
  critThres_    = 0.0;
  initCrit_     = 0.0;
  iIter_        = 0;
  crit_         = ACOUSTIC;
}

StableMNNMaterial::~StableMNNMaterial ()
{}

//-----------------------------------------------------------------------
//   configure
//-----------------------------------------------------------------------

void StableMNNMaterial::configure

  ( const Properties& props,
    const Properties& globdat )

{
  String fname;

  props.get ( fname, WEIGHTS );
  conf_.set ( WEIGHTS, fname );

  initEncoder_ ( props, globdat );
  initNetwork_ ( props, globdat );

  initWeights_ ( fname );

  Ref<TrainingData> tdata = 
    TrainingData::get ( netData_, "StableMNNMaterial::configure" );

  nizer_ = tdata->getInpNormalizer();

  props.find ( stabilize_,    STABILIZE     );
  props.find ( monitorLoss_,  MONITORLOSS   );
  props.find ( lrate_,        LEARNINGRATE  );
  props.find ( nepochs_,      SGDEPOCHS     );
  props.find ( retrainEvery_, RETRAINEVERY  );
  props.find ( critThres_,    CRITTHRESHOLD );
  props.find ( features_,     FEATURES      );

  String crit = "acoustic";

  props.find ( crit, CRITERIUM );

  if ( crit.equalsIgnoreCase ( "det" ) )
  {
    crit_ = DET;
  }
  else if ( crit.equalsIgnoreCase ( "acoustic" ) )
  {
    crit_ = ACOUSTIC;
  }
  else if ( crit.equalsIgnoreCase ( "diag" ) )
  {
    crit_ = DIAG;
  }
  else
  {
    throw Error ( JEM_FUNC, "Unknown stability criterium" );
  }

  // Copy material properties from the network

  bool found = false;

  Properties modelProps = props.getProps ( joinNames ( NETWORK, "model" ) );

  StringVector layers;
  modelProps.get ( layers, "layers" );

  for ( idx_t l = 0_idx; l < layers.size(); ++l )
  {
    String type;
    modelProps.get ( type, joinNames ( layers[l], "type" ) );

    if ( type == "Material" )
    {
      props_.set ( MATERIAL, modelProps.getProps ( joinNames ( layers[l], "material" ) ) );
      found  = true;
    }
  }

  if ( !found )
  {
    throw Error ( JEM_FUNC, "Material layer not found in the given network" );
  }

  idx_t childRank;

  props_.get ( childRank, joinNames ( MATERIAL, "rank" ) );

  if ( childRank != rank_ )
  {
    System::out() << "StableMNNMaterial: equalizing child rank...\n";
    
    props_.set ( joinNames ( MATERIAL, "rank" ), rank_ );
  }

  String anmodel = "";
  if ( props_.find ( anmodel, "anmodel" ) )
  {
    System::out() << "StableMNNMaterial: anmodel mismatch, continuing with " << anmodel << "...\n";
    props_.set ( joinNames ( MATERIAL, "anmodel" ), anmodel );
  }
}

//-----------------------------------------------------------------------
//   getConfig
//-----------------------------------------------------------------------

void StableMNNMaterial::getConfig

  ( const Properties& conf,
    const Properties& globdat ) const
    
{}

//-----------------------------------------------------------------------
//  createIntPoints 
//-----------------------------------------------------------------------

void StableMNNMaterial::createIntPoints

  ( const idx_t       npoints )

{
  const idx_t STRAIN_COUNTS[4] = { 0, 1, 3, 6 };

  idx_t size = STRAIN_COUNTS[rank_];

  preStrains_.resize ( size, npoints );
  newStrains_.resize ( size, npoints );
  isUnstable_.resize ( npoints );
  useSecant_.resize ( npoints );

  preStrains_ = 0.0; 
  newStrains_ = 0.0;
  isUnstable_ = false;
  useSecant_  = false;

  mat_. resize ( npoints );
  ext_. resize ( npoints );
  bmat_.resize ( npoints );
  bext_.resize ( npoints );

  for ( idx_t p = 0; p < npoints; ++p )
  {
    mat_[p] = newMaterial ( MATERIAL, Properties(), props_, globdat_ );
    mat_[p]->configure ( props_.findProps ( MATERIAL ), globdat_ );
    mat_[p]->createIntPoints ( 1 );

    ext_[p] = newMaterial ( FEATUREEXTRACTOR, Properties(), props_, globdat_ );
    ext_[p]->configure ( props_.findProps ( FEATUREEXTRACTOR ), globdat_ );
    ext_[p]->createIntPoints ( 1 );

    bmat_[p] = dynamicCast<Bayesian> ( mat_[p] );
    bext_[p] = dynamicCast<Bayesian> ( ext_[p] );
  }
  
  idx_t ninp = bext_[0_idx]->getFeatures ( 0_idx ).size();

  if ( !features_.size() )
  {
    features_.ref ( IdxVector ( iarray ( ninp ) ) );
  }
  else if ( testany ( features_ >= ninp ) )
  {
    throw Error ( JEM_FUNC, "Out-of-bound feature" );
  }

  updateProps_ ( newStrains_ );

  initStiff_.resize ( size, size );
  initStiff_ = 0.0;

  Vector stress ( size );
  Vector strain ( size );
  stress = 0.0; strain = 0.0;

  mat_[0]->update ( initStiff_, stress, strain, 0_idx );
}

//-----------------------------------------------------------------------
//  update 
//-----------------------------------------------------------------------

void StableMNNMaterial::update

  ( Matrix&       stiff,
    Vector&       stress,
    const Vector& strain,
    const idx_t   ipoint )
{
  bool lastPoint = (ipoint == newStrains_.size() - 1);
  bool firstIter = (iIter_ == 1);
  
  mat_[ipoint]->update ( stiff, stress, strain, 0_idx );

  newStrains_(ALL,ipoint) = strain;

  if ( evalCrit_ ( stiff ) < critThres_ )
  {
    isUnstable_[ipoint] = true;
    useSecant_ [ipoint] = true;
  }

  if ( lastPoint )
  {
    if ( stabilize_ && firstIter )
    {
      ArrayBuffer<idx_t> buf;
      for ( idx_t p = 0_idx; p < newStrains_.size(); ++p )
      {
        if ( isUnstable_[p] )
	{
	  buf.pushBack ( p );
	}
      }

      if ( buf.size() )
      {
	updateNetwork_ ( IdxVector ( buf.toArray() ) );
	updateProps_   ( preStrains_ );
      }
    }

    isUnstable_ = false;
    iIter_++;
  }

  if ( desperateMode_ && useSecant_[ipoint] )
  {
    stiff = initStiff_;
  }
}

//-----------------------------------------------------------------------
//  cancel
//-----------------------------------------------------------------------

void StableMNNMaterial::cancel ()

{
  newStrains_ = preStrains_; 
  iIter_ = 0;

  updateProps_ ( newStrains_ );
}

//-----------------------------------------------------------------------
//  commit
//-----------------------------------------------------------------------

void StableMNNMaterial::commit ()

{
  if ( stabilize_ && monitorLoss_ )
  {
    Ref<SGDModule> sgd = dynamicCast<SGDModule> ( sgd_ );

    System::out() << "StableMNNMaterial: Current validation loss: " << sgd->getValiError ( netData_ ) << '\n';
  }
  
  // NB: Update properties here with an advance() timing. That is why
  //     we first commit with the current properties. Otherwise we should
  //     move the update to checkCommit() and adopt a semi-implicit approach
  
  for ( idx_t p = 0_idx; p < newStrains_.size(); ++p )
  {
    mat_[p]->commit();
  }

  updateProps_ ( newStrains_ );

  // Now we commit the extractor, since it has seen the current strain
  // during updateProps_()

  for ( idx_t p = 0_idx; p < newStrains_.size(); ++p )
  {
    ext_[p]->commit();
  }
  
  preStrains_ = newStrains_;

  strains_.pushBack ( preStrains_.clone() );
  
  iIter_ = 0;
}

//-----------------------------------------------------------------------
//  stressAtPoint 
//-----------------------------------------------------------------------

void StableMNNMaterial::stressAtPoint

  ( Vector&       stress,
    const Vector& strain,
    const idx_t   ipoint )
{
  mat_[ipoint]->stressAtPoint ( stress, strain, 0_idx );
}

//-----------------------------------------------------------------------
//  clone 
//-----------------------------------------------------------------------

Ref<Material> StableMNNMaterial::clone ( ) const

{
  return newInstance<StableMNNMaterial> ( *this );
}

//-----------------------------------------------------------------------
//  addTableColumns 
//-----------------------------------------------------------------------

void StableMNNMaterial::addTableColumns

  ( IdxVector&     jcols,
    XTable&        table,
    const String&  name )

{
  mat_[0_idx]->addTableColumns ( jcols, table, name );

  if ( name == "history" || name == "nodalHistory" )
  {
    idx_t nout = bmat_[0_idx]->propCount();
    idx_t ncol = jcols.size();

    jcols.reshape ( ncol + nout );

    StringVector propNames;

    props_.get ( propNames, joinNames ( MATERIAL, "propNames" ) );

    if ( propNames.size() != nout )
    {
      throw Error ( JEM_FUNC, "addTableColumns: Property count mismatch" );
    }

    for ( idx_t p = 0_idx; p < nout; ++ p )
    {
      jcols[ncol+p] = table.addColumn ( propNames[p] );
    }
  }
}

//-----------------------------------------------------------------------
//  getHistory
//-----------------------------------------------------------------------

void StableMNNMaterial::getHistory

  ( Vector&        hvals,
    const idx_t    mpoint )

{
  mat_[mpoint]->getHistory ( hvals, 0_idx );

  idx_t  nhist = hvals.size();

  Vector props  = bmat_[mpoint]->getProps();
  idx_t  nprops = props.size();

  hvals.reshape ( nhist + nprops );

  hvals[slice(nhist,END)] = props;
}

//-----------------------------------------------------------------------
//  initWeights_
//-----------------------------------------------------------------------

void StableMNNMaterial::initWeights_

  ( const String&     fname )

{
  String context ( "StableMNNMaterial::initWeights_" );

  Ref<Model>    encoderModel = Model::   get ( encData_, context );
  Ref<Model>    networkModel = Model::   get ( netData_, context );
  Ref<DofSpace> encoderDofs  = DofSpace::get ( encData_, context );
  Ref<DofSpace> networkDofs  = DofSpace::get ( netData_, context );

  Ref<FileReader> in    = newInstance<FileReader> ( fname );

  idx_t dc = encoderDofs->dofCount();

  Vector wts ( dc );
  wts = 0.0;

  for ( idx_t i = 0; i < dc; ++i )
  {
    wts[i] = in->parseDouble();
  }

  // NB: This guarantees the "two" networks will always share weights
  StateVector::store ( wts, encoderDofs, encData_ );
  StateVector::store ( wts, networkDofs, netData_ );

  encoderModel->takeAction ( LearningActions::UPDATE, Properties(), encData_ );
  networkModel->takeAction ( LearningActions::UPDATE, Properties(), netData_ );
}

//-----------------------------------------------------------------------
//  writeWeights_
//-----------------------------------------------------------------------

void StableMNNMaterial::writeWeights_

  ( const String& fname )

{
  String context ( "StableMNNMaterial::writeWeights_" );

  Ref<PrintWriter> out = newInstance<PrintWriter> (
                    newInstance<FileWriter> ( fname, FileFlags::WRITE ) );

  Ref<DofSpace> encoderDofs  = DofSpace::get ( encData_, context );

  Vector state;
  StateVector::get ( state, encoderDofs, encData_ );

  for ( idx_t i = 0; i < state.size(); ++i )
  {
    *out << state[i] << '\n';
  }
}

//-----------------------------------------------------------------------
//  initNetwork_
//-----------------------------------------------------------------------

void StableMNNMaterial::initNetwork_

  ( const Properties& props,
    const Properties& globdat )

{
  Ref<ChainModule> chain;

  netData_ = Globdat::newInstance ( NETWORK );

  Globdat::getVariables ( netData_ );

  chain = newInstance<ChainModule> ( joinNames ( NETWORK, "chain" ) );

  sgd_ = newInstance<SGDModule> ( "sgd" );

  chain->pushBack ( newInstance<UserconfModule> ( joinNames ( NETWORK, "userinput"   ) ) );
  chain->pushBack ( newInstance<InitModule>     ( joinNames ( NETWORK, "init"        ) ) );

  chain->pushBack ( sgd_ );

  chain->configure ( props,  netData_        );
  chain->getConfig ( conf_,  netData_        );
  chain->init      ( conf_,  props, netData_ );

  network_ = chain;
}

//-----------------------------------------------------------------------
//  initEncoder_
//-----------------------------------------------------------------------

void StableMNNMaterial::initEncoder_

  ( const Properties& props,
    const Properties& globdat )

{
  Ref<ChainModule> chain;

  encData_ = Globdat::newInstance ( "encoder" );

  Properties chainProps = props.getProps ( NETWORK ).clone();
  Properties chainConf;

  Properties modelProps = chainProps.getProps ( "model" );

  StringVector        layers;
  ArrayBuffer<String> buf;

  modelProps.get ( layers, "layers" );

  // Remove unnecessary layers

  for ( idx_t l = 0_idx; l < layers.size(); ++l )
  {
    Properties m  = modelProps.getProps ( layers[l] );

    String type;
    m.get ( type, "type" );

    if ( type != "Dropout" && type != "Material" )
    {
      buf.pushBack ( layers[l] );
    }
  }

  layers.ref ( buf.toArray() );
  modelProps.set ( "layers", layers );

  Globdat::getVariables ( encData_ );

  chain = newInstance<ChainModule> ( "chain" );

  chain->pushBack ( newInstance<InitModule> ( "init" ) );

  chain->configure ( chainProps,             encData_ );
  chain->getConfig ( chainConf,              encData_ );
  chain->init      ( chainConf,  chainProps, encData_ );

  encoder_ = chain;
}

//-----------------------------------------------------------------------
//  updateNetwork_
//-----------------------------------------------------------------------

void StableMNNMaterial::updateNetwork_ 

  ( const IdxVector& points )

{
  System::out() << "Stabilizing " << points.size() << " points\n";

  double fdstep = 1.0e-8;

  String context ( "StableMNNMaterial::updateNetwork" );

  idx_t ninp = features_.size();
  idx_t nout = bmat_[0_idx]->propCount();

  Properties params;

  Ref<Model>    encoderModel = Model::   get ( encData_, context );
  Ref<Model>    networkModel = Model::   get ( netData_, context );
  Ref<DofSpace> encoderDofs  = DofSpace::get ( encData_, context );
  Ref<DofSpace> networkDofs  = DofSpace::get ( netData_, context );

  Vector props  ( nout );

  props = 0.0;

  double crit = 0.;

  for ( idx_t epoch = 0_idx; epoch < nepochs_; ++epoch )
  {
    Flex<Vector> vstrains;
    Flex<idx_t>  vips;

    for ( idx_t p = 0_idx; p < points.size(); ++p )
    {
      idx_t ip = points[p];

      props = getProps_ ( preStrains_(ALL,ip), ip );

      crit = evalCrit_ ( newStrains_(ALL,ip), props, ip ); 

      if ( crit < critThres_ )
      {
	vstrains.pushBack ( newStrains_(ALL,ip).clone() );
	vips.    pushBack ( ip );
      }
    }

    //System::out() << "Epoch " << epoch << " vstrains " << vstrains << '\n';

    idx_t bsize = vstrains.size();

    if ( bsize > 0 )
    {
      //System::out() << "Epoch " << epoch << ": dataset size: " << vips.size() << '\n';

      Ref<NData> data = newInstance<NData> ( bsize, ninp, nout );

      for ( idx_t vp = 0_idx; vp < vstrains.size(); ++vp )
      {
	data->inputs(ALL,vp) = nizer_->normalize ( getFeatures_ ( preStrains_(ALL,vips[vp]), vips[vp] ) );
      }

      params.set ( LearningParams::DATA, data );

      encoderModel->takeAction ( LearningActions::PROPAGATE, params, encData_ );

      // Loss + gradients

      Vector g ( encoderDofs->dofCount() );
      g = 0.0;

      double loss = 0.0;

      for ( idx_t vp = 0_idx; vp < bsize; ++vp )
      {
	Vector grad ( nout );
	grad = 0.0;

	props = data->outputs(ALL,vp);
	loss += -evalCrit_ ( vstrains[vp], props, vips[vp] );

	for ( idx_t mu = 0_idx; mu < nout; ++mu )
	{
	  props = data->outputs(ALL,vp);

	  props[mu] += fdstep;
	  double fcrit = evalCrit_ ( vstrains[vp], props, vips[vp] ); 

	  props[mu] -= 2.0*fdstep;
	  double bcrit = evalCrit_ ( vstrains[vp], props, vips[vp] ); 

	  grad[mu] = - ( fcrit- bcrit ) / 2.0 / fdstep;

	  if ( fcrit > 0.0 || bcrit > 0.0)
	  {
	    System::out() << "Point " << vips[vp] << " oscillating crit sign. Skipping.\n";
	    grad = 0.0;
	    break;
	  }
	}

	data->outputs ( ALL, vp ) = grad;
      }

      //System::out() << "Loss " << loss << '\n';

      params.set ( LearningParams::GRADS, g );
      encoderModel->takeAction ( LearningActions::BACKPROPAGATE, params, encData_ );

      g /= (double)bsize;

      // SGD update

      Vector state;
      StateVector::get ( state, encoderDofs, encData_ );

      //System::out() << "Updating with gradient norm " << jem::numeric::norm2(g) << '\n';

      state -= lrate_ * g;

      encoderModel->takeAction ( LearningActions::UPDATE, params, encData_ );
      networkModel->takeAction ( LearningActions::UPDATE, params, netData_ );

      // Retrain original dataset for one epoch

      if ( (epoch+1) % retrainEvery_ == 0 )
      {
	Properties  info = SolverInfo::get ( netData_ );

	Ref<SGDModule> sgd = dynamicCast<SGDModule> ( sgd_ );
	sgd->solveBatch ( info, netData_ );
      }

      networkModel->takeAction ( LearningActions::UPDATE, params, netData_ );
      encoderModel->takeAction ( LearningActions::UPDATE, params, encData_ );

      //System::out() << "State after " << state << '\n';

      //writeWeights_( "stabilized.net" );
    }
  }

  //for ( idx_t p = 0_idx; p < points.size(); ++p )
  //{
  //  updateHist_ ( points[p] );
  //}
}

//-----------------------------------------------------------------------
//  updateHist_
//-----------------------------------------------------------------------

void StableMNNMaterial::updateHist_

  ( const idx_t point )

{
  const idx_t STRAIN_COUNTS[4] = { 0, 1, 3, 6 };

  idx_t size = STRAIN_COUNTS[rank_];

  Vector strain ( size );
  Vector stress ( size );
  Matrix stiff  ( size, size );
  strain = 0.0;stress = 0.0; stiff = 0.0;

  mat_[point] = newMaterial ( MATERIAL, Properties(), props_, globdat_ );
  mat_[point]->configure ( props_.findProps ( MATERIAL ), globdat_ );
  mat_[point]->createIntPoints ( 1 );

  ext_[point] = newMaterial ( FEATUREEXTRACTOR, Properties(), props_, globdat_ );
  ext_[point]->configure ( props_.findProps ( FEATUREEXTRACTOR ), globdat_ );
  ext_[point]->createIntPoints ( 1 );

  bmat_[point] = dynamicCast<Bayesian> ( mat_[point] );
  bext_[point] = dynamicCast<Bayesian> ( ext_[point] );

  bmat_[point]->setProps ( getProps_ ( strain, point ) );

  for ( idx_t t = 0_idx; t < strains_.size(); ++t )
  {
    strain = strains_[t](ALL,point);

    mat_[point] ->update ( stiff, stress, strain, 0_idx );
    
    bmat_[point]->setProps ( getProps_ ( strain, point ) );

    mat_[point] ->commit ( );
    ext_[point] ->commit ( );
  }
}

//-----------------------------------------------------------------------
//  permutate_
//-----------------------------------------------------------------------

Vector StableMNNMaterial::permutate_

  ( const Vector& strain )

{
  Vector permutated ( strain.size() );

  permutated = 0.0;
  
  if ( perm23_ )
  {
    permutated[perm_] = strain;
  }
  else
  {
    permutated = strain;
  }

  return permutated;
}

//-----------------------------------------------------------------------
//  updateProps_
//-----------------------------------------------------------------------

void StableMNNMaterial::updateProps_ 

  ( const Matrix& strains )

{
  idx_t nips = strains.size();
  idx_t ninp = features_.size();
  idx_t nout = bmat_[0]->propCount();

  Properties params;

  String context ( "StableMNNMaterial::commit" );
  
  Ref<Model> model = Model::get ( encData_, context );

  Ref<NData> data = newInstance<NData> ( nips, ninp, nout );

  for ( idx_t p = 0_idx; p < nips; ++p )
  {
    Vector features = getFeatures_ ( strains(ALL,p), p );

    data->inputs(ALL,p) = nizer_->normalize ( features );

    //System::out() << "Probing network with inputs " << features << '\n';
  }

  params.set ( LearningParams::DATA, data );

  model->takeAction ( LearningActions::PROPAGATE, params, encData_ );

  for ( idx_t p = 0_idx; p < nips; ++p )
  {
    bmat_[p]->setProps ( data->outputs(ALL,p) );
  }
}

//-----------------------------------------------------------------------
//  evalCrit_
//-----------------------------------------------------------------------

double StableMNNMaterial::evalCrit_

  ( const Matrix& stiff )

{
  const idx_t STRAIN_COUNTS[4] = { 0, 1, 3, 6 };

  idx_t size = STRAIN_COUNTS[rank_];

  String context ( "StableMNNMaterial::evalCrit_" );
  
  if ( crit_ == DET )
  {
    Vector    scale  ( size );
    IdxVector iperm  ( size );
    int       sign;

    scale = 0.0; iperm = 0;

    if ( !LUSolver::factor ( stiff, scale, iperm, sign ) )
    {
      throw Error ( JEM_FUNC, "Singular stiffness matrix" );
    }

    if ( initCrit_ == 0.0 )
    {
      initCrit_ = sign*LUSolver::det ( stiff );
    }

    return sign*LUSolver::det ( stiff ) / initCrit_;
  }
  else if ( crit_ == DIAG )
  {
    double min = jem::maxOf(min);

    for ( idx_t i = 0_idx; i < size; ++ i )
    {
      if ( stiff(i,i) < min )
      {
	min = stiff(i,i);
      }
    }

    if ( initCrit_ == 0.0 )
    {
      initCrit_ = min;
    }

    return min / initCrit_;
  }
  else if ( crit_ == ACOUSTIC )
  {
    if ( rank_ != 2 )
    {
      throw Error ( JEM_FUNC, 
        "evalCrit_: Acoustic tensor implemented only for 2D" );
    }
    const double pi = 3.14159265358979323846;

    double min      = jem::maxOf(min);

    idx_t na = 100;
    Vector normal ( 2 );

    for ( idx_t a = 0_idx; a < na; ++a )
    {
      double angle = -0.5 * pi + (double)a * pi / (double)na;

      double n1 = std::cos ( angle );
      double n2 = std::sin ( angle );

      double det = (n1*n1*stiff(0,0)+n1*n2*(stiff(0,2)+stiff(2,0))+n2*n2*stiff(2,2)) *
                   (n1*n1*stiff(2,2)+n1*n2*(stiff(1,2)+stiff(2,1))+n2*n2*stiff(1,1)) -
                   (n1*n1*stiff(0,2)+n1*n2*(stiff(0,1)+stiff(2,2))+n2*n2*stiff(2,1)) *
                   (n1*n1*stiff(2,0)+n1*n2*(stiff(1,0)+stiff(2,2))+n2*n2*stiff(1,2));

      if ( det < min )
      {
        min = det;
      }
    }

    if ( initCrit_ == 0.0 )
    {
      initCrit_ = min;
    }

    return min / initCrit_;
  }
  else
  {
    throw Error ( JEM_FUNC, "evalCrit_: Unknown stability criterium" );
  }
}

//-----------------------------------------------------------------------
//  evalCrit_
//-----------------------------------------------------------------------

double StableMNNMaterial::evalCrit_

  ( const Vector& strain,
    const Vector& props,
    const idx_t   p       )

{
  const idx_t STRAIN_COUNTS[4] = { 0, 1, 3, 6 };

  idx_t size = STRAIN_COUNTS[rank_];

  String context ( "StableMNNMaterial::commit" );
  
  Vector    stress ( size );
  Matrix    stiff  ( size, size );

  stress = 0.0; stiff = 0.0; 

  bmat_[p]->setProps ( props );

  mat_[p]->update ( stiff, stress, strain, 0_idx );

  return evalCrit_ ( stiff );
}

//-----------------------------------------------------------------------
//  getProps_
//-----------------------------------------------------------------------

Vector StableMNNMaterial::getProps_

  ( const Vector& strain,
    const idx_t   p        )

{
  Properties params;
  Ref<Model>  model = Model::get ( encData_, "getProps_" );

  Vector features = getFeatures_ ( strain, p );

  Ref<NData> data = newInstance<NData> ( 1, features.size(), bmat_[p]->propCount() );

  data->inputs(ALL,0) = nizer_->normalize ( features );

  params.set ( LearningParams::DATA, data );

  model->takeAction ( LearningActions::PROPAGATE, params, encData_ );

  return data->outputs(ALL,0);
}

//-----------------------------------------------------------------------
//  getFeatures_
//-----------------------------------------------------------------------

Vector StableMNNMaterial::getFeatures_

  ( const Vector& strain,
    const idx_t   p        )

{
  const idx_t STRAIN_COUNTS[4] = { 0, 1, 3, 6 };

  idx_t size = STRAIN_COUNTS[rank_];

  Vector    stress ( size );
  Matrix    stiff  ( size, size );

  stress = 0.0; stiff = 0.0;
  ext_[p]->update ( stiff, stress, permutate_ ( strain ), 0_idx );

  //System::out() << "Returning features " << bext_[p]->getFeatures ( 0_idx )[features_] << '\n';
  return Vector ( bext_[p]->getFeatures ( 0_idx )[features_] );
}
