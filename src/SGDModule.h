/*
 *  TU Delft 
 *
 *  Iuri Rocha, Nov 2021
 *
 *  Simple SGD with fixed learning rate
 *
 */

#ifndef JIVE_IMPLICT_SGDMODULE_H
#define JIVE_IMPLICT_SGDMODULE_H

#include <jem/util/Flex.h>
#include <jem/util/Timer.h>
#include <jive/implict/SolverModule.h>
#include <jive/model/Model.h>
#include <jive/fem/typedefs.h>

#include "NeuralUtils.h"

using jive::model::Model;
using jem::util::Timer;
using jem::util::Flex;
using jive::MPContext;

using NeuralUtils::LossFunc;
using NeuralUtils::LossGrad;

JIVE_BEGIN_PACKAGE( implict )


//-----------------------------------------------------------------------
//   class SGDModule
//-----------------------------------------------------------------------


class SGDModule : public SolverModule
{
 public:

  JEM_DECLARE_CLASS       ( SGDModule, SolverModule );

  static const char*        TYPE_NAME;
  static const char*        SEED;
  static const char*        LEARNING_RATE;
  static const char*        MINIBATCH;  
  static const char*        LOSSFUNC;   
  static const char*        VALSPLIT;
  static const char*        SKIPFIRST;

  explicit                  SGDModule

    ( const String&           name = "sgd" );

  virtual Status            init

    ( const Properties&       conf,
      const Properties&       props,
      const Properties&       globdat );

  virtual void              shutdown

    ( const Properties&       globdat );

  virtual void              configure

    ( const Properties&       props,
      const Properties&       globdat );

  virtual void              getConfig

    ( const Properties&       conf,
      const Properties&       globdat )      const;

  virtual void              advance

    ( const Properties&       globdat );

  virtual void              solve

    ( const Properties&       info,
      const Properties&       globdat );

  void                      solveBatch

    ( const Properties&       info,
      const Properties&       globdat    );

  double                    getValiError

    ( const Properties&       globdat    );

  virtual void              cancel

    ( const Properties&       globdat );

  virtual bool              commit

    ( const Properties&       globdat );

  virtual void              setPrecision

    ( double                  eps );

  virtual double            getPrecision  () const;

  static Ref<Module>        makeNew

    ( const String&           name,
      const Properties&       conf,
      const Properties&       props,
      const Properties&       globdat );

  static void               declare       ();

 protected:

  virtual                  ~SGDModule  ();

  void                      mpSolve_

    ( const Properties&       info,
      const Properties&       globdat    );

  void                      solve_

    ( const Properties&       info,
      const Properties&       globdat    );

  double                    eval_

    ( const IdxVector&        samples,
      const bool              dograds,
      const Properties&       globdat    );

 private:

  idx_t                     seed_;
  idx_t                     batchSize_;
  idx_t                     epoch_;
  idx_t                     iiter_;
  idx_t                     threads_;
  idx_t                     skipFirst_;

  double                    alpha_;
  double                    beta1_;
  double                    beta2_;
  double                    eps_;
  double                    lambda_;
  double                    precision_;
  double                    valSplit_;

  double                    r_;

  Vector                    g_;
  Vector                    gt_;
  Vector                    rg_;
  Vector                    m_;
  Vector                    v_;
  Vector                    m0_;
  Vector                    v0_;

  String                    lossName_;
  LossFunc                  func_;
  LossGrad                  grad_;

  Ref<MPContext>            mpx_;
  bool                      mpi_;

  Timer                     total_;
  Timer                     t1_;
  Timer                     t2_;
  Timer                     t3_;
  Timer                     t4_;
  Timer                     t5_;
  Timer                     t6_;

  double                    jprop_;
};


JIVE_END_PACKAGE( implict )

#endif
