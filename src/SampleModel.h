/*
 * Copyright (C) 2019 TU Delft. All rights reserved.
 *
 * A model shell for SampleModule. Use this to write non-
 * converged data. Writing only happens when someone
 * calls the SolverNames::SUBSAMPLE action. This should go
 * at the end of the model list.
 *
 * Author: Iuri Barcelos, i.barceloscarneiromrocha@tudelft.nl
 * Date:   Sep 2019
 * 
 */

#ifndef SAMPLE_MODEL_H 
#define SAMPLE_MODEL_H

#include <jive/model/Model.h>
#include <jive/app/SampleModule.h>

using namespace jem;

using jive::model::Model;
using jem::util::Properties;
using jive::app::SampleModule;

class SampleModel : public Model
{
 public:

                     SampleModel

    ( const String&     name,
      const Properties& conf,
      const Properties& props,
      const Properties& globdat );

  virtual void       configure

    ( const Properties& props,
      const Properties& globdat );

  virtual void       getConfig

    ( const Properties& conf,
      const Properties& globdat )          const;

  virtual bool       takeAction

    ( const String&     action,
      const Properties& params,
      const Properties& globdat );

  static Ref<Model>  makeNew

    ( const String&     name,
      const Properties& conf,
      const Properties& props,
      const Properties& globdat );

 protected:
  
  virtual           ~SampleModel ();

 private:

  Ref<SampleModule>   sampler_;
};

#endif
