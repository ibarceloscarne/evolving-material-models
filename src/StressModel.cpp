/*
 *  TU Delft / Knowledge Centre WMC
 *
 *  Simple class for stress analysis.
 *
 *  Author: Iuri Barcelos, i.barcelos@wmc.eu
 *  Date: August 2015
 *
 *  Modified: Iuri Barcelos, i.barcelos@wmc.eu
 *  Date: October 2015
 *  Incorporation of MacroModel and MicroModel routines
 *  written by Frans van der Meer, Erik Jan Lingen and
 *  Vinh Phu Nguyen. The two-scale homogenization model
 *  can then be used by specifying MacroMicroInterface
 *  as the material for the macroscopic model. Likewise,
 *  the same model will be used in the microscale with
 *  a conventional material. This adaptation was performed
 *  in order to make the fe2 model compatible with existing
 *  implementation of the MultiPhysicsModule. Please refer
 *  to the original implementations of MacroModel and
 *  MicroModel for more details.
 *
 *  Modified: Iuri Barcelos, February 2016
 *
 *  Implementation of characteristic length calculation
 *  for crack band method. In this implementation, the
 *  calculated lengths are kept in the material object
 *  in order to avoid overloading the update and
 *  stressAtPoint functions.
 *
 *  Modified: Iuri Barcelos, March 2018
 *
 *  Code cleanup after implementation of POD and ECM
 *
 *  Modified: Iuri Barcelos, August 2018
 *
 *  Further cleaning for adaptive ROM. 
 *
 *  Modified: Iuri Barcelos, November 2018
 *
 *  Added tools for domain-based adaptive ECM
 *
 *  Modified: Iuri Barcelos, January 2020
 *  
 *  Support for bar models with varying section area
 *
 */

#include <jem/base/System.h>
#include <jem/base/Error.h>
#include <jem/base/Array.h>
#include <jem/base/array/tensor.h>
#include <jem/base/IllegalInputException.h>
#include <jem/numeric/algebra/matmul.h>
#include <jem/numeric/algebra/MatmulChain.h>
#include <jem/numeric/func/UserFunc.h>
#include <jem/io/Writer.h>
#include <jem/io/PrintWriter.h>
#include <jem/io/FileWriter.h>
#include <jem/io/FileReader.h>
#include <jem/io/StdoutWriter.h>
#include <jem/io/BufferedWriter.h>
#include <jem/io/FileFlags.h>
#include <jem/io/PatternLogger.h>
#include <jem/util/StringUtils.h>
#include <jem/util/Dictionary.h>
#include <jem/util/ArrayBuffer.h>
#include <jem/util/PropertyException.h>
#include <jem/mt/ThreadSafeWriter.h>
#include <jive/util/Globdat.h>
#include <jive/util/Table.h>
#include <jive/util/XItemGroup.h>
#include <jive/model/Actions.h>
#include <jive/model/StateVector.h>
#include <jive/model/ModelFactory.h>
#include <jive/geom/IShapeFactory.h>
#include <jive/Array.h>
#include <jive/algebra/AbstractMatrix.h>
#include <pthread.h>

#include "declare.h"
#include "StressModel.h"
#include "Material.h"
#include "SolverNames.h"
#include "Plasticity.h"
#include "Bayesian.h"
#include "Historic.h"
#include "LearningNames.h"

using jem::Lock;
using jem::numeric::matmul;
using jem::numeric::MatmulChain;
using jem::numeric::UserFunc;
using jem::io::Writer;
using jem::io::PrintWriter;
using jem::io::FileWriter;
using jem::io::FileReader;
using namespace jem::io;
using jem::util::StringUtils;
using jem::util::Dict;
using jem::util::DictEnum;
using jem::util::ArrayBuffer;
using jem::util::PropertyException;
using namespace jem::mt;
using jive::Matrix;
using jive::Cubix;
using jive::model::StateVector;
using jive::model::ActionParams;
using jive::algebra::AbstractMatrix;
using jem::io::endl;
using jive::StringVector;
using jive::util::Table;
using jive::util::ItemGroup;
using jive::util::XItemGroup;
using jem::io::Logger;
using jem::io::PatternLogger;
using jive::util::joinNames;

typedef Array<idx_t,2> IdxMatrix;

//=======================================================================
//   class StressModel
//=======================================================================

//-----------------------------------------------------------------------
//   static data
//-----------------------------------------------------------------------

const char*  StressModel::DOF_TYPE_NAMES[3]  = {"dx", "dy", "dz"};
const char*  StressModel::SHAPE_PROP         = "shape";
const char*  StressModel::MATERIAL_PROP      = "material";
const char*  StressModel::THICKNESS_PROP     = "thickness";
const char*  StressModel::WRITESTATE_PROP    = "writeState";
const char*  StressModel::WRITESTRAINS_PROP  = "writeStrains";
const char*  StressModel::WRITESTRESSES_PROP = "writeStresses";
const char*  StressModel::WRITEIPCOORDS_PROP = "writeIpCoords";

//-----------------------------------------------------------------------
//   constructor & destructor
//-----------------------------------------------------------------------

StressModel::StressModel

  ( const String&     name,
    const Properties& conf,
    const Properties& props,
    const Properties& globdat ) :

    Model ( name )

{
  // Data initialization.

  thickness_  = 1.;

  writeState_    = false;
  writeStrains_  = false;
  writeStresses_ = false;
  thickFunc_     = nullptr;


  using jive::geom::IShapeFactory;

  const idx_t STRAIN_COUNTS[4] = { 0, 1, 3, 6 };

  // Get myTag

  StringVector names ( StringUtils::split( myName_, '.' ) );
  myTag_     = names [ names.size() - 1 ];

  // Get the properties for this object.

  Properties myProps = props.getProps ( myName_ );
  Properties myConf  = conf.makeProps ( myName_ );

  props_ = myProps;

  // Gather necessary data.

  egroup_ = ElementGroup::get ( myConf, myProps, globdat, getContext( ) );

  IdxVector ielems = egroup_.getIndices ( );

  elems_   = egroup_.getElements ( );
  nodes_   = elems_.getNodes ( );
  rank_    = nodes_.rank ( );
  numElem_ = ielems.size ( ); 

  // Check element rank.

  if ( rank_ < 1 || rank_ > 3 )
    throw IllegalInputException (
      getContext( ),
      String::format (
        "Invalid node rank: %d (should be 1, 2 or 3)", rank_ ) );

  // Create the internal shapes.

  shape_ = IShapeFactory::newInstance
  ( joinNames ( myName_, SHAPE_PROP ), conf, props );

  nodeCount_ = shape_->nodeCount ( );
  ipCount_   = shape_->ipointCount ( );

  // Check shape rank.

  if ( shape_->globalRank( ) != rank_ )
    throw IllegalInputException (
      getContext ( ),
      String::format (
        "Shape has invalid rank: %d (should be %d)",
	shape_->globalRank ( ), rank_ ) );

  elems_.checkSomeElements (
    getContext( ), ielems, nodeCount_ );

  // Initialize the DOFs.

  initDofs_ ( globdat );

  // Initialize the material.

  material_ = newMaterial ( MATERIAL_PROP, myConf, myProps, globdat );

  strCount_ = STRAIN_COUNTS[rank_];

  if ( rank_ < 3 )
  {
    try
    {
      myProps.find ( thickness_, THICKNESS_PROP );
    }
    catch ( const PropertyException& ex )
    {
      String func, vars; 

      myProps.get ( func, THICKNESS_PROP );

      if ( rank_ == 1 )
      {
	vars = "x";
      }
      else
      {
        vars = "x,y";
      }

      thickFunc_ = newInstance<UserFunc> ( vars, func );
    }

    myConf.set   ( THICKNESS_PROP, thickness_ );
  }

  // Print IP coords, if needed 

  bool writeIps = false;

  myProps.find ( writeIps, WRITEIPCOORDS_PROP );

  if ( writeIps )
  {
    printIps_ ( Properties(), globdat );
  }
}

StressModel::~StressModel()
{}

//-----------------------------------------------------------------------
//   configure
//-----------------------------------------------------------------------

void StressModel::configure

  ( const Properties& props,
    const Properties& globdat )

{
  Properties myProps  = props.findProps   ( myName_ );
  Properties matProps = myProps.findProps ( MATERIAL_PROP );

  material_->configure ( matProps, globdat );

  material_->createIntPoints ( ipCount_ * numElem_ );

  if ( material_->hasCrackBand ( ) )
    initCharLength_ ( );

  myProps.find ( writeState_,    WRITESTATE_PROP    );
  myProps.find ( writeStrains_,  WRITESTRAINS_PROP  );
  myProps.find ( writeStresses_, WRITESTRESSES_PROP );

  if ( writeStrains_ )
  {
    epss_.resize ( ipCount_*numElem_, strCount_ );
    epss_ = 0.0;
  }

  if ( writeStresses_ )
  {
    sigs_.resize ( ipCount_*numElem_, strCount_ );
    sigs_ = 0.0;
  }

  if ( writeStrains_ || writeStresses_ )
  {
    ipcoords_.resize ( rank_, ipCount_*numElem_ );
    ipcoords_ = 0.0;
  }
}

//-----------------------------------------------------------------------
//   getConfig
//-----------------------------------------------------------------------

void StressModel::getConfig

  ( const Properties& conf,
    const Properties& globdat ) const

{
  Properties myConf  = conf.makeProps ( myName_ );
  Properties matConf = myConf.makeProps ( MATERIAL_PROP );

  material_->getConfig ( matConf, globdat );

  myConf.set ( WRITESTATE_PROP, writeState_ );
}

//-----------------------------------------------------------------------
//   takeAction
//-----------------------------------------------------------------------

bool StressModel::takeAction

  ( const String&     action,
    const Properties& params,
    const Properties& globdat )

{
  using jive::model::Actions;

  // TEMP
  if ( action == "getMaterialObj" )
  {
    
    if ( ipCount_ * numElem_ != 1 )
    {
      throw Error ( JEM_FUNC, "Expected a single integration point" );
    }
    params.set ( "materialObj", material_ );
    return true;
  }

  if ( action == LearningActions::GETFEATURES )
  {
    Ref<Bayesian> bay = dynamicCast<Bayesian> ( material_ );

    if ( bay == nullptr ) return false;

    Vector features;

    params.find ( features, LearningParams::FEATURES );

    Vector myfeats = getFeatures_ ();  
    //System::out() << "myfeats = " << myfeats << '\n';

    idx_t oldsize = features.size();

    features.reshape ( oldsize + myfeats.size() );

    features[slice(oldsize,END)] = myfeats;

    params.set ( LearningParams::FEATURES, features );

    return true;
  }

  if ( action == Actions::GET_MATRIX0 )
  {
    // Assemble the global stiffness matrix and the internal vector.

    Ref<MatrixBuilder> mbld;
    Vector fint;

    params.get  ( mbld, ActionParams::MATRIX0 );
    params.get  ( fint, ActionParams::INT_VECTOR );

    getMatrix_ ( fint, mbld, globdat );

    return true;
  }

  if ( action == Actions::GET_INT_VECTOR )
  {
    Vector fint;

    params.get ( fint, ActionParams::INT_VECTOR );

    getMatrix_ ( fint, nullptr, globdat );

    return true;
  }

  if ( action == Actions::GET_TABLE )
  {
    return getTable_ ( params, globdat );
  }

  if ( action == Actions::COMMIT )
  {
    material_->commit ( );

    return true;
  }

  if ( action == SolverNames::GET_MAT_STATE )
  {
    idx_t lcount = 0;
    idx_t ucount = 0;

    bool  inelastic = false;

    params.find ( inelastic, SolverNames::INELASTIC      );
    params.find ( lcount,    SolverNames::LOADINGCOUNT   );
    params.find ( ucount,    SolverNames::UNLOADINGCOUNT );

    for ( idx_t ip = 0; ip < numElem_*ipCount_; ++ip )
    {
      if ( !material_->isInelastic ( ip ) )
      {
        continue;
      }

      inelastic = true;

      if ( material_->wasLoading ( ip ) )
      {
        lcount++;
      }
      else
      {
        ucount++;
      }
    }

    params.set ( SolverNames::INELASTIC,      inelastic );
    params.set ( SolverNames::LOADINGCOUNT,   lcount    );
    params.set ( SolverNames::UNLOADINGCOUNT, ucount    );

    return true;
  }

  if ( action == Actions::CANCEL )
  {
    material_->cancel ( );

    return true;
  }

  if ( action == "WRITE_XOUTPUT" )
  {
    if ( writeState_ && 
         stateWriter_.amFirstWriter ( this ) )
    {
      printState_ ( params, globdat );
    }

    if ( writeStrains_ &&
         epsWriter_.amFirstWriter ( this ) )
    {
      printStrains_ ( params, globdat );
    }

    if ( writeStresses_ &&
         sigWriter_.amFirstWriter ( this ) )
    {
      printStresses_ ( params, globdat );
    }

    return true;
  }

  if ( action == "GET_DISSIPATION" )
  {
    getDissipation_ ( params );

    return true;
  }

  if ( action == SolverNames::GET_DISS_FORCE )
  {
    Ref<Plasticity> p = dynamicCast<Plasticity> ( material_ );

    if ( p == nullptr ) return false;

    Vector disp;
    Vector fDiss;

    StateVector::getOld ( disp, dofs_, globdat );

    globdat.get ( fDiss, SolverNames::DISSIPATION_FORCE );

    getDissForce_ ( fDiss, disp );

    return true;
  }

  if ( action == "DESPAIR" )
  {
    return material_->despair();
  }

  if ( action == "END_DESPAIR" )
  {
    material_->endDespair();

    return true;
  }

  if ( action == SolverNames::CHECK_COMMIT )
  {
    material_->checkCommit ( params );

    return true;
  }

  return false;
}

//-----------------------------------------------------------------------
//   initDofs_
//-----------------------------------------------------------------------

void StressModel::initDofs_

  ( const Properties& globdat )

{
  dofCount_ = rank_ * nodeCount_;

  dofs_ = XDofSpace::get ( nodes_.getData( ), globdat );

  dofTypes_.resize ( rank_ );

  for ( idx_t i = 0; i < rank_; i++ )
    dofTypes_[i] = dofs_->addType ( DOF_TYPE_NAMES[i] );

  IdxVector ielems = egroup_.getIndices ( );
 
  dofs_->addDofs ( elems_.getUniqueNodesOf ( ielems ), dofTypes_ );
}

//-----------------------------------------------------------------------
//   getMatrix_
//-----------------------------------------------------------------------

void StressModel::getMatrix_

  ( const Vector&      fint,
    Ref<MatrixBuilder> mbld,
    const Properties&  globdat )

{
  IdxVector inodes    ( nodeCount_ );
  IdxVector idofs     ( dofCount_ );
  Cubix     grads     ( rank_, nodeCount_, ipCount_ );
  Matrix    coords    ( rank_, nodeCount_ );
  Matrix    ipCoords  ( rank_, ipCount_ );
  Matrix    elmat     ( dofCount_, dofCount_ );
  Matrix    ptstiff   ( strCount_, strCount_ );
  Matrix    b         ( strCount_, dofCount_ );
  Matrix    bt        ( dofCount_, strCount_ );
  Vector    ptstress  ( strCount_ );
  Vector    ptstrain  ( strCount_ );
  Vector    ptvec     ( dofCount_ );
  Vector    elvec     ( dofCount_ );
  Vector    elstate   ( dofCount_ );
  Vector    ipWeights ( ipCount_ );
  Vector    state;

  IdxVector ielems = egroup_.getIndices ( );

  MatmulChain<double,3> mchain;

  idx_t     ipoint = 0;

  // Get current solution.

  StateVector::get ( state, dofs_, globdat );

  // Iterate over all elements.

  for ( idx_t ie = 0; ie < numElem_; ie++ )
  {
    idx_t ielem = ielems[ie];

    elems_.getElemNodes  ( inodes, ielem );
    nodes_.getSomeCoords ( coords, inodes );

    dofs_->getDofIndices ( idofs, inodes, dofTypes_ );

    shape_->getShapeGradients ( grads, ipWeights, coords );

    if ( thickFunc_ != nullptr )
    {
      shape_->getGlobalIntegrationPoints ( ipCoords, coords );

      for ( idx_t p = 0; p < ipCount_; ++p )
      {
        if ( rank_ == 1 )
	{
          ipWeights[p] *= thickFunc_->eval ( ipCoords(0,p) );
	}
	else
	{
          ipWeights[p] *= thickFunc_->eval ( ipCoords(0,p), ipCoords(1,p) );
	}
      }
    }
    else
    {
      ipWeights *= thickness_;
    }

    // Get current solution for this element.

    elstate = 0.0;
    elstate = select ( state, idofs );

    // Iterate over the element integration points.

    elmat   = 0.0;
    elvec   = 0.0;
    ptvec   = 0.0;

    if ( writeStrains_ || writeStresses_ )
    {
      shape_->getGlobalIntegrationPoints ( ipCoords, coords );

      ipcoords_(ALL,slice(ipoint,ipoint+ipCount_)) = ipCoords;
    }

    for ( idx_t ip = 0; ip < ipCount_; ip++ )
    {
      // Get shape function gradients and their transpose.

      getShapeGrads_ ( b, grads( ALL,ALL,ip ) );

      bt = b.transpose( );

      // Get the strains at the integration point.

      matmul ( ptstrain, b, elstate );

      // Get material stiffness and stresses at the integration point.

      material_->update ( ptstiff, ptstress, ptstrain, ipoint++ );

      if ( writeStrains_ )
      {
        epss_(ipoint-1,ALL) = ptstrain;
      }

      if ( writeStresses_ )
      {
        sigs_(ipoint-1,ALL) = ptstress;
      }

      // Add the integration point contribution to the force vector.

      Vector ptvec = matmul ( bt, ptstress );

      elvec += ipWeights[ip] * ptvec;

      // Add the integration point contribution to the stiffness matrix.

      elmat += ipWeights[ip] * mchain.matmul ( bt, ptstiff, b );
    }

    // Add contribution of current element to global stiffness matrix.

    if ( mbld != nullptr )
      mbld->addBlock ( idofs, idofs, elmat );

    // Add contribution of current element to global force vector.
    
    select ( fint, idofs ) += elvec;
  }
}

//-----------------------------------------------------------------------
//   getDissForce_
//-----------------------------------------------------------------------

void StressModel::getDissForce_

  ( const Vector&  fDiss,
    const Vector&  disp ) 
{
  IdxVector inodes    ( nodeCount_ );
  IdxVector idofs     ( dofCount_ );
  Cubix     grads     ( rank_, nodeCount_, ipCount_ );
  Matrix    coords    ( rank_, nodeCount_ );
  Matrix    ipCoords  ( rank_, ipCount_ );
  Matrix    b         ( strCount_, dofCount_ );
  Matrix    bt        = b.transpose (); 
  Vector    ptsstar   ( strCount_ );
  Vector    ptstrain  ( strCount_ );
  Vector    elvec     ( dofCount_ );
  Vector    elstate   ( dofCount_ );
  Vector    ipWeights ( ipCount_ );

  IdxVector ielems = egroup_.getIndices ( );

  Ref<Plasticity> p = dynamicCast<Plasticity> ( material_ );

  idx_t ipoint = 0;

  for ( idx_t ie = 0; ie < numElem_; ++ie )
  {
    idx_t ielem = ielems[ie];

    elems_.getElemNodes  ( inodes, ielem );
    nodes_.getSomeCoords ( coords, inodes );

    dofs_->getDofIndices ( idofs, inodes, dofTypes_ );

    shape_->getShapeGradients ( grads, ipWeights, coords );

    if ( thickFunc_ != nullptr )
    {
      shape_->getGlobalIntegrationPoints ( ipCoords, coords );

      for ( idx_t p = 0; p < ipCount_; ++p )
      {
	if ( rank_ == 1 )
	{
	  ipWeights[p] *= thickFunc_->eval ( ipCoords(0,p) );
	}
	else
	{
	  ipWeights[p] *= thickFunc_->eval ( ipCoords(0,p), ipCoords(1,p) );
	}
      }
    }
    else
    {
      ipWeights *= thickness_;
    }

    // Get current solution for this element.

    elstate = 0.0;
    elstate = select ( disp, idofs );

    // Iterate over the element integration points.

    elvec   = 0.0;

    for ( idx_t ip = 0; ip < ipCount_; ip++ )
    {
      // Get shape function gradients and their transpose.

      getShapeGrads_ ( b, grads( ALL,ALL,ip ) );

      // Get the strains at the integration point.

      matmul ( ptstrain, b, elstate );

      // Get material stiffness and stresses at the integration point.
      p->getDissipationStress ( ptsstar, ptstrain, ipoint++ );

      // Add the integration point contribution to the force vector.

      elvec += ipWeights[ip] * matmul ( bt, ptsstar );
    }

    // Add the element force to the global vector.

    select ( fDiss, idofs ) += elvec;
  }
}

//-----------------------------------------------------------------------
//   getTable_
//-----------------------------------------------------------------------

bool StressModel::getTable_

  ( const Properties&  params,
    const Properties&  globdat )

{
  Ref<XTable> table;
  String      name;

  // Get table and its name.

  params.get ( name,  ActionParams::TABLE_NAME );
  params.get ( table, ActionParams::TABLE );

  // Check if the requested table is supported.

  if ( name == "nodalStress" && table->getRowItems ( ) == nodes_.getData ( ) )
  {
    // Get table weights.

    Vector weights;

    params.get ( weights, ActionParams::TABLE_WEIGHTS );

    printNodalStresses_ ( *table, name, weights, globdat );

    return true;
  }

  if ( name == "nodalHistory" && table->getRowItems ( ) == nodes_.getData ( ) )
  {
    // Get table weights.

    Vector weights;

    params.get ( weights, ActionParams::TABLE_WEIGHTS );

    printNodalHistory_ ( *table, name, weights, globdat );
    
    return true;
  }

  return false;
}

//-----------------------------------------------------------------------
//   printNodalStresses_
//-----------------------------------------------------------------------

void StressModel::printNodalStresses_

  ( XTable&            table,
    const String&      tablename,
    const Vector&      weights,
    const Properties&  globdat )

{

  IdxVector      inodes    ( nodeCount_ );
  IdxVector      idofs     ( dofCount_ );
  Cubix          grads     ( rank_, nodeCount_, ipCount_ );
  Matrix         coords    ( rank_, nodeCount_ );
  Matrix         b         ( strCount_, dofCount_ );
  Matrix         ndStress;
  Vector         elstate   ( dofCount_ );
  Vector         ipWeights ( ipCount_ );
  Vector         ndWeights ( nodeCount_ );
  Vector         ipStrain  ( strCount_ );
  Vector         state;
  Vector         ipStress;
  IdxVector      jcols;

  IdxVector      ielems = egroup_.getIndices ( );

  idx_t          ipoint = 0;   

  // Add columns to the table depending on the material.

  material_->addTableColumns ( jcols, table, tablename );

  ipStress.resize ( jcols.size( ) );
  ndStress.resize ( nodeCount_, jcols.size( ) );

  // Get current displacement field.

  StateVector::get ( state, dofs_, globdat );

  // Iterate over all elements.

  for ( idx_t ie = 0; ie < numElem_; ie++ )
  {
    // Get the index of current element.

    idx_t ielem = ielems[ie];

    // Gather necessary data.

    ndStress  = 0.0;
    ndWeights = 0.0;

    elems_.getElemNodes  ( inodes, ielem );
    nodes_.getSomeCoords ( coords, inodes );
    dofs_->getDofIndices ( idofs, inodes, dofTypes_ );

    shape_->getShapeGradients ( grads, ipWeights, coords );

    // Get displacement field for this element.

    elstate = select ( state, idofs );

    // Get shape functions.

    Matrix sfuncs = shape_->getShapeFunctions ( );

    // Iterate over integration points of this element.

    for ( idx_t ip = 0; ip < ipCount_; ip++ )
    {
      ipStrain = 0.0;
      ipStress = 0.0;

      getShapeGrads_ ( b, grads( ALL,ALL,ip ) );

      matmul ( ipStrain, b, elstate );

      material_->stressAtPoint ( ipStress, ipStrain, ipoint++ );

      ndStress += matmul ( sfuncs( ALL, ip ), ipStress );
      
      ndWeights += sfuncs ( ALL, ip );
    }
    
    select ( weights, inodes ) += ndWeights;

    // Add block to the table.

    table.addBlock ( inodes, jcols, ndStress );
  }
}

//-----------------------------------------------------------------------
//   printNodalHistory_
//-----------------------------------------------------------------------

void StressModel::printNodalHistory_

  ( XTable&            table,
    const String&      tablename,
    const Vector&      weights,
    const Properties&  globdat )

{

  IdxVector      inodes    ( nodeCount_ );
  Matrix         ndHist;
  Vector         ipHist;
  Vector         ndWeights ( nodeCount_ );
  IdxVector      jcols;

  IdxVector      ielems = egroup_.getIndices ( );

  idx_t          ipoint = 0;   

  // Add columns to the table depending on the material.

  material_->addTableColumns ( jcols, table, tablename );

  ipHist.resize ( jcols.size( ) );
  ndHist.resize ( nodeCount_, jcols.size( ) );

  // Iterate over all elements.

  for ( idx_t ie = 0; ie < numElem_; ie++ )
  {
    // Get the index of current element.

    idx_t ielem = ielems[ie];

    // Gather necessary data.

    ndHist  = 0.0;
    ndWeights = 0.0;

    elems_.getElemNodes  ( inodes, ielem );

    // Get shape functions.

    Matrix sfuncs = shape_->getShapeFunctions ( );

    // Iterate over integration points of this element.

    for ( idx_t ip = 0; ip < ipCount_; ip++ )
    {
      ipHist = 0.0;

      material_->getHistory ( ipHist, ipoint++ );

      ndHist += matmul ( sfuncs( ALL, ip ), ipHist );
      
      ndWeights += sfuncs ( ALL, ip );
    }
    
    select ( weights, inodes ) += ndWeights;

    // Add block to the table.

    table.addBlock ( inodes, jcols, ndHist );
  }
}

//-----------------------------------------------------------------------
//   getShapeGrads_
//-----------------------------------------------------------------------

void StressModel::getShapeGrads_

  ( const Matrix& b,
    const Matrix& g )

{
  JEM_ASSERT ( rank_ >= 1 && rank_ <= 3 );

  if ( rank_ == 1 )
    get1DShapeGrads_ ( b, g );
  else if ( rank_ == 2 )
    get2DShapeGrads_ ( b, g );
  else
    get3DShapeGrads_ ( b, g );
}

//-----------------------------------------------------------------------
//   get1DShapeGrads_
//-----------------------------------------------------------------------

void StressModel::get1DShapeGrads_

  ( const Matrix& b,
    const Matrix& g )

{
  JEM_ASSERT ( b.size(0) == 1 && g.size(0) == 1 && 
               b.size(1) == g.size(1) );

  b = 0.0;

  b = g;
}

//-----------------------------------------------------------------------
//   get2DShapeGrads_
//-----------------------------------------------------------------------

void StressModel::get2DShapeGrads_

  ( const Matrix& b,
    const Matrix& g )

{
  JEM_ASSERT ( b.size(0) == 3 && g.size(0) == 2 &&
               b.size(1) == 2 * g.size(1) );

  b = 0.0;

  for ( idx_t i = 0; i < nodeCount_; i++ )
  {
    b( 0, 2*i   ) = g(0,i);

    b( 1, 2*i+1 ) = g(1,i);

    b( 2, 2*i   ) = g(1,i);
    b( 2, 2*i+1 ) = g(0,i);
  }
}

//-----------------------------------------------------------------------
//   get3DShapeGrads_
//-----------------------------------------------------------------------

void StressModel::get3DShapeGrads_

  ( const Matrix& b,
    const Matrix& g )

{
  JEM_ASSERT ( b.size(0) == 6 && g.size(0) == 3 &&
               b.size(1) == 3 * g.size(1) );

  b = 0.0;

  for ( idx_t i = 0; i < nodeCount_; i++ )
  {
    b( 0, 3*i   ) = g(0,i);
    
    b( 1, 3*i+1 ) = g(1,i);

    b( 2, 3*i+2 ) = g(2,i);

    b( 3, 3*i   ) = g(1,i);
    b( 3, 3*i+1 ) = g(0,i);

    b( 4, 3*i   ) = g(2,i);
    b( 4, 3*i+2 ) = g(0,i);

    b( 5, 3*i+1 ) = g(2,i);
    b( 5, 3*i+2 ) = g(1,i);
  }
}

//-----------------------------------------------------------------------
//   getDissipation_
//-----------------------------------------------------------------------

void StressModel::getDissipation_

  ( const Properties& params ) const

{
  IdxVector inodes    ( nodeCount_ );
  Matrix    coords    ( rank_, nodeCount_ );
  Vector    ipWeights ( ipCount_ );

  IdxVector ielems = egroup_.getIndices ( );

  idx_t ipoint = 0;
  double dissipation = 0.;

  for ( idx_t ie = 0; ie < numElem_; ++ie )
  {
    idx_t ielem = ielems[ie];

    elems_.getElemNodes  ( inodes, ielem );
    nodes_.getSomeCoords ( coords, inodes );

    shape_->getIntegrationWeights ( ipWeights, coords );

    for ( idx_t ip = 0; ip < ipCount_; ++ip )
      dissipation += ipWeights[ip] *
                     material_->getDissipation ( ipoint++ );
  }
  StringVector   names ( StringUtils::split ( myName_, '.' ) );
  String myTag = names [ names.size( ) - 1 ];
  params.set ( myTag, dissipation );
}

//-----------------------------------------------------------------------
//   initCharLength_
//-----------------------------------------------------------------------

void StressModel::initCharLength_ ()

{
  IdxVector   inodes     (            nodeCount_ );
  Matrix      coords     ( rank_,     nodeCount_ );
  Vector      ipWeights  ( ipCount_ ); 

  IdxVector ielems = egroup_.getIndices ( );

  // maimi07i for triangles
  // double      fac   = 2. / sqrt ( sqrt(3.) ); 

  // Frans's expression 6/pi/3^.25

  double      fac = ( rank_ == 3 ) ? 1. : 1.4512;

  idx_t ipoint = 0;

  for ( idx_t ie = 0; ie < numElem_; ++ie )
  {
    idx_t ielem   = ielems[ie];

    elems_.getElemNodes  ( inodes, ielem  );
    nodes_.getSomeCoords ( coords, inodes );

    shape_->getIntegrationWeights ( ipWeights, coords );

    double area = sum ( ipWeights );
    double le = ( rank_ == 3 ) ?
      fac * pow ( area, (1./3.) ) :
        fac * sqrt ( area );

    for ( idx_t ip = 0; ip < ipCount_; ++ip )
      material_->setCharLength ( ipoint++, le );
  }
}

//-----------------------------------------------------------------------
//    getFeatures_
//-----------------------------------------------------------------------

Vector StressModel::getFeatures_ ()

{
  IdxVector inodes    ( nodeCount_ );
  Matrix    coords    ( rank_, nodeCount_ );
  Vector    ipWeights ( ipCount_ );

  IdxVector ielems = egroup_.getIndices ( );

  idx_t ipoint = 0;
  double vol = 0.;

  Vector avgFeats;

  Ref<Bayesian> bay = dynamicCast<Bayesian> ( material_ );

  for ( idx_t ie = 0; ie < numElem_; ++ie )
  {
    idx_t ielem = ielems[ie];

    elems_.getElemNodes  ( inodes, ielem );
    nodes_.getSomeCoords ( coords, inodes );

    shape_->getIntegrationWeights ( ipWeights, coords );

    for ( idx_t ip = 0; ip < ipCount_; ++ip )
    {
      Vector ipFeats = bay->getFeatures ( ipoint++ );
      //System::out() << "ipFeats " << ipFeats << '\n';

      if ( ipoint == 1 )
      {
        avgFeats.resize ( ipFeats.size() );
	avgFeats = 0.0;
      }

      vol += ipWeights[ip];
      avgFeats += ipWeights[ip] * ipFeats;
    }
  }

  avgFeats /= vol;

  return avgFeats;
}

//-----------------------------------------------------------------------
//    printIps_
//-----------------------------------------------------------------------

void StressModel::printIps_

  ( const Properties& params,
    const Properties& globdat )

{
  using jive::util::Globdat;

  IdxVector ielems = egroup_.getIndices ( );
  IdxVector inodes    ( nodeCount_ );
  Matrix    coords    ( rank_, nodeCount_ );
  Matrix    ipCoords  ( rank_, ipCount_ );

  if ( strainOut_ == nullptr )
  {
    coordOut_ = initWriter_ ( params, "coords" );
  }

  idx_t ipoint = 0;

  for ( idx_t i = 0; i < numElem_; ++i )
  {
    idx_t ielem = ielems[i];

    elems_.getElemNodes  ( inodes, ielem            );
    nodes_.getSomeCoords ( coords, inodes           );

    shape_->getGlobalIntegrationPoints ( ipCoords, coords );

    for ( idx_t j = 0; j < ipCount_; ++j )
    {
      *coordOut_ << ipoint++ << " ";

      for ( idx_t r = 0; r < rank_; ++r )
      {
	*coordOut_ << ipCoords(r,j) << " ";
      }

      *coordOut_ << '\n';
    }
  }
}

//-----------------------------------------------------------------------
//    printState_
//-----------------------------------------------------------------------

void StressModel::printState_

  ( const Properties&  params,
    const Properties&  globdat )

{
  using jive::util::Globdat;

  Vector      state;
  idx_t         it;

  if ( stateOut_ == nullptr )
  {
    stateOut_ = initWriter_ ( params, "state" );
  }

  globdat.get ( it, Globdat::TIME_STEP );

  *stateOut_ << "newXOutput " << it << '\n';

  StateVector::get ( state, dofs_, globdat );

  for ( idx_t i = 0; i < state.size(); ++i )
  {
    *stateOut_ << state[i] << "\n";
  }
  stateOut_->flush();
}

//-----------------------------------------------------------------------
//    printStrains_
//-----------------------------------------------------------------------

void StressModel::printStrains_

  ( const Properties&  params,
    const Properties&  globdat )

{
  using jive::util::Globdat;

  idx_t       it;

  if ( strainOut_ == nullptr )
  {
    strainOut_ = initWriter_ ( params, "strains" );
  }

  globdat.get ( it, Globdat::TIME_STEP    );

  *strainOut_ << "Time step " << it << "\n";

  for ( idx_t i = 0; i < numElem_; ++i )
  {
    for ( idx_t j = 0; j < ipCount_; ++j )
    {
      *strainOut_ << i+1 << " " << j+1 << " ";

      for ( idx_t r = 0; r < rank_; ++r )
      {
	*strainOut_ << ipcoords_(r,i*ipCount_+j) << " ";
      }

      for ( idx_t k = 0; k < strCount_; ++k )
      {
	*strainOut_ << epss_(i*ipCount_+j,k) << " ";
      }

      *strainOut_ << "\n";
    }
  }

  *strainOut_ << "\n";

  strainOut_->flush();
}

//-----------------------------------------------------------------------
//    printStresses_
//-----------------------------------------------------------------------

void StressModel::printStresses_

  ( const Properties&  params,
    const Properties&  globdat )

{
  using jive::util::Globdat;

  idx_t       it;

  if ( stressOut_ == nullptr )
  {
    stressOut_ = initWriter_ ( params, "stresses" );
  }

  globdat.get ( it, Globdat::TIME_STEP    );

  *stressOut_ << "Time step " << it << "\n";

  for ( idx_t i = 0; i < numElem_; ++i )
  {
    for ( idx_t j = 0; j < ipCount_; ++j )
    {
      *stressOut_ << i+1 << " " << j+1 << " ";

      for ( idx_t r = 0; r < rank_; ++r )
      {
	*stressOut_ << ipcoords_(r,i*ipCount_+j) << " ";
      }

      for ( idx_t k = 0; k < strCount_; ++k )
      {
	*stressOut_ << sigs_(i*ipCount_+j,k) << " ";
      }
      *stressOut_ << "\n";
    }
  }

  *stressOut_ << "\n";

  stressOut_->flush();
}
//-----------------------------------------------------------------------
//    initWriter_
//-----------------------------------------------------------------------

Ref<PrintWriter>  StressModel::initWriter_

  ( const Properties&  params, 
    const String       name )  const

{
  // Open file for output

  StringVector fileName;
  String       prepend;

  if ( params.find( prepend, "prepend" ) )
  { 
    fileName.resize(3);

    fileName[0] = prepend;
    fileName[1] = myTag_;
    fileName[2] = name;
  }
  else
  {
    fileName.resize(2);

    fileName[0] = myTag_;
    fileName[1] = name;
  }

  return newInstance<PrintWriter>( newInstance<FileWriter> ( 
         StringUtils::join( fileName, "." ) ) );
}

//=======================================================================
//   related functions
//=======================================================================

//-----------------------------------------------------------------------
//   newStressModel
//-----------------------------------------------------------------------


Ref<Model>            newStressModel

  ( const String&       name,
    const Properties&   conf,
    const Properties&   props,
    const Properties&   globdat )

{
  // Return an instance of the class.

  return newInstance<StressModel> ( name, conf, props, globdat );
}

//-----------------------------------------------------------------------
//   declareStressModel
//-----------------------------------------------------------------------

void declareStressModel ()
{
  using jive::model::ModelFactory;

  // Register the StressModel with the ModelFactory.

  ModelFactory::declare ( "Stress", newStressModel );
}

