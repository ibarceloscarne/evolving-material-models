/*
 *  TU Delft 
 *
 *  Iuri Barcelos, May 2019
 *
 *  Stochastic gradient descent algorithm for neural network training
 *
 *  Kingma, D. P.; Ba, J. L. Adam: A method for stochastic optimization.
 *  In: Proceedings of the International Conference on Learning
 *  Representations (ICLR 2015), San Diego, 2015.
 *
 */

#include <cstdlib>
#include <random>

#include <jem/base/Error.h>
#include <jem/base/limits.h>
#include <jem/base/Float.h>
#include <jem/base/System.h>
#include <jem/base/Exception.h>
#include <jem/base/ClassTemplate.h>
#include <jem/base/array/operators.h>
#include <jem/base/Thread.h>
#include <jem/base/Monitor.h>
#include <jem/io/Writer.h>
#include <jem/io/PrintWriter.h>
#include <jem/io/FileWriter.h>
#include <jem/io/FileReader.h>
#include <jem/io/FileInputStream.h>
#include <jem/io/FileFlags.h>
#include <jem/io/FileStream.h>
#include <jem/util/Event.h>
#include <jem/util/Flex.h>
#include <jem/numeric/algebra/utilities.h>
#include <jem/mp/MPException.h>
#include <jem/mp/Context.h>
#include <jem/mp/Buffer.h>
#include <jem/mp/Status.h>
#include <jive/util/utilities.h>
#include <jive/util/Globdat.h>
#include <jive/util/FuncUtils.h>
#include <jive/algebra/VectorSpace.h>
#include <jive/model/Actions.h>
#include <jive/model/StateVector.h>
#include <jive/app/ModuleFactory.h>
#include <jive/implict/Names.h>
#include <jive/implict/SolverInfo.h>
#include <jive/util/XDofSpace.h>
#include <jive/mp/Globdat.h>

#include "AdamModule.h"
#include "SolverNames.h"
#include "declare.h"
#include "TrainingData.h"
#include "LearningNames.h"
#include "XNeuronSet.h"

JEM_DEFINE_CLASS( jive::implict::AdamModule );


JIVE_BEGIN_PACKAGE( implict )


using jem::max;
using jem::newInstance;
using jem::Float;
using jem::System;
using jem::Exception;
using jem::Error;
using jem::Thread;
using jem::Monitor;
using jem::io::endl;
using jem::io::Writer;
using jem::io::PrintWriter;
using jem::io::FileWriter;
using jem::io::FileReader;
using jem::io::FileFlags;
using jem::io::FileStream;
using jem::numeric::axpy;
using jem::mp::MPException;
using jem::mp::SendBuffer;
using jem::mp::RecvBuffer;
using jem::mp::Status;

using jive::util::FuncUtils;
using jive::util::XDofSpace;
using jive::model::Actions;
using jive::model::StateVector;

//=======================================================================
//   class AdamModule
//=======================================================================

//-----------------------------------------------------------------------
//   static data
//-----------------------------------------------------------------------

const char* AdamModule::TYPE_NAME   = "Adam";
const char* AdamModule::SEED        = "seed";
const char* AdamModule::ALPHA       = "alpha";
const char* AdamModule::BETA1       = "beta1";
const char* AdamModule::BETA2       = "beta2";
const char* AdamModule::EPSILON     = "epsilon";
const char* AdamModule::L2REG       = "l2reg";
const char* AdamModule::MINIBATCH   = "miniBatch";
const char* AdamModule::LOSSFUNC    = "loss";
const char* AdamModule::PRECISION   = "precision";
const char* AdamModule::VALSPLIT    = "valSplit";
const char* AdamModule::SKIPFIRST   = "skipFirst";
const char* AdamModule::JPROP       = "jProp";
const char* AdamModule::CHECKPOINT  = "checkpoint";
const char* AdamModule::CPEVERY     = "checkpointEvery";

//-----------------------------------------------------------------------
//   constructor & destructor
//-----------------------------------------------------------------------

AdamModule::AdamModule ( const String& name ) :

  Super ( name )

{
  alpha_ = 0.001;
  beta1_ = 0.9;
  beta2_ = 0.999;
  eps_   = 1.e-8;
  lambda_ = 0.0;
  batchSize_ = 1;
  epoch_     = 0;
  iiter_     = 1;
  precision_ = 1.e-3;
  valSplit_  = 0.0;
  skipFirst_ = 0;

  mpi_   = false;
  mpx_   = nullptr;

  jprop_ = 0.0;

  cpFile_     = "";
  checkpoint_ = false;
  cpEvery_    = jem::maxOf(cpEvery_);
}


AdamModule::~AdamModule ()
{}

//-----------------------------------------------------------------------
//   init
//-----------------------------------------------------------------------

Module::Status AdamModule::init

  ( const Properties&  conf,
    const Properties&  props,
    const Properties&  globdat )

{
  Ref<DofSpace>     dofs  = DofSpace::   get ( globdat, getContext() );
  Ref<Model>        model = Model::      get ( globdat, getContext() );
  Ref<TrainingData> data = TrainingData::get ( globdat, getContext() );

  // Parallelization stuff

  mpx_ = jive::mp::Globdat::getMPContext ( globdat );

  if ( mpx_ == nullptr )
  {
    throw Error ( JEM_FUNC, String::format (
       "MPContext has not been found" ) );
  }

  if ( mpx_->size() > 1 )
  {
    print ( System::info( myName_ ), getContext(), 
      ": Running in MP mode with ", mpx_->size(), " processes" );
    mpi_ = true;
  }

  if ( !mpi_ )
  {
    print ( System::info( myName_ ), getContext(), 
      ": Running in sequential mode" );
  }

  // Resize some stuff

  idx_t dc = dofs->dofCount();

  g_.resize     ( dc );
  g_ = 0.0;

  if ( lambda_ > 0.0 )
  {
    rg_.resize ( dc );
    rg_ = 0.0;
  }

  if ( mpi_ && mpx_->myRank() == 0 )
  {
    gt_.resize ( dc );
    gt_ = 0.0;
  }

  if ( !mpi_ || mpx_->myRank() == 0 )
  {
    m_.resize     ( dc );
    v_.resize     ( dc );
    m0_.resize    ( dc );
    v0_.resize    ( dc );

    m_ = 0.0;  v_ = 0.0;
    m0_ = 0.0; v0_ = 0.0;
  }

  if ( valSplit_ > 0.0 && skipFirst_ == 0 )
  {
    skipFirst_ = valSplit_ * data->sampleSize();
  }

  if ( checkpoint_ )
  {
    loadCheckPoint_ ( globdat );
  }

  return OK;
}

//-----------------------------------------------------------------------
//   shutdown
//-----------------------------------------------------------------------

void AdamModule::shutdown ( const Properties& globdat )
{
  bool root = ( !mpi_ || mpx_->myRank() == 0 );

  if ( root )
  {
    System::out() << "AdamModule statistics ..." 
      << "\n-- total # of epochs: " << epoch_ 
      << "\n-- total optimization time: " << total_ << ", of which"
      << "\n---- " << t5_ << " shuffling the dataset"
      << "\n---- " << t6_ << " updating weights"
      << "\n---- " << t1_ << " computing loss and grads, of which"
      << "\n------ " << t2_ << " allocating sample batches"
      << "\n------ " << t3_ << " propagating through the network"
      << "\n------ " << t4_ << " backpropagating through the network"
      << "\n\n";
  }
}

//-----------------------------------------------------------------------
//   configure
//-----------------------------------------------------------------------

void AdamModule::configure

  ( const Properties&  props,
    const Properties&  globdat )

{
  using jem::maxOf;

  if ( props.contains( myName_ ) )
  {
    Properties  myProps = props.findProps ( myName_ );

    myProps.get  ( lossName_,  LOSSFUNC  );

    func_  = NeuralUtils::getLossFunc ( lossName_ );
    grad_  = NeuralUtils::getLossGrad ( lossName_ );

    myProps.find ( alpha_,      ALPHA      );
    myProps.find ( eps_,        EPSILON    );
    myProps.find ( batchSize_,  MINIBATCH  );
    myProps.find ( precision_,  PRECISION  );

    myProps.find ( beta1_,     BETA1,    0.0, 1.0            );
    myProps.find ( beta2_,     BETA2,    0.0, 1.0            );
    myProps.find ( lambda_,    L2REG,    0.0, maxOf(lambda_) );
    myProps.find ( valSplit_,  VALSPLIT, 0.0, 1.0            );

    if ( valSplit_ == 0.0 )
    {
      myProps.find ( skipFirst_, SKIPFIRST, 0, maxOf ( skipFirst_ ) );
    }

    myProps.find ( jprop_, JPROP, 0.0, 1.0 );

    if ( myProps.find ( cpFile_, CHECKPOINT ) )
    {
      checkpoint_ = true;

      myProps.find ( cpEvery_, CPEVERY );
    }
  }
}

//-----------------------------------------------------------------------
//   getConfig
//-----------------------------------------------------------------------

void AdamModule::getConfig

  ( const Properties&  conf,
    const Properties&  globdat ) const

{
  Properties  myConf = conf.makeProps ( myName_ );

  myConf.set ( LOSSFUNC,   lossName_   );
  myConf.set ( ALPHA,      alpha_      );
  myConf.set ( EPSILON,    eps_        );
  myConf.set ( MINIBATCH,  batchSize_  );
  myConf.set ( PRECISION,  precision_  );
  myConf.set ( BETA1,      beta1_      );
  myConf.set ( BETA2,      beta2_      );
  myConf.set ( VALSPLIT,   valSplit_   );
  myConf.set ( SKIPFIRST,  skipFirst_  );
  myConf.set ( CHECKPOINT, cpFile_     );
  myConf.set ( CPEVERY,    cpEvery_    );
}

//-----------------------------------------------------------------------
//   advance
//-----------------------------------------------------------------------

void AdamModule::advance ( const Properties& globdat )
{
  using jive::util::Globdat;

  globdat.set ( Globdat::TIME_STEP, ++epoch_ );
  //epoch_++;
}

//-----------------------------------------------------------------------
//   solve
//-----------------------------------------------------------------------

void AdamModule::solve

  ( const Properties& info,
    const Properties& globdat )

{
  total_.start();

  if ( mpi_ )
  {
    mpSolve_ ( info, globdat );
  }
  else
  {
    solve_ ( info, globdat );
  }

  total_.stop();
}

//-----------------------------------------------------------------------
//   cancel
//-----------------------------------------------------------------------

void AdamModule::cancel ( const Properties& globdat )
{
}

//-----------------------------------------------------------------------
//   commit
//-----------------------------------------------------------------------

bool AdamModule::commit ( const Properties& globdat )
{
  bool  root = ( !mpi_ || mpx_->myRank() == 0 );

  if ( checkpoint_ && root && !(epoch_%cpEvery_) )
  {
    saveCheckPoint_ ( globdat );
  }

  return true;
}

//-----------------------------------------------------------------------
//   setPrecision
//-----------------------------------------------------------------------

void AdamModule::setPrecision ( double eps )
{
}

//-----------------------------------------------------------------------
//   getPrecision
//-----------------------------------------------------------------------

double AdamModule::getPrecision () const
{
  return precision_;
}

//-----------------------------------------------------------------------
//   mpSolve_
//-----------------------------------------------------------------------

void AdamModule::mpSolve_

  ( const Properties& info,
    const Properties& globdat )

{
  idx_t rank = mpx_->myRank();
  idx_t size = mpx_->size();
  bool  root = ( rank == 0 );
  bool  last = ( rank == size - 1 );

  Ref<TrainingData> data = TrainingData::get ( globdat, getContext() );
  Ref<DofSpace>     dofs = DofSpace::get     ( globdat, getContext() );
  Ref<Model>        model = Model::get       ( globdat, getContext() );

  idx_t dc = dofs->dofCount();

  Vector state;
  StateVector::get ( state, dofs, globdat );

  idx_t n = data->sampleSize();

  idx_t batch = 0, load = 0, end = 0, beg = 0;

  IdxVector valset;   
  IdxVector trainset;

  if ( skipFirst_ )
  {
    print ( System::info( myName_ ), getContext(),
	    " : Epoch "          , epoch_,
	    ", skipping first ", skipFirst_, " samples" );
    trainset.ref ( IdxVector ( iarray ( slice ( skipFirst_, n ) ) ) );
    n -= skipFirst_;
  }
  else
  {
    trainset.ref ( IdxVector ( iarray ( n ) ) );
  }

  t5_.start();
  NeuralUtils::shuffle ( trainset, globdat );
  t5_.stop();

  double trainloss = 0.0;

  while ( n > 0 )
  {
    batch = min ( n, batchSize_    );
    load  = max ( 1, batch / size  );
    end   = max ( n - rank*load, 0 );
    beg   = last ? n - batch : max ( end - load, 0_idx );

    //System::out() << "rank " << rank << " load " << load << " beg " << beg << " end " << end << '\n';

    n    -= batch;

    g_ = 0.0;
    gt_ = 0.0;

    double localloss = 0.0;
    
    if ( batch > rank )
    {
      //System::out() << "rank " << rank << " running eval_\n";
      localloss = eval_ ( trainset[slice(beg,end)], true, globdat );    
      //System::out() << "rank " << rank << " local loss " << localloss << '\n';
    }

    double accloss = 0.0;

    mpx_->reduce ( RecvBuffer ( &accloss,   1 ),
                   SendBuffer ( &localloss, 1 ),
		   0,
		   jem::mp::SUM );

    mpx_->reduce ( RecvBuffer ( gt_.addr(), dc ),
		   SendBuffer ( g_. addr(), dc ),
		   0,
		   jem::mp::SUM );

    t6_.start();
    if ( root )
    {
      trainloss += accloss;
      //System::out() << "ROOT: trainloss " << trainloss << " accloss " << accloss << '\n';

      gt_ /= (double)batch;

      m_ = beta1_*m0_ + (1.-beta1_)*gt_;
      v_ = beta2_*v0_ + (1.-beta2_)*gt_*gt_;

      double alphat = alpha_ * (
		      sqrt ( 1. - pow( beta2_,(double)iiter_ ) ) /
			   ( 1. - pow( beta1_,(double)iiter_ ) ) );

      state -= alphat * m_ / ( sqrt(v_) + eps_ );

      m0_ = m_;
      v0_ = v_;


      mpx_->broadcast ( SendBuffer ( state.addr(), dc ) );
    }

    if ( !root )
    {
      mpx_->broadcast ( RecvBuffer ( state.addr(), dc ), 0 );
    }

    model->takeAction ( LearningActions::UPDATE, Properties(), globdat );

    if ( lambda_ > 0.0 )
    {
      IdxVector iwts ( dofs->dofCount() );
      IdxVector iaxs ( dofs->dofCount() );

      idx_t nw = dofs->getDofsForType ( iwts, iaxs,
		 dofs->findType ( LearningNames::WEIGHTDOF ) );

      iwts.reshape ( nw );

      r_        = 0.5 * lambda_ * dot ( state[iwts] );
      rg_[iwts] = lambda_ * state[iwts];
    }

    iiter_++;
    t6_.stop();
  }

  if ( root )
  {
    System::out() << "\n";
    print ( System::info( myName_ ), getContext(),
	    " : Epoch "          , epoch_,
	    ", training loss = ", trainloss / trainset.size(), endl );
    System::out() << "\n";
  }
}

//-----------------------------------------------------------------------
//   solve_
//-----------------------------------------------------------------------

void AdamModule::solve_

  ( const Properties&  info,
    const Properties&  globdat )

{
  Ref<TrainingData> data = TrainingData::get ( globdat, getContext() );
  Ref<DofSpace>     dofs = DofSpace::get     ( globdat, getContext() );
  Ref<Model>        model = Model::get       ( globdat, getContext() );

  Vector state;
  StateVector::get ( state, dofs, globdat );

  idx_t n = data->sampleSize();

  idx_t batch = 0;
  IdxVector valset, trainset;

  double loss;

  if ( skipFirst_ )
  {
    print ( System::info( myName_ ), getContext(),
	    " : Epoch "          , epoch_,
	    ", skipping first ", skipFirst_, " out of ", n, " samples" );

    trainset.ref ( IdxVector ( iarray ( slice ( skipFirst_, n ) ) ) );
    n -= skipFirst_;
  }
  else
  {
    trainset.ref ( IdxVector ( iarray ( n ) ) );
  }

  t5_.start();
  NeuralUtils::shuffle ( trainset, globdat );
  t5_.stop();

  loss = 0.0;

  while ( n > 0 )
  {
    batch = min ( n, batchSize_ );

    g_ = 0.0;

    loss += eval_ ( trainset[slice(n-batch,n)], true, globdat );    
    //System::out() << "loss " << loss << '\n';

    g_ /= (double)batch;

    t6_.start();

    m_ = beta1_*m0_ + (1.-beta1_)*g_;
    v_ = beta2_*v0_ + (1.-beta2_)*g_*g_;

    double alphat = alpha_ * (
		    sqrt ( 1. - pow( beta2_,(double)iiter_ ) ) /
			 ( 1. - pow( beta1_,(double)iiter_ ) ) );

    state -= alphat * m_ / ( sqrt(v_) + eps_ );

    //System::out() << "Weights after" << state << "\n";
    
    m0_ = m_;
    v0_ = v_;
    t6_.stop();

    model->takeAction ( LearningActions::UPDATE, Properties(), globdat );

    if ( lambda_ > 0.0 )
    {
      IdxVector iwts ( dofs->dofCount() );
      IdxVector iaxs ( dofs->dofCount() );

      idx_t nw = dofs->getDofsForType ( iwts, iaxs,
		 dofs->findType ( LearningNames::WEIGHTDOF ) );

      iwts.reshape ( nw );

      r_        = 0.5 * lambda_ * dot ( state[iwts] );
      rg_[iwts] = lambda_ * state[iwts];
    }

    iiter_++;

    n -= batch;
  }

  System::out() << "\n";
  print ( System::info( myName_ ), getContext(),
	  " : Epoch "          , epoch_,
	  ", training loss = ", loss / (double)trainset.size(), endl );
	  //", training loss = ", loss / (double)batch, endl );
  System::out() << "\n";
}

//-----------------------------------------------------------------------
//   eval_ 
//-----------------------------------------------------------------------

double AdamModule::eval_

  ( const IdxVector&  samples,
    const bool        dograds,
    const Properties& globdat )

{
  t1_.start();

  Properties params;
 
  Ref<Model>        model = Model::get        ( globdat, getContext() );
  Ref<TrainingData> tdata = TrainingData::get ( globdat, getContext() );

  double loss = 0.0;

  t2_.start();
  Batch b = tdata->getData ( samples );
  t2_.stop();

  
  for ( idx_t t = 0; t < tdata->sequenceSize(); ++t )
  {
    //if ( t == 0 ) System::out() << "First time step\n";

    params.erase ( LearningParams::STATE );

    if ( t > 0 )
    {
      params.set      ( LearningParams ::STATE,             b[t-1]  );
    }

    params.set        ( LearningParams ::DATA,              b[t]    );

    t3_.start();
    model->takeAction ( LearningActions::PROPAGATE, params, globdat );
    t3_.stop();

    //System::out() << "Time step " << t << " old loss " << loss;
    //System::out() << "Time step " << t << '\n';
    loss += func_ ( b[t]->outputs, b[t]->targets );
    //System::out() << " new loss " << loss << '\n';
    //System::out() << "targets " << b[t]->targets << " outputs " << b[t]->outputs << '\n';

    if ( lambda_ > 0.0 )
    {
      loss += r_;
    }
  }

  //System::out() << "eval_ loss " << loss << '\n';

  if ( dograds )
  {
    for ( idx_t t = tdata->sequenceSize() - 1; t >= 0; --t )
    {
      grad_ ( b[t]->outputs, b[t]->targets );

      params.erase ( LearningParams::STATE );

      if ( t > 0 )
      {
	params.set      ( LearningParams ::STATE,                 b[t-1]  );
      }

      params.set        ( LearningParams ::GRADS,                 g_       );
      params.set        ( LearningParams ::DATA,                  b[t]     );

      t4_.start();
      model->takeAction ( LearningActions::BACKPROPAGATE, params, globdat  );
      t4_.stop();

      //System::out() << "Grads " << g_ << '\n';

      if ( lambda_ > 0.0 )
      {
        g_ += rg_;
      }

      if ( jprop_ > 0.0 )
      {
	idx_t ni = tdata->inputSize();
	idx_t no = tdata->outputSize();
	idx_t ns = samples.size();

	Ref<NData>  data = tdata->stretchData ( b[t] );
	Ref<NData> rdata = newInstance<NData> ( ns*ni, ni, no );

	Vector dummyg ( g_.size() );
	Vector jg     ( g_.size() );

	dummyg = 0.0; jg = 0.0;

	double jloss = 0.0;

	data->outputs = 0.0;

	for ( idx_t i = 0; i < ni; ++i )
	{
          data->outputs(i,slice(i*ns,(i+1)*ns)) = 1.0;
	}

	params.set ( LearningParams::DATA,  data   );
	params.set ( LearningParams::GRADS, dummyg );
	model->takeAction ( LearningActions::BACKPROPAGATE, params, globdat );

	rdata->inputs = -tdata->getJacobian ( samples );

	params.set ( LearningParams::RDATA, rdata );
	model->takeAction ( LearningActions::FORWARDJAC, params, globdat );

	jloss += 0.5 * dot ( rdata->inputs );

	params.set ( LearningParams::GRADS, jg );
	model->takeAction ( LearningActions::BACKWARDJAC, params, globdat );

	loss = (1.0-jprop_) * loss + jprop_ * jloss;
	g_   = (1.0-jprop_) * g_   + jprop_ * jg;

	//Ref<NData> rdata = newInstance<NData> ( samples.size(), 
	//                                        tdata->inputSize(),
	//					tdata->outputSize() );

	//Ref<Normalizer> inl = tdata->getInpNormalizer();

	//Vector dummyg ( g_.size() );
	//dummyg = 0.0;

	//Vector jg ( g_.size() );
	//jg = 0.0;

	//double jloss = 0.0;

        //for ( idx_t i = 0; i < tdata->inputSize(); ++i )
	//{
	//  b[t]->outputs = 0.0;
	//  b[t]->outputs(i,ALL) = 1.0;

	//  params.set ( NeuralParams::GRADS, dummyg );
	//  model->takeAction ( NeuralActions::BACKPROPAGATE, params, globdat );

	//  for ( idx_t s = 0; s < samples.size(); ++s )
	//  {
	//    Vector fac = inl->getJacobianFactor ( b[t]->inputs(ALL,s) );

	//    rdata->inputs(ALL,s) = - tdata->getJacobianRow ( i, samples[s] ) / fac;
	//  }

	//  params.set ( NeuralParams::RDATA, rdata );

	//  model->takeAction ( NeuralActions::FORWARDJAC, params, globdat );

	//  jloss += 0.5 * dot ( rdata->inputs );

	//  params.set ( NeuralParams::GRADS, jg );
	//  model->takeAction ( NeuralActions::BACKWARDJAC, params, globdat );
	//}

	//loss = (1.0-jprop_) * loss + jprop_ * jloss;
	//g_   = (1.0-jprop_) * g_   + jprop_ * jg;
      }
    }

    // dbg (FD check)

    //double fdstep = 1.e-8;
    //double floss = 0.0;
    //double bloss = 0.0;

    //Ref<DofSpace>     dofs = DofSpace::get     ( globdat, getContext() );
    //Vector state;
    //StateVector::get ( state, dofs, globdat );

    ////System::out() << "State before " << state << '\n';
    //state[0] += fdstep;
    ////System::out() << "State after " << state << '\n';

    //StateVector::store ( state, dofs, globdat );
    //model->takeAction ( LearningActions::UPDATE, Properties(), globdat );

    //for ( idx_t t = 0; t < tdata->sequenceSize(); ++t )
    //{
    //  params.erase ( LearningParams::STATE );

    //  if ( t > 0 )
    //  {
    //    params.set      ( LearningParams ::STATE,             b[t-1]  );
    //  }

    //  params.set        ( LearningParams ::DATA,              b[t]    );

    //  t3_.start();
    //  model->takeAction ( LearningActions::PROPAGATE, params, globdat );
    //  t3_.stop();

    //  floss += func_ ( b[t]->outputs, b[t]->targets );

    //}

    //state[0] -= 2.0*fdstep;
    ////System::out() << "State after " << state << '\n';

    //StateVector::store ( state, dofs, globdat );
    //model->takeAction ( LearningActions::UPDATE, Properties(), globdat );

    //for ( idx_t t = 0; t < tdata->sequenceSize(); ++t )
    //{
    //  params.erase ( LearningParams::STATE );

    //  if ( t > 0 )
    //  {
    //    params.set      ( LearningParams ::STATE,             b[t-1]  );
    //  }

    //  params.set        ( LearningParams ::DATA,              b[t]    );

    //  t3_.start();
    //  model->takeAction ( LearningActions::PROPAGATE, params, globdat );
    //  t3_.stop();

    //  bloss += func_ ( b[t]->outputs, b[t]->targets );

    //}

    //double fdgrad = ( floss - bloss ) / fdstep / 2.0;

    ////System::out() << "Loss " << loss << " fdloss " << fdloss << '\n';
    //System::out() << "First grad " << g_[0] << " fdgrad " << fdgrad << '\n';
  }

  t1_.stop();
  
  return loss;
}

//-----------------------------------------------------------------------
//   saveCheckPoint_
//-----------------------------------------------------------------------

void AdamModule::saveCheckPoint_ 

  ( const Properties& globdat )

{
  Ref<DofSpace> dofs  = DofSpace::   get ( globdat, getContext() );

  idx_t dc = dofs->dofCount();

  Ref<PrintWriter> out = newInstance<PrintWriter> (
                    newInstance<FileWriter> ( cpFile_, FileFlags::WRITE ) );

  out->nformat.setFractionDigits ( 10 );

  *out << dc << " " << epoch_ << " " << iiter_ << "\n"; 

  for ( idx_t w = 0_idx; w < dc; ++w )
  {
    *out << m0_[w] << " ";
  }
  *out << "\n";

  for ( idx_t w = 0_idx; w < dc; ++w )
  {
    *out << v0_[w] << " ";
  }
  *out << "\n";

  Vector state;
  StateVector::get ( state, dofs, globdat );

  for ( idx_t w = 0_idx; w < dc; ++w )
  {
    *out << state[w] << '\n';
  }
  print ( System::info( myName_ ), getContext(), 
    ": Checkpoint saved to file ", cpFile_, endl, endl );
}

//-----------------------------------------------------------------------
//   loadCheckPoint_
//-----------------------------------------------------------------------

void AdamModule::loadCheckPoint_ 

  ( const Properties& globdat )

{
  bool  root = ( !mpi_ || mpx_->myRank() == 0 );

  Ref<DofSpace> dofs  = DofSpace::   get ( globdat, getContext() );
  Ref<Model>    model = Model::      get ( globdat, getContext() );
  idx_t dc = dofs->dofCount();

  Ref<FileReader> in;
  
  try 
  {
    in = newInstance<FileReader> ( cpFile_ );
  }
  catch ( const Exception& ex )
  {
    System::out() << "No checkpoint data has been found\n";
    return;
  }

  JEM_PRECHECK ( dc == in->parseInt() );

  epoch_ = in->parseInt();
  iiter_ = in->parseInt();

  Vector state;
  StateVector::get ( state, dofs, globdat );

  if ( root )
  {
    for ( idx_t w = 0_idx; w < dc; ++w )
    {
      m0_[w] = in->parseDouble();
    }

    for ( idx_t w = 0_idx; w < dc; ++w )
    {
      v0_[w] = in->parseDouble();
    }

    for ( idx_t w = 0_idx; w < dc; ++w )
    {
      state[w] = in->parseDouble();
    }

    mpx_->broadcast ( SendBuffer ( state.addr(), dc ) );
  }
  else
  {
    mpx_->broadcast ( RecvBuffer ( state.addr(), dc ), 0 );
  }

  model->takeAction ( LearningActions::UPDATE, Properties(), globdat );

  print ( System::info( myName_ ), getContext(), 
    ": Checkpoint loaded. Starting training at Epoch ", epoch_, endl );
}

//-----------------------------------------------------------------------
//   makeNew
//-----------------------------------------------------------------------


Ref<Module> AdamModule::makeNew

  ( const String&      name,
    const Properties&  conf,
    const Properties&  props,
    const Properties&  globdat )

{
  return newInstance<Self> ( name );
}

//-----------------------------------------------------------------------
//   declare
//-----------------------------------------------------------------------

void AdamModule::declare ()
{
  using jive::app::ModuleFactory;

  ModuleFactory::declare ( TYPE_NAME, & makeNew );
  ModuleFactory::declare ( CLASS_NAME, & makeNew );
}

JIVE_END_PACKAGE( implict )

