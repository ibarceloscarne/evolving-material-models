/*
 *  TU Delft 
 *
 *  Iuri Barcelos, August 2019
 *
 *  Material class that uses a nested neural network to compute
 *  stress and stiffness.
 *
 */

#ifndef ANN_MATERIAL_H
#define ANN_MATERIAL_H

#include <jive/app/Module.h>
#include <jive/model/Model.h>

#include "Material.h"
#include "NData.h"
#include "Normalizer.h"

using jive::app::Module;

//-----------------------------------------------------------------------
//   class ANNMaterial
//-----------------------------------------------------------------------

class ANNMaterial : public Material
{
 public:

  typedef Array< Ref<NData>, 1 > Batch;

  static const char*     NETWORK;
  static const char*     WEIGHTS;
  static const char*     NORMALIZER;
  static const char*     RECURRENT;

  explicit               ANNMaterial

    ( const idx_t          rank,
      const Properties&    globdat );

  virtual void           configure

    ( const Properties&    props,
      const Properties&    globdat );

  virtual void           getConfig

    ( const Properties&    conf,
      const Properties&    globdat )   const;

  virtual void           update

    ( Matrix&              stiff,
      Vector&              stress,
      const Vector&        strain,
      const idx_t          ipoint );

  virtual void           commit  ();

  virtual void           stressAtPoint

    ( Vector&              stress,
      const Vector&        strain,
      const idx_t          ipoint );

  virtual void           addTableColumns

    ( IdxVector&           jcols,
      XTable&              table,
      const String&        name );
  
  virtual void           createIntPoints

    ( const idx_t           npoints );

  virtual Ref<Material>  clone ( ) const;

 protected:

  virtual               ~ANNMaterial();

 protected:

  idx_t                   rank_;
  bool                    recurrent_;
  bool                    first_;

  Ref<Module>             network_;
  Properties              netData_;
  Ref<Normalizer>         nizer_;

  Batch                   data_;
  Batch                   state_;

  Properties              conf_;

  IdxVector               perm_;
  bool                    perm23_;

 protected:

  void                    initWeights_

    ( const String&         fname        );
};

#endif
