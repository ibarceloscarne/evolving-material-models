/*
 * Copyright (C) 2019 TU Delft. All rights reserved.
 *
 * Class that implements a fully-connected recurrent neural layer.
 * Neurons are connected with the previous and next layers as well
 * as with its own state at the time when forward propagation
 * occurs. This layer can be stacked with other RecurrentLayers
 * as well as with DenseLayers.
 *
 * Author: Iuri Barcelos, i.barceloscarneiromrocha@tudelft.nl
 * Date:   June 2019
 * 
 */

#ifndef RECURRENTLAYER_H
#define RECURRENTLAYER_H

#include <jive/model/Model.h>
#include <jem/base/System.h>
#include <jem/util/Properties.h>
#include <jive/util/Assignable.h>
#include <jive/util/XDofSpace.h>

#include "NeuralUtils.h"
#include "XAxonSet.h"
#include "XNeuronSet.h"

#include "NData.h"

using jem::String;
using jem::idx_t;
using jem::util::Properties;

using jive::model::Model;
using jive::util::Assignable;
using jive::util::DofSpace;

using NeuralUtils::ActivationFunc;
using NeuralUtils::ActivationGrad;

using NeuralUtils::InitFunc;

typedef Array<idx_t,2> IdxMatrix;

class RecurrentLayer : public Model
{
 public:
  
  static const char*   SIZE;
  static const char*   ACTIVATION;
  static const char*   INPINIT;
  static const char*   RECINIT;

                       RecurrentLayer

    ( const String&      name,
      const Properties&  conf,
      const Properties&  props,
      const Properties&  globdat  );

  virtual void         configure

    ( const Properties&  props,
      const Properties&  globdat  );

  virtual void         getConfig

    ( const Properties&  conf,
      const Properties&  globdat  );

  virtual bool         takeAction

    ( const String&      action,
      const Properties&  params,
      const Properties&  globdat  );

 protected:

  virtual             ~RecurrentLayer  ();

  virtual void         update_

    ( const Properties&  globdat  );

  virtual void         propagate_

    ( const Ref<NData>   data,
      const Ref<NData>   state,
      const Properties&  globdat  );

  virtual void         backPropagate_

    ( const Ref<NData>   data,
      const Ref<NData>   state,
            Vector&      grads,
      const Properties&  globdat  );

  virtual void         getJacobian_

    ( const Ref<NData>   data,
      const Properties&  globdat  );

 private:
  
  idx_t                size_;
  idx_t                weightType_;
  idx_t                biasType_;

  InitFunc             inpInit_;
  InitFunc             recInit_;

  ActivationFunc       func_;
  ActivationGrad       grad_;

  Ref<DofSpace>        dofs_;
  Ref<AxonSet>         axons_;
  Ref<NeuronSet>       neurons_;

  IdxVector            iNeurons_;
  IdxVector            inpNeurons_;

  IdxVector            iBiases_;
  IdxVector            iInits_;
  IdxMatrix            iInpWts_;
  IdxMatrix            iMemWts_;

  Vector               biases_;
  Vector               inits_;
  Matrix               inpWeights_;
  Matrix               memWeights_;

  Matrix               jacobian_;
};

#endif
