/*
 * Copyright (C) 2019 TU Delft. All rights reserved.
 *
 * Class that implements a dropout layer for regularization.
 * Values come from previous layer, activations are either
 * zero or a scaled-up version of the values. The choice of
 * which neurons are zeroed is done based on a dropout rate
 * and a uniform random distribution.
 *
 * Author: Iuri Barcelos, i.barceloscarneiromrocha@tudelft.nl
 * Date:   Aug 2019
 * 
 */

#include <jem/base/Error.h>
#include <jem/base/System.h>
#include <jem/base/Float.h>
#include <jem/numeric/algebra/matmul.h>
#include <jem/util/Timer.h>
#include <jive/Array.h>
#include <jive/model/Actions.h>
#include <jive/model/ModelFactory.h>
#include <jive/model/StateVector.h>
#include <jive/util/error.h>
#include <jive/util/Random.h>

#include "DropoutLayer.h"
#include "LearningNames.h"

using jem::Error;
using jem::numeric::matmul;
using jem::util::Timer;

using jive::IdxVector;
using jive::util::XDofSpace;
using jive::util::sizeError;
using jive::util::Random;
using jive::model::StateVector;

//-----------------------------------------------------------------------
//   static data
//-----------------------------------------------------------------------

const char* DropoutLayer::RATE = "rate";

//-----------------------------------------------------------------------
//   constructor & destructor
//-----------------------------------------------------------------------

DropoutLayer::DropoutLayer

  ( const String&      name,
    const Properties&  conf,
    const Properties&  props,
    const Properties&  globdat ) : Model ( name )

{
  rate_ = 0.0;

  Properties myProps = props.getProps ( myName_ );
  Properties myConf  = conf.makeProps ( myName_ );

  myProps.get ( rate_, RATE );
  myConf. set ( RATE, rate_ );

  bool input  = false;
  bool output = false;

  myProps.find ( input, LearningNames::FIRSTLAYER );
  myProps.find ( output, LearningNames::LASTLAYER );

  if ( input || output )
  {
    throw Error ( JEM_FUNC, "Dropout layers must be hidden" );
  }

  Ref<Model> layer;

  if ( myProps.find ( layer, LearningParams::IMAGE ) )
  {
    throw Error ( JEM_FUNC,
      "Dropout layers do not support symmetry" );
  }

  Ref<XNeuronSet> nset = XNeuronSet::get ( globdat, getContext()    );

  globdat.get( inpNeurons_, LearningParams::PREDECESSOR );

  idx_t presize = inpNeurons_.size();

  iNeurons_.resize ( presize );

  for ( idx_t in = 0; in < presize; ++in )
  {
    iNeurons_[in] = nset->addNeuron();
  }

  globdat.set ( LearningParams::PREDECESSOR, iNeurons_.clone() );
}

DropoutLayer::~DropoutLayer ()
{}

//-----------------------------------------------------------------------
//   configure
//-----------------------------------------------------------------------

void DropoutLayer::configure

  ( const Properties& props,
    const Properties& globdat )

{
  Properties myProps = props.getProps ( myName_ );

  myProps.get ( rate_, RATE );
}

//-----------------------------------------------------------------------
//   getConfig
//-----------------------------------------------------------------------

void DropoutLayer::getConfig

  ( const Properties& conf,
    const Properties& globdat )

{
  Properties myConf  = conf.makeProps ( myName_ );

  myConf. set ( RATE, rate_ );
}

//-----------------------------------------------------------------------
//   takeAction
//-----------------------------------------------------------------------

bool DropoutLayer::takeAction

  ( const String&     action,
    const Properties& params,
    const Properties& globdat )

{
  using jive::model::Actions;

  if ( action == LearningActions::PROPAGATE )
  {
    Ref<NData> data;
    params.get ( data, LearningParams::DATA );

    propagate_ ( rate_, data, globdat );

    return true;
  }

  if ( action == LearningActions::RECALL )
  {
    Ref<NData> data;
    params.get ( data, LearningParams::DATA );

    propagate_ ( 0.0, data, globdat );
  }

  if ( action == LearningActions::BACKPROPAGATE )
  {
    Ref<NData> data;
    params.get ( data, LearningParams::DATA );

    Vector grads;
    params.get ( grads, LearningParams::GRADS );

    backPropagate_ ( data, grads, globdat );

    return true;
  }

  return false;
}

//-----------------------------------------------------------------------
//   propagate_
//-----------------------------------------------------------------------

void DropoutLayer::propagate_

  ( const double      rate,
    const Ref<NData>  data,
    const Properties& globdat  )

{
  Ref<Random> generator = Random::get ( globdat );

  double scale = 1. / ( 1. - rate );

  Matrix acts      ( select ( data->activations, inpNeurons_, ALL ) );

  select ( data->values, iNeurons_, ALL ) = acts;

  Matrix myacts ( acts.clone() );

  myacts *= scale;

  for ( idx_t i = 0; i < myacts.size(0); ++i )
  {
    for ( idx_t j = 0; j < myacts.size(1); ++j )
    {
      if ( generator->next() < rate )
      {
        myacts(i,j) = 0.0;
      }
    }
  }
      
  select ( data->activations, iNeurons_, ALL ) = myacts;
}

//-----------------------------------------------------------------------
//   backPropagate_
//-----------------------------------------------------------------------

void DropoutLayer::backPropagate_

  ( const Ref<NData>   data,
          Vector&      grads,
    const Properties&  globdat  )

{
  double scale = 1. / ( 1. - rate_ );

  Matrix myacts ( select ( data->activations, iNeurons_, ALL ) );
  Matrix deltas ( select ( data->deltas,      iNeurons_, ALL ) );

  deltas *= scale;

  for ( idx_t i = 0; i < myacts.size(0); ++i )
  {
    for ( idx_t j = 0; j < myacts.size(1); ++j )
    {
      if ( myacts(i,j) == 0.0 )
      {
        deltas(i,j) = 0.0;
      }
    }
  }

  select ( data->deltas, inpNeurons_, ALL ) = deltas;
}

//=======================================================================
//   related functions
//=======================================================================

//-----------------------------------------------------------------------
//   newDropoutLayer
//-----------------------------------------------------------------------


Ref<Model>            newDropoutLayer

  ( const String&       name,
    const Properties&   conf,
    const Properties&   props,
    const Properties&   globdat )

{
  // Return an instance of the class.

  return newInstance<DropoutLayer> ( name, conf, props, globdat );
}

//-----------------------------------------------------------------------
//   declareDropoutLayer
//-----------------------------------------------------------------------

void declareDropoutLayer ()
{
  using jive::model::ModelFactory;

  // Register the StressModel with the ModelFactory.

  ModelFactory::declare ( "Dropout", newDropoutLayer );
}

