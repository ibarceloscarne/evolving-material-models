/*
 * Copyright (C) 2021 TU Delft. All rights reserved.
 *
 * Author: Iuri Barcelos, i.rocha@tudelft.nl
 * Date:   Apr 2021
 * 
 */

#ifndef MATERIALLAYER_H
#define MATERIALLAYER_H

#include <jive/model/Model.h>
#include <jem/base/System.h>
#include <jem/util/Properties.h>
#include <jem/util/Timer.h>
#include <jive/util/Assignable.h>
#include <jive/util/XDofSpace.h>

#include "NeuralUtils.h"

#include "NData.h"
#include "Material.h"
#include "Bayesian.h"

using jem::String;
using jem::idx_t;
using jem::util::Properties;
using jem::util::Timer;

using jive::model::Model;
using jive::util::Assignable;
using jive::util::DofSpace;

typedef Array<idx_t,2> IdxMatrix;
typedef Ref<Material>  MatRef;
typedef Ref<Bayesian>  BayRef;

class MaterialLayer : public Model
{
 public:
  
  static const char*   MATERIAL;
  static const char*   RANK;
  static const char*   HISTSIZE;

                       MaterialLayer

    ( const String&      name,
      const Properties&  conf,
      const Properties&  props,
      const Properties&  globdat  );

  virtual void         configure

    ( const Properties&  props,
      const Properties&  globdat  );

  virtual void         getConfig

    ( const Properties&  conf,
      const Properties&  globdat  );

  virtual bool         takeAction

    ( const String&      action,
      const Properties&  params,
      const Properties&  globdat  );

 protected:

  virtual             ~MaterialLayer  ();

  virtual void         propagate_

    ( const Ref<NData>   data,
      const Ref<NData>   state,
      const Properties&  globdat  );

  virtual void         backPropagate_

    ( const Ref<NData>   data,
      const Ref<NData>   state,
      const Properties&  globdat  );

 private:
  
  idx_t                sSize_;
  idx_t                hSize_;

  IdxVector            sNeurons_;
  IdxVector            hNeurons_;
  IdxVector            inpNeurons_;

  Properties           props_;
  Properties           globdat_;

  Array<MatRef>        material_;

  Timer                tUpd_;

};

#endif
