/*
 * Copyright (C) 2019 TU Delft. All rights reserved.
 *
 * Class that reads input/output layer for ANN training. It can
 * also be used by an external agent (forked chain, module parallel
 * to the solver) to gradually add training data to be used by
 * the ANN solver.
 *
 * Author: Iuri Barcelos, i.barceloscarneiromrocha@tudelft.nl
 * Date:   May 2019
 * 
 */

#include <jem/base/System.h>
#include <jem/base/Error.h>
#include <jem/base/Slice.h>
#include <jem/util/Tokenizer.h>
#include <jem/util/ArrayBuffer.h>
#include <jem/io/FileReader.h>
#include <jem/io/FileInputStream.h>
#include <jem/io/InputStreamReader.h>
#include <jem/base/IllegalInputException.h>

#include "TrainingData.h"
#include "XNeuronSet.h"

using namespace jem;
using namespace jem::literals;

//-----------------------------------------------------------------------
//   class TrainingData
//-----------------------------------------------------------------------

//-----------------------------------------------------------------------
//   static data
//-----------------------------------------------------------------------

const char* TrainingData::GLOBNAME         = "trainingData";
const char* TrainingData::INPUTNORMALIZER  = "inpNormalizer"; 
const char* TrainingData::OUTPUTNORMALIZER = "outNormalizer";
const char* TrainingData::XINPUTNORMALIZER = "xinNormalizer";
const char* TrainingData::JACOBIANS        = "jacobians";

//-----------------------------------------------------------------------
//   constructors
//-----------------------------------------------------------------------

TrainingData::TrainingData

  ( const Properties& globdat )

{
  //JEM_ASSERT ( *this != jem::NIL );

  inputs_. resize ( 0, 0, 0 );
  outputs_.resize ( 0, 0, 0 );

  globdat.set ( GLOBNAME, const_cast<TrainingData*> ( this ) );
}

TrainingData::TrainingData

  ( const String&     fname,
    const Properties& globdat ) : Self ( globdat )

{
  using jem::util::Tokenizer;
  using jem::util::ArrayBuffer;
  using jem::io::FileInputStream;
  using jem::io::InputStreamReader;

  idx_t sampsize = 0;
  idx_t linesize = 0;
  idx_t seqsize  = 0;

  idx_t pos = 0;

  ArrayBuffer<double> linebuf;
  Matrix imat;

  Ref<Tokenizer> tok = 
    newInstance<Tokenizer> ( 
      newInstance<InputStreamReader> ( 
        newInstance<FileInputStream> ( fname ) ) );

  idx_t token = tok->nextToken(); 
  long int ln = tok->getLineNumber();

  while ( true )
  {
    bool eof = token == Tokenizer::EOF_TOKEN;
    bool eol = tok->getLineNumber() > ln;
    bool eos = tok->getLineNumber() - ln > 1;

    if ( eof || eol )
    {
      Vector line ( linebuf.toArray() );

      if ( !linesize )
      {
        linesize = line.size();
      }
      else if ( line.size() != linesize )
      {
        throw Error ( JEM_FUNC,
	  "TrainingData: Inconsistent number of rows" );
      }

      if ( !seqsize )
      {
	imat.reshape ( pos + 1, linesize );
      }

      imat(pos,ALL) = line;

      pos++;

      if ( eof || eos )
      {
	if ( !seqsize )
	{
          seqsize = pos;
	  inputs_.resize  ( seqsize, linesize, 0 );

	  inputs_  = 0.0;
	}
	else if ( pos != seqsize )
	{
          throw Error ( JEM_FUNC,
	    "TrainingData: Inconsistent sequence length" );
	}

	inputs_. reshape ( sampsize + 1 );

	inputs_ (ALL,ALL,sampsize) = imat;

	imat.resize ( seqsize, linesize );

	imat = 0.0; 

	pos = 0;
	sampsize++;
      }

      linebuf.clear();

      if ( eof )
      {
        break;
      }

      ln = tok->getLineNumber();
    }

    if ( token == Tokenizer::FLOAT_TOKEN ||
         token == Tokenizer::INTEGER_TOKEN  )
    {
      linebuf.pushBack ( tok->getFloat() );
    }
    else
    {
      throw Error ( JEM_FUNC,
        "TrainingData: Invalid input character" );
    }

    token = tok->nextToken();
  }

  System::out() << "TrainingData: Read " << sampsize << " samples of size " << imat.shape() << "\n";

  outputs_.ref ( inputs_ );

  //updNorms_();
}

TrainingData::TrainingData

  ( const String&     fname,
    const IdxVector&  inputs,
    const IdxVector&  xinputs,
    const IdxVector&  outputs,
    const Properties& globdat ) : Self ( globdat )

{
  using jem::util::Tokenizer;
  using jem::util::ArrayBuffer;
  using jem::io::FileInputStream;
  using jem::io::InputStreamReader;

  idx_t sampsize = 0;
  idx_t linesize = 0;
  idx_t seqsize  = 0;

  idx_t pos = 0;

  idx_t isize = inputs. size();
  idx_t osize = outputs.size();
  idx_t xsize = xinputs.size();

  ArrayBuffer<double> linebuf;
  Matrix imat ( 0_idx, isize );
  Matrix omat ( 0_idx, osize );
  Matrix xmat ( 0_idx, xsize );

  for ( idx_t i = 0; i < inputs.size(); ++i )
  {
    if ( testany ( outputs == inputs[i] ) || testany ( outputs == xinputs[i] ) )
    {
      throw Error ( JEM_FUNC,
	"TrainingData: Ambiguous input/output choice" );
    }
  }

  Ref<Tokenizer> tok = 
    newInstance<Tokenizer> ( 
      newInstance<InputStreamReader> ( 
        newInstance<FileInputStream> ( fname ) ) );

  idx_t token = tok->nextToken(); 
  long int ln = tok->getLineNumber();

  while ( true )
  {
    bool eof = token == Tokenizer::EOF_TOKEN;
    bool eol = tok->getLineNumber() > ln;
    bool eos = tok->getLineNumber() - ln > 1;

    if ( eof || eol )
    {
      Vector line ( linebuf.toArray() );

      if ( !linesize )
      {
        linesize = line.size();

	if ( linesize <= max(max(max(inputs),max(xinputs)),max(outputs)) )
	{
          throw Error ( JEM_FUNC,
	    "TrainingData: Incompatible input/output data" );
	}
      }
      else if ( line.size() != linesize )
      {
        throw Error ( JEM_FUNC,
	  "TrainingData: Inconsistent number of rows" );
      }

      if ( !seqsize )
      {
	imat.reshape ( pos + 1, isize );
	omat.reshape ( pos + 1, osize );
	xmat.reshape ( pos + 1, xsize );
      }

      imat(pos,ALL) = line[inputs];
      omat(pos,ALL) = line[outputs];
      xmat(pos,ALL) = line[xinputs];

      pos++;

      if ( eof || eos )
      {
	if ( !seqsize )
	{
          seqsize = pos;
	  inputs_.resize  ( seqsize, isize, 0 );
	  outputs_.resize ( seqsize, osize, 0 );
	  xinputs_.resize ( seqsize, xsize, 0 );

	  inputs_  = 0.0;
	  outputs_ = 0.0;
	  xinputs_ = 0.0;
	}
	else if ( pos != seqsize )
	{
	  System::out() << line << '\n';
          throw Error ( JEM_FUNC,
	    "TrainingData: Inconsistent sequence length" );
	}

	inputs_. reshape ( sampsize + 1 );
	outputs_.reshape ( sampsize + 1 );
	xinputs_.reshape ( sampsize + 1 );

	inputs_ (ALL,ALL,sampsize) = imat;
	outputs_(ALL,ALL,sampsize) = omat;
	xinputs_(ALL,ALL,sampsize) = xmat;

	imat.resize ( seqsize, isize );
	omat.resize ( seqsize, osize );
	xmat.resize ( seqsize, xsize );

	imat = 0.0; omat = 0.0; xmat = 0.0;

	pos = 0;
	sampsize++;
      }

      linebuf.clear();

      if ( eof )
      {
        break;
      }

      ln = tok->getLineNumber();
    }

    if ( token == Tokenizer::FLOAT_TOKEN ||
         token == Tokenizer::INTEGER_TOKEN  )
    {
      linebuf.pushBack ( tok->getFloat() );
    }
    else
    {
      throw Error ( JEM_FUNC,
        "TrainingData: Invalid input character" );
    }

    token = tok->nextToken();
  }

  //updNorms_();
}

TrainingData::TrainingData

  ( const String&     fname,
    const IdxVector&  inputs,
    const IdxVector&  outputs,
    const Properties& globdat ) : Self ( globdat )

{
  using jem::util::Tokenizer;
  using jem::util::ArrayBuffer;
  using jem::io::FileInputStream;
  using jem::io::InputStreamReader;

  idx_t sampsize = 0;
  idx_t linesize = 0;
  idx_t seqsize  = 0;

  idx_t pos = 0;

  idx_t isize = inputs. size();
  idx_t osize = outputs.size();

  ArrayBuffer<double> linebuf;
  Matrix imat ( 0_idx, isize );
  Matrix omat ( 0_idx, osize );

  for ( idx_t i = 0; i < inputs.size(); ++i )
  {
    if ( testany ( outputs == inputs[i] ) )
    {
      throw Error ( JEM_FUNC,
	"TrainingData: Ambiguous input/output choice" );
    }
  }

  Ref<Tokenizer> tok = 
    newInstance<Tokenizer> ( 
      newInstance<InputStreamReader> ( 
        newInstance<FileInputStream> ( fname ) ) );

  idx_t token = tok->nextToken(); 
  long int ln = tok->getLineNumber();

  while ( true )
  {
    bool eof = token == Tokenizer::EOF_TOKEN;
    bool eol = tok->getLineNumber() > ln;
    bool eos = tok->getLineNumber() - ln > 1;

    if ( eof || eol )
    {
      Vector line ( linebuf.toArray() );

      if ( !linesize )
      {
        linesize = line.size();

	if ( linesize <= max(max(inputs),max(outputs)) )
	{
          throw Error ( JEM_FUNC,
	    "TrainingData: Incompatible input/output data" );
	}
      }
      else if ( line.size() != linesize )
      {
        throw Error ( JEM_FUNC,
	  "TrainingData: Inconsistent number of rows" );
      }

      if ( !seqsize )
      {
	imat.reshape ( pos + 1, isize );
	omat.reshape ( pos + 1, osize );
      }

      imat(pos,ALL) = line[inputs];
      omat(pos,ALL) = line[outputs];

      pos++;

      if ( eof || eos )
      {
	if ( !seqsize )
	{
          seqsize = pos;
	  inputs_.resize  ( seqsize, isize, 0 );
	  outputs_.resize ( seqsize, osize, 0 );

	  inputs_  = 0.0;
	  outputs_ = 0.0;
	}
	else if ( pos != seqsize )
	{
          throw Error ( JEM_FUNC,
	    "TrainingData: Inconsistent sequence length" );
	}

	inputs_. reshape ( sampsize + 1 );
	outputs_.reshape ( sampsize + 1 );

	inputs_ (ALL,ALL,sampsize) = imat;
	outputs_(ALL,ALL,sampsize) = omat;

	imat.resize ( seqsize, isize );
	omat.resize ( seqsize, osize );

	imat = 0.0; omat = 0.0;

	pos = 0;
	sampsize++;
      }

      linebuf.clear();

      if ( eof )
      {
        break;
      }

      ln = tok->getLineNumber();
    }

    if ( token == Tokenizer::FLOAT_TOKEN ||
         token == Tokenizer::INTEGER_TOKEN  )
    {
      linebuf.pushBack ( tok->getFloat() );
    }
    else
    {
      throw Error ( JEM_FUNC,
        "TrainingData: Invalid input character" );
    }

    token = tok->nextToken();
  }

  //updNorms_();
}

//-----------------------------------------------------------------------
//   configure
//-----------------------------------------------------------------------

void TrainingData::configure

  ( const Properties& props,
    const Properties& globdat )

{
  String type = "none";

  props.find ( type, INPUTNORMALIZER );

  inl_ = newNormalizer ( type );

  type = "none";

  props.find ( type, OUTPUTNORMALIZER );

  onl_ = newNormalizer ( type );

  props.find ( type, XINPUTNORMALIZER );

  xnl_ = newNormalizer ( type );

  inl_->update ( inputs_  );
  onl_->update ( outputs_ );
  xnl_->update ( xinputs_ );

  String fname;

  if ( props.find ( fname, JACOBIANS ) )
  {
    readJacs_ ( fname, globdat );
  }
}

//-----------------------------------------------------------------------
//   addData
//-----------------------------------------------------------------------

idx_t TrainingData::addData

  ( const Vector& inputs,
    const Vector& outputs )

{
  idx_t n = sampleSize();
  idx_t i = inputs. size();
  idx_t o = outputs.size();

  if ( !n )
  {
    inputs_. resize ( 1_idx, i, 1_idx );
    outputs_.resize ( 1_idx, o, 1_idx );
    inputs_ = 0.0; outputs_ = 0.0;
  }
  else
  {
    JEM_ASSERT ( i == inputSize()  );
    JEM_ASSERT ( o == outputSize() );

    inputs_. reshape ( n + 1_idx ); 
    outputs_.reshape ( n + 1_idx );
  }

  inputs_ (0_idx,ALL,n) = inputs;
  outputs_(0_idx,ALL,n) = outputs;

  inl_->update ( inputs_  );
  onl_->update ( outputs_ );

  newDataEvent.emit ( *this );

  return n;
}

//-----------------------------------------------------------------------
//   addData
//-----------------------------------------------------------------------

idx_t TrainingData::addData

  ( const Matrix& inputs,
    const Matrix& outputs )

{
  idx_t n = sampleSize();
  idx_t i = inputs. size(0);
  idx_t o = outputs.size(0);

  JEM_PRECHECK ( inputs.size(1) == outputs.size(1) );

  if ( !n )
  {
    inputs_. resize ( 1_idx, i, inputs.size(1) ); 
    outputs_.resize ( 1_idx, o, inputs.size(1) ); 
    inputs_ = 0.0; outputs_ = 0.0;
  }
  else
  {
    JEM_PRECHECK ( i == inputSize () );
    JEM_PRECHECK ( o == outputSize() );

    inputs_. reshape ( n + inputs.size(1) );
    outputs_.reshape ( n + inputs.size(1) );
  }

  inputs_ (0_idx,ALL,slice(n,END)) = inputs;
  outputs_(0_idx,ALL,slice(n,END)) = outputs;

  inl_->update ( inputs_  );
  onl_->update ( outputs_ );

  newDataEvent.emit ( *this );

  return n;
}

//-----------------------------------------------------------------------
//   refreshData
//-----------------------------------------------------------------------

void TrainingData::refreshData

  ( const idx_t   id,
    const Vector& inputs,
    const Vector& outputs )

{
  inputs_ (0,ALL,id) = inputs;
  outputs_(0,ALL,id) = outputs;

  inl_->update ( inputs_  );
  onl_->update ( outputs_ );

  newDataEvent.emit ( *this );
}


//-----------------------------------------------------------------------
//   getData
//-----------------------------------------------------------------------

Batch TrainingData::getData

  ( const   IdxVector& ids )

{
  Batch data ( sequenceSize() );

  for ( idx_t t = 0; t < sequenceSize(); ++t )
  {
    if ( xinputSize() )
    {
      data[t] = newInstance<NData> 
	( ids.size(), inputSize(), outputSize(), xinputSize() );
    }
    else
    {
      data[t] = newInstance<NData> 
	( ids.size(), inputSize(), outputSize() );
    }

    for ( idx_t s = 0; s < ids.size(); ++s )
    {
      data[t]->inputs(ALL,s)  = inl_->normalize ( inputs_ ( t, ALL, ids[s] ) );
      data[t]->targets(ALL,s) = onl_->normalize ( outputs_( t, ALL, ids[s] ) ); 

      if ( xinputSize() )
      {
        data[t]->xinputs(ALL,s) = xnl_->normalize ( xinputs_ ( t, ALL, ids[s] ) );
      }
    }
  }

  return data;
}

//-----------------------------------------------------------------------
//   stretchData
//-----------------------------------------------------------------------

Ref<NData> TrainingData::stretchData

  ( const Ref<NData> data )

{
  idx_t ns = data->batchSize();

  Ref<NData> ret = newInstance<NData> 
    ( ns * inputSize(), inputSize(), outputSize() );

  // NB: ugly, change later
  ret->init ( data->values.size(0) );

  for ( idx_t i = 0; i < inputSize(); ++i )
  {
    ret->values     (ALL,slice(i*ns,(i+1)*ns)) = data->values;
    ret->activations(ALL,slice(i*ns,(i+1)*ns)) = data->activations;
  }

  return ret;
}

//-----------------------------------------------------------------------
//   getJacobian
//-----------------------------------------------------------------------

Matrix TrainingData::getJacobian

  ( const IdxVector& ids ) 

{
  idx_t ns = ids.size();

  Matrix ret ( inputSize(), ns * inputSize() );
  ret = 0.0;

  for ( idx_t i = 0; i < inputSize(); ++i )
  {
    for ( idx_t s = 0; s < ns; ++s )
    {
      ret(ALL,i*ns+s) = jacobians_(i,ALL,s);
    }
  }

  return ret;
}

//-----------------------------------------------------------------------
//   get
//-----------------------------------------------------------------------

Ref<TrainingData> TrainingData::get

  ( const Properties&  globdat,
    const String&      context )

{
  Ref<TrainingData> data;

  globdat.find ( data, GLOBNAME );

  if ( data == nullptr )
  {
    throw jem::IllegalInputException (
      context,
      "No TrainingData object exists."
    );
  }

  return data;
}

//-----------------------------------------------------------------------
//   readJacs_
//-----------------------------------------------------------------------

void TrainingData::readJacs_

  ( const String&     fname,
    const Properties& globdat )

{
  using jem::io::FileReader;

  idx_t ni = inputSize();
  idx_t no = outputSize();
  idx_t ns = sampleSize();

  if ( sequenceSize() > 1 )
  {
    throw Error ( JEM_FUNC, "J-prop augmentation not implemented for recurrent layers" );
  }

  jacobians_.resize ( ni, no, ns );
  jacobians_ = 0.0;

  Ref<FileReader> in    = newInstance<FileReader> ( fname );

  for ( idx_t s = 0; s < ns; ++s )
  {
    for ( idx_t i = 0; i < ni; ++i )
    {
      for ( idx_t o = 0; o < no; ++o )
      {
        jacobians_( i, o, s ) = in->parseDouble();
      }

      Vector fac = inl_->getJacobianFactor ( inputs_ ( 0, ALL, s ) );
      jacobians_ ( i, ALL, s ) /= fac;
    }
  }
}
