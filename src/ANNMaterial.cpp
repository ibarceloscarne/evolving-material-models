/*
 *  TU Delft 
 *
 *  Iuri Barcelos, August 2019
 *
 *  Material class that uses a nested neural network to compute
 *  stress and stiffness.
 *
 */

#include <jem/base/Error.h>
#include <jem/base/System.h>
#include <jem/base/Array.h>
#include <jem/util/Properties.h>
#include <jem/numeric/algebra/matmul.h>
#include <jem/numeric/algebra/utilities.h>
#include <jem/io/FileReader.h>
#include <jive/model/Model.h>
#include <jive/model/StateVector.h>
#include <jive/app/ChainModule.h>
#include <jive/app/UserconfModule.h>
#include <jive/app/InfoModule.h>
#include <jive/fem/InitModule.h>
#include <jive/fem/ElementSet.h>
#include <jive/fem/NodeSet.h>
#include <jive/util/Globdat.h>
#include <jive/util/utilities.h>
#include <jive/util/DofSpace.h>
#include <jive/util/Assignable.h>

#include "ANNMaterial.h"

#include "utilities.h"
#include "LearningNames.h"

using namespace jem;

using jem::io::FileReader;

using jive::model::Model;
using jive::model::StateVector;
using jive::app::ChainModule;
using jive::app::UserconfModule;
using jive::app::InitModule;
using jive::app::InfoModule;
using jive::util::Globdat;
using jive::util::joinNames;
using jive::util::DofSpace;
using jive::util::Assignable;
using jive::fem::ElementSet;
using jive::fem::NodeSet;

//=======================================================================
//   class ANNMaterial
//=======================================================================

//-----------------------------------------------------------------------
//   static data
//-----------------------------------------------------------------------

const char* ANNMaterial::NETWORK    = "network";
const char* ANNMaterial::WEIGHTS    = "weights";
const char* ANNMaterial::NORMALIZER = "normalizer";
const char* ANNMaterial::RECURRENT  = "recurrent";

//-----------------------------------------------------------------------
//   constructor and destructor
//-----------------------------------------------------------------------

ANNMaterial::ANNMaterial

  ( const idx_t       rank,
    const Properties& globdat )

  : Material ( rank, globdat )

{

  rank_ = rank;

  JEM_PRECHECK ( rank_ >= 1 && rank_ <= 3 );

  data_. resize ( 0 );
  state_.resize ( 0 );

  // Check for permutation

  Assignable<ElementSet> eset;
  Assignable<NodeSet>    nset;
  eset = ElementSet::get ( globdat, "ANNMaterial" );
  nset = eset.getNodes ( );

  idx_t meshrank = nset.rank ( );

  if ( meshrank < rank_ )
  {
    perm23_ = true;

    System::out() << "Performing plane strain analysis on 3D network\n";

    perm_.resize ( 3 );

    perm_[0] = 0;   
    perm_[1] = 1;   
    perm_[2] = 3;   
  }
}

ANNMaterial::~ANNMaterial ()
{}

//-----------------------------------------------------------------------
//   configure
//-----------------------------------------------------------------------

void ANNMaterial::configure

  ( const Properties& props,
    const Properties& globdat )

{
  String fname;

  Ref<ChainModule> chain;

  netData_ = Globdat::newInstance ( NETWORK );

  Globdat::getVariables ( netData_ );

  chain = newInstance<ChainModule> ( joinNames ( NETWORK, "chain" ) );

  chain->pushBack ( newInstance<UserconfModule> ( joinNames ( NETWORK, "userinput"   ) ) );
  chain->pushBack ( newInstance<InitModule>     ( joinNames ( NETWORK, "init"        ) ) );
  chain->pushBack ( newInstance<InfoModule>     ( joinNames ( NETWORK, "info"        ) ) );

  chain->configure ( props,  netData_        );
  chain->getConfig ( conf_,  netData_        );
  chain->init      ( conf_,  props, netData_ );

  network_ = chain;

  props.get ( fname, WEIGHTS );
  conf_.set ( WEIGHTS, fname );

  initWeights_ ( fname );

  props.get ( fname, NORMALIZER );
  conf_.set ( NORMALIZER, fname );

  nizer_ = newNormalizer ( fname );

  props.get ( recurrent_, RECURRENT  );
  conf_.set ( RECURRENT,  recurrent_ );

  if ( perm23_ )
  {
    System::out() << "Performing plane strain analysis on 3D network\n";

    perm_.resize ( 3 );

    perm_[0] = 0;   
    perm_[1] = 1;   
    perm_[2] = 3;   
  }
}

//-----------------------------------------------------------------------
//   getConfig
//-----------------------------------------------------------------------

void ANNMaterial::getConfig

  ( const Properties& conf,
    const Properties& globdat ) const
    
{
  String fname;
  Properties netconf;

  conf_.get ( fname, WEIGHTS );
  conf. set ( WEIGHTS, fname );

  conf_.get ( fname, NORMALIZER );
  conf. set ( NORMALIZER, fname );

  conf_.get ( netconf, NETWORK );
  conf. set ( NETWORK, netconf );
}

//-----------------------------------------------------------------------
//  createIntPoints 
//-----------------------------------------------------------------------

void ANNMaterial::createIntPoints

  ( const idx_t       npoints )

{
  const idx_t STRAIN_COUNTS[4] = { 0, 1, 3, 6 };

  idx_t size = STRAIN_COUNTS[rank_];

  data_.resize ( npoints );

  for ( idx_t p = 0; p < npoints; ++p )
  {
    data_[p] = newInstance<NData> ( 1, size, size );
  }

  if ( recurrent_ )
  {
    state_.resize ( npoints );

    for ( idx_t p = 0; p < npoints; ++p )
    {
      state_[p] = newInstance<NData> ( 1, size, size );
    }
  }
}

//-----------------------------------------------------------------------
//  update 
//-----------------------------------------------------------------------

void ANNMaterial::update

  ( Matrix&       stiff,
    Vector&       stress,
    const Vector& strain,
    const idx_t   ipoint )
{
  Properties params;

  String context ( "ANNMaterial::update" );

  Ref<Model> model = Model::get ( netData_, context );

  if ( perm23_ )
  {
    Vector eps3d ( 6 );
    eps3d = 0.0;
    
    eps3d[perm_] = strain;
    data_[ipoint]->inputs(ALL,0) = nizer_->normalize ( eps3d );
  }
  else
  {
    data_[ipoint]->inputs(ALL,0) = nizer_->normalize ( strain );
  }

  params.set ( LearningParams::DATA, data_[ipoint] );

  if ( recurrent_ && !first_ )
  {
    params.set ( LearningParams::STATE, state_[ipoint] );
  }

  model->takeAction ( LearningActions::PROPAGATE,   params, netData_ );
  model->takeAction ( LearningActions::GETJACOBIAN, params, netData_ );

  if ( perm23_ )
  {
    stress = data_[ipoint]->outputs(ALL,0)[perm_];
  }
  else
  {
    stress = data_[ipoint]->outputs(ALL,0); 
  }

  Vector fac = nizer_->getJacobianFactor ( strain );

  Matrix jac ( data_[ipoint]->jacobian.clone() );
  for ( idx_t row = 0; row < fac.size(); ++row )
  {
    jac(row,ALL) *= fac;
  }

  if ( perm23_ )
  {
    stiff = jac(perm_,perm_); 
  }
  else
  {
    stiff = jac;
  }
}

//-----------------------------------------------------------------------
//  commit
//-----------------------------------------------------------------------

void ANNMaterial::commit ()

{
  if ( recurrent_ )
  {
    data_.swap ( state_ );
  }
  
  first_ = false;
}

//-----------------------------------------------------------------------
//  stressAtPoint 
//-----------------------------------------------------------------------

void ANNMaterial::stressAtPoint

  ( Vector&       stress,
    const Vector& strain,
    const idx_t   ipoint )
{
}

//-----------------------------------------------------------------------
//  clone 
//-----------------------------------------------------------------------

Ref<Material> ANNMaterial::clone ( ) const

{
  return newInstance<ANNMaterial> ( *this );
}

//-----------------------------------------------------------------------
//  addTableColumns 
//-----------------------------------------------------------------------

void ANNMaterial::addTableColumns

  ( IdxVector&     jcols,
    XTable&        table,
    const String&  name )

{
  if ( name == "nodalStress" || name == "ipStress" )
  {
    if ( rank_ == 2 )
    {
      jcols.resize ( 3 );

      jcols[0] = table.addColumn ( "s_xx" );
      jcols[1] = table.addColumn ( "s_yy" );
      jcols[2] = table.addColumn ( "s_xy" );
    }
    else
    {
      jcols.resize ( 6 );

      jcols[0] = table.addColumn ( "s_xx" );
      jcols[1] = table.addColumn ( "s_yy" );
      jcols[2] = table.addColumn ( "s_zz" );
      jcols[3] = table.addColumn ( "s_xy" );
      jcols[4] = table.addColumn ( "s_xz" );
      jcols[5] = table.addColumn ( "s_yz" );
    }
  }
}

//-----------------------------------------------------------------------
//  initWeights_
//-----------------------------------------------------------------------

void ANNMaterial::initWeights_

  ( const String&     fname )

{
  String context ( "ANNMaterial::initWeights_" );

  Ref<DofSpace> dofs  = DofSpace::get ( netData_, context );
  Ref<Model>    model = Model::   get ( netData_, context );

  Ref<FileReader> in    = newInstance<FileReader> ( fname );

  idx_t dc = dofs->dofCount();

  Vector wts ( dc );
  wts = 0.0;

  for ( idx_t i = 0; i < dc; ++i )
  {
    wts[i] = in->parseDouble();
  }

  StateVector::store ( wts, dofs, netData_ );

  model->takeAction ( LearningActions::UPDATE, Properties(), netData_ );
}

/* Purgatory

  //stiff  = data_[ipoint]->jacobian;

  //for ( idx_t row = 0; row < fac.size(); ++row )
  //{
  //  stiff(row,ALL) *= fac;
  //}

  // DBG: some tests

  //Vector tstrain ( 6 );

  //tstrain[0] = -0.032077;
  //tstrain[1] = 0.080439;
  //tstrain[2] = -0.019818;
  //tstrain[3] = 1.1599e-07;
  //tstrain[4] = 6.7652e-08;
  //tstrain[5] = 0.00038062;

  //data_[ipoint]->inputs(ALL,0) = nizer_->normalize ( tstrain );

  //model->takeAction ( NeuralActions::PROPAGATE,   params, netData_ );
  //model->takeAction ( NeuralActions::GETJACOBIAN, params, netData_ );

  //System::out() << "Test stress " << data_[ipoint]->outputs(ALL,0) << "\n";
  //System::out() << "Test jacobian " << data_[ipoint]->jacobian << "\n";


  // DEBUG: tangent check with finite diffs

  //idx_t icol = 5;

  //Vector strainp ( strain.clone() );

  //strainp[icol] += 1.e-10;

  //data_[ipoint]->inputs(ALL,0) = nizer_->normalize ( strainp );
  //model->takeAction ( NeuralActions::PROPAGATE, params, netData_ );

  //Vector stressp ( stress.shape() );
  //stressp = 0.;

  //stressp = data_[ipoint]->outputs(ALL,0);

  //Vector col ( 6 );
  //col = 0.0;

  //col = ( stressp - stress ) / 1.e-10;
  //System::out() << "Col " << col << "\n";
  //System::out() << "Jac col " << jac(ALL,icol) << "\n";

  //

  //stiff = 0.5 * ( jac + jac.transpose() );

  //if ( ipoint == 0 ) System::out() << "Jac shape " << data_[ipoint]->jacobian.shape() << "\n";
  //if ( ipoint == 0 ) System::out() << "Fac shape " << fac.shape() << "\n";
  //if ( ipoint == 0 ) System::out() << "Point " << ipoint << " fac " << fac << "\n";
  //if ( ipoint == 0 ) System::out() << "Point " << ipoint << " strain " << strain << "\n";
  //if ( ipoint == 0 ) System::out() << "Stiff " << stiff << "\n";
  //if ( ipoint == 0 ) System::out() << "Point " << ipoint << " stress " << stress << "\n";

*/
