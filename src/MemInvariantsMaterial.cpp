#include <jem/io/PrintWriter.h>
#include <jem/base/Error.h>
#include <jem/base/System.h>
#include <jem/base/Array.h>
#include <jem/util/Properties.h>
#include <jem/numeric/algebra/matmul.h>
#include <jem/numeric/algebra/utilities.h>

#include "MemInvariantsMaterial.h"

#include "utilities.h"

#include <cstdlib>

using namespace jem;
using jem::numeric::matmul;
using jem::numeric::norm2;
using jem::io::endl;

using jem::io::PrintWriter;

//=======================================================================
//   class MemInvariantsMaterial
//=======================================================================

//-----------------------------------------------------------------------
//   static data
//-----------------------------------------------------------------------

const char* MemInvariantsMaterial::E_PROP        = "E";
const char* MemInvariantsMaterial::NU_PROP       = "nu";
const char* MemInvariantsMaterial::AREA_PROP     = "area";
const char* MemInvariantsMaterial::ANMODEL_PROP  = "anmodel";

//-----------------------------------------------------------------------
//   constructor and destructor
//-----------------------------------------------------------------------

MemInvariantsMaterial::MemInvariantsMaterial

  ( const idx_t       rank,
    const Properties& globdat )

  : Material ( rank, globdat )

{
  const idx_t STRAIN_COUNTS[4] = { 0, 1, 3, 6 };

  rank_ = rank;

  JEM_PRECHECK ( rank_ >= 1 && rank_ <= 3 );

  e_       = 1.0;
  nu_      = 0.0;
  area_    = 1.0;

  stiffMatrix_.resize ( STRAIN_COUNTS[rank_], STRAIN_COUNTS[rank_] );
  stiffMatrix_ = 0.0;
}

MemInvariantsMaterial::~MemInvariantsMaterial ()
{}

//-----------------------------------------------------------------------
//   configure
//-----------------------------------------------------------------------

void MemInvariantsMaterial::configure

  ( const Properties& props,
    const Properties& globdat )

{
  //System::out() << "Configure props = \n" << props << "\n";

  props.get  ( e_, E_PROP );
  props.get  ( nu_, NU_PROP );
  props.get  ( anmodel_, ANMODEL_PROP );

  if ( rank_ == 1 )
    props.get ( area_, AREA_PROP );

  computeStiffMatrix_ ();
}

//-----------------------------------------------------------------------
//   getConfig
//-----------------------------------------------------------------------

void MemInvariantsMaterial::getConfig

  ( const Properties& conf,
    const Properties& globdat ) const
{
  conf.set   ( E_PROP, e_ );
  conf.set   ( NU_PROP, nu_ );
  conf.set   ( ANMODEL_PROP, anmodel_ );

  if ( rank_ == 1 )
    conf.set ( AREA_PROP, area_ );
}

//-----------------------------------------------------------------------
//  createIntPoints 
//-----------------------------------------------------------------------

void MemInvariantsMaterial::createIntPoints

  ( const idx_t       npoints )

{
  preHist_.reserve ( npoints );
  newHist_.reserve ( npoints );

  for ( idx_t p = 0_idx; p < npoints; ++p )
  {
    preHist_.pushBack ( Hist_() );
    newHist_.pushBack ( Hist_() );
  }
}

//-----------------------------------------------------------------------
//  update 
//-----------------------------------------------------------------------

void MemInvariantsMaterial::update

  ( Matrix&       stiff,
    Vector&       stress,
    const Vector& strain,
    const idx_t   ipoint )
{
  stiff = stiffMatrix_;

  matmul ( stress, stiffMatrix_, strain );

  StrainInvariants inv ( fill3DStrain_ ( strain ) );

  double alpha = 0.5;

  double newcrit = alpha * inv.getFirstInvariant() * inv.getFirstInvariant() + (1.0 - alpha) * inv.getJ2();
  double oldcrit = alpha * preHist_[ipoint].i1 * preHist_[ipoint].i1 + (1.0 - alpha) * preHist_[ipoint].j2;
  
  //if ( true )
  if ( newcrit > oldcrit )
  {
    newHist_[ipoint].i1 = inv.getFirstInvariant();
    newHist_[ipoint].i2 = inv.getSecondInvariant();
    newHist_[ipoint].i3 = inv.getThirdInvariant();
    newHist_[ipoint].j2 = inv.getJ2();
    newHist_[ipoint].j3 = inv.getJ3();
  }
  else
  {
    newHist_[ipoint].i1 = preHist_[ipoint].i1;
    newHist_[ipoint].i2 = preHist_[ipoint].i2;
    newHist_[ipoint].i3 = preHist_[ipoint].i3;
    newHist_[ipoint].j2 = preHist_[ipoint].j2;
    newHist_[ipoint].j3 = preHist_[ipoint].j3; 
  }
}

//-----------------------------------------------------------------------
//  commit
//-----------------------------------------------------------------------

void MemInvariantsMaterial::commit ()

{
  newHist_.swap ( preHist_ );
}

//-----------------------------------------------------------------------
//  stressAtPoint 
//-----------------------------------------------------------------------

void MemInvariantsMaterial::stressAtPoint

  ( Vector&       stress,
    const Vector& strain,
    const idx_t   ipoint )
{
  throw Error ( JEM_FUNC, "MemInvariantsMaterial::stressAtPoint() not implemented" );
}

//-----------------------------------------------------------------------
//  clone 
//-----------------------------------------------------------------------

Ref<Material> MemInvariantsMaterial::clone ( ) const

{
  return newInstance<MemInvariantsMaterial> ( *this );
}

//-----------------------------------------------------------------------
//  addTableColumns 
//-----------------------------------------------------------------------

void MemInvariantsMaterial::addTableColumns

  ( IdxVector&     jcols,
    XTable&        table,
    const String&  name )

{
  // Check if the requested table is supported by this material.

  if ( name == "nodalStress" || name == "ipStress" )
  {
    if ( anmodel_ == "BAR" )
    {
      jcols.resize ( 1 );

      jcols[0] = table.addColumn ( "s_xx" );
    }

    else if ( anmodel_ == "PLANE_STRESS" )
    {
      jcols.resize ( 3 );

      jcols[0] = table.addColumn ( "s_xx" );
      jcols[1] = table.addColumn ( "s_yy" );
      jcols[2] = table.addColumn ( "s_xy" );
    }

    else if ( anmodel_ == "PLANE_STRAIN" )
    {
      jcols.resize ( 4 );

      jcols[0] = table.addColumn ( "s_xx" );
      jcols[1] = table.addColumn ( "s_yy" );
      jcols[2] = table.addColumn ( "s_xy" );
      jcols[3] = table.addColumn ( "s_zz" );
    }

    else if ( anmodel_ == "SOLID" )
    {
      jcols.resize ( 6 );

      jcols[0] = table.addColumn ( "s_xx" );
      jcols[1] = table.addColumn ( "s_yy" );
      jcols[2] = table.addColumn ( "s_zz" );
      jcols[3] = table.addColumn ( "s_xy" );
      jcols[4] = table.addColumn ( "s_xz" );
      jcols[5] = table.addColumn ( "s_yz" );
    }

    else
      throw Error ( JEM_FUNC, "Unexpected analysis model: " + anmodel_ );
  }
}


//-----------------------------------------------------------------------
//  computeStiffMatrix_ 
//-----------------------------------------------------------------------

void MemInvariantsMaterial::computeStiffMatrix_

  ( )
{

  if ( anmodel_ == "BAR" )
  {
    stiffMatrix_(0,0) = e_ * area_;
  }
  
  else if ( anmodel_ == "PLANE_STRESS" )
  {
    stiffMatrix_(0,0) = stiffMatrix_(1,1) = e_/(1.0 - nu_*nu_);
    stiffMatrix_(0,1) = stiffMatrix_(1,0) = (nu_*e_)/(1.0 - nu_*nu_);
    stiffMatrix_(2,2) = (0.5*e_)/(1.0 + nu_);
  }

  else if ( anmodel_ == "PLANE_STRAIN" )
  {
    const double d = (1.0 + nu_) * (1.0 - 2.0*nu_);

    stiffMatrix_(0,0) = stiffMatrix_(1,1) = e_*(1.0 - nu_)/d;
    stiffMatrix_(0,1) = stiffMatrix_(1,0) = e_*nu_/d;
    stiffMatrix_(2,2) = 0.5*e_/(1.0 + nu_);
  }

  else if ( anmodel_ == "SOLID" )
  {
    const double d = (1.0 + nu_) * (1.0 - 2.0*nu_);

    stiffMatrix_(0,0) = stiffMatrix_(1,1) =
    stiffMatrix_(2,2) = e_*(1.0 - nu_)/d;
    stiffMatrix_(0,1) = stiffMatrix_(1,0) =
    stiffMatrix_(0,2) = stiffMatrix_(2,0) =
    stiffMatrix_(1,2) = stiffMatrix_(2,1) = e_*nu_/d;
    stiffMatrix_(3,3) = stiffMatrix_(4,4) =
    stiffMatrix_(5,5) = 0.5*e_/(1.0 + nu_);
  }
  else
    throw Error ( JEM_FUNC, "Unexpected analysis model: " + anmodel_ );
}

//-----------------------------------------------------------------------
//   fill3DStrain_
//-----------------------------------------------------------------------

Tuple<double,6> MemInvariantsMaterial::fill3DStrain_

  ( const Vector&       v ) const

{
  Tuple<double,6> ret;

  if ( v.size() == 1 )
  {
    ret[0_idx] = v[0_idx];
    ret[1_idx] = 0.0;
    ret[2_idx] = 0.0;
    ret[3_idx] = 0.0;
    ret[4_idx] = 0.0;
    ret[5_idx] = 0.0;
  }

  else if ( v.size() == 3 )
  {
    double eps_zz = anmodel_ == "PLANE_STRESS"
                  ? -nu_ / (1.-nu_) * (v[0_idx]+v[1_idx])
		  : 0.;
    
    ret[0_idx] = v[0_idx];
    ret[1_idx] = v[1_idx];
    ret[2_idx] = eps_zz;
    ret[3_idx] = v[2_idx];
    ret[4_idx] = 0.0;
    ret[5_idx] = 0.0;
  }

  else if ( v.size() == 6 )
  {
    ret[0_idx] = v[0_idx];
    ret[1_idx] = v[1_idx];
    ret[2_idx] = v[2_idx];
    ret[3_idx] = v[3_idx];
    ret[4_idx] = v[4_idx];
    ret[5_idx] = v[5_idx];
  }

  else
    throw Error ( JEM_FUNC,
      "Unexpected strain vector size in fill3DStrain_ (MemInvariantsMaterial)" );

  return ret;
}

//-----------------------------------------------------------------------
//  setProps
//-----------------------------------------------------------------------

void MemInvariantsMaterial::setProps

  ( const Vector& props )

{
  e_  = props[0];
  nu_ = 0.49 / ( 1. + exp ( -props[1] ) );

  computeStiffMatrix_ ();
  //System::out() << "Setting props to " << e_ << " " << nu_ << '\n';
}

//-----------------------------------------------------------------------
//  getFeatures
//-----------------------------------------------------------------------

Vector MemInvariantsMaterial::getFeatures

  ( const idx_t ipoint )

{
  Vector ret ( 5 );
  ret = 0.0;

  ret[0_idx] = preHist_[ipoint].i1;
  ret[1_idx] = preHist_[ipoint].i2;
  ret[2_idx] = preHist_[ipoint].i3;
  ret[3_idx] = preHist_[ipoint].j2;
  ret[4_idx] = preHist_[ipoint].j3;

  return ret;
}

//-----------------------------------------------------------------------
//  propCount
//-----------------------------------------------------------------------

idx_t MemInvariantsMaterial::propCount ()

{
  return 2;
}

//-----------------------------------------------------------------------
//  clearHist
//-----------------------------------------------------------------------

void MemInvariantsMaterial::clearHist ()

{
  idx_t npoints = preHist_.size();

  preHist_.clear();

  createIntPoints ( npoints );
}

//=======================================================================
//   class MemInvariantsMaterial::Hist_
//=======================================================================

MemInvariantsMaterial::Hist_::Hist_ 
   
  ()
     
{
  i1 = 0.;
  i2 = 0.;
  i3 = 0.;
  j2 = 0.;
  j3 = 0.;
}
