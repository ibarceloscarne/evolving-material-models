# Evolving material models

Code from the paper "Machine learning of evolving physics-based material models for multiscale solid mechanics"

## Upcoming

This repository will be updated with the final code and representative project files soon after acceptance for publication.
